import json
import logging
import sys
from contextlib import asynccontextmanager

from aiokafka import AIOKafkaConsumer, AIOKafkaProducer
from fastapi import FastAPI
from starlette.responses import JSONResponse
from uvicorn import Config, Server
from pydantic import BaseModel, StrictStr


class EndpointFilter(logging.Filter):
    def filter(self, record: logging.LogRecord) -> bool:
        return record.args and len(record.args) >= 3 and record.args[2] != "/health"


logging.getLogger("uvicorn.access").addFilter(EndpointFilter())
log = logging.getLogger("authlib")
log.addHandler(logging.StreamHandler(sys.stdout))
log.setLevel(logging.DEBUG)


class ProducerResponse(BaseModel):
    name: StrictStr
    message_id: StrictStr
    topic: StrictStr
    timestamp: StrictStr = ""


class ProducerMessage(BaseModel):
    name: StrictStr
    message_id: StrictStr = ""
    timestamp: StrictStr = ""


# KAFKA_INSTANCE = "ueba-kafka.infra.svc.cluster.local:9092"
KAFKA_INSTANCE = "localhost:9092"
# loop = asyncio.get_event_loop()
producer = AIOKafkaProducer(bootstrap_servers=KAFKA_INSTANCE)
consumer = AIOKafkaConsumer("luna_auth_all", bootstrap_servers=KAFKA_INSTANCE)


async def consume():
    await consumer.start()
    try:
        async for msg in consumer:
            print(
                "consumed: ",
                msg.topic,
                msg.partition,
                msg.offset,
                msg.key,
                msg.value,
                msg.timestamp,
            )

    finally:
        await consumer.stop()


@asynccontextmanager
async def lifespan(app: FastAPI):
    # Load the ML model
    await producer.start()
    await consumer.consume()
    # loop.create_task(consume())
    yield
    # Clean up the ML models and release the resources
    await producer.stop()
    await consumer.stop()


# @app.on_event("startup")
# async def startup_event():
#     await producer.start()
#     loop.create_task(consume())
#
#
# @app.on_event("shutdown")
# async def shutdown_event():
#     await producer.stop()
#     await consumer.stop()

app = FastAPI(lifespan=lifespan)


@app.get("/")
def read_root():
    return {"Ping": "Pong"}


@app.post("/producer/{topicname}")
async def kafka_produce(msg: ProducerMessage, topicname: str):
    await producer.send(topicname, json.dumps(msg.dict()).encode("ascii"))
    response = ProducerResponse(name=msg.name, message_id=msg.message_id, topic=topicname)
    return response


@app.get("/auth/health")
async def health():
    return JSONResponse({"healthy": True})


if __name__ == "__main__":
    config = Config(
        app=app,
        host="0.0.0.0",
        port=8000,
        log_level="debug",
        reload=True,
        reload_dirs=".",
        workers=1,
        use_colors=True,
    )
    server = Server(config=config)
    server.run()
