#!/usr/bin/env python
import asyncio
import json

from aiokafka import AIOKafkaProducer
from pathlib import Path

# from luna.framework.common.logger import LOGGER


async def send_one(topic=None, json_file_path=None, json_data=None):
    producer = AIOKafkaProducer(
        bootstrap_servers="localhost:9092",
        value_serializer=lambda v: json.dumps(v).encode("utf-8"),
    )
    # Get cluster layout and initial topic/partition leadership information
    if json_file_path:
        json_file_path = Path(json_file_path)
        value_json = json.loads(Path(json_file_path).read_text())
    if json_data:
        value_json = json_data
    await producer.start()
    try:
        # LOGGER.debug(f"[kafka] Sending json message from file: {json_file_path.name}")
        await producer.send_and_wait(
            topic=topic,
            value=value_json,
            headers=[
                ("content-type", b"application/json"),
            ],
        )
    finally:
        # Wait for all pending messages to be delivered or expire.
        await producer.stop()


if __name__ == "__main__":
    # asyncio.run(send_one(json_file_path=sys.argv[1]))
    asyncio.run(send_one(topic="test123", json_file_path="/Users/amit.bakhru/src/luna/temp.json"))
