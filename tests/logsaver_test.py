"""
Unit tests for luna.framework.common.logger.logsaver module.
"""
from luna.framework.common import common_unittest
from luna.framework.common.logger import LOGGER, logsaver

_FAKE_NOW = 123456789.123456


def _FakeNow():
    return _FAKE_NOW


class LogSaverTest(common_unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.__real_now = logsaver._Now
        logsaver._Now = _FakeNow
        self._logsaver = logsaver.LogSaver()
        LOGGER.addHandler(self._logsaver)

    def tearDown(self):
        LOGGER.removeHandler(self._logsaver)
        logsaver._Now = self.__real_now
        super().tearDown()

    def testEmpty(self):
        self.assertListEqual([], self._logsaver.messages)


if __name__ == "__main__":
    common_unittest.main()
