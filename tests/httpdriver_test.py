"""
Unit tests for luna.framework.common.common_unittest module.
"""
import json

from luna.driver.http.httpdriver import HttpDriver
from luna.framework.common.common_unittest import TestCase
from luna.framework.common.logger import LOGGER


class HttpDriverTest(TestCase):
    @classmethod
    def setUpClass(cls):
        url = "reqres.in"
        cls.http_driver = HttpDriver(protocol="https", host=url, ssl_verify=True)
        cls._id = 4
        cls.payload = {"email": "eve.holt@reqres.in", "password": "pistol"}
        cls.http_driver.Get(url="/api/login", params=cls.payload, parse_output=False)

    def test_connection_no_auth(self):
        """Test the http Connection without authentication"""
        self.http_driver.__connect__()
        assert self.http_driver.connected is True, "HttpDriver Connection failed"

    def test_get_all(self):
        """Test the Get method for HttpDriver"""
        params = {"page": 1, "per_page": 20}
        resp = self.http_driver.Get(url="/api/users", params=params)
        assert resp.status_code == 200, "HttpDriver GET Request failed"

    def test_get_single(self):
        """Test the GET method for HttpDriver"""
        resp = self.http_driver.Get(url="/api/users", params={"id": "2"})
        LOGGER.info(f"Response: {resp.json()}")
        assert resp.status_code == 200, "HttpDriver GET Request failed"

    def test_post(self):
        """Test the POST method for HttpDriver"""
        self.payload.update({"first_name": "test", "last_name": "abc"})
        resp = self.http_driver.Post(url="/api/register", data=json.dumps(self.payload))
        data = resp.json()
        LOGGER.info(f"Response: {data}")
        assert resp.status_code == 400, "HttpDriver POST Request failed"
        self.http_driver.Get(url="/api/users", params={"id": self._id})

    def test_put(self):
        """Test the PUT method for HttpDriver"""
        payload = {"first_name": "Update Name"}
        resp = self.http_driver.Put(url=f"/api/users/{self._id}", data=json.dumps(payload))
        LOGGER.info(f"Response: {resp.json()}")
        assert resp.status_code == 200, "HttpDriver PUT Request failed"
        self.http_driver.Get(url="/api/users", params={"id": self._id})

    def test_patch(self):
        """Test the PATCH method for HttpDriver"""
        payload = {"name": "test", "job": "leader"}
        resp = self.http_driver.Post(url="/api/users", data=json.dumps(payload))
        assert resp.status_code == 201, "HttpDriver POST Request failed"
        self._id = resp.json()["id"]
        self.http_driver.Get(url="/api/users", params={"id": self._id})
        payload = {"name": "Updated Name"}
        resp = self.http_driver.Patch(url=f"/api/users/{self._id}", data=json.dumps(payload))
        LOGGER.info(f"Response: {resp.json()}")
        assert resp.status_code == 200, "HttpDriver PATCH Request failed"
        self.http_driver.Get(url="/api/users", params={"id": self._id})

    def test_delete(self):
        """Test the DELETE method for HttpDriver"""
        payload = {"name": "test", "job": "leader", "age": "23"}
        resp = self.http_driver.Post(url="/api/users", data=json.dumps(payload))
        assert resp.status_code == 201, "HttpDriver POST Request failed"
        self._id = resp.json()["id"]
        self.http_driver.Get(url="/api/users", params={"id": self._id})
        resp = self.http_driver.Delete(url="/api/users", params={"id": self._id})
        assert resp.status_code == 204, "HttpDriver DELETE Request failed"
