"""
Unit tests for the luna.framework.common.testcase module.
"""

from luna.framework.common.testcase import TestCase


class TestCaseTest(TestCase):
    BASE_VARIABLE = 0
    TEST_VARIABLE = 0

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.BASE_VARIABLE = 5

    def setUp(self):
        # For a test with only setUp, the value will be 10
        self.TEST_VARIABLE = 10
        super().setUp()

    def test_class_level_setup(self):
        self.assertEqual(self.BASE_VARIABLE, 5)

    def test_generic_level_setup(self):
        self.assertEqual(self.TEST_VARIABLE, 10)
