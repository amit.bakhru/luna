"""
Unit tests for framework.common.logger module.
"""

import io
import logging
import sys
from logging.handlers import SysLogHandler

import pytest

from luna.framework.common import common_unittest, logger
from luna.framework.common.logger import LOGGER


class LoggerTest(common_unittest.TestCase):
    def testLogPriorityCode_good(self):
        self.assertEqual(logging.INFO, logger.LogPriorityCode("iNFo"))

    def testLogPriorityCode_nonstandard(self):
        self.assertEqual(17, logger.LogPriorityCode("prIORiTy-17"))

    def testLogPriorityCode_bad(self):
        self.assertRaises(logger.BadPriorityError, lambda: logger.LogPriorityCode("fubar"))

    def testLogPriorityName_good(self):
        self.assertEqual("INFO", logger.LogPriorityName(logging.INFO))

    def testLogPriorityName_nonstandard(self):
        self.assertEqual("PRIORITY-17", logger.LogPriorityName(17))

    def testLogFacilityCode_good(self):
        self.assertEqual(SysLogHandler.LOG_NEWS, logger.LogFacilityCode("nEwS"))

    def testLogFacilityCode_bad(self):
        self.assertRaises(logger.BadFacilityError, lambda: logger.LogFacilityCode("fubar"))

    def testLogFacilityCode_security(self):
        self.assertEqual(SysLogHandler.LOG_AUTH, logger.LogFacilityCode("seCUriTY"))

    def testLogFacilityName_good(self):
        self.assertEqual("uucp", logger.LogFacilityName(SysLogHandler.LOG_UUCP))

    def testLogFacilityName_bad(self):
        self.assertRaises(logger.BadFacilityError, lambda: logger.LogFacilityName(293847923874))

    def testLogPriorityCode_Schema(self):
        self.assertEqual(logging.INFO, logger.LogPriorityCode("notice"))
        self.assertEqual(logging.CRITICAL, logger.LogPriorityCode("emerg"))
        self.assertEqual(logging.CRITICAL, logger.LogPriorityCode("ALert"))
        self.assertEqual(logging.WARNING, logger.LogPriorityCode("WARNING"))
        self.assertEqual(logging.CRITICAL, logger.LogPriorityCode("CRITICAL"))
        self.assertEqual(logging.ERROR, logger.LogPriorityCode("ERR"))


@pytest.mark.skipif(sys.version_info.major < 3, reason="requires python3 or above")
class LogDebugStringIfTest(common_unittest.TestCase):
    """Tests for logger.LogDebugStringIf."""

    def setUp(self):
        self.__closure_was_run = False

    def Closure(self):
        LOGGER.info("Closure INFO output")
        LOGGER.debug("Closure DEBUG output")
        self.__closure_was_run = True

    def test_condition_False(self):
        log_output = logger.LogDebugStringIf(self.Closure, condition=False)
        self.assertEqual(None, log_output)
        self.assertTrue(self.__closure_was_run)

    def test_nominal(self):
        log_output = logger.LogDebugStringIf(self.Closure)
        self.assertIn("Closure DEBUG output", log_output)
        self.assertIn("Closure INFO output", log_output)
        self.assertTrue(self.__closure_was_run)

    def test_priority_INFO(self):
        log_output = logger.LogDebugStringIf(self.Closure, priority=logging.INFO)
        self.assertNotIn("Closure DEBUG output", log_output)
        self.assertIn("Closure INFO output", log_output)
        self.assertTrue(self.__closure_was_run)

    def test_sticky_DEBUG_priority(self):
        def OuterClosure():
            logger.LogDebugStringIf(self.Closure, priority=logging.WARN)

        log_output = logger.LogDebugStringIf(OuterClosure)
        # Since the outer closure enables debug logging, the inner priority setting should be
        # ignored.
        self.assertIn("Closure DEBUG output", log_output)
        self.assertIn("Closure INFO output", log_output)
        self.assertTrue(self.__closure_was_run)


@pytest.mark.skipif(sys.version_info.major < 3, reason="requires python3 or above")
class LogStreamTest(common_unittest.TestCase):
    """Unit tests for logger.LogStream."""

    def test_nominal(self):
        stream = io.StringIO()
        _id = logger.LogStream.Register(stream)
        try:
            LOGGER.info("This should be there.")
            LOGGER.setLevel("DEBUG")
            LOGGER.info("Testing INFO logger")
            LOGGER.error("Testing ERROR logger")
            LOGGER.warning("Testing WARNING logger")
            LOGGER.debug("Testing DEBUG logger")
            LOGGER.critical("Testing CRITICAL logger")
        finally:
            logger.LogStream.Unregister(_id)
            LOGGER.info("This should NOT.")

        output = stream.getvalue()
        self.assertIn("This should be there.", output)
        self.assertNotIn("This should NOT.", output)


if __name__ == "__main__":
    common_unittest.main()
