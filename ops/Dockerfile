# debian-bullseye-11 with jdk8 baked
FROM openjdk:8-jdk

LABEL description="A jenkins agent with build requirements for the luna tests" \
      version="0.1" \
      maintainer="amit.bakhru@devo.com"

ENV USER=jenkins \
    PG_MAJOR=13 \
    DEBIAN_FRONTEND=noninteractive \
    luna_DEBUG=1 \
    PYTHONIOENCODING=utf-8 \
    HOME=/home/jenkins

ARG JENKINS_VERSION=3.29.1
ARG WORKSPACE=${WORKSPACE}

RUN useradd -m -d ${HOME} -s /bin/bash ${USER} && \
    echo "${USER}:${USER}" | chpasswd && \
    adduser ${USER} sudo && \
    echo "${USER} ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

RUN apt-get update -y && \
    apt-get install -y \
    --allow-downgrades --allow-remove-essential --allow-change-held-packages \
    apt-utils \
    apt-transport-https \
    curl \
    ca-certificates \
    lsb-release \
    gpg \
    gnupg2 \
    software-properties-common && \
    apt-get install -y sudo --option=Dpkg::Options::=--force-confdef && \
    apt-add-repository 'deb http://security.debian.org/debian-security stretch/updates main' && \
    curl -fsSL https://www.postgresql.org/media/keys/ACCC4CF8.asc| gpg --dearmor -o /etc/apt/trusted.gpg.d/postgresql.gpg && \
    echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" | tee  /etc/apt/sources.list.d/pgdg.list

RUN apt-get update && \
    apt-get install -y \
    wget \
    bash \
    git \
    bsdmainutils \
    dnsutils \
    python3-dev \
    python3-venv \
    python3-pip \
    build-essential \
    libjansson-dev \
    libffi-dev \
    libssl-dev \
    libfuzzy-dev \
    libpq-dev \
    shellcheck \
    maven \
    galternatives \
    redis-server \
    htop \
    vim \
    net-tools \
    postgresql-${PG_MAJOR} \
    postgresql-client-${PG_MAJOR} \
    postgresql-contrib-${PG_MAJOR} \
    redis-server \
    iproute2 && \
    /var/lib/dpkg/info/ca-certificates-java.postinst configure && \
    rm -rf /var/lib/apt/list/* /tmp/* /var/tmp/* && \
    apt-get update -y && apt-get upgrade -y && \
    apt-get clean autoclean && apt-get autoremove -y && \
    apt-get purge -y man perl-modules && \
    sed -i "s/bind .*/bind 127.0.0.1/g" /etc/redis/redis.conf

RUN echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && \
    curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg add - && \
    apt-get update && apt-get install -y google-cloud-cli && \
    wget -q "https://github.com/GoogleCloudPlatform/docker-credential-gcr/releases/download/v2.1.6/docker-credential-gcr_linux_amd64-2.1.6.tar.gz" -P /tmp && \
    tar xvzf /tmp/docker-credential-gcr_*.tar.gz -C /usr/bin/ && \
    chmod +x /usr/bin/docker-credential-gcr

# install docker-ce-cli
RUN mkdir -p /etc/apt/keyrings && \
    curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg && \
    echo \
      "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
      $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null && \
    apt-get update && \
    chmod a+r /etc/apt/keyrings/docker.gpg && \
    apt-get update && apt-get install -y docker-ce-cli

RUN curl --create-dirs -sSLo /usr/share/jenkins/slave.jar https://repo.jenkins-ci.org/public/org/jenkins-ci/main/remoting/${JENKINS_VERSION}/remoting-${JENKINS_VERSION}.jar && \
    chmod 755 /usr/share/jenkins && \
    chmod 644 /usr/share/jenkins/slave.jar

WORKDIR /tmp
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install && \
    rm -rf aws awscli*.zip *.gz

COPY --from=jenkinsci/jnlp-slave:3.29-1 /usr/local/bin/jenkins-slave /usr/local/bin/jenkins-agent

USER postgres
RUN echo "local   all  all  trust\nhost  all  all   127.0.0.1/32  trust\nhost    all   all   localhost   trust\nhost all  all    0.0.0.0/0  md5" > /etc/postgresql/${PG_MAJOR}/main/pg_hba.conf && \
    echo "listen_addresses='*'" >> /etc/postgresql/${PG_MAJOR}/main/postgresql.conf

WORKDIR ${HOME}
COPY --chown=jenkins:jenkins ./requirements.txt /tmp/
RUN mkdir -p ${HOME}/.cache/pip && \
    chown -R ${USER}:${USER} ${HOME}/.cache/ && \
    python3 -m venv ${HOME}/env && \
    ${HOME}/env/bin/pip install -U pip setuptools wheel && \
    ${HOME}/env/bin/pip install -r /tmp/requirements.txt && \
    rm -rf ${HOME}/.cache/* /tmp/requirements.txt && \
    echo "alias l='ls -larth'" >> ${HOME}/.bashrc

EXPOSE 5432
VOLUME  ["/var/log/postgresql", "/var/lib/postgresql"]
CMD ["/usr/local/bin/jenkins-agent"]
