#!/bin/bash

set -xe
env|sort

python3 -V
make venv
make test
make lint
