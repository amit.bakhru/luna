#
# (c) 2023-2024, Devo Inc.
# luna: QA automation framework for Devo services
#
# setup.py
#
import sys

from pathlib import Path
from setuptools import find_packages, setup
from setuptools.command.test import test as TestCommand


class PyTest(TestCommand):
    user_options = [("pytest-args=", "a", "Arguments to pass to py.test")]

    def initialize_options(self):
        TestCommand.initialize_options(self)
        self.pytest_args = ["tests"]

    def finalize_options(self):
        TestCommand.finalize_options(self)

        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        import pytest

        sys.exit(pytest.main(self.pytest_args))


all_reqs = Path(__file__).parent.resolve().joinpath("requirements.txt").read_text().splitlines()
install_requires = [x.strip() for x in all_reqs if "git+" not in x]
tests_require = ["pytest"]

setup(
    name="luna",
    version="0.0.1",
    description="Base Automation Test Framework files for Devo apps testing",
    install_requires=install_requires,
    include_package_data=True,
    packages=find_packages(exclude=("tests", "testdata", "tests.*", "tools")),
    package_data={"luna": ["driver/ui/lighthouse/lighthouse.js"]},
    download_url="https://gitlab.devotools.com/amit.bakhru/luna",
    license="Internal",
    zip_safe=False,
    tests_require=tests_require,
    author="Amit Bakhru",
    author_email="amit.bakhru@devo.com",
    cmdclass={"test": PyTest},
)
