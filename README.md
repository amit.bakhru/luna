## Luna
Automation framework for Devo Backend & Frontend Applications

### Why luna?
"Luna" name is associated with the moon, which is a symbol of mystery, intuition, and transformation.
These qualities are all relevant to the task of test automation, which is to uncover hidden bugs, improve the
quality of software, and transform the way software is developed and tested.
Overall, "Luna" is a strong name for a test automation framework that is both elegant and effective.

### Configuration
- All Configuration is in env/pytest.conf file (check
luna/luna/config/pytest.conf to get started)
- most defaults endpoints are localhost
- to enable DEBUG logging
  - export LUNA_DEBUG=1
  - (or) change `config["common"]["logging"]` from INFO=>DEBUG in `config.json` file
- to enabled fancy COLOR(rich) based logging
  - export COLORLOG=1

### Running unit tests
```
mkdir ~/temp && cd ~/temp; git clone git@gitlab.devotools.com:amit.bakhru/luna.git
cd ~/temp/luna; make venv test
```
