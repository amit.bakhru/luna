.PHONY: clean clean-build clean-pyc clean-out docs help
.DEFAULT_GOAL := help
SHELL = /bin/bash
IMAGE_NAME := luna-base
IMAGE_TAG := $(or ${DOCKER_TAG}, test)
VENV_DIR := ${PWD}/.venv

## help
help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)";echo;sed -ne"/^## /{h;s/.*//;:d" -e"H;n;s/^## //;td" -e"s/:.*//;G;s/\\n## /---/;s/\\n/ /g;p;}" ${MAKEFILE_LIST}|LC_ALL='C' sort -f|awk -F --- -v n=$$(tput cols) -v i=19 -v a="$$(tput setaf 6)" -v z="$$(tput sgr0)" '{printf"%s%*s%s ",a,-i,$$1,z;m=split($$2,w," ");l=n-i;for(j=1;j<=m;j++){l-=length(w[j])+1;if(l<= 0){l=n-i-length(w[j])-1;printf"\n%*s ",-i," ";}printf"%s ",w[j];}printf"\n";}'|more $(shell test $(shell uname) == Darwin && echo '-Xr')

## build the python virtual env for the project
venv:
	uv venv -p python3.12 --seed
	uv pip install ruff uv poetry-plugin-export poetry
	#if [ -f "pyproject.toml" ]; then ${VENV_DIR}/bin/poetry install; fi;
	if [ -f "requirements.txt" ]; then uv pip install -r ./requirements.txt; fi;
	uv pip install -e .

## clean pyc files
clean:
	(for i in "*.py[co]" "[.]*cache" "*.egg*" "build" "dist*" "*test-reports" "[.]coverage*" \
	"coverage*" "o" "__pycache__"; \
	do find . -name "$$i" -not -path "./.venv/*" -not -path "./.scannerwork/*" -exec rm -rv {} + ; done)

## lint python files using black
lint:
	${VENV_DIR}/bin/ruff format --config ./ruff.toml luna/
	${VENV_DIR}/bin/pre-commit run -a

## build docker image
build-docker:
	docker build \
     --tag ${IMAGE_NAME}:${IMAGE_TAG} \
     --pull \
     -f ./ops/Dockerfile .

## push docker image
push-docker:
	docker push ${IMAGE_NAME}:${IMAGE_TAG}

## run all tests
test:
	PYTHONPATH=${PWD}/luna \
	${VENV_DIR}/bin/pytest \
		--junitxml=o/pytest-junit-report.xml -p no:logging -sv \
		tests/
