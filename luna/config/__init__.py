import json
import os

from pathlib import Path


def find_git_dir(_current_dir):
    """recursively find .git dir of this repo"""
    current_path = Path(_current_dir).resolve()  # Get the absolute path
    while current_path != current_path.parent:  # Loop until we reach the root directory
        git_dir = current_path / ".git"
        if git_dir.exists() and git_dir.is_dir() and ".venv" not in git_dir.parts:
            return git_dir
        current_path = current_path.parent
    return None  # ".git" directory not found


# check if LUNA_CONFIG path is defined
LUNA_CONFIG = os.environ.get("LUNA_CONFIG")
# if not, defined try to find the config.json file for this repo
if not LUNA_CONFIG:
    current_dir = Path(__file__).parent.parent.parent
    git_dir = find_git_dir(current_dir)
    if not git_dir:
        print(".git directory not found in the directory hierarchy.")
    repo_path = git_dir.parent
    config_json_files = [i.resolve() for i in repo_path.glob("**/config.json")]
    LUNA_CONFIG = [i for i in config_json_files if ".venv" not in i.parts].pop()

# if still not found, try using the REPO_PATH/luna/config.json path, else FAIL
if not LUNA_CONFIG:
    REPO_PATH = os.environ.get("REPO_PATH")
    if REPO_PATH:
        LUNA_CONFIG = Path(REPO_PATH).joinpath("luna", "config.json")
    else:
        raise SystemExit("Please define either LUNA_CONFIG or REPO_PATH env variable")

print(f"LUNA_CONFIG: {LUNA_CONFIG}")
config = json.loads(Path(LUNA_CONFIG).read_text())
