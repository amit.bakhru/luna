import sys
import time

from playwright.sync_api import expect
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from luna.driver.ui import base
from luna.driver.ui import custom_waits as EC2

# from luna.driver.ui.wuielement import WUIElement
from luna.driver.ui.wuielement import PlaywrightElement as WUIElement
from luna.driver.ui.wuiexception import WUIException
from luna.framework.common.logger import LOGGER


class Waits:
    """Mixin class that defines the commonly used selenium waits.

    Method names has the following naming convention:
    (1) Using 'Element' in the method name if the locator needs to be passed.
    (2) Using 'WebElement' in the method name if the Element is already located, and that
        directly needs to be passed.
    """

    TIMEOUT = 15
    POLL_FREQUENCY = 0.5

    def __init__(self):
        self.base = base
        self.driver = self.base.driver
        self.log = self.base.log

    def WaitForTitleContains(
        self, text, timeout=None, poll_frequency=POLL_FREQUENCY, raise_exception=True
    ):
        """An expectation for checking that the title contains the case-sensitive text.

        Args:
            text: text to find in title (str)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            raise_exception: whether to raise an exception or return False (bool)

        Returns:
            True if the expected title is a fragment of the title
        """
        timeout = self.TIMEOUT if not timeout else timeout
        self.log.debug("Waiting for title to contain %s", text)
        try:
            # return WebDriverWait(self.driver, timeout, poll_frequency).until(
            #     EC.title_contains(text)
            # )
            return expect(self.base.page).to_have_title(text)
        except (TimeoutException, AssertionError):
            if raise_exception:
                raise WUIException(
                    f'Title does not contain "{text}". Waited for "{timeout}" seconds'
                )
            else:
                return False
        except Exception:
            self.log.critical(f"Unexpected error: {sys.exc_info()[0]}")
            raise

    def WaitForTitleIs(
        self, text, timeout=None, poll_frequency=POLL_FREQUENCY, raise_exception=True
    ):
        """An expectation for checking the title of a page.

        Args:
            text: title text (str)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            raise_exception: whether to raise an exception or return False (bool)

        Returns:
            True if the expected title is an exact match with the title
        """
        timeout = self.TIMEOUT if not timeout else timeout
        self.log.debug("Waiting for title to match %s", text)
        try:
            return WebDriverWait(self.driver, timeout, poll_frequency).until(EC.title_is(text))
        except TimeoutException:
            if raise_exception:
                raise WUIException('Title is not "%s". Waited for "%d" seconds.' % (text, timeout))
            else:
                return False
        except Exception:
            self.log.critical("Unexpected error: %s", sys.exc_info()[0])
            raise

    def WaitForAlertIsPresent(
        self, timeout=None, poll_frequency=POLL_FREQUENCY, raise_exception=True
    ):
        """An expectation for an alert to be present.

        Args:
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            raise_exception: whether to raise an exception or return False (bool)

        Returns:
            Alert if present
        """
        timeout = self.TIMEOUT if not timeout else timeout
        self.log.debug("Waiting for alert to be present")
        try:
            return WebDriverWait(self.driver, timeout, poll_frequency).until(EC.alert_is_present())
        except TimeoutException:
            if raise_exception:
                raise WUIException('Alert is not present. Waited for "%d" seconds.' % timeout)
            else:
                return False
        except Exception:
            self.log.critical("Unexpected error: %s", sys.exc_info()[0])
            raise

    def WaitForTextPresentInBody(
        self, text, timeout=None, poll_frequency=POLL_FREQUENCY, raise_exception=True
    ):
        """An expectation for checking if the given text is present in the body.

        Args:
            text: string to find in body (str)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            raise_exception: whether to raise exception on timeout (bool)

        Returns:
            True if the text is present in the body
        """
        timeout = self.TIMEOUT if not timeout else timeout
        self.log.debug("Waiting for text: %s to be present in body", text)
        locator = ("CSS_SELECTOR", "body")
        # Returns a selenium locator tuple of (by, selector)
        locator = WUIElement(locator, find=False)
        try:
            return WebDriverWait(self.driver, timeout, poll_frequency).until(
                EC.text_to_be_present_in_element(locator, text)
            )
        except TimeoutException:
            if raise_exception:
                raise WUIException(
                    'Text "%s" is not present in body. Waited for "%d" seconds.' % (text, timeout)
                )
            else:
                return False
        except Exception:
            self.log.critical("Unexpected error: %s", sys.exc_info()[0])
            raise

    def WaitForPresenceOfElement(
        self, locator, timeout=None, poll_frequency=POLL_FREQUENCY, raise_exception=True
    ):
        """An expectation for checking that an element is present on the DOM of a page.

        This does not necessarily mean that the element is visible.

        Args:
            locator: strategy technique and strategy selector (tuple)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            raise_exception: whether to raise exception on timeout (bool)

        Returns:
            The WebElement once it is located
        """
        timeout = self.TIMEOUT if not timeout else timeout
        self.log.debug("Waiting for presence of locator %s", locator)
        # Returns a selenium locator tuple of (by, selector)
        locator = WUIElement(locator, find=False)
        try:
            return WebDriverWait(self.driver, timeout, poll_frequency).until(
                EC.presence_of_element_located(locator)
            )
        except TimeoutException:
            if raise_exception:
                raise WUIException(
                    'Element "%s" is not present in DOM. Waited for "%d" seconds.'
                    % (locator, timeout)
                )
            else:
                return False
        except Exception:
            self.log.critical("Unexpected error: %s", sys.exc_info()[0])
            raise

    def WaitForElementDisplayed(
        self, locator, timeout=None, poll_frequency=POLL_FREQUENCY, raise_exception=True
    ):
        """An expectation for checking that an element is present on the DOM of a page and visible.

        Visibility means that the element is not only displayed but
        also has a height and width that is greater than 0

        Args:
            locator: strategy technique and strategy selector (tuple)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            raise_exception: whether to raise an exception or return False (bool)

        Returns:
            The WebElement once it is located and visible
        """
        timeout = self.TIMEOUT if not timeout else timeout
        self.log.debug(f"Waiting for locator {locator} to be displayed")
        # Returns a selenium locator tuple of (by, selector)
        locator = WUIElement(locator, find=True)
        try:
            # return WebDriverWait(self.driver, timeout, poll_frequency).until(
            #     EC.visibility_of_element_located(locator)
            # )
            locator.wait_for(timeout=timeout * 60)  # in milliseconds
        except (TimeoutException, AssertionError):
            if raise_exception:
                raise WUIException(
                    f'Element "{locator}" is not displayed in DOM. Waited for "{timeout}" seconds'
                )
            else:
                return False
        except Exception:
            self.log.critical(f"Unexpected error: {sys.exc_info()[0]}")
            raise

    def WaitForWebElementDisplayed(
        self,
        webelement,
        timeout=None,
        poll_frequency=POLL_FREQUENCY,
        raise_exception=True,
    ):
        """An expectation for checking that the webelement is present on the DOM of a
            page and visible.

        Visibility means that the element is not only displayed but
        also has a height and width that is greater than 0.

        Args:
            webelement: webelement to check (webdriver object)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            raise_exception: whether to raise an exception or return False (bool)

        Returns:
            The WebElement once it is located and visible
        """
        timeout = self.TIMEOUT if not timeout else timeout
        self.log.debug("Waiting for webelement to be displayed")
        try:
            return WebDriverWait(self.driver, timeout, poll_frequency).until(
                EC.visibility_of(webelement)
            )
        except TimeoutException:
            if raise_exception:
                raise WUIException(
                    'WebElement "%s" is not visible in DOM. '
                    'Waited for "%d" seconds.' % (webelement, timeout)
                )
            else:
                return False
        except Exception:
            self.log.critical("Unexpected error: %s", sys.exc_info()[0])
            raise

    def WaitForPresenceOfAllElements(
        self, locator, timeout=None, poll_frequency=POLL_FREQUENCY, raise_exception=True
    ):
        """An expectation for checking that there is at least one element present on a web page.

        Args:
            locator: strategy technique and strategy selector (tuple)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            raise_exception: whether to raise an exception or return False (bool)

        Returns:
            The list of WebElements once they are located or False if timeout occurs and
            raise_exception= False
        """
        timeout = self.TIMEOUT if not timeout else timeout
        self.log.debug("Waiting for presence of all elements with locator %s", locator)
        # Returns a selenium locator tuple of (by, selector)
        locator = WUIElement(locator, find=False)
        try:
            return WebDriverWait(self.driver, timeout, poll_frequency).until(
                EC.presence_of_all_elements_located(locator)
            )
        except TimeoutException:
            if raise_exception:
                raise WUIException(
                    'At least one element "%s" is not present in DOM.'
                    ' Waited for "%d" seconds.' % (locator, timeout)
                )
            else:
                return False
        except Exception:
            self.log.critical("Unexpected error: %s", sys.exc_info()[0])
            raise

    def WaitForTextToBePresentInElement(
        self,
        locator,
        text,
        timeout=None,
        poll_frequency=POLL_FREQUENCY,
        raise_exception=True,
    ):
        """An expectation for checking if the given text is present in the specified element.

        Similar to checking if text in ".text" method called on a webelement

        Args:
            locator: strategy technique and strategy selector (tuple)
            text: string to find in element (str)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            raise_exception: whether to raise an exception or return False (bool)

        Returns:
            True if the text is present in the element
        """
        timeout = self.TIMEOUT if not timeout else timeout
        self.log.debug("Waiting for text: %s to be present in locator: %s", text, locator)
        # Returns a selenium locator tuple of (by, selector)
        locator = WUIElement(locator, find=False)
        try:
            return WebDriverWait(self.driver, timeout, poll_frequency).until(
                EC.text_to_be_present_in_element(locator, text)
            )
        except TimeoutException:
            if raise_exception:
                raise WUIException(
                    'Text "%s" is not present in Element "%s". '
                    'Waited for "%d" seconds.' % (text, locator, timeout)
                )
            else:
                return False
        except Exception:
            self.log.critical("Unexpected error: %s", sys.exc_info()[0])
            raise

    def WaitForTextToBePresentInWebElement(
        self,
        webelement,
        text,
        timeout=None,
        poll_frequency=POLL_FREQUENCY,
        raise_exception=True,
    ):
        """An expectation for checking if the given text is present in the specified webelement.

        Similar to checking if text in ".text" method called on a webelement

        Args:
            webelement: webelement to check (webdriver object)
            text: string to find in element (str)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            raise_exception: whether to raise an exception or return False (bool)

        Returns:
            True if the text is present in the webelement
        """
        timeout = self.TIMEOUT if not timeout else timeout
        self.log.debug("Waiting for text %s to be present in webelement", text)
        try:
            return WebDriverWait(self.driver, timeout, poll_frequency).until(
                lambda webelement: text in webelement.text
            )
        except TimeoutException:
            if raise_exception:
                raise WUIException(
                    'Text "%s" is not present in WebElement "%s".'
                    ' Waited for "%d" seconds.' % (text, webelement, timeout)
                )
            else:
                return False
        except Exception:
            self.log.critical("Unexpected error: %s", sys.exc_info()[0])
            raise

    def WaitForTextToNotBeEqualInElement(
        self,
        locator,
        text,
        timeout=None,
        poll_frequency=POLL_FREQUENCY,
        raise_exception=True,
    ):
        """
        An expectation for checking if the given text is not present in the specified webelement.

        Similar to checking if text not in ".text" method called on a webelement

        Args:
            locator: strategy technique and strategy selector (tuple)
            text: string to find in element (str)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            raise_exception: whether to raise an exception or return False (bool)

        Returns:
            True if the text is not equal to the text in the webelement
        """
        timeout = self.TIMEOUT if not timeout else timeout
        locator = WUIElement(locator, find=False)
        self.log.debug(f'Waiting for text "{text}" to not be present in webelement: {locator}')
        try:
            # until_not here will return False if the text is not present. The function should
            # return True if the text is not present, so the not is added.
            return not WebDriverWait(self.driver, timeout, poll_frequency).until_not(
                EC2.text_to_equal_in_element(locator, text)
            )
        except TimeoutException:
            if raise_exception:
                raise WUIException(
                    f'Text "{text}" is present in WebElement "{locator}".'
                    f' Waited for "{timeout}" seconds.'
                )
            else:
                return False
        except Exception:
            self.log.critical(f"Unexpected error: {sys.exc_info()[0]}")
            raise

    def WaitForTextToBePresentInElementValue(
        self,
        locator,
        text,
        timeout=None,
        poll_frequency=POLL_FREQUENCY,
        raise_exception=True,
    ):
        """An expectation for checking if the given text is present in element's value attribute

        Similar to checking if text matches string in the "value" attribute of the webelement

        Args:
            locator: strategy technique and strategy selector (tuple)
            text: string to find in element's val (str)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            raise_exception: whether to raise an exception or return False (bool)

        Returns:
            True if the text is present in the element's val
        """
        timeout = self.TIMEOUT if not timeout else timeout
        self.log.debug("Waiting for text: %s to be present in locator: %s value", text, locator)
        # Returns a selenium locator tuple of (by, selector)
        locator = WUIElement(locator, find=False)
        try:
            return WebDriverWait(self.driver, timeout, poll_frequency).until(
                EC.text_to_be_present_in_element_value(locator, text)
            )
        except TimeoutException:
            if raise_exception:
                raise WUIException(
                    'Text "%s" is not present in value of Element "%s".'
                    ' Waited for "%d" seconds.' % (text, locator, timeout)
                )
            else:
                return False
        except Exception:
            self.log.critical("Unexpected error: %s", sys.exc_info()[0])
            raise

    def WaitForTextToBePresentInWebElementValue(
        self,
        webelement,
        text,
        timeout=None,
        poll_frequency=POLL_FREQUENCY,
        raise_exception=True,
    ):
        """An expectation for checking if the given text is present in webelement's value attribute

        Similar to checking if text matches string in the "value" attribute of the webelement

        Args:
            webelement: webelement to check (webdriver object)
            text: string to find in element's val (str)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            raise_exception: whether to raise an exception or return False (bool)

        Returns:
            True if the text is present in the webelement's val
        """
        timeout = self.TIMEOUT if not timeout else timeout
        self.log.debug("Waiting for text: %s to be present in element value", text)
        try:
            return WebDriverWait(webelement, timeout, poll_frequency).until(
                lambda webelement: text in webelement.get_attribute("value")
            )
        except TimeoutException:
            if raise_exception:
                raise WUIException(
                    'Text "%s" is not present in value of WebElement "%s".'
                    ' Waited for "%s" seconds.' % (text, webelement, timeout)
                )
            else:
                return False
        except Exception:
            self.log.critical("Unexpected error: %s", sys.exc_info()[0])
            raise

    def WaitForInvisibilityOfElement(
        self, locator, timeout=None, poll_frequency=POLL_FREQUENCY, raise_exception=True
    ):
        """An expectation for checking that an element is neither visible or present on the DOM

        Args:
            locator: strategy technique and strategy selector (tuple)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            raise_exception: whether to raise an exception or return False (bool)

        Returns:
            True if element is invisible
        """
        timeout = self.TIMEOUT if not timeout else timeout
        self.log.debug("Waiting for invisibility of locator %s", locator)
        # Returns a selenium locator tuple of (by, selector)
        locator = WUIElement(locator, find=False)
        try:
            return WebDriverWait(self.driver, timeout, poll_frequency).until(
                EC.invisibility_of_element_located(locator)
            )
        except TimeoutException:
            if raise_exception:
                raise WUIException(
                    'Element "%s" is not invisible in DOM. Waited for "%s" seconds.'
                    % (locator, timeout)
                )
            else:
                return False
        except Exception:
            self.log.critical("Unexpected error: %s", sys.exc_info()[0])
            raise

    def WaitForInvisibilityOfWebElement(
        self,
        webelement,
        timeout=None,
        poll_frequency=POLL_FREQUENCY,
        raise_exception=True,
    ):
        """An expectation for checking that a webelement is neither visible or present on the DOM

        Args:
            webelement: webelement to check (webdriver object)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            raise_exception: whether to raise an exception or return False (bool)

        Returns:
            True if webelement is invisible
        """
        timeout = self.TIMEOUT if not timeout else timeout
        self.log.debug("Waiting for invisibility of locator webelement")
        try:
            return WebDriverWait(self.driver, timeout, poll_frequency).until_not(
                EC.visibility_of(webelement)
            )
        except TimeoutException:
            if raise_exception:
                raise WUIException(
                    'WebElement "%s" is not invisible in DOM. '
                    'Waited for "%d" seconds.' % (webelement, timeout)
                )
            else:
                return False
        except Exception:
            self.log.critical("Unexpected error: %s", sys.exc_info()[0])
            raise

    def WaitForElementToBeClickable(
        self, locator, timeout=None, poll_frequency=POLL_FREQUENCY, raise_exception=True
    ):
        """An expectation for checking an element is visible and enabled such that you can click it

        Args:
            locator: strategy technique and strategy selector (tuple)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            raise_exception: whether to raise an exception or return False (bool)

        Returns:
            The WebElement once it is clickable
        """
        timeout = self.TIMEOUT if not timeout else timeout
        self.log.debug("Waiting for locator %s to be clickable", locator)
        # Returns a selenium locator tuple of (by, selector)
        locator = WUIElement(locator, find=False)
        try:
            return WebDriverWait(self.driver, timeout, poll_frequency).until(
                EC.element_to_be_clickable(locator)
            )
        except TimeoutException:
            if raise_exception:
                raise WUIException(
                    'Element "%s" is not clickable in DOM. '
                    'Waited for "%d" seconds.' % (locator, timeout)
                )
            else:
                return False
        except Exception:
            self.log.critical("Unexpected error: %s", sys.exc_info()[0])
            raise

    def WaitForWebElementToBeClickable(
        self,
        webelement,
        timeout=None,
        poll_frequency=POLL_FREQUENCY,
        raise_exception=True,
    ):
        """An expectation for checking webelement is visible and enabled such that you can click it

        Args:
            webelement: webelement to check (webdriver object)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            raise_exception: whether to raise an exception or return False (bool)

        Returns:
            The WebElement once it is clickable
        """
        timeout = self.TIMEOUT if not timeout else timeout
        self.log.debug("Waiting for webelement to be clickable")
        try:
            return WebDriverWait(webelement, timeout, poll_frequency).until(
                lambda webelement: webelement.is_enabled()
            )
        except TimeoutException:
            if raise_exception:
                raise WUIException(
                    'WebElement "%s" is not clickable in DOM. '
                    'Waited for "%d" seconds.' % (webelement, timeout)
                )
            else:
                return False
        except Exception:
            self.log.critical("Unexpected error: %s", sys.exc_info()[0])
            raise

    def WaitForStalenessOfWebElement(
        self,
        webelement,
        timeout=None,
        poll_frequency=POLL_FREQUENCY,
        raise_exception=True,
    ):
        """An expectation for checking the webelement is no longer attached to the DOM.

        Args:
            webelement: webelement to check (webdriver object)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            raise_exception: whether to raise an exception or return False (bool)

        Returns:
            True if element is stale
        """
        timeout = self.TIMEOUT if not timeout else timeout
        self.log.debug("Waiting for staleness of webelement")
        try:
            return WebDriverWait(self.driver, timeout, poll_frequency).until(
                EC.staleness_of(webelement)
            )
        except TimeoutException:
            if raise_exception:
                raise WUIException(
                    'WebElement "%s" is not stale in DOM. '
                    'Waited for "%d" seconds.' % (webelement, timeout)
                )
            else:
                return False
        except Exception:
            self.log.critical("Unexpected error: %s", sys.exc_info()[0])
            raise

    def WaitForElementToBeSelected(
        self, locator, timeout=None, poll_frequency=POLL_FREQUENCY, raise_exception=True
    ):
        """An expectation for the element to be located is selected.

        Args:
            locator: strategy technique and strategy selector (tuple)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            raise_exception: whether to raise an exception or return False (bool)

        Returns:
            True if element is selected
        """
        timeout = self.TIMEOUT if not timeout else timeout
        self.log.debug("Waiting for locator %s to be selected", locator)
        # Returns a selenium locator tuple of (by, selector)
        locator = WUIElement(locator, find=False)
        try:
            return WebDriverWait(self.driver, timeout, poll_frequency).until(
                EC.element_located_to_be_selected(locator)
            )
        except TimeoutException:
            if raise_exception:
                raise WUIException(
                    'Element "%s" is not selected. Waited for "%d" seconds.' % (locator, timeout)
                )
            else:
                return False
        except Exception:
            self.log.critical("Unexpected error: %s", sys.exc_info()[0])
            raise

    def WaitForWebElementToBeSelected(
        self,
        webelement,
        timeout=None,
        poll_frequency=POLL_FREQUENCY,
        raise_exception=True,
    ):
        """An expectation for checking the selection WebElement is selected.

        Args:
            webelement: webelement to check (webdriver object)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            raise_exception: whether to raise an exception or return False (bool)

        Returns:
            True if WebElement is selected
        """
        timeout = self.TIMEOUT if not timeout else timeout
        self.log.debug("Waiting for webeelement to be selected")
        try:
            return WebDriverWait(self.driver, timeout, poll_frequency).until(
                EC.element_to_be_selected(webelement)
            )
        except TimeoutException:
            if raise_exception:
                raise WUIException(
                    'WebElement "%s" is not selected. Waited for "%d" seconds.'
                    % (webelement, timeout)
                )
            else:
                return False
        except Exception:
            self.log.critical("Unexpected error: %s", sys.exc_info()[0])
            raise

    def WaitForElementToBeNotSelected(
        self, locator, timeout=None, poll_frequency=POLL_FREQUENCY, raise_exception=True
    ):
        """An expectation for the element to be located is not selected.

        Args:
            locator: strategy technique and strategy selector (tuple)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            raise_exception: whether to raise an exception or return False (bool)

        Returns:
            True if element is not selected
        """
        timeout = self.TIMEOUT if not timeout else timeout
        self.log.debug("Waiting for locator %s to be not selected", locator)
        # Returns a selenium locator tuple of (by, selector)
        locator = WUIElement(locator, find=False)
        try:
            return WebDriverWait(self.driver, timeout, poll_frequency).until_not(
                EC.element_located_to_be_selected(locator)
            )
        except TimeoutException:
            if raise_exception:
                raise WUIException(
                    'Element "%s" is not selected. Waited for "%d" seconds.' % (locator, timeout)
                )
            else:
                return False
        except Exception:
            self.log.critical("Unexpected error: %s", sys.exc_info()[0])
            raise

    def WaitForWebElementToBeNotSelected(
        self,
        webelement,
        timeout=None,
        poll_frequency=POLL_FREQUENCY,
        raise_exception=True,
    ):
        """An expectation for checking the selection WebElement is not selected.

        Args:
            webelement: webelement to check (webdriver object)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            raise_exception: whether to raise an exception or return False (bool)

        Returns:
            True if WebElement is not selected
        """
        timeout = self.TIMEOUT if not timeout else timeout
        self.log.debug("Waiting for webelement to be not selected")
        try:
            return WebDriverWait(self.driver, timeout, poll_frequency).until_not(
                EC.element_to_be_selected(webelement)
            )
        except TimeoutException:
            if raise_exception:
                raise WUIException(
                    'WebElement "%s" is not selected. Waited for "%d" seconds.'
                    % (webelement, timeout)
                )
            else:
                return False
        except Exception:
            self.log.critical("Unexpected error: %s", sys.exc_info()[0])
            raise

    def WaitForPageLoaded(self, timeout=None, poll_frequency=POLL_FREQUENCY, raise_exception=True):
        """An expectation for checking that javascript returns readyState as complete

        Args:
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            raise_exception: whether to raise an exception or return False (bool)

        Returns:
            True if page's readyState is complete
        """
        timeout = self.TIMEOUT if not timeout else timeout
        self.log.debug("Waiting for page to be loaded")
        try:
            return WebDriverWait(self.driver, timeout, poll_frequency).until(
                lambda driver: self.driver.execute_script("return document.readyState")
                in "complete"
            )
        except TimeoutException:
            if raise_exception:
                raise WUIException(
                    'Page readyState is not complete. Waited for "%d" seconds.' % timeout
                )
            else:
                return False
        except Exception:
            self.log.critical("Unexpected error: %s", sys.exc_info()[0])
            raise

    def WaitUntilCSSPropertyIs(
        self,
        locator,
        css_property,
        property_value,
        timeout=None,
        poll_frequency=POLL_FREQUENCY,
        raise_exception=True,
    ):
        """An expectation for checking that an element exists, but may not be visible until the
        css_property specified has the value of property_value

        Args:
            locator: strategy technique and strategy selector (tuple)
            css_property: name of CSS property to query (str)
            property_value: value css_property to satisfy wait
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            raise_exception: whether to raise an exception or return False (bool)

        Returns:
            Whether the css_property value matches property_value for locator (bool)

        Raises:
            WUIException if css_property does not equal property_value in the timeout duration
        """
        timeout = self.TIMEOUT if not timeout else timeout
        self.log.debug(
            "Waiting for element with locator %s css property %s to be %s",
            locator,
            css_property,
            property_value,
        )
        locator = WUIElement(locator, find=False)
        try:
            return WebDriverWait(self.driver, timeout, poll_frequency).until(
                EC2.CSSPropertyIs(locator, css_property, property_value)
            )
        except TimeoutException:
            if raise_exception:
                raise WUIException(
                    'Timeout waiting for element "%s" css property "%s" to '
                    'change to "%s".  Waited "%d" seconds'
                    % (locator, css_property, property_value, timeout)
                )
            else:
                return False
        except Exception:
            self.log.critical("Unexpected error: %s", sys.exc_info()[0])
            raise

    def WaitForNewTab(self, new_window_count, timeout=None):
        """This method waits for new window to appear based on the values passed in

        Fails if it takes more than 10 seconds for the new window to appear

        Args:
            new_window_count: Window count after the desired window is open (int)
            timeout: time to wait for the new window to appear (int)

        Raises:
            WUIException if the new window does not appear within 10 seconds default timeout
        """
        timeout = self.TIMEOUT if not timeout else timeout
        counter = 0
        current_handles = len(self.driver.window_handles)
        if current_handles == new_window_count:
            return
        else:
            while current_handles != new_window_count and counter < timeout:
                LOGGER.debug("Waiting for new window...")
                self.driver.implicitly_wait(1)
                current_handles = len(self.driver.window_handles)
                counter += 1
            if current_handles == new_window_count:
                return
            else:
                raise WUIException("Waited for %s seconds but new window not visible" % timeout)

    def WaitForPresenceOfTextOnReloadClick(
        self, reload_btn, text, timeout=None, raise_exception=True
    ):
        """An expectation for checking that an element is present on the DOM of a page after
        reload click.

        This does not necessarily mean that the element is visible.

        Args:
            reload_btn: strategy technique and strategy selector (tuple)(webelement)
            text: string to find in body (str)
            timeout: time to wait (int)
            raise_exception: whether to raise exception on timeout (bool)

        Returns:
            The WebElement once it is located

        Exceptions:
            WUI exception when element in locator not found for the duration of the timeout
        """
        timeout = self.TIMEOUT if not timeout else timeout

        try:
            end_time = time.time() + timeout
            while True:
                self.log.debug("Click on reload button: %s", reload_btn)
                reload_btn().click()
                value = self.WaitForTextPresentInBody(
                    text=text, timeout=5, poll_frequency=1, raise_exception=False
                )
                LOGGER.debug("Value found: %s", value)
                if (time.time() > end_time) or value:
                    break
            if not value:
                raise TimeoutException()
        except TimeoutException:
            if raise_exception:
                raise WUIException(
                    'Text "%s" is not present in DOM. Waited for "%d" seconds.' % (text, timeout)
                )
            else:
                return False
        except Exception:
            self.log.critical("Unexpected error: %s", sys.exc_info()[0])
            raise

    def WaitForWebElementAnimationToStop(
        self, web_element, interval=1.0, timeout=None, raise_exception=True
    ):
        timeout = timeout or self.TIMEOUT
        location = web_element.location
        self.log.debug(
            f"Waiting for webelement to stop being animated. original location: {location}"
        )

        intervals_waited = 0
        stopped = False
        while not stopped and intervals_waited <= timeout:
            time.sleep(interval)
            if location == web_element.location:
                stopped = True
            location = web_element.location
            intervals_waited += interval
        if not stopped and raise_exception:
            raise WUIException("The Web Element never stopped animating.")
        self.log.debug(f"webelement stopped animating. final location: {location}")
        return stopped
