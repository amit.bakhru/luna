FAILED_TO_LAUNCH_BROWSER = "Failed to launch browser"


def browser_fails_to_launch(err, *args):
    return FAILED_TO_LAUNCH_BROWSER in str(err[1])
