"""Wrapper for the Selenium Driver and the Test Cases"""

import os
import platform
import shutil
import sys

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webelement import WebElement

from luna.config import config
from luna.driver.ui import base
from luna.driver.ui import wui_locators
from luna.driver.ui.wuielement import WUIElement
from luna.driver.ui.wuiexception import WUIException
from luna.framework.common.exception import FileBasedException
from luna.framework.common.logger import LOGGER
from luna.framework.utils.file_utils import FileUtilities


class Interactions:
    """Mixin class that defines the commonly used interactions on a web page."""

    def __init__(self):
        self.base = base
        self.driver = self.base.driver
        self.log = self.base.log

    def ClickButtonWithText(self, text=None):
        """Clicks on a button with text specified

        Args:
            text: text value of the button (str)
        """
        return self.driver.execute_script(f"return $('button:contains(\"{text}\")').click()")

    def ClickLinkWithText(self, text=None):
        """Clicks a anchor link with text specified

        Args:
            text: text value of the link element to be clicked (str)

        Raises:
            WUIException if element not found or not accessible
        """
        try:
            return self.driver.find_element_by_link_text(text).click()
        except NoSuchElementException as e:
            raise WUIException(str(e))

    def TypeInActiveInputElement(self, text, return_key=True):
        """Type in active input element

        Args:
            text: text value to be entered in the active element (str)
            return_key: returns "RETURN key" based on return_key value
        Raises:
            WUIException if element not found or not accessible
        """
        try:
            if return_key:
                self.driver.switch_to.active_element.send_keys(text + Keys.RETURN)
            else:
                self.driver.switch_to.active_element.send_keys(text)
        except NoSuchElementException as e:
            raise WUIException(str(e))

    def LeftShiftDown(self):
        """Simulate press and hold keyboard's L-Shift button"""

        actionChains = ActionChains(self.driver)
        actionChains.key_down(Keys.LEFT_SHIFT).perform()

    def LeftShiftUp(self):
        """Release simulated hold on keyboard's L-Shift button"""

        actionChains = ActionChains(self.driver)
        actionChains.key_up(Keys.LEFT_SHIFT).perform()

    def DoubleClick(self, webelement):
        """Double clicks on the webelement supplied

        Args:
            webelement: webelement to double click on (selenium webelement object)
        """
        actionChains = ActionChains(self.driver)
        actionChains.double_click(webelement).perform()

    def Hover(self, webelement, xoffset=0, yoffset=0):
        """Performs a mouse-over/hover on element

        Moves mouse to the middle of the element
        Args:
            webelement: webelement to hover over (selenium webelement object)
            xoffset: offset from upper left to click on element (int)
            yoffset: offset from upper left to click on element (int)
        """
        actionChains = ActionChains(self.driver)
        if xoffset > 0 or yoffset > 0:
            actionChains.move_to_element_with_offset(webelement, xoffset, yoffset).perform()
        else:
            actionChains.move_to_element(webelement).perform()

    def id(self, locator):
        """Returns id attribute of the element

        Args:
            locator: strategy technique and strategy selector (tuple)

        Raises:
            WUIException if element not found or not accessible
        """
        webelement = WUIElement(locator)
        try:
            return webelement.get_attribute("id")
        except NoSuchElementException as e:
            raise WUIException(str(e))

    def SelectFromDropDown(self, locator=None, option=None):
        """Selects an item from drop-down-menu.

        Args:
            locator: strategy technique and stratgey selector for the dropdown (tuple)
            option: option to be selected from drop-down-menu (str)
        """
        if locator is None:
            locator = wui_locators.DEFAULT_DRP_DWN_OPT
        self.WaitForPresenceOfAllElements(locator)
        attempts = 3
        while attempts > 0:
            try:
                for elem in WUIElement(locator, multiple=True):
                    LOGGER.debug("Looking at %s", elem.text)
                    if elem.is_displayed() and option in elem.text:
                        LOGGER.info("Clicking on %s", elem.text)
                        elem.click()
                        return
            except StaleElementReferenceException as e:
                attempts -= 1
                if attempts == 0:
                    raise WUIException(str(e))
            except NoSuchElementException as e:
                raise WUIException(str(e))
            attempts -= 1
        if attempts == 0:
            raise WUIException("Could not find %s in the dropdown list" % option)

    @staticmethod
    def SelectDropDownOpt(drop_down_list, option):
        """Selects drop down option from drop_down_list

        Args:
            drop_down_list: list of drop down webelements (list)
            option: option from list to select (str)

        Raises:
            WUIException if option is not found

        This is a bit more efficient than SelectFromDropDown
        """
        for list_option in drop_down_list:
            LOGGER.debug('Comparing option value "%s" to "%s"', list_option.text, option)
            if list_option.text.lower() == option.lower():
                LOGGER.info("Clicking on grouping %s", list_option.text)
                list_option.click()
                break
        else:
            raise WUIException("Could not find metavalue %s" % option)

    def type_and_enter(self, locator, val, clear=False):
        """Type the value in element object and then press RETURN key.

        Args:
            locator: strategy technique and strategy selector or webelement (tuple/webelement)
            val: value to be typed in the element. (string)
            clear: whether or not to clear the contents prior to sending keys (bool)
        """
        if isinstance(locator, WebElement):
            webelement = locator
        else:
            webelement = self.WaitForElementDisplayed(locator, timeout=15)

        if clear:
            webelement.clear()
        return webelement.send_keys(val, Keys.RETURN)

    def send_return(self, locator):
        """Types the Keys.RETURN in element object

        Args:
            locator: strategy technique and strategy selector (tuple)
        """
        webelement = self.WaitForElementDisplayed(locator, timeout=15)
        return webelement.send_keys(Keys.RETURN)

    def IsSelected(self, table_locator=None, tbl_elem=None):
        """Checks if a web element is checked

        Args:
            table_locator: strategy technique and strategy selector (tuple)
            tbl_elem: table webelement to be checked

        Returns: True if checkbox is enabled, False otherwise
        """

        class_list = ["x-form-cb-checked", "x-grid-row-selected"]

        if tbl_elem is not None:
            webelm = tbl_elem
        else:
            webelm = WUIElement(table_locator)

        for class_name in class_list:
            if class_name in webelm.get_attribute("class"):
                return True

        return False

    def IsDisabled(self, table_locator=None, tbl_elem=None):
        """Checks if a web element is disabled

        Args:
            table_locator: strategy technique and strategy selector (tuple)
            tbl_elem: table webelement to be checked (Webelement)

        Returns: True if checkbox is disabled, False otherwise
        """

        class_list = ["x-item-disabled"]

        if tbl_elem is not None:
            webelm = tbl_elem
        else:
            webelm = WUIElement(table_locator)

        for class_name in class_list:
            if class_name in webelm.get_attribute("class"):
                return True

        return False

    def IsStatusAsExpected(self, table_locator=None, tbl_elem=None, expected="enabled"):
        """Checks if a web element is disabled

        Args:
            table_locator: strategy technique and strategy selector (tuple)
            tbl_elem: table webelement to be checked (Webelement)
            expected: expected value (str)

        Returns: True if checkbox is enabled, False otherwise
        """

        if expected == "enabled":
            class_list = ["icon-enabled"]
        else:
            class_list = ["icon-disabled-check"]

        if tbl_elem is not None:
            webelm = tbl_elem
        else:
            webelm = WUIElement(table_locator)

        for class_name in class_list:
            if class_name in webelm.get_attribute("class"):
                return True

        return False

    def ToggleChecks(self, flag, tbl_select_elem, tbl_locator=None, element=None):
        """This method toggles the select boxes

        Args:
            flag: flag for the  element(bool)
            tbl_select_elem: checkbox WUI element associated with element
            tbl_locator: strategy locator for table locator(tuple)
            element:  table WUIelement for select box(WUIElement)
        """

        if tbl_locator is None:
            if self.IsSelected(tbl_elem=element):
                if not flag:
                    tbl_select_elem.click()
            else:
                if flag:
                    tbl_select_elem.click()
        else:
            if self.IsSelected(table_locator=tbl_locator):
                if not flag:
                    tbl_select_elem.click()
            else:
                if flag:
                    tbl_select_elem.click()

    def FindElementWithText(self, root_elem, tag_name, text, multiple=False):
        """Finds element that has the text specified

        Args:
            root_elem: base element to start the search from(WUIElement)
            tag_name: tagname for the element that has the text(HTML element)
            text: text value to find (str)
            multiple: flag to mention if we need single or multiple matching elements(bool)
        Returns:
            First element in the DOM that has the text specified if multiple=False
            List of all elements in the DOM that have the text specified if multiple=True
            None if no elements are found
        Note: Please use this method sparingly. Only use it in very rare cases, like when it is
            difficult to get to the elements that may be deeply nested. This method has a higher
            performance hit.
        """

        all_elements = root_elem.find_elements_by_tag_name(tag_name)
        result_elms = list()
        if not multiple:
            for elm in all_elements:
                if elm.text == text:
                    return elm
            LOGGER.info("No element found with text %s", text)
            return None
        else:
            for elm in all_elements:
                if elm.text == text:
                    result_elms.append([elm])
        if len(result_elms) == 0:
            LOGGER.info("No elements found with text %s", text)
            return None
        return result_elms

    def FindIndexFromTableColOrList(self, searchVal=None, selector=None, param=None):
        """This method returns row index of searchVal for the table column or a list for selector.

        This method can be used to get index for any selector which returns multiple values (
        list of text)

        Args:
            searchVal: The value being searched for in the column selector specified.
            selector: The column/drop-down List locator tuple specified
            param: "In" to be specified if the value of the selector might not be exact match.
                    ["In"]

        Returns:
            Index of the specific even in the column. If not found, then returns -1.
        """
        self.WaitForPresenceOfAllElements(selector)

        val_list = WUIElement(selector, multiple=True)

        if searchVal is not None:
            if param is None:
                for index in range(len(val_list)):
                    if val_list[index].text.strip() == searchVal:
                        self.log.debug("%s found at index %d", searchVal, index)
                        return index
            else:
                for index in range(len(val_list)):
                    if searchVal in val_list[index].text.strip():
                        self.log.debug("%s found at index %d", searchVal, index)
                        return index
        else:
            index = 0
            self.log.info("Since the searchVal is None, %s found at index %d", val_list[0], index)
            return index

        return -1

    def ClickElement(self, locator):
        """This method is a wrapper around webdriver click method

        Purpose of this method is to have flexibility to modify this method to our needs if needed
        Also, reduce the direct calls to webdriver.click method

        Args:
            locator: strategy locator or WUIElement(tuple)
        """
        if isinstance(locator, WebElement):
            webelement = locator
        else:
            webelement = self.WaitForElementDisplayed(locator, timeout=15)
        return webelement.click()

    def Select(self, tbl_name_elm, tbl_elm, tbl_select_elm, to_select):
        """This method selects one or multiple items on the web page based on parameters passed

        Args:
            tbl_name_elm: HTML tag that contains the text to be clicked on (HTMLElement)
            tbl_elm: WUIElement that has clicked status in its class attribute (WUIElement)
            tbl_select_elm: checkbox associated with the element to click (WUIElement)
            to_select: list of names to select (list)
        """
        counter = 0
        for elm_sel in to_select:
            for elm in tbl_elm:
                el_to_clk = self.FindElementWithText(
                    root_elem=elm, tag_name=tbl_name_elm, text=elm_sel
                )
                if el_to_clk is not None:
                    LOGGER.debug("Found element with text %s", elm_sel)
                    self.WaitForWebElementToBeClickable(tbl_select_elm[counter])
                    self.ClickElement(tbl_select_elm[counter])
                    counter = 0
                    break
                else:
                    counter += 1

    def IsButtonWithTextPresent(self, btn_text):
        """This method checks if button having a particular text is present in the DOM

        Args:
            btn_text: Text to search for in the button(str)

        Returns:
            True if button having text is present, else False
        """
        locator = wui_locators.BUTTON
        all_texts = list()
        all_elms = WUIElement(locator, multiple=True)
        for elm in all_elms:
            if elm.text not in [" ", ""]:
                all_texts.append(elm.text)
        if btn_text in all_texts:
            return True
        return False

    def SwitchToWindow(self, window_handle=None):
        """This method switches to the last window in the currently open window handles

        If window_handle is passed in, it will switch to the specific window handle

        Args:
            window_handle: window_handle to switch to (str)

        Returns:
            window handle of the new window (str)
        """
        if window_handle is not None:
            self.driver.switch_to_window(window_handle)
        return self.driver.switch_to_window(self.driver.window_handles[-1])

    def CloseWindow(self, window_handle=None):
        """This method closes the active browser session

        Args:
            window_handle: window handle to switch to before closing the window (str)
        """
        if window_handle is not None:
            self.SwitchToWindow(window_handle)
            self.driver.close()
        self.driver.close()

    def IsElementWithTextPresent(self, text, locator):
        """Determine if an element with the text presents in the DOM

        Args:
            text: The text to be searched (str)
            locator: The locator to be searched (tuple)

        Returns:
            True if found, False otherwise. (boolean)
        """
        element_list = locator(multiple=True)
        for element in element_list:
            LOGGER.debug("Looking at %s", element.text)
            if element.text == text:
                return True
        return False

    def DragAndDrop(self, root_elem, tag_name, source_text, target_elem):
        """This method drags the source element and drops to target

        Args:
            root_elem: base element to start the search from(WUIElement)
            tag_name: tagname for the element that has the text(HTML element)
            source_text: The text of the element to drag (str)
            target_elem: The element to drop (WUIElement)
        """
        try:
            source_element = self.FindElementWithText(root_elem, tag_name, source_text)
            ActionChains(self.driver).click_and_hold(source_element).perform()
            ActionChains(self.driver).move_to_element(target_elem).perform()
            ActionChains(self.driver).release(target_elem).perform()

        except NoSuchElementException as e:
            raise WUIException(str(e))

    def GetWindowHandle(self, window_index=0):
        """Gets browser Window/Tab handle by window/tab index

        Args:
            window_index: index of window/tab (int)
                          example input: [0 (default), 1 , 2 ...]
                                         window_index=-1 returns handle for last tab.
        Returns:
               Returns Window Handle object
        """

        if self.driver is not None:
            return self.driver.window_handles[window_index]

    def GetUrl(self):
        """Gets current windows/tabs url

        Returns:
               Current window URL
        """

        if self.driver is not None:
            return self.driver.current_url

    def GetAttributeValue(self, element, attr):
        """Gets attribute value for element specified.
        Args:
            element: Element to be checked for attribute.(WUIElement)
            attr: Name of the attribute.(str)
                Example Input: attr='class'
        Returns:
               Attribute value for specified element.(str)
        Raises:
              NoSuchElementException if element not found.
        """

        try:
            return str(element.get_attribute(attr))
        except NoSuchElementException as e:
            raise WUIException(str(e))

    def ScrollIntoView(self, webelement):
        """Scrolls to webelement specified

        Args:
            webelement: element to bring into view (webelement)
        """
        LOGGER.debug("Scrolling element into view")
        self.driver.execute_script("return arguments[0].scrollIntoView();", webelement)

    def GetButtonElementUsingText(self, btn_text=None):
        """Get UI Element of button having the text specified

        Args:
            btn_text: text value of the button (str)

        Returns:
            elm: WUI Element which is having the text as Args
        """

        locator = wui_locators.PARENT_BUTTON
        all_elms = WUIElement(locator, multiple=True)
        for elm in all_elms:
            if (elm.text not in [" ", ""]) and (btn_text == elm.text):
                return elm

    def EnterTextInInputElement(self, locator, text):
        """Wrapper function to click and enter text into Active Input

        Args:
            locator: Input element locator (Element)
            text: Text to enter (str)
        """

        self.WaitForElementDisplayed(locator=locator, timeout=15)
        self.ClickElement(locator=locator)
        self.TypeInActiveInputElement(text=text)

    def GetChromeOrFirefoxDownloadedPath(self):
        """Get the downloads folder for Firefox.

        Returns:
            downloaded_path: Content downloaded path
        """

        user_home = os.getenv("USERPROFILE")
        platfrm = platform.platform()
        if "Windows-7" in platfrm:
            downloads = os.sep.join([user_home, "Downloads"])
        else:
            downloads = os.sep.join([user_home, "My Documents", "Downloads"])
        return downloads

    def GetDownloadedPath(self):
        """Gets the browsers path of the content downloaded

        Returns:
            downloaded_path: Content downloaded path
        """

        if "LINUX" in sys.platform.upper():
            download_dir = os.path.join(os.getenv("HOME"), "Downloads")
            if not os.path.exists(download_dir):
                os.mkdir(download_dir)
            downloaded_path = download_dir
        else:
            browser_name = config["test_driver"]["grid"]["browser"]
            if ("FIREFOX" or "CHROME") == browser_name.upper():
                downloaded_path = self.GetChromeOrFirefoxDownloadedPath()
            elif browser_name.upper() == "IE":
                LOGGER.info("Need to Set !!")
            LOGGER.info("Downloaded Path is %s", downloaded_path)
        return downloaded_path

    def MoveDownloadedContent(self, zip_content_name, destination_dir):
        """Moves the Downloaded content from the Downloaded path to required location

        Args:
            zip_content_name: Name of the downloaded content in zip format
            destination_dir: Destination Directory for moving the downloaded content

        Return:
            status: Status of the Presence of the content in the downloaded path and the content
            is moved to the destination.
        """

        downloaded_path = os.sep.join([self.GetDownloadedPath(), zip_content_name])
        LOGGER.info("Downloaded Path is %s", downloaded_path)
        status = FileUtilities().Wait_for_file_download_complete(
            file_path=downloaded_path, timeout=30
        )
        if status:
            try:
                dst_dir = shutil.move(downloaded_path, destination_dir)
                moved_status = True
                LOGGER.info("File Moved to the %s", dst_dir)
                return moved_status
            except Exception as e:
                raise FileBasedException(e)

    def HoverUsingJquery(self, selector):
        """Hovers over the element using jquery mouse-over

        Args:
            selector: CSS selector of the element to mouse-over (locator)
        """
        self.driver.execute_script("$('%s').trigger('mouseover')" % selector)
