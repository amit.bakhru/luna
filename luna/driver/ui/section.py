from luna.driver.ui import wui_locators
from luna.driver.ui.element import Element
from luna.driver.ui.wuielement import PlaywrightElement
from luna.driver.ui.wuiexception import WUIException


class Section(Element):
    def __init__(self, config=None, runvalidations=True, *args, **kwargs):
        """Section wui class that all section objects inherit from.

        Section level interaction methods reside in this class. On initialization all elements
        defined in their section config are set as attributes that are objects of the Element class.

        A section is defined as something that pop ups/common element that appears in multiple pages
        that does not have a direct URL navigation

        Params:
            config: configuration (dict)
            runvalidations: run section load validations (bool)
        """
        super().__init__()
        self.section_config = config

        if hasattr(config, "elements"):
            for element in list(config.elements.keys()):
                # import pdb;pdb.set_trace()
                self.__setattr__(element, Element(locator=config.elements[element]))

        if runvalidations:
            self.RunValidations()

    def RunValidations(self, config=None):
        """Runs validations to ensure page/section is loaded

        Args:
            config: configuration (dict)
        """
        if config is None:
            config = self.section_config
        if hasattr(config, "validations"):
            for validation in config.validations:
                for criteria in validation:
                    getattr(self, criteria)(validation[criteria])

    def GetWindowHeader(self, active_window_header=None):
        """Returns header element of the active window."""
        if active_window_header is None:
            active_window_header = wui_locators.DEFAULT_ACTIVE_WINDOW_HEADER
        return PlaywrightElement(active_window_header)

    def ClickWindowButton(self, button_text="Save"):
        """Clicks on button with text in active window"""
        self.log.debug("Clicking '%s' button", button_text)
        active_window_btns = wui_locators.DEFAULT_ACTIVE_WINDOW_BUTTONS
        for btn in PlaywrightElement(active_window_btns, multiple=True):
            if btn.is_displayed() and btn.text == button_text:
                btn.click()
                return
        raise WUIException("Button with name '%s' not found!!" % button_text)

    def GetErrorMessage(self, error_message=None):
        """Returns error message text for active window"""
        if error_message is None:
            error_message = wui_locators.DEFAULT_ERROR_MSG
        return PlaywrightElement(error_message).text

    def GetColumnHeader(self, table_header=None):
        """Returns table column header element for active window"""
        if table_header is None:
            table_header = wui_locators.DEFAULT_TABLE_HEADER
        return PlaywrightElement(table_header)

    def CheckResult(self, message, msg_popup_selector=("css", ".x-message-box")):
        """Checks result of completed action.  This typical involves a popup message
        displaying the result of the action then disappearing of the popup.

        Args:
            message: message to look for. (str)
            msg_popup_selector: selector for popup message box. (str)
        """
        self.WaitForTextPresentInBody(message)
        self.WaitForInvisibilityOfElement(msg_popup_selector)
