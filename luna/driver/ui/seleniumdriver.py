import os

from selenium import webdriver

from luna.config import config
from luna.framework.common.logger import LOGGER
from luna.framework.utils.dotdict import DotDict


class SeleniumDriver:
    _instance = None

    BASE_CHROME_OPTIONS = ["--no-sandbox", "--headless", "--verbose"]

    def __init__(self):
        """Initializes the Selenium Driver object and sets all its attributes to None apart from

        the logger. The logger is a global variable imported from Common Framework.
        """

        self.__base_url = None
        self.__base_data = None
        self.__base_ip = None
        self.__driver = None
        self.__driver_config = None
        self.__log = LOGGER
        self.__protocol = None
        self.__servers_config = None
        self.__test = DotDict(dict())

    def __new__(cls, *args, **kwargs):
        """Overrides the special method __new__ to ensure there is only one object instance of

        this class for any given test case.
        """

        if not cls._instance:
            cls._instance = super().__new__(cls, *args, **kwargs)
        return cls._instance

    @property
    def data(self):
        return self.__base_data

    @property
    def driver(self):
        return self.__driver

    @property
    def driver_config(self):
        return self.__driver_config

    @property
    def ip(self):
        return self.__base_ip

    @property
    def log(self):
        return self.__log

    @property
    def protocol(self):
        return self.__protocol

    @property
    def servers_config(self):
        return self.__servers_config

    @property
    def test(self):
        return self.__test

    @property
    def url(self):
        return self.__base_url

    def setup(self, config=None):
        """Sets up the test harness.

        Reads data from the configuration file that is passed as a command line argument
        to nosetests.
        """

        if config:
            self.__driver_config = config["driver"]
            self.__servers_config = config["servers"]
        else:
            raise Exception("Missing test configuration")

        self.__base_ip = os.environ.get("PORTAL_HOSTNAME", config["servers"]["portal"]["ip"][0])
        self.__protocol = os.environ.get("PORTAL_PROTOCOL", config["servers"]["portal"]["protocol"])
        self.__base_url = self.__protocol + "://" + self.__base_ip

        if config["common"]["logging"] == "DEBUG" or os.environ.get("luna_DEBUG") == "1":
            self.log.setLevel("DEBUG")

        self.__base_data = dict()
        self.data["ip"] = self.ip
        self.data["url"] = self.url

        if self.ip:
            self.LaunchDriver()
        else:
            msg = """
            Portal host information is not present in the configuration file and PORTAL_HOSTNAME
            environment variable not set.
            """

            self.log.critical(msg)
            raise Exception(msg)

        return self.driver

    def LaunchDriver(self):
        """Launches the selenium webdriver browser based on the driver config."""

        if self.driver_config["connect"].lower() == "grid":
            # Creating the Selenium Grid Hub URL
            if self.driver_config["grid"]:
                selenium_grid = "http://{}:{}/wd/hub".format(
                    self.driver_config["grid"]["hub"],
                    self.driver_config["grid"]["port"],
                )

            # Defining capabilities based on browser selection
            if os.environ.get("BROWSER", None).lower() == "firefox":
                options = webdriver.FirefoxOptions()
                options.headless = True
            elif os.environ.get("BROWSER", None).lower() == "chrome":
                options = webdriver.ChromeOptions()
                [options.add_argument(arg) for arg in self.BASE_CHROME_OPTIONS]
            elif self.driver_config["grid"]["browser"].lower() == "ie":
                options = webdriver.IeOptions()
            else:
                raise Exception("Browser type not recognized. Select firefox, chrome or ie")
            desired_capabilities = options.to_capabilities()

            # Defining proxy capabilities based on proxy configurations
            if self.driver_config["proxy"]["enable"]:
                proxy_url = self.driver_config["proxy"]["url"]
                proxy_port = self.driver_config["proxy"]["port"]
                proxy = proxy_url + ":" + proxy_port
                desired_capabilities["proxy"] = {
                    "httpProxy": proxy,
                    "ftpProxy": proxy,
                    "sslProxy": proxy,
                    "noProxy": self.ip,
                    "proxyType": "MANUAL",
                    "class": "org.openqa.selenium.Proxy",
                    "autodetect": False,
                }

            # Defining node selection capabilities based on grid node configuration
            desired_capabilities["applicationName"] = self.driver_config["grid"]["node"]["name"]
            desired_capabilities["version"] = self.driver_config["grid"]["node"]["version"]
            desired_capabilities["platform"] = self.driver_config["grid"]["node"][
                "platform"
            ].upper()

            # Launching the browser
            try:
                self.__driver = webdriver.Remote(
                    desired_capabilities=desired_capabilities,
                    command_executor=selenium_grid,
                )
            except Exception as e:
                raise Exception("Failed to launch remote browser: %s" % str(e))

        elif self.driver_config["connect"].lower() == "standalone":
            if self.driver_config["standalone"]["browser"].lower() == "firefox":
                profile = webdriver.FirefoxProfile()
                profile.native_events_enabled = False
                profile.set_preference(
                    "browser.helperApps.neverAsk.saveToDisk",
                    "application/zip"
                    "application/octet"
                    "-stream, text/csv, "
                    "application/zip, "
                    "application/pdf, "
                    "application/text, "
                    "application/xml, "
                    "application/vnd."
                    "tcpdump.pcap, text/"
                    "plain, application/"
                    "json+encrypted",
                )

                # Defining debug capabilities based on development configurations
                if config["common"]["firefox_debug"]:
                    profile.add_extension(extension="plugins/firebug-2.0.8.xpi")
                    profile.add_extension(extension="plugins/netExport-0.9b7.xpi")
                    profile.add_extension(extension="plugins/fireStarter-0.1a6.xpi")
                    profile.set_preference("extensions.firebug.currentVersion", "2.0.8")
                    profile.set_preference("extensions.firebug.DBG_NETEXPORT", False)
                    profile.set_preference("extensions.firebug.allPagesActivation", "on")
                    profile.set_preference("extensions.firebug.onByDefault", True)
                    profile.set_preference("extensions.firebug.defaultPanelName", "net")
                    profile.set_preference("extensions.firebug.net.enableSites", True)
                    profile.set_preference("extensions.firebug.net.persistent", True)
                    profile.set_preference(
                        "extensions.firebug.netexport.alwaysEnableAutoExport", True
                    )
                    profile.set_preference("extensions.firebug.netexport.autoExportToFile", True)
                    profile.set_preference("extensions.firebug.netexport.autoExportToServer", False)
                    # profile.set_preference('extensions.firebug.netexport.defaultLogDir',
                    #                         self.test_output)
                    profile.set_preference("extensions.firebug.netexport.showPreview", True)
                    profile.set_preference("extensions.firebug.netexport.sendToConfirmation", False)
                    profile.set_preference("extensions.firebug.netexport.pageLoadedTimeout", 1500)
                    profile.set_preference("extensions.firebug.netexport.Automation", True)
                    profile.set_preference(
                        "browser.helperApps.neverAsk.saveToDisk",
                        "application/zip"
                        "application/octet"
                        "-stream, text/csv, "
                        "application/zip, "
                        "application/pdf, "
                        "application/text, "
                        "application/xml, "
                        "application/vnd."
                        "tcpdump.pcap, text/"
                        "plain, application/"
                        "json+encrypted",
                    )

                # Defining proxy capabilities based on proxy configurations
                if self.driver_config["proxy"]["enable"]:
                    for proxy_type in [
                        "backup.ssl",
                        ".backup.ftp",
                        "http",
                        "socks",
                        "ssl",
                    ]:
                        proxy_type = "network.proxy." + proxy_type
                        proxy_type_port = proxy_type + "_port"
                        profile.set_preference(proxy_type, self.driver_config["proxy"]["url"])
                        profile.set_preference(proxy_type_port, self.driver_config["proxy"]["port"])

                    profile.set_preference("network.proxy.type", 1)
                    profile.set_preference("network.proxy.share_proxy_settings", True)
                    profile.set_preference("network.proxy.no_proxies_on", self.ip)

                # Launching the browser
                try:
                    self.__driver = webdriver.Firefox(profile)
                except Exception as e:
                    raise Exception("Failed to launch browser: %s" % str(e))

            elif self.driver_config["standalone"]["browser"].lower() == "chrome":
                try:
                    chrome_options = webdriver.ChromeOptions()
                    if self.driver_config["standalone"]["headless"]:
                        [chrome_options.add_argument(arg) for arg in self.BASE_CHROME_OPTIONS]
                    self.__driver = webdriver.Chrome(options=chrome_options)
                except Exception as e:
                    raise Exception("Failed to launch browser: %s" % str(e))
            elif self.driver_config["standalone"]["browser"].lower() == "ie":
                try:
                    self.__driver = webdriver.Ie()
                except Exception as e:
                    raise Exception("Failed to launch browser: %s" % str(e))
            elif self.driver_config["standalone"]["browser"].lower() == "safari":
                try:
                    self.__driver = webdriver.Safari()
                except Exception as e:
                    raise Exception("Failed to launch browser: %s" % str(e))
            # unsupported browser
            else:
                raise Exception(
                    "Browser '%s' not recognized. Select firefox/chrome/safari/ie"
                    % self.driver_config["standalone"]["browser"]
                )
            if not self.driver_config["standalone"]["headless"]:
                self.__driver.maximize_window()
        else:
            raise Exception("Config file must have 'standalone' or 'grid' for selenium connect.")
        self.__driver.set_window_size(self.driver_config["width"], self.driver_config["height"])
