import time

from selenium.common.exceptions import JavascriptException, WebDriverException

from luna.driver.ui import base


class Toastr:
    """Python interface for the javascript library Toastr"""

    def __init__(self):
        self.available = False

    def setup_and_growl(self, message, title):
        """Method to setup toastr if not already setup and growl the message with the title

        Args:
            message: text to be displayed in the growl message body (str)
            title: title of the growl popup (str)
        """
        if not self.is_setup():
            self.setup()
        else:
            self.available = True

        if self.available:
            self.clear()
            self.growl(message, title)

    def growl(self, message, title):
        """Displays the growl message popup

        Args:
            message: text to be displayed in the growl message body (str)
            title: title of the growl popup (str)
        """

        if self.available:
            # base.driver.execute_script(f'toastr["error"]("{message}", "{title}")')
            base.page.evaluate(f'toastr["error"]("{message}", "{title}")')
        else:
            base.log.debug("Unable to perform growl operations as toastr is not available")

    def clear(self):
        """Clears all open growl popups"""

        if self.available:
            base.page.evaluate("toastr.clear()")
        else:
            base.log.debug("Unable to perform growl operations as toastr is not available")

    def setup(self):
        """Sets up toastr related javascript and css on the page

        Also defines toastr options. These options control attributes of the growl style popup that
        is displayed.

        Returns:
            True/False if toastr successfully setup
        """
        # Update to Latest Jquery
        base.page.evaluate(
            "var jq = document.createElement('script');"
            'jq.src = "https://ajax.googleapis.com/'
            'ajax/libs/jquery/2.1.3/jquery.min.js";'
            "document.getElementsByTagName('head')[0].appendChild(jq);"
        )

        attempts = 3
        while attempts > 0:
            try:
                # Includes Toastr's Javascript
                base.page.evaluate(
                    "$.getScript('https://cdnjs.cloudflare.com/"
                    "ajax/libs/toastr.js/latest/js/toastr.min.js' "
                    ", function () { window.toastrFlag = true; })"
                )

                # Includes Toastr's CSS
                base.page.evaluate(
                    "$('head').append('<link rel=\"stylesheet\" "
                    'href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js'
                    '/latest/css/toastr.min.css" type="text/css" />\');'
                )

                # Sets Toastr's Options
                base.page.evaluate(
                    """
                    toastr.options = {
                      "newestOnTop": true,
                      "positionClass": "toast-top-right",
                      "showDuration": "1",
                      "hideDuration": "1",
                      "timeOut": "3000",
                      "extendedTimeOut": "1000"
                    }"""
                )
                base.log.debug("Toastr successfully loaded.")
                self.available = True
                return self.available
            except WebDriverException as e:
                base.log.info(f"[WebDriverException] {e}. {attempts} attempts remaining.")
                time.sleep(1)
                attempts -= 1
        base.log.debug("Toastr/Jquery2.0+ failed to load successfully.")
        self.available = False
        return self.available

    @staticmethod
    def is_setup():
        """Returns True/False if toastr is setup"""

        js_check = base.page.evaluate("return window.toastrFlag")

        from luna.driver.ui.wuiexception import WUIJavascriptException

        try:
            css_check = base.page.evaluate(
                "document.querySelectorAll(\"head>link[href*='toastr.min.css'\")"
            )
        except JavascriptException as e:
            raise WUIJavascriptException(e.msg)

        if js_check and css_check:
            return True
        else:
            return False
