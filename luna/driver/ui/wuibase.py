import configparser
import errno
import logging
import os
import re
import sys

import simplejson as json
from flaky import flaky

from luna.config import config
from luna.driver.ui import base
from luna.driver.ui import flaky_filters

# from luna.driver.ui.interactions import Interactions
# from luna.driver.ui.waits import Waits
from luna.driver.ui.wuiassertionerror import WUIAssertionError
from luna.framework.common.testcase import TestCase
from luna.driver.ui.wuiexception import WUIException

# from selenium.common.exceptions import WebDriverException
# from selenium.webdriver.remote.webelement import WebElement


@flaky(rerun_filter=flaky_filters.browser_fails_to_launch)
# class WUIBase(TestCase, Waits, Interactions):
class WUIBase(TestCase):
    """Base Web UI Framework (WUI) Class

    This class is a grand child of unittest.TestCase, and provides all the asserts that
    unittest provides, apart from custom asserts that are necessary for front end automation.

    Usage:
        Import this package and classname in a test file, and define test methods.
        Example:
            from luna.driver.webdriver.wuibase import WUIBase

            class RuleTests(WUIBase):

                def setUp(self):
                    # To define a Test Class level setup method
                    super(RuleTests, self).setUp()

                def test_greater_than(self):
                    a = 3
                    b = 2
                    self.assertTrue(a>b, 'a:%s is greater than b:%s' % (a, b))

                def tearDown(self):
                    # To define a Test Class level teardown method
                    WUIBase.tearDown(self)

    Note:
        Test file/module name must end with '_test' while test method names must start with 'test_'
        to be picked by nose, which is the harness for running WUI tests.
    """

    _multiprocess_can_split_ = True
    ASSERT_TIMEOUT = 1
    ASSERT_POLL_FREQUENCY = 0.5
    SAVE_SCREENSHOT_ON_ERROR = True

    def setUp(self):
        """Special unittest method, that behaves like __init__ (a constructor).

        This method is invoked at the start of each test.
        Sets up the base test class WUIBase that all test classes must inherit from,
        and does the following for each test:
            (1) Generates test specific details like test module name, test name etc,
                and stores it in the shared singleton 'base' instance as/key-of an attribute.
            (2) Initializes WUIElement which allows the inheriting test class instances to
                make custom selenium/playwright webdriver API calls on itself.
            (3) Sets up the logger.
            (4) Launches a selenium/playwright driver
        """
        super().setUp()
        self.base = base

        # Store the test specific details in the shared singleton 'base'
        self.base.test["id"] = self.test_id
        self.base.test["component"] = self.test_component
        self.base.test["dir"] = self.test_dir
        self.base.test["module_name"] = self.test_module_name
        self.base.test["class_name"] = self.test_class_name
        self.base.test["case_name"] = self.test_case_name

        # Knowngoods
        self.base.test["module_kg_dir"] = self.test_module_kg_dir
        self.base.test["class_kg_dir"] = self.test_class_kg_dir
        self.base.test["case_kg_dir"] = self.test_case_kg_dir

        self._kg_file = os.path.join(
            self.base.test["case_kg_dir"], (self.base.test["case_name"] + ".txt")
        )
        self._kg_section = self.base.test["case_name"]
        self._kg_loaded = False

        self.base.test["kg_file"] = self._kg_file
        self.base.test["kg_section"] = self._kg_section
        self.base.test["kg_loaded"] = self._kg_loaded

        # Test Data
        self.base.test["td_dir"] = self.test_td_dir
        self.base.test["module_td_dir"] = self.test_module_td_dir
        self.base.test["class_td_dir"] = self.test_class_td_dir
        self.base.test["case_td_dir"] = self.test_case_td_dir

        # Test Output
        self.base.test["o_dir"] = self.test_o_dir
        self.base.test["module_o_dir"] = self.test_module_o_dir
        self.base.test["class_o_dir"] = self.test_class_o_dir
        self.base.test["case_o_dir"] = self.test_case_o_dir

        self._test_o_file = os.path.join(
            self.base.test["case_o_dir"], (self.base.test["case_name"] + ".out")
        )

        self.base.test["o_file"] = self._test_o_file

        # Set up the logger
        fh = logging.FileHandler(self.base.test["o_file"], mode="w")
        formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        fh.setFormatter(formatter)
        self.base.log.addHandler(fh)
        self.base.log.name = "WUI:" + self.base.test["module_name"]
        self.addCleanup(self._LogCleanUp)

        # Launch the selenium/playwright driver and initialize WUIElement
        self.driver, self.context, self.page = self.base.setup(config)

        # self.addCleanup(self._DriverCleanUp)
        # Waits.__init__(self)
        # Interactions.__init__(self)

    def tearDown(self):
        """Special unittest method, that behaves like __del__ (a destructor).

        This method is invoked at the end of each test to create knowngoods and sections,
        if the update_knowngood flag is set in the config.

        Note: This method is only called if the test setup succeeds irrespective of the
        outcome of the test method.
        """
        super().tearDown()
        if config["artifacts"]["screenshots"]["enabled"]:
            self.page.screenshot(
                path=f"{config['artifacts']['screenshots']['path']}/{self.base.test['case_name']}.png"
            )
        ws = [item for item in sys.argv if item.startswith("--ws")]
        if (not ws) and (config["artifacts"]["videos"]["enabled"]):
            self.page.close()
            self.page.video.save_as(
                f"{config['artifacts']['videos']['path']}/{self.base.test['case_name']}.webm"
            )
        self.context.close()
        self.driver.close()
        # Create the knowngood files and sections
        if self.base.test["kg_loaded"] and config["update_knowngood"]:
            try:
                os.makedirs(self.base.test["case_kg_dir"])
            except OSError as exception:
                if exception.errno != errno.EEXIST:
                    raise
            with open(self.base.test["kg_file"], "w+") as file_obj:
                self._knowngoods.write(file_obj)

    @property
    def failureException(self):
        """Calls custom assertion error class on test failure/assertion error"""
        WUIAssertionError.__name__ = AssertionError.__name__
        return WUIAssertionError

    def _LogCleanUp(self):
        """Cleans up the logger at the end of each test.

        Made part of each test flow by being included in WUIBase's setup with addCleanup,
        to flush out the file handle of the logger, to prevent log messages to be recorded in
        .out files of other tests.
        """
        if self.base.log.handlers:
            try:
                file_handler = self.base.log.handlers[1]
                file_handler.flush()
                self.base.log.removeHandler(file_handler)
            except IndexError as e:
                raise IndexError("Logger does not have a file.\n%s" % e)

    def _DriverCleanUp(self):
        """Cleans up the Selenium Webdriver browser instance at the end of each test.

        Made part of each test flow by being included in WUIBase's setup with addCleanup,
        to ensure the browser is always quit, even if an exception is raised in a test setup.
        """
        if self._outcome.errors and self.SAVE_SCREENSHOT_ON_ERROR:
            try:
                url = self.driver.current_url
                url = re.sub(r"https?://", "", url)
                url = url.replace("/", "_")
                filename = f"{url}_fail.png"
                self.screenshot_path = os.path.join(self.test_case_o_dir, filename)
                self.base.log.debug(f"Saving a screenshot to {self.screenshot_path}")
                # get the height of the body element,
                # which is usually the largest element on the page and add some padding
                padding = 250
                screenshot_height = (
                    self.driver.find_element_by_css_selector("body").size["height"] + padding
                )
                # use the current window width
                screenshot_width = self.driver.get_window_size()["width"]
                self.base.log.debug(
                    f"setting window size for screenshot: ({screenshot_width}, {screenshot_height})"
                )
                self.driver.set_window_size(width=screenshot_width, height=screenshot_height)
                self.driver.save_screenshot(self.screenshot_path)
            except WUIException as e:
                self.base.log.debug(f"Error trying to save screenshot:\n{e}")
        self.base.log.debug("Quitting Selenium WebDriver")
        try:
            self.driver.quit()
        except AttributeError as e:
            self.base.log.critical("Unable to quit Selenium WebDriver.")
            raise AttributeError("Unable to quit Selenium WebDriver.\n%s" % e)
        except OSError as e:
            self.base.log.critical("Selenium WebDriver has already quit.")
            raise OSError("Selenium WebDriver has already quit.\n%s" % e)

    def ReadTestData(self, file_name, td_dir=None):
        """Returns testdata from file

        If no value is passed to the testdata_dir argument, all three folders will be searched
        in the following order:
        (1) base.test.case_td_dir (test method specific testdata)
        (2) base.test.class_td_dir (test class specific testdata)
        (3) base.test.module_td_dir (test module specific testdata)
        failing which, an IOError will be raised.

        Args:
            file_name: name of the file (str)
            td_dir: path to the file (str)

        Returns:
            Dict if file_name has a .json extension
            String if file_name does not have a .json extension

        Raises:
            IOError exception if testdata file_name is not found.
        """
        self.log.info("Searching testdata file: %s" % file_name)
        fh = None
        if td_dir is None:
            td_dir = self.base.test.case_td_dir
            try:
                fh = open(os.path.join(td_dir, file_name))
            except OSError as e:
                self.log.debug(
                    "Testdata file: %s not found in location: %s, exception: %s"
                    % (file_name, td_dir, e)
                )

            if not fh:
                td_dir = self.base.test.class_td_dir
                try:
                    fh = open(os.path.join(td_dir, file_name))
                except OSError as e:
                    self.log.debug(
                        "Testdata file: %s not found in location: %s, exception: %s"
                        % (file_name, td_dir, e)
                    )
                if not fh:
                    td_dir = self.base.test.module_td_dir
                    try:
                        fh = open(os.path.join(td_dir, file_name))
                    except OSError as e:
                        self.log.critical(
                            f"Testdata file: {file_name} not found in location: {td_dir}"
                        )
                        raise OSError(e)
        else:
            try:
                fh = open(os.path.join(td_dir, file_name))
            except OSError as e:
                self.log.critical(
                    f"Testdata file: {file_name} does not exist in location: {td_dir}"
                )
                raise OSError(e)

        self.log.info(f"Reading testdata file: {file_name} from location: {td_dir}")
        test_data = fh.read()
        fh.close()

        if file_name.endswith(".json"):
            test_data = json.loads(test_data)

        return test_data

    def _LoadKnowngoods(self, update_knowngood=False):
        """Loads knowngoods from file or creates new object if file doesn't exist.

        Args:
            update_knowngood: create new knowngoods (boolean)
        """
        self._knowngoods = configparser.RawConfigParser()
        if update_knowngood:
            self._knowngoods.add_section(self.base.test["kg_section"])
        elif os.path.exists(self.base.test["kg_file"]):
            self._knowngoods.read_file(open(self.base.test["kg_file"]))
        else:
            raise Exception("No knowngood file found.")
        self.base.test["kg_loaded"] = True

    def assertEqualKnowngood(self, first, second, msg=""):
        """Asserts first EQUALS second

        Checks string(first) EQUALS string under the defined section name(second)
        in the known good file for the test method.

        Args:
            first: string to compare text under section labeled 'second' (str)
            second: section name in the knowngood file (str)
            msg: assertion failure message (str)
        """
        if not self.base.test["kg_loaded"]:
            self._LoadKnowngoods(config["update_knowngood"])

        if config["update_knowngood"]:
            self._knowngoods.set(self.base.test["kg_section"], second, first)

        # Stripping leading whitespaces as ConfigParser while reading a section value
        # strips of any leading whitespaces
        first = first.encode("utf-8").lstrip().decode()
        second = self._knowngoods.get(self.base.test["kg_section"], second).lstrip()
        if type(second) is bytes:
            second = second.decode("utf-8")

        self.assertEqual(first, second, msg=msg)

    def assertInKnowngood(self, first, second, msg=""):
        """Asserts first IN second

        Checks string(first) IN string under the defined section name(second)
        in the known good file for the test method.

        Args:
            first: string to be found IN text under section labeled 'second' (str)
            second: section name in the knowngood file (str)
            msg: assertion failure message (str)
        """
        if not self.base.test["kg_loaded"]:
            self._LoadKnowngoods(config["update_knowngood"])

        if config["update_knowngood"]:
            self._knowngoods.set(self.base.test["kg_section"], second, first)

        # Stripping leading whitespaces as ConfigParser while reading a section value
        # strips of any leading whitespaces
        first = first.encode("utf-8").lstrip().decode()
        second = self._knowngoods.get(self.base.test["kg_section"], second).lstrip()
        if type(second) is bytes:
            second = second.decode("utf-8")

        self.assertIn(first, second, msg=msg)

    def assertTextPresentInBody(
        self, text, timeout=ASSERT_TIMEOUT, poll_frequency=ASSERT_POLL_FREQUENCY, msg=""
    ):
        """Asserts given text is present in the body.

        Args:
            text: string to find in body (str)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            msg: assertion failure message (str)
        """

        self.assertTrue(
            self.WaitForTextPresentInBody(
                text,
                timeout=timeout,
                poll_frequency=poll_frequency,
                raise_exception=False,
            ),
            msg=msg,
        )

    def assertTitleContains(
        self, text, timeout=ASSERT_TIMEOUT, poll_frequency=ASSERT_POLL_FREQUENCY, msg=""
    ):
        """Asserts title contains the case-sensitive text.

        Args:
            text: text to find in title (str)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            msg: assertion failure message (str)
        """

        self.assertTrue(
            self.WaitForTitleContains(
                text,
                timeout=timeout,
                poll_frequency=poll_frequency,
                raise_exception=False,
            ),
            msg=msg,
        )

    def assertTitleIs(
        self, text, timeout=ASSERT_TIMEOUT, poll_frequency=ASSERT_POLL_FREQUENCY, msg=""
    ):
        """Asserts the title of a page exactly matches the text.

        Args:
            text: title text (str)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            msg: assertion failure message (str)
        """

        self.assertTrue(
            self.WaitForTitleIs(
                text,
                timeout=timeout,
                poll_frequency=poll_frequency,
                raise_exception=False,
            ),
            msg=msg,
        )

    def assertAlertIsPresent(
        self, timeout=ASSERT_TIMEOUT, poll_frequency=ASSERT_POLL_FREQUENCY, msg=""
    ):
        """Asserts alert to be present.

        Args:
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            msg: assertion failure message (str)
        """

        self.assertTrue(
            self.WaitForAlertIsPresent(
                timeout=timeout, poll_frequency=poll_frequency, raise_exception=False
            ),
            msg=msg,
        )

    def assertPresenceOfElement(
        self,
        locator,
        timeout=ASSERT_TIMEOUT,
        poll_frequency=ASSERT_POLL_FREQUENCY,
        msg="",
    ):
        """Asserts element present on DOM of page.

        This does not necessarily mean that the element is visible.

        Args:
            locator: strategy technique and strategy selector (tuple)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            msg: assertion failure message (str)
        """

        self.assertIsInstance(
            self.WaitForPresenceOfElement(
                locator,
                timeout=timeout,
                poll_frequency=poll_frequency,
                raise_exception=False,
            ),
            WebElement,
            msg=msg,
        )

    def assertElementDisplayed(
        self,
        locator,
        timeout=ASSERT_TIMEOUT,
        poll_frequency=ASSERT_POLL_FREQUENCY,
        msg="",
    ):
        """Asserts element displayed on DOM of page.

        Visibility means that the element is not only displayed but
        also has a height and width that is greater than 0.

        Args:
            locator: strategy technique and strategy selector (tuple)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            msg: assertion failure message (str)
        """

        self.assertIsInstance(
            self.WaitForElementDisplayed(
                locator,
                timeout=timeout,
                poll_frequency=poll_frequency,
                raise_exception=False,
            ),
            WebElement,
            msg=msg,
        )

    def assertPresenceOfAllElements(
        self,
        locator,
        timeout=ASSERT_TIMEOUT,
        poll_frequency=ASSERT_POLL_FREQUENCY,
        msg="",
    ):
        """Asserts at least one the elements is present on DOM of page.

        Visibility means that the element is not only displayed but
        also has a height and width that is greater than 0.

        Args:
            locator: strategy technique and strategy selector (tuple)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            msg: assertion failure message (str)
        """

        self.assertIsInstance(
            self.WaitForPresenceOfAllElements(
                locator,
                timeout=timeout,
                poll_frequency=poll_frequency,
                raise_exception=False,
            ),
            list,
            msg=msg,
        )

    def assertTextToBePresentInElement(
        self,
        locator,
        text,
        timeout=ASSERT_TIMEOUT,
        poll_frequency=ASSERT_POLL_FREQUENCY,
        msg="",
    ):
        """Asserts given text is present in the specified element.

        Similar to checking if text in '.text' method called on a webelement

        Args:
            locator: strategy technique and strategy selector (tuple)
            text: string to find in element (str)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            msg: assertion failure message (str)
        """

        self.assertTrue(
            self.WaitForTextToBePresentInElement(
                locator,
                text,
                timeout=timeout,
                poll_frequency=poll_frequency,
                raise_exception=False,
            ),
            msg=msg,
        )

    def assertTextToBePresentInElementValue(
        self,
        locator,
        text,
        timeout=ASSERT_TIMEOUT,
        poll_frequency=ASSERT_POLL_FREQUENCY,
        msg="",
    ):
        """Asserts given text is present in element's value attribute

        Similar to checking if text matches string in the 'value' attribute of the webelement

        Args:
            locator: strategy technique and strategy selector (tuple)
            text: string to find in element's val (str)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            msg: assertion failure message (str)
        """

        self.assertTrue(
            self.WaitForTextToBePresentInElementValue(
                locator,
                text,
                timeout=timeout,
                poll_frequency=poll_frequency,
                raise_exception=False,
            ),
            msg=msg,
        )

    def assertInvisibilityOfElement(
        self,
        locator,
        timeout=ASSERT_TIMEOUT,
        poll_frequency=ASSERT_POLL_FREQUENCY,
        msg="",
    ):
        """Asserts element is neither visible or present on the DOM

        Args:
            locator: strategy technique and strategy selector (tuple)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            msg: assertion failure message (str)
        """

        self.assertTrue(
            self.WaitForInvisibilityOfElement(
                locator,
                timeout=timeout,
                poll_frequency=poll_frequency,
                raise_exception=False,
            ),
            msg=msg,
        )

    def assertElementToBeClickable(
        self,
        locator,
        timeout=ASSERT_TIMEOUT,
        poll_frequency=ASSERT_POLL_FREQUENCY,
        msg="",
    ):
        """Asserts the element is clickable.

        Args:
            locator: strategy technique and strategy selector (tuple)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            msg: assertion failure message (str)
        """

        self.assertIsInstance(
            self.WaitForElementToBeClickable(
                locator,
                timeout=timeout,
                poll_frequency=poll_frequency,
                raise_exception=False,
            ),
            WebElement,
            msg=msg,
        )

    def assertStalenessOfWebElement(
        self,
        element,
        timeout=ASSERT_TIMEOUT,
        poll_frequency=ASSERT_POLL_FREQUENCY,
        msg="",
    ):
        """Asserts element is no longer attached to the DOM.

        Args:
            element: Element to check (webdriver object)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            msg: assertion failure message (str)
        """

        self.assertTrue(
            self.WaitForStalenessOfWebElement(
                element,
                timeout=timeout,
                poll_frequency=poll_frequency,
                raise_exception=False,
            ),
            msg=msg,
        )

    def assertWebElementToBeSelected(
        self,
        element,
        timeout=ASSERT_TIMEOUT,
        poll_frequency=ASSERT_POLL_FREQUENCY,
        msg="",
    ):
        """Asserts the selection element is selected.

        Args:
            element: Element to check (webdriver object)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            msg: assertion failure message (str)
        """

        self.assertTrue(
            self.WaitForWebElementToBeSelected(
                element,
                timeout=timeout,
                poll_frequency=poll_frequency,
                raise_exception=False,
            ),
            msg=msg,
        )

    def assertElementToBeSelected(
        self,
        locator,
        timeout=ASSERT_TIMEOUT,
        poll_frequency=ASSERT_POLL_FREQUENCY,
        msg="",
    ):
        """Asserts element to be located is selected.

        Args:
            locator: strategy technique and strategy selector (tuple)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            msg: assertion failure message (str)
        """

        self.assertTrue(
            self.WaitForElementToBeSelected(
                locator,
                timeout=timeout,
                poll_frequency=poll_frequency,
                raise_exception=False,
            ),
            msg=msg,
        )

    def assertElementToBeNotSelected(
        self,
        locator,
        timeout=ASSERT_TIMEOUT,
        poll_frequency=ASSERT_POLL_FREQUENCY,
        msg="",
    ):
        """Asserts element to be located is not selected.

        Args:
            locator: strategy technique and strategy selector (tuple)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            msg: assertion failure message (str)
        """

        self.assertTrue(
            self.WaitForElementToBeNotSelected(
                locator,
                timeout=timeout,
                poll_frequency=poll_frequency,
                raise_exception=False,
            ),
            msg=msg,
        )

    def assertWebElementDisplayed(
        self,
        element,
        timeout=ASSERT_TIMEOUT,
        poll_frequency=ASSERT_POLL_FREQUENCY,
        msg="",
    ):
        """Asserts element is visible.

        Visibility means that the element is not only displayed but
        also has a height and width that is greater than 0

        Args:
            element: Element to check (webdriver object)
            timeout: time to wait (int)
            poll_frequency: sleep interval between calls (int)
            msg: assertion failure message (str)
        """

        self.assertIsInstance(
            self.WaitForWebElementDisplayed(
                element,
                timeout=timeout,
                poll_frequency=poll_frequency,
                raise_exception=False,
            ),
            WebElement,
            msg=msg,
        )
