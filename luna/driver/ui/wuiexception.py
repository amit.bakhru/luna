from selenium.common.exceptions import JavascriptException

from luna.driver.ui import base
from luna.driver.ui.toastr import Toastr


class WUIException(Exception):
    """WUI exception class takes an error message to get initialized

    Calling a WUIException will take a screen capture of the current browser screen, and store it in
    the 'screenshots' folder with a name that includes the full test id along with a count of the
    number of times the WUIException was called in the life of the test case up to that point.

    It also makes a log entry of level critical using the error message used while calling
    WUIException. The log entry has a tag of [WUIException], for the purpose of
    easy filtering.

    WUIException can be directly raised as an exception as well.

    Usage:
        raise WUIException("Element is not displayed in DOM")
    """

    # Class variable to keep count of the number of times the WUIException is raised in
    # the life of a test case.
    __counter = 0

    def __init__(self, error_message):
        super().__init__()
        WUIException.__counter += 1
        base.log.critical("[WUIException] %s", error_message)

        toastr = Toastr()
        js_sanitized_error_message = error_message.replace('"', '\\"')
        # toastr.setup_and_growl(js_sanitized_error_message, "WUIException")
        # time.sleep(1)
        # Add extra capture in case the test is launched in a directory other than test/frontend
        # base.driver.save_screenshot(f"{base.test.id}_{WUIException.__counter}.png")
        # base.driver.save_screenshot(f"screenshots/{base.test.id}_{WUIException.__counter}.png")
        self.error_message = error_message

    def __str__(self):
        return self.error_message


class WUIJavascriptException(JavascriptException):
    """
    This is a wrapper around selenium.common.exceptions.JavascriptException
    """
