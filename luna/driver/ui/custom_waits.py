from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.support import expected_conditions as EC


class CSSPropertyIs:
    """Class to handle comparison of a webelement's css property"""

    def __init__(self, locator, css_property, expected_value):
        """Class initializer

        Args:
            locator: locator-strategy & method of finding element (tuple)
            css_property: name of CSS property to check (str)
            expected_value: value of property to determine if True
        """
        self.locator = locator
        self.css_property = css_property
        self.expected_value = expected_value

    def __call__(self, driver):
        """Call method that does the comparison"""
        try:
            actual_value = EC._find_element(driver, self.locator).value_of_css_property(
                self.css_property
            )
            # any invalid css_property will yield an empty string
            return actual_value == self.expected_value
        except StaleElementReferenceException:
            return False


class text_to_equal_in_element:
    """
    An expectation for checking if the given text is equal to the specified element's text
    """

    def __init__(self, locator, text_):
        self.locator = locator
        self.text = text_

    def __call__(self, driver):
        try:
            return self.text == EC._find_element(driver, self.locator).text
        except StaleElementReferenceException:
            return False
