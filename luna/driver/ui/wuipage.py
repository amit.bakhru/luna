import time

from luna.driver.ui import base
from luna.driver.ui.section import Section
from luna.config import config as master_config
from luna.driver.ui.wuiexception import WUIException
from luna.framework.common.logger import LOGGER


class WUIPage(Section):
    is_popups_handled = False

    def __init__(self, config, launch: bool = False, runvalidations: bool = True, *args, **kwargs):
        """Page wui class that all page objects inherit from.

        Page level interaction methods reside in this class. On initialization
        all elements defined in their page config are set as attributes that
        are objects of the Element class.

        Args:
            config: configuration (dict)
            launch: open the start url and login (bool)
            runvalidations: run page load validations (bool)
            td_http: Flag to use http or https (bool)

        Raises:
            WUIException: Raises WUIException if url containing '#' fails to load
            after trying for 5 times
        """
        super().__init__(config, runvalidations=False, *args, **kwargs)
        self.page = base.page
        self.page_config = config
        self.master_config = master_config
        self.url = f"{master_config["servers"]["soar"]["url"]}{self.page_config.startUrl}"
        LOGGER.debug(f"Start url = {self.page_config.startUrl}")

        if launch:
            LOGGER.debug(f"Login URL = {self.url}")
            self.page.goto(self.url)

        counter = 0
        if "#" in self.url and launch:
            curr_url = ""
            while self.page.url != self.url and counter < 5:
                self.page.goto(self.url)
                curr_url = self.page.url
                counter += 1
            if self.page.url != self.url:
                raise WUIException(f"Unable to launch page with url {self.url}")
        if runvalidations:
            self.RunValidations()

    @staticmethod
    def HandlePopUps():
        """Handles different popups"""
        WUIPage.is_popups_handled = True

    def InjectCookie(self):
        """Adds auth cookie"""
        expiration_date = int(time.time()) + 100
        cookie = {
            "domain": master_config["host"],
            "name": "DevoCookie",
            "value": "true",
            "expiry": expiration_date,
            "path": "/",
            "secure": False,
        }
        # self.driver.add_cookie(cookie)
        self.context.cookies(cookie)

    def VisitSection(self, section_name):
        """This method visits a particular section based on the name passed in

        Args:
            section_name: Name of the section to visit(str)

        Raises:
            WUIException if button containing text is not found
        """
        # if not self.IsButtonWithTextPresent(section_name):
        # raise WUIException("Button with %s not found" % section_name)
        # self.ClickButtonWithText(section_name)
        pass
