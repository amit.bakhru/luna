import time

from luna.driver.ui import base
from luna.driver.ui.toastr import Toastr


class WUIAssertionError(AssertionError):
    """Custom WUI Assertion class to take a screen capture on an assertion error"""

    def __init__(self, *args, **kwargs):
        failure_message = self.args[0]
        base.log.critical("[WUIAssertionError] %s", failure_message)

        toastr = Toastr()
        js_sanitized_failure_message = (
            failure_message.split("\n")[0].replace('"', '\\"') if failure_message else ""
        )
        toastr.setup_and_growl(js_sanitized_failure_message, "WUIAssertionError")
        time.sleep(1)
        base.driver.save_screenshot("screenshots/%s.png" % base.test.id)
        super().__init__(*args, **kwargs)
