# Constants used in page.py
LICENSE_COMPLIANCE_ACCEPT_BTN = (
    "css",
    'button[id="accept-eula-button-btnEl"]',
)
NEW_FEATURES_ENABLED_ACCEPT_BTN = (
    "css",
    'button[id="new-features-ok-button-btnEl"]',
)
CONTEXTUAL_HELP_ELM = ("css", "button[title=Help]")
CONTEXTHUB_POPUP_DONOTSHOW_CBOX = ("css", 'input[id^="checkboxfield-"]')
CONTEXTHUB_POPUP_CLOSE_BTN = (
    "css",
    'div[class*="x-toolbar-footer-docked-bottom"] span[id^="button"]',
)

# Constants used in interactions.py
DEFAULT_DRP_DWN_OPT = ("css", ".x-boundlist-item")
BUTTON = ("css", "button span")
PARENT_BUTTON = ("css", "button")

# Constants used by section.py
DEFAULT_ACTIVE_WINDOW_HEADER = (
    "css",
    "div[id^=ext-comp-].x-window .x-window-header-text",
)
DEFAULT_ACTIVE_WINDOW_BUTTONS = (
    "css",
    "div[id^=ext-comp-].x-window .x-btn-center",
)
DEFAULT_ERROR_MSG = (
    "css",
    "div[id^=ext-comp-].x-window .x-form-invalid-under",
)
DEFAULT_TABLE_HEADER = ("css", "div[id^=ext-comp-].x-window .x-grid-header-ct")
