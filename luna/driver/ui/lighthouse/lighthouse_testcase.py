import os
import subprocess

from luna.driver.ui.wuibase import WUIBase


class LighthouseTestCase(WUIBase):
    LIGHTHOUSE_NOT_INSTALLED_FAILURE = (
        "Lighthouse is not installed. "
        "It must installed at the root directory of your virtual environment."
    )
    SAVE_SCREENSHOT_ON_ERROR = False

    def setUp(self):
        super().setUp()
        self.debugger_port = self.driver.capabilities["goog:chromeOptions"][
            "debuggerAddress"
        ].split(":")[1]
        self.env = {
            **{"CHROME_DEBUGGING_PORT": self.debugger_port},
            **os.environ.copy(),
        }

    def run_lighthouse(
        self,
        url,
        report_name,
        categories=None,
        emulated_form_factor="desktop",
        throttline_rttMs=150,
        throttling_throughputKbps=12000,
        throttling_requestLatencyMs=50,
        throttling_downloadThroughputKbps=12000,
        throttling_uploadThroughputKbps=6000,
        throttling_cpuSlowdownMultiplier=1,
    ):
        output_file = os.path.join(self.test_case_o_dir, f"lighthouse_report_{report_name}.html")
        lighthouse_cmd = [
            "lighthouse",
            url,
            "--port",
            f"{self.debugger_port}",
            "--output",
            "html",
            "--output-path",
            output_file,
            "--emulated-form-factor",
            emulated_form_factor,
            "--throttling.rttMs",
            str(throttline_rttMs),
            "--throttling.requestLatencyMs",
            str(throttling_requestLatencyMs),
            "--throttling.throughputKbps",
            str(throttling_throughputKbps),
            "--throttling.downloadThroughputKbps",
            str(throttling_downloadThroughputKbps),
            "--throttling.uploadThroughputKbps",
            str(throttling_uploadThroughputKbps),
            "--throttling.cpuSlowdownMultiplier",
            str(throttling_cpuSlowdownMultiplier),
        ]
        if categories is not None:
            lighthouse_cmd.append("--only-categories")
            if isinstance(categories, list):
                lighthouse_cmd.append(",".join(categories))
            elif isinstance(categories, str):
                lighthouse_cmd.append(categories)
            else:
                raise ValueError(
                    f'"categories arg must be "list" or "str". "{type(categories)}" was given'
                )
        self.log.info(f"Running comamnd:\n{' '.join(lighthouse_cmd)}")
        output = subprocess.run(
            lighthouse_cmd,
            env=self.env,
            stderr=subprocess.STDOUT,
            stdout=subprocess.PIPE,
        ).stdout.decode("utf-8")
        with open(
            os.path.join(self.test_case_o_dir, f"lighthouse_output_{report_name}.txt"),
            "w",
            encoding="utf-8",
        ) as f:
            f.write(output)
        self.assertTrue(
            os.path.isfile(output_file),
            "The report file does not exist. Check the "
            "lighthouse output file for more information.",
        )
        return output
