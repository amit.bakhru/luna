# Lighthouse -- Web App Quality Testing
Lighthouse is an open-source, automated tool for improving the quality of web pages. You can run it against any web page, public or requiring authentication. It has audits for performance, accessibility, progressive web apps, SEO and more.

https://developers.google.com/web/tools/lighthouse

## Use Case
Lighthouse does not have any python bindings, thus in order to use luna UI module and re-use your page objects, specifically to handle authentication, this harness was created.

### Installing Lighthouse
You must have node installed.
https://nodejs.org/en/download/

Lighthouse can be used with the default package manager, npm or yarn.


`npm install -g lighthouse`

`yarn global add lighthouse`


### Using the test case

Extend the LighthouseTestCase and utilize the `setUp` method to authenticate into your test case. The luna config object contains keys for the url and names of reports `config['driver']['ui']['lighthouse']'` is a list of objects that will contain a `url`  key and a `report_name` key. An HTML report will be created in luna's output folder.


```
   from luna.config import config
   from luna.driver.ui.lighthouse.lighthouse_testcase import LighthouseTestCase
   from luna.framework.common.logger import LOGGER
   from parameterized import parameterized

   from luna_m.page_objects.login import Login

   config['driver']['ui']['lighthouse'] = [
       {'url': 'app.com', 'report_name': 'home'},
       {'url': 'app.com/settings', 'report_name': 'settings'}
       ]


   class PerformanceTestCase(LighthouseTestCase):
       def setUp(self):
           super().setUp()
           login_page = Login(launch=True)
           login_page.login(
               config['common']['test_users'][0]['username'],
               config['common']['test_users'][0]['password'],
               config['common']['test_users'][0]['otp_secret'],
           )

       @parameterized.expand(
           [(entry['url'], entry['report_name']) for entry in config['driver']['ui']['lighthouse']]
       )
       def test_lighthouse(self, endpoint, report_name):
           LOGGER.info(f'\nrunning lighthouse\nurl: {url}\nreport_name: {report_name}\n\n')
           output = self.run_lighthouse(url, report_name)
           LOGGER.info(output)
```
