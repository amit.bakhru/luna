from selenium.common.exceptions import NoSuchElementException

from luna.driver.ui import base


class WUIElement:
    """Standalone class to convert objects to selenium webdriver objects

    Creating a new object of this class returns one of the three objects:
        (1) Selenium recognized tuple of strategy technique and strategy selector (By, "selector")
        (2) Selenium webdriver object
        (3) List of webdriver objects

    Usage:
        (1) WUIElement(("CSS_SELECTOR", "body"), find=False)
        (2) WUIElement(("CSS_SELECTOR", "header > div"))
        (3) WUIElement(("CSS_SELECTOR", "table tr"), multiple=True)
    """

    def __new__(self, locator=None, multiple=False, find=True):
        """Returns the desired object on class initialization

        Args:
            locator: strategy technique and strategy selector (tuple)
            multiple: decision to find one or all web elements (bool)
            find: decision to find the webelement(s) (bool)

        Returns:
            (1) Selenium recognized tuple of strategy technique and strategy
                selector (By, "selector")
            (2) Selenium webdriver object
            (3) List of webdriver objects

        Raises:
            NoSuchElementException if webelement is not found
        """
        by_class = locator[0]
        selector = locator[1]
        try:
            by_class_string = "By." + by_class
            by_class_object = eval(by_class_string)
            if find:
                if multiple:
                    return base.driver.find_elements(by_class_object, "%s" % selector)
                else:
                    return base.driver.find_element(by_class_object, "%s" % selector)
            else:
                selenium_webdriver_locator = (by_class_object, f"{selector}")
                if type(selenium_webdriver_locator) is tuple:
                    return selenium_webdriver_locator
                else:
                    base.log.critical(
                        "Unable to convert {} to a Selenium locator tuple, got this"
                        " instead {}".format(locator, selenium_webdriver_locator)
                    )
                    raise Exception(
                        "Unable to convert {} to a Selenium locator tuple, got this"
                        " instead {}".format(locator, selenium_webdriver_locator)
                    )
        except NoSuchElementException as e:
            raise Exception(str(e))


class PlaywrightElement:
    def __new__(self, locator=None, multiple=False, find=True):
        """
        page.get_by_role() to locate by explicit and implicit accessibility attributes.
        page.get_by_text() to locate by text content.
        page.get_by_label() to locate a form control by associated label's text.
        page.get_by_placeholder() to locate an input by placeholder.
        page.get_by_alt_text() to locate an element, usually image, by its text alternative.
        page.get_by_title() to locate an element by its title attribute.
        page.locator('css=button').click();
        page.locator('xpath=//button').click();
        """
        by_class = locator[0]
        selector = locator[1]
        if len(locator) == 3:
            value = locator[-1]
        try:
            match by_class:
                case "css":
                    return base.page.locator(f"css={selector}")
                case "title":
                    return base.page.get_by_title(selector)
                case "text":
                    return base.page.get_by_text(selector)
                case "role":
                    # Role locators include buttons, checkboxes, headings, links, lists, tables, and many more
                    # page.get_by_role("button", name=re.compile("submit", re.IGNORECASE)).click()
                    return base.page.get_by_role(role=selector, name=value)
                case "label":
                    return base.page.get_by_label(selector)
                case "xpath":
                    return base.page.locator(f"xpath={selector}")
                case "placeholder":
                    return base.page.get_by_placeholder(selector)
                case "test_id":
                    return base.page.get_by_test_id(selector)
                case "locator":
                    return base.page.locator(selector)
        except NoSuchElementException as e:
            raise Exception(str(e))
