from luna.driver.ui.waits import Waits
from luna.driver.ui.wuielement import PlaywrightElement


# class Element(Waits, Interactions):
class Element(Waits):
    """Element wui class that wui level page and section classes inherit from."""

    def __init__(self, locator=None):
        super().__init__()
        self.__locator = locator

    def __get__(self, obj, cls=None):
        return self.locator

    def __delete__(self, obj):
        pass

    def __call__(self, multiple=False):
        """Returns either a selenium webdriver object or list of selenium webdriver objects

        Args:
            multiple: decision to find one or all webelements (bool)
        """
        # return WUIElement(self.locator, multiple=multiple)
        return PlaywrightElement(self.locator, multiple=multiple, find=True)

    def __str__(self):
        """Returns the locator tuple as the string representation"""
        return f"{self.locator}"

    def __repr__(self):
        """Returns the locator tuple as the object representation"""
        return f"{self.locator}"

    def __getitem__(self, index):
        """Returns the value of the index of the object

        Args:
            index: position of the index (int)
        """
        return self.locator[index]

    def __setitem__(self, index, value):
        """Sets the value to the index of the object

        Args:
            index: position of the index (int)
            value: text to be assigned (str)
        """
        self.locator[index] = value

    @property
    def locator(self):
        return self.__locator
