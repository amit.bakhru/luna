import asyncio
import json
from pathlib import Path

from aiokafka import AIOKafkaProducer

from luna.framework.common.logger import LOGGER


class KafkaProducer:
    def __init__(self, boot_server):
        LOGGER.debug(f"Initializing AIOKafkaProducer: bootstrap-server={boot_server}")
        # ssl_context = ssl.create_default_context()
        # self.producer = AIOKafkaProducer(
        #     loop=asyncio.get_event_loop(),
        #     bootstrap_servers=boot_server,
        #     sasl_mechanism=sasl_mechanism,
        #     sasl_plain_username=sasl_plain_username,
        #     sasl_plain_password=sasl_plain_password,
        #     security_protocol="SASL_SSL",
        #     request_timeout_ms=45000,
        #     ssl_context=ssl_context,
        # )
        self.bootstrap_servers = boot_server

    async def publish_msg(self, msg, topic, _headers=None):
        """get cluster layout and initial topic/partition leadership information"""
        LOGGER.debug(f"[kafka] Sending message with value: {msg}")
        value_json = json.dumps(msg).encode("utf-8")
        headers = [
            ("content-type", b"application/json"),
        ]
        if _headers:
            headers.append(_headers)
        await self.producer.send(topic=topic, value=value_json, headers=headers)

    async def send(self, msg, topic, _headers=None):
        producer = AIOKafkaProducer(bootstrap_servers=self.bootstrap_servers)
        await producer.start()
        try:
            LOGGER.debug(f"[kafka] Sending message with value: {msg}")
            value_json = json.dumps(msg).encode("utf-8")
            headers = [("content-type", b"application/json")]
            if _headers:
                headers.append(_headers)
            await producer.send_and_wait(topic=topic, value=value_json, headers=_headers)
        finally:
            await producer.stop()


if __name__ == "__main__":
    json_data = (
        Path(
            "/Users/amit.bakhru/src/ueba/ueba-backend/luna/"
            "testdata/test_auth_dynamic_failed_logins_internal.py/"
            "auth_failed_malware.txt"
        )
        .read_text()
        .rsplit("\n", 1)
    )
    p = KafkaProducer(boot_server="ueba-kafka.infra.svc.cluster.local")
    for msg in json_data:
        p.send(msg=msg, topic="luna_proxy_all_access")

    loop = asyncio.get_event_loop()
