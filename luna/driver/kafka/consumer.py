#!/usr/bin/env python

import asyncio
import json
import ssl
import uuid

from aiokafka import AIOKafkaConsumer
from rich.pretty import pretty_repr

from luna.framework.common import LOGGER


class KafkaConsumer:
    def __init__(
        self,
        boot_server,
        topic,
        sasl_mechanism,
        sasl_plain_username,
        sasl_plain_password,
        loop=None,
    ):
        self.group_id = str(uuid.uuid4())
        ssl_context = ssl.create_default_context()
        LOGGER.debug(
            f"Initializing AIOKafkaConsumer: topic={topic}, "
            f"group_id={self.group_id} bootstrap-server={boot_server}"
        )
        self.consumer = AIOKafkaConsumer(
            topic,
            bootstrap_servers=boot_server,
            loop=asyncio.get_event_loop(),
            group_id=self.group_id,
            enable_auto_commit=False,
            auto_offset_reset="latest",
            request_timeout_ms=45000,
            sasl_mechanism=sasl_mechanism,
            sasl_plain_username=sasl_plain_username,
            sasl_plain_password=sasl_plain_password,
            security_protocol="SASL_SSL",
            ssl_context=ssl_context,
        )

    async def consume(self):
        await self.consumer.start()
        try:
            async for msg in self.consumer:
                await self.kafka_consume(msg)
        finally:
            await self.consumer.stop()

    @staticmethod
    async def kafka_consume(msg):
        LOGGER.debug(
            f"[kafka-consume] Consumed: {msg.topic}, {msg.partition}, "
            f"{msg.offset}, {msg.key}, {msg.timestamp} {msg.headers}"
        )
        message = json.loads(msg.value.decode())
        LOGGER.debug(f"[kafka] VALUES:\n {pretty_repr(message, expand_all=True)}")
