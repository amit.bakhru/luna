"""TAXII Client utilities for Devo apps automation."""

from cabby import create_client
from cabby.exceptions import ClientException

from luna.framework.common.exception import TAXIIException
from luna.framework.common.logger import LOGGER


class TAXIIClient:
    """Class for implementing API for connecting to taxii servers"""

    def __init__(
        self,
        host,
        discovery_path,
        use_https=False,
        auth="basic",
        username=None,
        password=None,
        port=None,
        version="1.1",
    ):
        """Initialise the TAXII Client Object

        Args:
            host: Host name of TAXII Server (str)
            discovery_path: path for discovery (str)
            use_https: to use https connection (bool)
            auth: authenticate type (str)
            username: username to access TAXII server (str)
            password: password to access TAXII server (str)
            port: TAXII server listening port (int)
            version: protocol to use (str)

        Raises:
            TAXII Exception when object creation fails
        """

        self.client = None
        self.collection_map = dict()
        self.service_map = dict()
        self.COLLECTION_MANAGEMENT = "COLLECTION_MANAGEMENT"
        try:
            self.host = host
            self.discovery_service = discovery_path
            self.username = username
            self.password = password
            self.port = port
            self.version = version
            self.client = create_client(
                host,
                use_https=use_https,
                port=port,
                discovery_path=discovery_path,
                version=self.version,
            )
            if auth == "basic":
                self.client.set_auth(username=username, password=password)
        except ClientException as e:
            raise TAXIIException(message=f"Failed to create TAXII client object : {e}")

    def discovery(self, uri=None):
        if not uri:
            uri = "http://127.0.0.1:8080/papillon/blockable-indicators/discovery"
        services = self.client.discover_services(uri)
        for service in services:
            LOGGER.debug(f"{service.type}: {service.address}")
            self.service_map[service.type] = service
        LOGGER.debug(f"service map: {self.service_map}")
        return self.service_map

    def get_collections(self, uri=None):
        """Get and store a map for collections"""

        if not uri:
            uri = "http://127.0.0.1:8080/papillon/blockable-indicators/collection-management/"
        collections = self.client.get_collections(uri)
        self.collection_map = {}
        for collection in collections:
            self.collection_map[collection.name] = collection
        LOGGER.debug(f"Collections map: {self.collection_map}")
        return self.collection_map

    def poll(self, collection_name, uri=None):
        """Perform polling operation

        Args:
            collection_name: Name of collection where polling is performed (str)
            uri: uri for the poll call (str)

        Returns:
            content block received from polling (list)
        """
        if not uri:
            uri = "http://127.0.0.1:8080/papillon/blockable-indicators/poll"
        LOGGER.debug("Performing polling operation for collection : %s", collection_name)
        content_blocks = self.client.poll(collection_name=collection_name, uri=uri)
        for block in content_blocks:
            LOGGER.debug(block.content)
        return content_blocks


if __name__ == "__main__":
    p = TAXIIClient(
        host="127.0.0.1",
        port=8080,
        discovery_path="http://127.0.0.1:8080/papillon/blockable-indicators/discovery",
        username="test_149151369129@test.com",
        password="whysosassy",
    )
    p.discovery()
    p.get_collections()
    p.poll(collection_name="a1s.Indicators")
