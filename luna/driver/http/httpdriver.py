"""Driver for http/https based web-servers"""

import requests
import simplejson as json
import urllib3
from rich.pretty import pretty_repr

from luna.driver.http.exception import HttpDriverException
from luna.framework.common.logger import LOGGER

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
requests.packages.urllib3.disable_warnings()


class HttpDriver:
    """An HttpConnection implementation using the popular requests module.

    Making as generic as possible for connecting to al possible http servers using different
    architectures (SOAP, REST..). Instead of using the get, post methods from request module
    itself we are using the Session object to persist the authentication and session data.
    """

    JSON_HEADERS = {"content-type": "application/json"}

    def __init__(
        self,
        host,
        protocol="http",
        port=None,
        auth=None,
        ssl_verify=False,
        http_adapter=None,
        headers=None,
    ):
        """Initialize the connection to an http server

        Args:
            protocol: Protocol to use http/https (str)
            host: a host address/name of the server to connect (str)
            auth: An authentication tuple (username, password) if required or a defined
                  type of authentication (tuple)
            ssl_verify: If the server is on SSL verify certificate or not (bool)
            http_adapter: An adapter for http connection
            headers: common headers for all requests
        """

        # urllib3.disable_warnings()
        self.__protocol = protocol
        self.__host = host
        self.__port = port
        if port:
            self.__base_url = f"{self.__protocol}://{self.__host}:{self.__port}"
        else:
            self.__base_url = f"{self.__protocol}://{self.__host}"
        self.__ssl_verify = ssl_verify
        self.__session = requests.session()
        if auth:
            self.__session.auth = auth
        if http_adapter:
            self.__session.mount(f"{protocol}://", http_adapter)
        self.__session.verify = self.__ssl_verify
        self.__connected = False
        # A default parameter list maintaining for the request if server always need it,
        # For example in some POST calls, if the server always expects 'ctoken' as a parameter we
        #  can get it and save to default_params, so whenever we make a connection these
        # parameters will be automatically send
        self.__default_params = {}
        # When we create the connection object itself will make sure we can connect to the server
        #  this ensures that we won't go further when the server is not available
        # self.__connect__()
        # self.__session.cookies = None
        if headers:
            self.__session.headers.update(headers)

    @property
    def protocol(self):
        return self.__protocol

    @property
    def host(self):
        return self.__host

    @property
    def base_url(self):
        return self.__base_url

    @base_url.setter
    def base_url(self, base_url):
        self.__base_url = base_url

    @property
    def port(self):
        return self.__port

    @port.setter
    def port(self, port):
        self.__port = port

    @property
    def session(self):
        return self.__session

    @property
    def connected(self):
        return self.__connected

    @property
    def default_params(self):
        return self.__default_params

    @property
    def headers(self):
        return self.__session.headers

    @staticmethod
    def __valid_json__(json_var):
        try:
            json.loads(json_var)
        except ValueError as _:
            return False
        return True

    def __connect__(self):
        """Start an HTTP Session to the host, to make sure we can connect to it"""

        try:
            self.Get("", parse_output=False)
            self.__connected = True
        except Exception as e:
            raise HttpDriverException("ConnectionError", self.base_url, str(e))

    def SetDefaultParams(self, params):
        """Set the default params if any

        Args:
            params: Default Parameters to set (dict)
        """

        if params is not None:
            for key, value in params.items():
                self.__default_params[key] = value

    @staticmethod
    def PrettyPrintJson(json_dict):
        """Returns JSON provided in pretty format

        Args:
            json_dict: JSON to convert into pretty format (json)
        """
        return pretty_repr(json_dict, expand_all=True)

    def __merge_default_params__(self, params):
        """Merge the given parameters to DefaultParams (private use for HTTP methods)

        Args:
            params: Parameters to merge with default params (dict)
        """

        if params is not None:
            for key, value in self.__default_params.items():
                if key not in params:
                    params[key] = value
            return params
        else:
            return self.__default_params

    @staticmethod
    def GetJsonResponse(response):
        """verify the response code and return json response

        Args:
            response: json response from requests (obj)

        Returns:
            validated json response (json)

        Raises:
            UnknownResponseError if response doesnt have any JSON (HttpDriverException)
        """

        if response.status_code == 200:
            return response.json()
        else:
            raise HttpDriverException(
                "UnknownResponseError",
                response.url,
                "ERROR RESPONSE: %s" % response.status_code,
            )

    def Get(self, url, params=None, headers=None, timeout=300, parse_output=True):
        """A wrapper method for sending HTTP GET Requests to the Server

        Args:
            url: A Resource Reference Path to send the request (str)
            params: parameters for the request (dict)
            headers: header information for http requests
            timeout: request timeout value in seconds (int)
            parse_output: parse the JSON output for validity (bool)

        Returns:
            response (object)

        Raises:
            GetRequestError if response doesnt have any JSON (HttpDriverException)
        """

        request_url = self.base_url + url
        params = self.__merge_default_params__(params)
        try:
            response = self.session.get(
                request_url, params=params, headers=headers, timeout=timeout
            )
            LOGGER.debug(f"[GET] Request URL: {response.request.url}")
            LOGGER.debug(f"[GET] Request Headers:\n{response.request.headers}")
            LOGGER.debug(f"[GET] Response Status: {response.status_code}")
            conditions = [
                response.content.decode() != "",
                self.__valid_json__(response.text),
                parse_output is True,
            ]
            if all(conditions):
                LOGGER.debug(f"[GET] Response Content:\n{self.PrettyPrintJson(response.json())}")
            return response
        except HttpDriverException as e:
            raise HttpDriverException("[GET] GetRequestError", request_url, str(e))

    def Post(self, url, params=None, data=None, json=None, files=None, headers=None, timeout=300):
        """A wrapper method for sending HTTP POST Requests to the Server

        Args:
            url: A Resource Reference Path to send the request (str)
            params: parameters for the request (dict)
            data: payload for the POST request that will be sent either in form-urlencoded encoding
                  if it is a dict, or without any encoding if it is a str. This is to align with the
                  'data' parameter in the underlying session.post() call. (dict or str)
            json: payload for the POST request, Request will be loading the payload as
                  JSON while sending the call so the format should be in JSON decode-able (dict)
            files: If any file data to be send (obj)
            headers: HTTP Post headers (str)
            timeout: request timeout value in seconds (int)

        Returns:
            response (object)

        Raises:
            PostRequestError if response doesn't have any JSON (HttpDriverException)
        """

        request_url = self.base_url + url
        params = self.__merge_default_params__(params)
        try:
            response = self.session.post(
                request_url,
                params=params,
                data=data,
                json=json,
                files=files,
                headers=headers,
                timeout=timeout,
                cookies=self.session.cookies,
            )
            LOGGER.debug(f"[POST] Request URL: {response.request.url}")
            LOGGER.debug(f"[POST] Request Headers:\n{response.request.headers}")
            if json:
                LOGGER.debug(f"[POST] Request Payload:\n{pretty_repr(json, expand_all=True)}")
            if data:
                LOGGER.debug(f"[POST] Request Payload:\n{pretty_repr(data, expand_all=True)}")
            LOGGER.debug(f"[POST] Response Status: {response.status_code}")
            conditions = [
                response.content.decode() != "",
                self.__valid_json__(response.text),
            ]
            if all(conditions):
                LOGGER.debug(f"[POST] Response Content:\n{self.PrettyPrintJson(response.json())}")
            else:
                LOGGER.debug(f"[POST] Response Content:\n{response.text}")
            return response
        except HttpDriverException as e:
            raise HttpDriverException("PostRequestError", request_url, str(e))

    def Delete(self, url, params=None, data=None, headers=None, timeout=300):
        """A wrapper method for sending HTTP DELETE Requests to the Server

        Args:
            url: A Resource Reference Path to send the request
            params: parameters for the request
            data: payload for the DELETE request, Request will be loading the payload as
                  JSON while sending the call so the format should be in JSON decode-able (dict)
            headers: header information for http requests
            timeout: request timeout value in seconds (int)

        Returns:
            response (object)

        Raises:
            DeleteRequestError if response doesn't have any JSON (HttpDriverException)
        """

        request_url = self.base_url + url
        params = self.__merge_default_params__(params)
        try:
            LOGGER.debug(f"[DELETE] Payload: {data}")
            response = self.session.delete(
                request_url, params=params, json=data, headers=headers, timeout=timeout
            )
            LOGGER.debug(f"[DELETE] Request URL: {response.request.url}")
            LOGGER.debug(f"[DELETE] Request Headers:\n{response.request.headers}")
            LOGGER.debug(f"[DELETE] Response Status: {response.status_code}")
            conditions = [
                response.content.decode() != "",
                self.__valid_json__(response.text),
            ]
            if all(conditions):
                LOGGER.debug(f"[DELETE] Response Content:\n{self.PrettyPrintJson(response.json())}")
            else:
                LOGGER.debug(f"[DELETE] Response Content:\n{response.text}")
            return response
        except HttpDriverException as e:
            raise HttpDriverException("DeleteRequestError", request_url, str(e))

    def Put(self, url, params=None, data=None, json=None, files=None, headers=None, timeout=300):
        """A wrapper method for sending HTTP PUT Requests to the Server

        Args:
            url: A Resource Reference Path to send the request
            params: parameters for the request (Dict)
            data: payload for the PUT request that will be sent either in form-urlencoded encoding
                  if it is a dict, or without any encoding if it is a str. (dict or str)
            json: payload for the PUT request, Request will be loading the payload as
                  JSON while sending the call so the format should be in JSON decode-able (dict)
            timeout: request timeout value in seconds (int)
            files: If any file data to be sent (obj)
            headers: HTTP Post headers (str)

        Returns:
            response (object)

        Raises:
            PutRequestError if response doesnt have any JSON (HttpDriverException)
        """

        request_url = self.base_url + url
        params = self.__merge_default_params__(params)
        try:
            LOGGER.debug(f"[PUT] Payload: {data}")
            response = self.session.put(
                request_url,
                params=params,
                data=data,
                json=json,
                timeout=timeout,
                cookies=self.session.cookies,
                headers=headers,
                files=files,
            )
            LOGGER.debug(f"[PUT] Request URL: {response.request.url}")
            LOGGER.debug(f"[PUT] Request Headers:\n{response.request.headers}")
            LOGGER.debug(f"[PUT] Response Status: {response.status_code}")
            conditions = [
                response.content.decode() != "",
                self.__valid_json__(response.text),
            ]
            if all(conditions):
                LOGGER.debug(f"[PUT] Response Content:\n{self.PrettyPrintJson(response.json())}")
            else:
                LOGGER.debug(f"[PUT] Response Content:\n{response.text}")
            return response
        except HttpDriverException as e:
            raise HttpDriverException("PutRequestError", request_url, str(e))

    def Patch(self, url, params=None, data=None, json=None, files=None, headers=None, timeout=300):
        """A wrapper method for sending HTTP PATCH Requests to the Server

        Args:
            url: A Resource Reference Path to send the request
            params: parameters for the request (Dict)
            data: payload for the PATCH request that will be sent either in form-urlencoded encoding
                  if it is a dict, or without any encoding if it is a str. (dict or str)
            json: payload for the PATCH request, Request will be loading the payload as
                  JSON while sending the call so the format should be in JSON decodable (dict)
            timeout: request timeout value in seconds (int)
            files: If any file data to be send (obj)
            headers: HTTP Post headers (str)

        Returns:
            response (object)

        Raises:
            PutRequestError if response doesnt have any JSON (HttpDriverException)
        """

        request_url = self.base_url + url
        params = self.__merge_default_params__(params)
        try:
            LOGGER.debug(f"[PATCH] Payload: {data}")
            response = self.session.patch(
                request_url,
                params=params,
                data=data,
                json=json,
                timeout=timeout,
                cookies=self.session.cookies,
                headers=headers,
                files=files,
            )
            LOGGER.debug(f"[PATCH] Request URL: {response.request.url}")
            LOGGER.debug(f"[PATCH] Request Headers:\n{response.request.headers}")
            LOGGER.debug(f"[PATCH] Response Status: {response.status_code}")
            conditions = [
                response.content.decode() != "",
                self.__valid_json__(response.text),
            ]
            if all(conditions):
                LOGGER.debug(f"[PATCH] Response Content:\n{self.PrettyPrintJson(response.json())}")
            else:
                LOGGER.debug(f"[PATCH] Response Content:\n{response.text}")
            return response
        except HttpDriverException as e:
            raise HttpDriverException("PatchRequestError", request_url, str(e))

    @staticmethod
    def assertValidArg(default_conf, testcase_conf):
        """Verifies if test case configuration is a subset of default configuration

        Args:
             default_conf: default configuration that will be used for comparison.(str)
             testcase_conf: Configuration that will be used to verify if any of
             the values exists in default configuration.(str)

        Raises:
            KeyError if a key cannot be found in default configuration.
        """

        default_conf = list(default_conf)
        testcase_conf = list(testcase_conf)
        LOGGER.debug(f"default_conf: {default_conf}")
        LOGGER.debug(f"testcase_conf: {testcase_conf}")
        if set(testcase_conf).issubset(default_conf):
            pass
        else:
            raise KeyError(
                "{} key element not part of default configuration,"
                " accepted variables are: {}".format(
                    set(testcase_conf).difference(default_conf), default_conf
                )
            )

    def ConfigUpdate(self, orig_config, updates):
        """Update the settings of the nested configuration.

        Args:
            orig_config: the default configuration (dict)
            updates: the settings to be updated (dict)

        Returns:
            - updated configuration (dict)
        """

        for key, value in orig_config.items():
            if key in updates.keys():
                orig_config[key] = updates.get(key)
            if isinstance(value, dict):
                returned = self.ConfigUpdate(orig_config.get(key, {}), updates)
                orig_config[key] = returned
        return orig_config
