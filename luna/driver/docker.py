#!/usr/bin/env python

"""
Docs: https://docker-py.readthedocs.io/en/stable/containers.html
"""

import timeit
from tqdm import tqdm
from time import sleep
import socket

import docker

from luna.framework.common.logger import LOGGER


class DockerHarness:
    """
    docker run -it --rm -v ./db-dump:/docker-entrypoint-initdb.d \
        -p 3306:3306 -e MYSQL_DATABASE=ueba -e MYSQL_ROOT_PASSWORD=password mysql:8.0
    """

    def __init__(
        self,
        container_name,
        image,
        ports=None,
        environment=None,
        volumes=None,
        auto_remove=True,
        remove=True,
        detach=True,
        command=None,
        network="luna",
        healthcheck_cmd=None,
    ):
        """Harness for start docker container with given parameters"""
        self.docker_client = docker.from_env()
        self.container = None
        self.container_name = container_name
        self.image = image
        self.ports_dict = ports
        self.environment_list = environment
        self.volumes_list = volumes
        self.auto_remove = auto_remove
        self.remove = remove
        self.detach = detach
        self.command = command
        self.network = network
        self.healthcheck_cmd = healthcheck_cmd
        if self.network not in [i.name for i in self.docker_client.networks.list()]:
            self.docker_client.networks.create(
                name=self.network, check_duplicate=True, attachable=True
            )

    def start(self):
        self.find_running_container()
        if self.container:
            LOGGER.debug(f"{self.container_name} container already running, not restarting")
            return True
        LOGGER.info(f"Starting {self.container_name} container with image: {self.image}")
        self.docker_client.images.pull(self.image)
        self.container = self.docker_client.containers.run(
            image=self.image,
            auto_remove=self.auto_remove,
            name=self.container_name,
            hostname=self.container_name,
            remove=self.remove,
            detach=self.detach,
            ports=self.ports_dict,
            environment=self.environment_list,
            volumes=self.volumes_list,
            command=self.command,
            network=self.network,
        )
        self.wait_for_ready()

    def wait_for_ready(self, timeout=60):
        """healthcheck based on port is listening or not"""
        app_ready = self.is_listening()
        if app_ready:
            LOGGER.debug(f"{self.container.name} already Running")
            return True
        t0 = timeit.default_timer()
        with tqdm(
            total=int(timeout), desc=f"{self.container.name.capitalize()} Starting:"
        ) as progress_bar:
            while not app_ready and timeout >= timeit.default_timer() - t0:
                app_ready = self.is_listening()
                sleep(0.3)
                progress_bar.update(1)
        if app_ready:
            LOGGER.debug(
                f"Successfully launched {self.container.name} in {timeit.default_timer() - t0} seconds"
            )
            return True
        else:
            return False

    def list_running_container(self):
        """list running containers with given `container_name`"""
        LOGGER.debug(f"Current list of running containers: {self.docker_client.containers.list()}")

    def find_running_container(self):
        """list running containers with given `container_name`"""
        if not self.container:
            for container in self.docker_client.containers.list():
                if container.attrs.get("Name") == f"/{self.container_name}":
                    self.container = container

    def stop(self):
        LOGGER.info(f"Stopping container with name: {self.container_name}")
        self.container.stop(timeout=3)

    def logs(self, stream=True):
        if not stream:
            for line in self.container.logs(stream=stream, timestamps=False).decode().splitlines():
                LOGGER.debug(line)
        else:
            for line in self.container.logs(stream=stream, timestamps=False):
                LOGGER.debug(line.decode("utf-8").strip())

    def exec_run(
        self,
        cmd,
        stdout=True,
        stderr=True,
        user="",
        privileged=False,
        stream=False,
        environment=None,
    ):
        exit_code, output = self.container.exec_run(
            cmd=cmd,
            stdout=stdout,
            stderr=stderr,
            stream=stream,
            privileged=privileged,
            user=user,
            environment=environment,
        )
        LOGGER.info(f"Running in {self.container_name} >>> cmd: {cmd}")
        for line in output.decode().splitlines():
            print(f"{line}")
        return exit_code, output

    def is_listening(self, host="127.0.0.1", port=None):
        """Checks if the port is listening or not

        Args:
            host: host to check port on (str)
            port: port number to check (str)

        Returns:
            True if listening, False otherwise (bool)
        """
        if not port:
            port = list(self.ports_dict.values())[0]
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            result = sock.connect_ex((host, port))
            if result == 0:
                return True
            else:
                return False
        except ConnectionRefusedError as _:
            return False

    def healthcheck(self, timeout=60):
        """Simulates the docker-compose healthcheck cmd and runs the cmd inside the container

        For eg: ["CMD", "mysqladmin" ,"ping", "-h", "localhost"]
        """
        if not self.healthcheck_cmd:
            LOGGER.error(f"Please define a healthcheck cmd: {self.healthcheck_cmd}")
            return False
        exit_code, output = self.container.exec_run(self.healthcheck_cmd, stream=False)
        if exit_code == 0:
            LOGGER.debug(f"{self.container.name} is Healthy")
            return True
        t0 = timeit.default_timer()
        with tqdm(
            total=int(timeout), desc=f"{self.container.name.capitalize()} Starting:"
        ) as progress_bar:
            while exit_code == 1 and timeout >= timeit.default_timer() - t0:
                exit_code, output = self.container.exec_run(self.healthcheck_cmd, stream=False)
                sleep(0.3)
                progress_bar.update(1)
        if exit_code == 0:
            LOGGER.debug(
                f"{self.container.name.capitalize()} is healthy in {timeit.default_timer() - t0} seconds"
            )
            LOGGER.critical(f"==== {output} ====")
            return True
        else:
            return False
