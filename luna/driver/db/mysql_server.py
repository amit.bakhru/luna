#!/usr/bin/env python
import os

from luna.config import config
from luna.driver.docker import DockerHarness


class MySQLHarness(DockerHarness):
    """
    docker run -it --rm -v ./db-dump:/docker-entrypoint-initdb.d \
        -p 3306:3306 -e MYSQL_DATABASE=ueba -e MYSQL_ROOT_PASSWORD=password mysql:8.0
    """

    def __init__(self):
        self.mysql_port = config["servers"]["mysql"]["port"]
        super().__init__(
            container_name="ueba-mysql",
            image=f"mysql:{config['servers']['mysql']['version']}",
            ports={f"{self.mysql_port}/tcp": self.mysql_port},
            environment=[
                f"MYSQL_DATABASE={config['servers']['mysql']['database']}",
                f"MYSQL_ROOT_PASSWORD={config['servers']['mysql']['password']}",
            ],
            volumes=[f"{os.getcwd()}/db-dump/:/docker-entrypoint-initdb.d"],
            healthcheck_cmd=f"/usr/bin/mysql --user=root "
            f"--password={config['servers']['mysql']['password']} "
            f'--execute "SHOW DATABASES;"',
        )


if __name__ == "__main__":
    p = MySQLHarness()
    p.start()
    p.list_running_container()
    p.healthcheck()
    # p.stop()
    p.healthcheck()
    p.logs(stream=False)
    p.exec_run('mysql -uroot -ppassword -e "show databases;"')
