#!/usr/bin/env python

"""PostgresSQL DB tools for Devo Apps automation."""

import glob
import os
import pipes
import re
import shutil
import signal
import subprocess
import sys
import time
import timeit

import psycopg2 as pg
from psycopg2.extras import DictCursor

from luna.framework.common.logger import LOGGER

# Used by CreateUser to create a user and grant necessary permissions.
_SQL_CREATE_USER = """
CREATE USER %(user)s;
GRANT ALL ON %(tables)s TO %(user)s;
"""


class Error(Exception):
    """Base class for exceptions raised by this module."""

    pass


class ConfError(Error):
    """Configuration error raised by this module."""

    pass


class ShellError(Error):
    """Raised when shell command fails."""

    pass


class PostgresDB:
    """Administrative interface to the Postgres DB.

    Properties:
        dbname: name of the database to be used for test
        serverport: postgres server port number
    """

    @property
    def dbname(self):
        return self.__dbname

    @property
    def serverport(self):
        return self.__serverport

    PG_CONF = None

    def __init__(
        self,
        db_name,
        db_dir=None,
        user="postgres",
        host="127.0.0.1",
        port=5432,
        password=None,
        use_password=False,
        use_existing_db=False,
        remote=False,
    ):
        """Initializes an PostgresDB object.

        Args:
            host: pg host (str)
            db_dir: postgres db directory (str)
            db_name: name of the database (str)
            user: name of the db owner (str)
            port: PostgresSQL Server listen Port (int)
        """
        self.bindir = ""
        _p = subprocess.Popen(
            "which pg_ctl",
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True,
            env=os.environ.copy(),
        )
        binary_path = _p.communicate()[0].strip().decode()
        if not binary_path:
            if sys.platform == "darwin":  # On a mac
                binary_path = glob.glob("/usr/local/Cellar/**/bin/pg_ctl", recursive=True)[0]
            elif sys.platform in ["linux", "linux2"]:  # Ubuntu path
                if sys.version_info.major < 3:
                    binary_path = glob.glob("/usr/lib/postgresql/*/bin/pg_ctl")[0]
                else:
                    binary_path = glob.glob("/usr/lib/postgresql/*/bin/pg_ctl", recursive=True)[0]
        assert os.path.exists(binary_path) is True, "pg_ctl not found on this machine"
        self.bindir = binary_path[: binary_path.index("pg_ctl")]
        LOGGER.debug(f"PG bin dir: {self.bindir}")

        self.dbdir = db_dir
        # db_dir should only be set to None if using a remote db
        assert (self.dbdir is not None and not remote) or (self.dbdir is None and remote)
        self.__dbname = db_name
        self.__serverport = port
        self.db_user = user
        self.remote = remote
        # support not passing in password while using password auth so password defaults to
        # db_user which can be overridden by PGPASSWORD env var
        self.password = password or (self.db_user if use_password else None)
        self.connection = None
        self.host = host
        self.cursor = None
        assert self.VerifyBinaryExists() is True
        if use_existing_db and not remote:
            LOGGER.debug("Attempting to use pre-existing database")
            if not self.Status():
                self.Start()
                started = self._WaitForReadyPgServer()
                if not started:
                    LOGGER.debug("PostgreSQL failed to start with pre-existing db")
        elif not remote:
            # if not running, init-db and start
            self.Install()
            if self.Status():
                self.current_users = self.ListUsers()
                self.CreateUser()
                self.current_dbs = self.ListDb()
                self.CreateDb(db_name)
                self.current_tables = self.ListTables()

        self._Connect()

    def VerifyBinaryExists(self):
        """Verify all postgres related binary commands are in the Path"""
        required_binaries = [
            "pg_ctl",
            "initdb",
            "createdb",
            "dropdb",
            "createuser",
            "dropuser",
        ]
        for rb in required_binaries:
            if not os.path.exists(os.path.join(self.bindir, rb)):
                raise pg.DatabaseError(
                    "Postgres binary %s required for PG Server is not present "
                    "in the PATH. Please make sure the following binaries are "
                    "in PATH: %s." % (rb, ", ".join(required_binaries))
                )
        else:
            return True

    def ListUsers(self):
        """List current pg users"""
        out = self.ExecShell(
            self.bindir + "psql",
            "--no-psqlrc",
            "-h",
            self.host,
            "-p",
            self.serverport,
            "--command",
            "\\du",
            "template1",
        )
        # ignoring first 3 header lines
        users_list = out.split("\n")[3:]
        users_list = [i.split("|")[0].strip() for i in users_list]
        LOGGER.info("[PostgreSQL.ListUsers] Current users: %s", users_list)
        return users_list

    def _WaitForReadyPgServer(self, wait_time=30):
        # waiting maximum wait_time seconds for PG server to be ready
        start_time = timeit.default_timer()
        running = self.Status()
        while (not running) and ((timeit.default_timer() - start_time) < wait_time):
            LOGGER.debug("Waiting for PG server to be ready")
            time.sleep(5)
            running = self.Status()
        else:
            if not running:
                LOGGER.error(f"PG server not ready after waiting {wait_time} seconds")
        return running

    def Install(self):
        """Performs install of Postgres Db, including a new PostgreSQL server if needed.

        - Creates postgres server data directory if it doesn't already exists.
        - initializes the postgres server data directory.
        - Starts Postgres Server if not already running.
        - Creates 'enrichment' user if not already created.
        - Creates the db according to the schema defined.
        - Verify the db creation was successful.
        """
        if self.Status():
            return

        self.Kill()
        if os.path.isdir(self.dbdir):
            shutil.rmtree(self.dbdir)
        os.makedirs(self.dbdir)
        self.Init()
        self.UpdatePostgresConf()
        if not self.Status():
            LOGGER.debug("[PostgreSQL.Install] PostgreSQL server is not running, starting it.")
            self.Start()
            self._WaitForReadyPgServer()

        # if db_user user not already present
        self.CreateUser()

        # Loop while retrying CreateDb, since there is a race with the server starting.
        num_tries = 0
        max_tries = 30
        while num_tries < max_tries:
            try:
                self.CreateDb()
                break
            except Exception as exc:
                LOGGER.debug(exc)
                num_tries += 1
                LOGGER.debug("Retrying CreateDb after %d tries", num_tries)
                time.sleep(1)

        self._Connect()

    def PgVersion(self):
        """Prints the PostgreSQL version information.

        Returns:
            (major, minor, subminor) as tuple of int
        """
        pg_ctl = subprocess.Popen(
            [self.bindir + "pg_ctl", "--version"],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        stdout, stderr = pg_ctl.communicate()
        if pg_ctl.returncode:
            raise ShellError(
                "Unable to determine PostgreSQL version (%d):\n%s"
                % (pg_ctl.returncode, stderr.decode())
            )
        else:
            match = re.match(r".*PostgreSQL.*(\d+)\.(\d+)\.(\d+).*", stdout.decode())
            if not match:
                raise ShellError("Unrecognized PostgreSQL version output: %s" % stdout.decode())
            else:
                version = tuple([int(x) for x in match.groups()])
                print("%d.%d.%d" % version)
                return version

    def Status(self):
        """Checks the postgres SQL server running status"""
        cmd = self.bindir + "pg_ctl -D " + self.dbdir + " status | head -1"
        LOGGER.debug(f"Running cmd: {cmd}")
        _p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        status = _p.communicate()[0].strip().decode()
        LOGGER.debug("[PostgreSQL.Status] postgres status: %s", status)
        if "PID" in status:
            LOGGER.debug("[PostgreSQL.Status] PostgreSQL server is already running")
            return True
        return False

    def Init(self):
        """Initializes a new PostgreSQL server file system repository."""
        self.ExecShell(self.bindir + "initdb", "--pgdata", self.dbdir)

    def UpdatePostgresConf(self):
        postgres_conf_path = os.path.join(self.dbdir, "postgresql.conf")
        if not self.PG_CONF:
            LOGGER.info("[PostgreSQL.UpdatePostgresConf] No configs to update")
            return
        LOGGER.info(
            f"[PostgreSQL.UpdatePostgresConf] updating {postgres_conf_path} with: {self.PG_CONF}"
        )
        with open(postgres_conf_path) as postgres_conf:
            conf = postgres_conf.read()
        if self.PG_CONF.get("max_connections"):
            conf.replace(
                "max_connections = 100",
                f"max_connections = {self.PG_CONF['max_connections']}",
            )
        if self.PG_CONF.get("work_mem"):
            conf.replace("#work_mem = 4MB", f"work_mem = {self.PG_CONF['work_mem']}")
        if self.PG_CONF.get("work_buffers"):
            conf.replace("#temp_buffers = 8MB", f"temp_buffers = {self.PG_CONF['work_buffers']}")
        with open(postgres_conf_path, "w") as postgres_conf:
            postgres_conf.write(conf)
        if self.Status():
            self.Stop()
            self.Start()

    def Start(self, log_path=None):
        """Launches the PostgreSQL server processes.

        Args:
            log_path: path to the log file (str)
        """
        LOGGER.info("[PostgeSQL.Start] Starting postgresql...")
        assert os.path.exists(self.dbdir)
        if log_path is None:
            log_path = os.path.join(self.dbdir, "postgresql.log")
        self.ExecShell(
            self.bindir + "pg_ctl",
            "--pgdata",
            self.dbdir,
            "--log",
            log_path,
            "-o",
            "-h {} -p {} -c unix_socket_directories={}".format(
                self.host, self.serverport, self.dbdir
            ),
            "start",
        )
        # TODO: remove/optimize later
        # PG startup is slow on jenkins, as a workaround adding sleep
        time.sleep(10)
        assert self.Status(), f"[PostgeSQL.Start] failed to start, status: {self.Status()}"

    def Stop(self):
        """Shuts down the PostgreSQL server processes (pending connections closed)."""
        LOGGER.info("[PostgeSQL.Stop] Stopping postgresql...")
        self.ExecShell(self.bindir + "pg_ctl", "--pgdata", self.dbdir, "stop")
        assert not self.Status(), f"[PostgeSQL.Stop] failed to stop, status: {self.Status()}"

    def CreateDb(self, db_name=None):
        """Creates the PostgreSQL database that will contain the tables."""
        if db_name is None:
            db_name = self.dbname
        try:
            if db_name not in self.ListDb():
                self.ExecShell(
                    self.bindir + "createdb",
                    "-h",
                    self.host,
                    "-p",
                    self.serverport,
                    "--owner",
                    self.db_user,
                    db_name,
                )
        except ShellError as e:
            LOGGER.error(f"result: {e}")
            if "exists" in str(e).split("\n")[1]:
                return True

    def ListDb(self):
        """Returns list of existing PostgreSQL databases."""
        out = self.ExecShell(
            self.bindir + "psql",
            "--no-psqlrc",
            "-h",
            self.host,
            "-p",
            self.serverport,
            "--list",
        )
        out = out.split("\n")[3:]
        existing_dbs = [i.split("|")[0].strip() for i in out[:-3]]
        LOGGER.info("[PostgreSQL] Current dbs: %s", existing_dbs)
        return existing_dbs

    def DropDb(self, db_name=None):
        """Drops the database name provided."""
        if db_name is None:
            db_name = self.dbname
        o = self.SqlCmd(cmd=f"""SELECT * FROM pg_stat_activity WHERE datname='{db_name}';""")
        out = [int(i.split("|")[2].strip()) for i in o[:-3]]
        LOGGER.debug(f"Existing pg sessions pid: {out}")
        for _pid in out:
            self.SqlCmd(
                cmd="""SELECT pg_terminate_backend({}) from pg_stat_activity """
                """where datname='{}';""".format(_pid, db_name)
            )
        self.ExecShell(self.bindir + "dropdb", "-h", self.host, "-p", self.serverport, db_name)
        self.current_dbs = self.ListDb()

    def execute_file(self, _file):
        """Drops the database.

        Args:
            _file: SQL file to execute (str)
        """
        assert os.path.exists(_file)
        self.ExecShell(
            self.bindir + "psql",
            "--no-psqlrc",
            "-h",
            self.host,
            "-p",
            self.serverport,
            "--file",
            _file,
            "--username",
            self.db_user,
            self.dbname,
        )

    def CreateUser(self, user=None):
        """Creates the PostgreSQL user as configured in the conf."""
        if user is None:
            user = self.db_user
        if user not in self.ListUsers():
            self.ExecShell(
                self.bindir + "createuser",
                "-h",
                self.host,
                "-p",
                self.serverport,
                "--createdb",
                "--superuser",
                "--echo",
                user,
            )

    def DropUser(self):
        """Drops the PostgreSQL user."""
        self.ExecShell(
            self.bindir + "dropuser",
            "-h",
            self.host,
            "-p",
            self.serverport,
            self.db_user,
        )

    def GetTables(self):
        """Returns list of existing tables in the database provided"""
        out = self.ExecShell(
            self.bindir + "psql",
            "--no-psqlrc",
            "-h",
            self.host,
            "-p",
            self.serverport,
            "-c",
            "\\dt",
            self.dbname,
        )
        # ignoring first 3 header lines
        tables_list = out.split("\n")[3:]
        LOGGER.debug(f"Output: {tables_list}")
        return tables_list

    def SqlCmd(self, cmd, ignore_error=True):
        out = self.ExecShell(
            self.bindir + "psql",
            "--no-psqlrc",
            "-h",
            self.host,
            "-p",
            self.serverport,
            "-U",
            self.db_user,
            "-c",
            cmd,
            "-d",
            self.dbname,
            ignore_error=ignore_error,
        )
        # ignoring first 2 header lines
        all_data = out.split("\n")[2:]
        LOGGER.debug(f"Output: {all_data}")
        return all_data

    def SelectAll(self, table_name="template1"):
        all_data = self.SqlCmd(cmd=f"""SELECT * FROM {table_name}""")
        return all_data

    # Protected:
    def _Connect(self):
        """Connects to the PostgreSQL database specified in the conf.

        Returns:
            pg.connection object

        Raises:
            pg.Error if unable to connect
        """
        LOGGER.debug("[PostgreSQL] Connecting to PostgreSQL")
        self.connection = pg.connect(
            database=self.dbname,
            host=self.host,
            port=self.serverport,
            user=self.db_user,
            password=os.environ.get("PGPASSWORD", self.password),
        )
        self.cursor = self.connection.cursor()
        self.dict_cursor = self.connection.cursor(cursor_factory=DictCursor)

    def ListTables(self):
        """Returns a list of all table names for provided dbname"""
        tables = self.GetTables()
        table_names = [i.split("|")[1].strip() for i in tables[:-3]]
        LOGGER.info("[PostgreSQL] Current tables: %s", table_names)
        return table_names

    def ResetAllTables(self, tables=None):
        """Empty all tables

        Args:
            tables: list of tables to empty (list)
        """
        if tables is None:
            tables = self.current_tables
        self.SqlCmd(cmd="TRUNCATE {}".format(",".join(tables)))

    @staticmethod
    def ExecShell(*args, **kwargs):
        """Executes a shell subprocess.

        Args:
            args: Command-line, as individual arguments (list of str)
            kwargs:
                quote_args: If True (which is the default), args will be escaped (with pipes.quote);
                            If False, then caller must ensure that args survive the shell.

        Raises:
            ShellError if exit code is nonzero
        """
        ignore_error = kwargs.pop("ignore_error", False)
        quote_args = kwargs.get("quote_args", True)
        if quote_args:
            transform = pipes.quote
        else:
            transform = lambda x: x
        cmd = " ".join([transform(str(x)) for x in args])
        LOGGER.debug("[PostgreSQL] ExecShell: %s", cmd)
        popen = subprocess.Popen(
            cmd,
            stderr=subprocess.PIPE,
            stdout=subprocess.PIPE,
            shell=True,
            close_fds=True,
        )
        out, err = popen.communicate()
        LOGGER.debug(f"Output:\n{out}")
        if not ignore_error and popen.returncode:
            raise ShellError(
                "[PostgreSQL] Shell command failed (%d): %s\n%s" % (popen.returncode, cmd, err)
            )
        if not isinstance(out, str):
            out = out.decode()
        return out

    @staticmethod
    def Kill():
        """Killing any existing postgres instances if needed."""
        _p = subprocess.Popen(
            "ps -f|grep 'bin/postgres'|grep -v grep |awk '{print $2}'",
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True,
        )
        pids = [s.strip().decode() for s in _p.communicate()[0].splitlines()]

        if len(pids):
            for pid in pids:
                LOGGER.debug("Existing pid of postgres process: %s", pid)
                LOGGER.info("Killing the stale postgres process.")
                os.kill(int(pid), signal.SIGKILL)
        return True


if __name__ == "__main__":
    p = PostgresDB(db_dir="/tmp/postgres", db_name="blackbox", user="blackbox")
    b = p.ListTables()
    # p.DropDb('blackbox')
    # p.DropDb('indicators_test')
