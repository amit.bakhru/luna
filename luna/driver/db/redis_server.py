#!/usr/bin/env python

import os
import signal
import subprocess
import sys
import time

import redis

from luna.framework.common.harness import ProcessHarness
from luna.framework.common.logger import LOGGER


class RedisServerHarness(ProcessHarness):
    """Harness for launching and stopping Redis Server."""

    PROGRAM = "redis-server"

    def __init__(
        self,
        test_case,
        host="localhost",
        port=6379,
        own_dir=None,
        command_line_args="/usr/local/etc/redis.conf",
        **kwargs,
    ):
        """Redis is a third-party binary, not one we build ourselves."""

        if own_dir is not None and not os.path.isdir(own_dir):
            os.makedirs(own_dir)

        redis_path = None

        if sys.platform.lower() == "darwin":
            p = subprocess.Popen(
                f"which {self.PROGRAM}",
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                shell=True,
            )
            redis_path = p.communicate()[0].strip().decode()
            if not redis_path:
                redis_path = f"/usr/bin/{self.PROGRAM}"  # Linux path
                if sys.platform.lower() == "darwin":  # On a mac
                    redis_path = f"/usr/local/bin/{self.PROGRAM}"
        # So on Linux, if we can use the service command, do so
        elif (sys.platform.lower() != "darwin") and os.path.isfile(f"/etc/init.d/{self.PROGRAM}"):
            redis_path = f'"sudo service {self.PROGRAM} start"'

        # Find the full path to bash our binary
        p = subprocess.Popen(
            "which bash", stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True
        )
        binary_path = p.communicate()[0].rstrip().decode()
        bash_args = "-c"  # '-c' Execute the following string as a command

        # final_cmd_line_args = [bash_args, '{} {}'.format(redis_path, command_line_args)]
        final_cmd_line_args = [bash_args, redis_path]
        if sys.platform.lower() == "darwin":
            final_cmd_line_args.append(command_line_args)

        LOGGER.debug("redis command will be: %s %s", binary_path, final_cmd_line_args)
        kwargs["command_line_args"] = final_cmd_line_args
        super().__init__(test_case, binary_path, **kwargs)

        if not self.IsRunning():
            self.Launch()
            timeout = 0
            redis_ready = 0
            while not redis_ready and timeout < 60:
                time.sleep(1)
                timeout += 1
                try:
                    self.conn = redis.StrictRedis(host=host, port=port)
                    redis_ready = 1
                except redis.ConnectionError as e:
                    LOGGER.error(str(e))

    def ModifyArgs(self, args):
        """Redis config file as argument.

        Args:
            args: list of arguments for redis launch (list)
        """

        # Only add arguments if the command isn't a service command
        # That means that there are only two arguments (-c and the string to execute)
        # And the string to execute is not a "service redis start" command (in quotes)

        if sys.platform.lower() == "darwin":
            LOGGER.debug(f"args: {args}")
            if not args[1].startswith(("'sudo service ", "'service ")):
                if not os.path.exists(args[2]):
                    LOGGER.error("redis.conf file not found at: %s", args[2])
        return args

    @staticmethod
    def GetPid():
        """Returns pid of redis process."""

        cmd = "ps -ef|grep 'redis-server' | grep -v grep | awk '{print $2}'|tail -1"
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        pid = p.communicate()[0].strip().decode()
        if pid:
            LOGGER.debug("Redis pid is %s", pid)
            return int(pid)
        else:
            LOGGER.info("Redis not running.")
            return 0

    def Quit(self):
        """Kill Redis process."""
        pid = self.GetPid()
        if not pid:
            LOGGER.debug("[Redis] Redis does not appear to be running!")
            return
        try:
            os.kill(int(pid), signal.SIGKILL)
        except PermissionError as e:
            LOGGER.error("[Redis] Permission denied attempting stop, using sudo")
            LOGGER.error(str(e))
            cmd = f"sudo kill -s {signal.SIGKILL} {pid}".split()
            subprocess.check_output(cmd)

    def IsRunning(self):
        """Returns True is Redis server already running."""

        pid = self.GetPid()
        if pid:
            LOGGER.debug("[Redis] Redis already running, not Re-Launching!!")
            return True
        return False


if __name__ == "__main__":
    p = RedisServerHarness(test_case=None, env=os.environ.copy(), own_dir="/tmp/")
    p.IsRunning()
