#!/usr/bin/env python

"""MongoDB driver for Devo Apps automation."""

import datetime
import os
import subprocess
import time

import pymongo
from bson import json_util
from pymongo.errors import ConnectionFailure, OperationFailure

from luna.driver.ssh.ssh_driver import SshDriver
from luna.driver.ssh.tunnel_util import SshTunnel
from luna.framework.common.logger import LOGGER


class MongoDBDriver:
    """MongoDbDriver to interact with mongodb using pymongo.

    Note that this is driver API ie only API calls generic to mongodb here,
    if we want to write specific APIs related to a database like esa/im/sa and all we should
    define the corresponding product libraries or use in conjunction with the service APIs (
    esa, contexthub, sa etc)
    """

    @property
    def host(self):
        return self.__host

    @property
    def port(self):
        return self.__port

    @property
    def conn(self):
        return self.__conn

    @property
    def username(self):
        return self.__username

    @property
    def password(self):
        return self.__password

    @property
    def database(self):
        return self.__database

    @property
    def database_name(self):
        return self.__database_name

    @property
    def auth_source(self):
        return self.__auth_source

    @property
    def url(self):
        return self.__url

    def __init__(
        self,
        host="localhost",
        port=27017,
        username=None,
        password=None,
        database_name=None,
        auth_source=None,
        connect=True,
        host_username=None,
        host_password=None,
        is_tunnel=False,
        **kwargs,
    ):
        """Initialize the mongodb driver

        Args:
            host: Host to connect (str)
            port: Port to connect (int)
            username: Username for DB authentication (str)
            password: Password for DB authentication (str)
            database_name: Database name to initialize by default (str)
            auth_source: Authentication Source to authenticate by default (str)
            connect: Connect on init (bool)
            host_username: SSH username for host. (str)
            host_password: SSH username for host. (str)
            is_tunnel: Flag to decide whether to perform Ssh tunnelling to connect to MongoDB or
                       not. (Bool)
        """

        LOGGER.debug("[MongoDBDriver] Connecting to MongoDb @%s:%s", host, port)
        self.__host = host
        self.__port = port
        self.__username = username
        self.__password = password
        self.__auth_source = auth_source
        self.__url = f"mongodb://{host}:{port}/"
        self.__conn = None
        self.__database = None
        self.__database_name = database_name
        self.__ssh_tunnel = None

        if is_tunnel:
            LOGGER.debug("Tunnelling to localhost to connect to MongoDB")
            self.__host = "localhost"
            self.__ssh_tunnel = SshTunnel(host, host_username, host_password)
            self.__ssh_tunnel.Open([port])

        if connect:
            self.Connect()

    def Connect(self):
        try:
            if self.username and self.password:
                self.__url = f"mongodb://{self.username}:{self.password}@{self.host}:{self.port}/"
                if self.auth_source:
                    self.__url = f"mongodb://{self.username}:{self.password}@{self.host}:{self.port}/?authSource={self.auth_source}"
            self.__conn = pymongo.MongoClient(self.__url)
            LOGGER.debug("[MongoDBDriver] Connected to MongoDB successfully!!!")
            self.__conn = pymongo.MongoClient(self.host, self.port)
            if self.database_name:
                self.__database = self.__conn[self.database_name]
                if self.username and self.password:
                    self.database.authenticate(self.username, self.password)
            LOGGER.debug("Connected to MongoDB successfully!!!")
        except ConnectionFailure as e:
            LOGGER.error("[MongoDBDriver] Could not connect to MongoDB: %s", str(e))
            raise ConnectionFailure("[MongoDBDriver] Unable to Connect to Mongo DD")
        if self.database_name:
            self.__database = self.GetDatabase(self.database_name)

    def GetDatabase(self, database_name):
        """Get a database instance from mongodb

        Args:
            database_name: Database name to get instance (str)

        Returns:
            database object (type:object)
        """
        self.__database = self.__conn[database_name]
        return self.database

    def Authenticate(self, username, password, auth_source=None):
        """Authenticate to the database associated to the driver

        Args:
            username: Username to authenticate (str)
            password: Password to authenticate (str)
            auth_source: authentication source if any (str)

        """
        if auth_source:
            self.__database.authenticate(username, password, auth_source)
        else:
            self.__database.authenticate(username, password)

    def GetCollection(self, collection_name):
        """

        Args:
            collection_name: Name of the collection instance to be returned (str)

        Returns:
            returns collection object (type:object) of class pymongo.collection.Collection

        """
        if self.__database:
            return self.__database[collection_name]
        else:
            raise LookupError("[MongoDBDriver] No Database Initialized")

    def GetCollectionNames(self):
        """Get all the collection names from Database"""
        if self.__database:
            return self.__database.collection_names(include_system_collections=False)
        else:
            raise LookupError("[MongoDBDriver] No Database Initialized")

    def InsertIntoCollectionFromJSON(self, json_array, collection_name):
        """Inserts JSON elements into collection

        Args:
            json_array: array of json objects to insert(json)
            collection_name: collection to insert into (str)

        Returns:
            list of _id values corresponding to document(s) inserted
        """

        LOGGER.debug(
            "[MongoDBDriver] Inserting %d items in collection %s; data:\n %s",
            len(json_array),
            collection_name,
            json_array,
        )
        id_list = self.__database[collection_name].insert_one(
            json_array, bypass_document_validation=True
        )
        return id_list

    @staticmethod
    def FindDocuments(collection, doc_dict=None):
        """This method returns list of documents w.r.t appliance id specified in doc_dict dictionary

        Args:
            collection : collection object
                        (Database(MongoClient('sa host ip ',port),db_name)
                        ,collection_name)(object).
            doc_dict :  key value pairs of attributes related to components(dict).
                Example input:
                        {'name':'LCPol',
                        'serviceType':'LOG_COLLECTOR'
                        'host' : '' }

        Note:
            If doc_dict is empty : returns all documents in the collection

        Returns:
            doc_list : returns list of all the key values(dict) for collection name specified.
        """

        cursor = collection.find(doc_dict)
        doc_list = list()
        for doc in cursor:
            doc_list.append(doc)
        LOGGER.info("Documents List : %s", doc_list)
        return doc_list

    @staticmethod
    def PrettyDumpMongo(doc_list, outfile):
        """Pretty print and write Mongo output to a file using the bson/json tools.

        Args:
            doc_list: list of documents (str)
            outfile: out file path (str)
        """

        with open(outfile, "wb") as _file:
            json_formatted_doc = json_util.dumps(
                doc_list, sort_keys=False, indent=4, default=json_util.default
            )
            LOGGER.debug(
                "[MongoDBDriver] Generated Alert notification in JSON form:\n%s",
                json_formatted_doc,
            )
            _file.write(bytes(json_formatted_doc, "UTF-8"))
            return

    def cleanup_mongo(self, collection_name):
        """Removes all documents from the provided collection_name.
        Args:
            collection_name: Collection from where the documents are to be removed (list)
        """
        status_list = []
        for collection in collection_name:
            LOGGER.debug('[MongoDBDriver] Cleaning all documents in "%s" collection.', collection)
            status = self.__database[collection].remove()
            status_list.append(status)
        LOGGER.debug("[MongoDBDriver] Removal result: %s", status_list)

    def Close(self):
        """ """
        try:
            if self.__ssh_tunnel is not None:
                self.__ssh_tunnel.Close()
            LOGGER.debug("[MongoDBDriver] pymongo client connection closed")
            self.__conn.close()
        except ConnectionFailure as e:
            LOGGER.error(e)
            raise ConnectionFailure("[MongoDBDriver] Connection failed to Close")

    def GetServiceID(self, appliance_dict=None, service_type=None, collection_name=None):
        """This method gets the service id of specified service

        Args:
            appliance_dict : details of appliance
            Example Input:
                        {'host':'',
                        'name' : ''}
            service_type : type of service for which service Id is needed(str)
            collection_name: name of the mongodb collection.(str)

        Returns:
            service Id of the service type mentioned in parameters (int)
        """

        appliance_obj = self.GetCollection(collection_name)
        LOGGER.info("[MongoDBDriver] appliance obj 1 %s", appliance_obj)
        appliance_info_list = self.FindDocuments(collection=appliance_obj, doc_dict=appliance_dict)
        LOGGER.info("Appliance info list : %s", appliance_info_list)
        appliance_id = [d["_id"] for d in appliance_info_list]
        appliance_id = str(appliance_id[0])
        service_dict = {"applianceId": appliance_id, "type": service_type}
        service_obj = self.GetCollection(collection_name="service_info")
        service_info_list = self.FindDocuments(collection=service_obj, doc_dict=service_dict)
        LOGGER.info("Service info list :%s", service_info_list)
        service_id = [d["legacyId"] for d in service_info_list]
        service_id = str(service_id[0])
        LOGGER.info("Service ID : %s", service_id)
        return service_id

    def DropCollection(self, collection_name="aggregation_rule"):
        """Drop a collection from the mongodb

        Args:
            collection_name: name of the mongodb collection to be removed (str)
        """
        self.database.drop_collection(collection_name)

    def DropDatabase(self, database_name="esa"):
        """Drop a database from the mongodb

        Args:
            database_name: name of the mongodb collection to be removed (str)
        """
        self.__conn.drop_database(database_name)

    @staticmethod
    def ImportData(
        host_name, db_name, file_path, collection_name=None, auth=None, json_array=False
    ):
        """This method imports data into mongo db

        Args:
            host_name: Host name to use (str)
            db_name: Database name to use (str)
            file_path: Full Path to file to import (str)
            collection_name: Collection to insert into (str)
            auth: If authentication is required, pass the auth details (dict)
                Example input: auth = {'user': 'im',  'pass': 'im'}
            json_array: Whether the data file is in JSON list format (boolean)

        Raises:
            ImportError: if mongoimport fails to import the given file.

        NOTE: make sure to use mongoexport from the appliances as the version of mongodb is older
              and expects for the date to be in epoch milliseconds format
        """

        if not os.path.exists(file_path):
            IOError(f'File "{file_path}" not found')

        if host_name in ["localhost", "127.0.0.1", ""]:
            cmd = f"mongoimport --db {db_name} --collection {collection_name} --file {file_path} -v"
            if json_array:
                cmd += " --jsonArray"
            if auth:
                cmd += f" --username {auth['user']} --password {auth['pass']}"
            LOGGER.info("Executing command:\n'%s'", cmd)
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
            out = p.communicate()[0].strip().decode()
            LOGGER.debug("[MongoImport] Command output:\n%s", out)
            if p.returncode != 0:
                raise ImportError("[MongoImport] mongoimport failing for file %s" % file_path)
        else:
            ssh_driver = SshDriver(host=host_name, username="root", password="netwitness")
            dest_file = os.path.join("/tmp", "mongo_import_test.json")
            ssh_driver.put_file(from_file_path=file_path, to_file_path=dest_file)
            cmd = f"mongoimport --db {db_name} --collection {collection_name} --file {dest_file} -v"
            if json_array:
                cmd += " --jsonArray"
            if auth:
                cmd += f" --username {auth['user']} --password {auth['pass']}"
            ssh_driver.Exec(cmd)
            ssh_driver.remove_file(dest_file)
            ssh_driver.Close()

    def IsCollectionAvailableInDB(self, find_collection):
        """Check if the given collection is present in the db

        Args:
            find_collection: collection to be searched in DB

        Returns:
            If collection is present or not (bool)
        """

        LOGGER.debug("Collection being search for : %s", find_collection)
        collection_names = self.GetCollectionNames()
        LOGGER.debug(collection_names)
        if find_collection in collection_names:
            LOGGER.debug("Collection %s found!", find_collection)
            return True
        else:
            LOGGER.debug("Collection %s does not exist!", find_collection)
            return False

    def CleanupMongo(self, collection_name="alert", id_list=None, **kwargs):
        """Removes all documents or specified documents by id from the provided collection_name.

        Note:
            If the record does not exist/wrong document specified removal mongo will still return
        err: None

        Args:
            collection_name: collection from where the documents are to be removed (str)
            id_list: list of document ids [{"_id": "<id>"}])to remove (dict)

        Returns:
            whether or not removal of all items was successful (bool)
        """

        status = dict()
        status_list = []
        return_status = True
        if id_list:
            for json_item in id_list:
                try:
                    status = self.database[collection_name].remove(json_item, **kwargs)
                    status_list.append(status)
                except Exception as e:
                    err_msg = "[MongoDBDriver] Cleanup Failed. status %s" % status_list[-1]
                    LOGGER.error(err_msg)
                    LOGGER.error(e)
                    return_status = False
                # format: {u'connectionId': 333, u'ok': 1.0, u'err': None, u'n': 1}
                if status["err"] is not None:
                    return_status = False
        else:
            LOGGER.debug(
                f'[MongoDBDriver] Cleaning all documents in "{collection_name}" collection.'
            )
            try:
                status = self.database[collection_name].remove({})
                status_list.append(status)
            except OperationFailure as e:
                LOGGER.error("[MongoDBDriver] Cleanup Failed.")
                LOGGER.error(e)
                return_status = False
        LOGGER.debug(f"[MongoDBDriver] Removal result: {status_list}")
        return return_status

    def GetAlerts(
        self,
        collection_name="alert",
        expected_num_alerts=1,
        module_id=None,
        sort_field="events.esa_time",
        timeout=60,
    ):
        """Extract alerts from mongodb based on module_id.

        Args:
            collection_name: name of the mongodb collection.
            expected_num_alerts: expected number of alerts.
            module_id: list of unique module ids of deployed epl rules.
            sort_field: generated alerts will be sorted on this field.
            timeout: The amount of time to wait for all the alerts to be returned (seconds)

        Returns:
            List of alerts_list for each module_id provided.
        """

        alerts_list = []
        endtime = time.time() + timeout
        query = {}
        if module_id:
            query = {"module_id": {"$in": module_id}}
        while len(alerts_list) < expected_num_alerts and time.time() < endtime:
            try:
                alerts_list = list(self.database[collection_name].find(query).sort(sort_field, 1))
            except OperationFailure as e:
                LOGGER.error("[MongoDBDriver] Unable to extract alerts")
                LOGGER.error(e)
            wake_time = time.time() + 1
            while wake_time > time.time():
                pass
        LOGGER.debug("[MongoDBDriver] Found %d alerts.", len(alerts_list))
        if len(alerts_list) < expected_num_alerts:
            LOGGER.error(
                '[MongoDBDriver] Timeout "%s" reached when trying to retrieve alerts.',
                timeout,
            )
        return alerts_list

    def GetAlertsCount(
        self, collection_name="alert", expected_num_alerts=1, module_id=None, timeout=60
    ):
        """Extract alerts count from mongodb based on module_id.

        Args:
            collection_name: name of the mongodb collection. (default=alert)
            expected_num_alerts: expected number of alerts.
            module_id: list of unique module ids of deployed epl rules.
            timeout: The amount of time to wait for all the alerts to be returned (seconds)

        Returns:
            Total Count of alerts for each module_id provided.
        """

        return len(
            self.GetAlerts(
                collection_name=collection_name,
                expected_num_alerts=expected_num_alerts,
                module_id=module_id,
                timeout=timeout,
            )
        )

    def AdjustIncidentTime(self, collection_name, hours=None):
        """Adjust the created time of incidents in DB collection,

        to begin certain hours prior to the current time

        Args:
            collection_name: The collection that contains the incidents (str)
            hours: how many hours prior to the current time to adjust the incident time (int)
                    24 hours if not provided.
        """
        incident_1 = self.database[collection_name].find_one()
        if hours:
            start_time = datetime.datetime.utcnow() - datetime.timedelta(hours=hours)
        else:
            start_time = datetime.datetime.utcnow() + datetime.timedelta(days=1)
        time_diff = start_time - incident_1["created"]
        for document in self.database[collection_name].find():
            new_datetime = document["created"] + time_diff
            self.database[collection_name].update(
                {"_id": document["_id"]}, {"$set": {"created": new_datetime}}
            )
