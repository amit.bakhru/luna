#!/usr/bin/env python

import os
import signal
import subprocess
import sys
import time

from pymongo import MongoClient
from pymongo.errors import ConnectionFailure

from luna.framework.common.harness import ProcessHarness
from luna.framework.common.logger import LOGGER


class MongoDBServerHarness(ProcessHarness):
    """Harness for launching and stopping MongoDB Server."""

    PROGRAM = "MongoDB"

    def __init__(self, test_case, binary_path=None, **kwargs):
        """MongoDB is a third-party binary, not one we build ourselves."""

        mongod_cmd = "mongod"
        p = subprocess.Popen(
            f"which {mongod_cmd}",
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True,
        )
        mongo_path = p.communicate()[0].strip().decode()
        if not mongo_path:
            mongod_cmd = "/usr/bin/mongod"  # Linux path
            if sys.platform.lower() == "darwin":  # On a mac
                mongod_cmd = "/usr/local/bin/mongod"

        # So on Linux, if we can use the service command, do so
        if (sys.platform.lower() != "darwin") and os.path.isfile("/etc/init.d/mongod"):
            mongod_cmd = "service mongod start"

        # Mongod never needs sudo from Mac, and only when not root on Linux
        if sys.platform.lower() != "darwin" and os.getuid() != 0:
            mongod_cmd = "sudo " + mongod_cmd
        mongod_cmd = "'" + mongod_cmd + "'"

        # Find the full path to bash our binary
        p = subprocess.Popen(
            "which bash", stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True
        )
        binary_path = p.communicate()[0].rstrip().decode()
        bash_args = "-c"  # '-c' Execute the following string as a command

        command_line_args = [bash_args, mongod_cmd]
        if "command_line_args" in kwargs:  # I'm not sure this is a valid use case
            command_line_args += kwargs["command_line_args"]  # But just in case

        LOGGER.debug("Mongod command will be: %s %s", binary_path, command_line_args)
        kwargs["command_line_args"] = command_line_args

        super().__init__(test_case, binary_path, **kwargs)

        if not self.IsRunning():
            self.Launch()
            timeout = 0
            mongoready = 0
            while not mongoready and timeout < 60:
                time.sleep(1)
                timeout += 1
                try:
                    self.conn = MongoClient("localhost", 27017)
                    mongoready = 1
                except ConnectionFailure as e:
                    LOGGER.error(str(e))

    def ModifyArgs(self, args):
        """MongoDB config file as argument.

        Args:
            args: list of arguments for mongo launch (list)
        """

        # Only add arguments if the command isn't a service command
        # That means that there are only two arguments (-c and the string to execute)
        # And the string to execute is not a "service mongod start" command (in quotes)
        if (
            (len(args) == 2)
            and (args[0] == "-c")
            and (not args[1].startswith(("'sudo service ", "'service ")))
        ):
            conf_file = "/etc/mongod.conf"  # prefer mongod on linux
            if not os.path.isfile(conf_file):
                conf_file = "/etc/mongod.conf"  # But still use mongo.org config

            if sys.platform.lower() == "darwin":  # On a mac, switch to brew
                conf_file = "/usr/local/etc/mongod.conf"

            if os.path.isfile(conf_file):
                # Add -f conf_file to the end of the command for bash
                args[1] = f"{args[1][:-1]} -f {conf_file}{args[1][-1:]}"
            else:
                LOGGER.error("mongod.conf file not found at: %s", conf_file)
        return args

    @staticmethod
    def GetPort():
        """Will be used, if we want to run multiple mongod instances."""

    @staticmethod
    def GetPid():
        """Returns pid of mongod process."""

        p = subprocess.Popen(
            "ps -ef|grep 'mongod ' | grep -v grep | awk '{print $2}'",
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True,
        )
        pid = p.communicate()[0].strip().decode()

        if pid:
            LOGGER.debug("MongoDB pid is %s", pid)
            return int(pid)
        else:
            LOGGER.info("MongoDB not running.")
            return 0

    def Quit(self):
        """Kill MongoDB process."""
        pid = self.GetPid()
        if pid:
            os.kill(int(pid), signal.SIGKILL)

    def IsRunning(self):
        """Returns True is MongoDB server already running."""

        pid = self.GetPid()
        if pid:
            LOGGER.debug("[MongoDB] MongoDB already running, not Re-Launching!!")
            return True
        return False
