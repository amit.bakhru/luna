import os
import re
import time

import pexpect

from luna.framework.common.exception import MissingElementError
from luna.framework.common.logger import LOGGER


class SshCommandClient:
    """Ssh utility that allows executing remote commands on a specified host.

    Connection is established on init and maintained until close.
    """

    def __init__(self, host, user=None, password=None, timeout=120):
        """Initializes SSH object for the host specified.

        Args:
            host: hostname to establish SSH connection to (str)
            user: username to connection with (str)
            password: password of the username (str)
            timeout: wait for SSH connection to establish in seconds (int)

        Handles three types of SSH connections:
            - new SSH connection
            - with password
            - without password.
        """

        self.host = host
        self.user = user
        self.password = password
        if user is None:
            raise MissingElementError("user config element not specified")
        if password is None:
            raise MissingElementError("password config element not specified")
        try:
            self.connection = pexpect.spawnu(f"ssh {self.user}@{self.host}")
            # NOTE: uncomment below line to enable connection level logging, extremely verbose
            # import sys; self.connection.logfile = sys.stdout
            _index = self.connection.expect(
                [r"[Pp]assword:", r"yes/no", r"#", r"Server:", r"The authenticity"],
                timeout=timeout,
            )
            if _index == 0:
                self.connection.sendline(self.password)
                ova_index = self.connection.expect([r"Server:", r"#"], timeout=timeout)
                if ova_index == 0:
                    self.connection.sendline()
                    self.connection.expect(r"#", timeout=timeout)
                else:
                    pass
            elif _index == 1:
                self.connection.sendline()
                self.connection.expect(r"#", timeout=timeout)
            elif _index == 1:
                self.connection.sendline("yes")
                self.connection.expect(r"[Pp]assword:", timeout=timeout)
                self.connection.sendline(self.password)
                self.connection.expect(r"#", timeout=timeout)
            elif _index == 2:
                pass
            elif _index == 3:
                self.connection.sendline()
                self.connection.expect(r"#", timeout=timeout)
            elif _index == 4:
                self.connection.sendline("yes")
                self.connection.expect(r"[Pp]assword:", timeout=timeout)
                self.connection.sendline(self.password)
                ova_index = self.connection.expect([r"Server:", r"#"], timeout=timeout)
                if ova_index == 0:
                    self.connection.sendline()
                    self.connection.expect(r"#", timeout=timeout)
        except Exception:
            raise pexpect.ExceptionPexpect(f"Unable to Connect to Host {self.user}@{self.host}")

    def Exec(self, command, prompt=r"#", timeout=60, remove_colors=False):
        """Send a command and return the response.

        Args:
            command: the command to be sent (str)
            prompt: the expected prompt after command execution (regex)
            timeout: wait before command execution finishes in seconds (int)
            remove_colors: remove console output colors from response (bool)

        Returns:
            response: the response after command execution

        Raises:
            pexpect.ExceptionPexpect: if command execution timeout or EOF
        """
        try:
            self.connection.sendline(command)
            self.connection.expect(prompt, timeout=timeout)
            response = self.connection.before
            if remove_colors:
                # removing color codes from console output
                response = re.sub(r"\033\[[0-9;]+m", "", response)
            LOGGER.debug(response)
            return response
        except pexpect.ExceptionPexpect as msg:
            raise pexpect.ExceptionPexpect(
                'Reached an unexpected state during "{}" command execution: {}'.format(command, msg)
            )

    def close(self):
        """Close the ssh connection"""

        try:
            self.connection.close(force=1)
        except pexpect.ExceptionPexpect as msg:
            raise pexpect.ExceptionPexpect("This should never happen.\n%s" % msg)

    def MakeTemp(self, file_content):
        """Makes a temp file.

        Args:
            file_content: the content to be saved in a temp file (str)
        """

        temp_file = self.Exec("mktemp", "#")
        self.Exec("echo '{}' > {}".format(file_content, temp_file.split("\n")[1][:-1]), "#")
        print(temp_file.split("\n")[1][:-1])
        return temp_file.split("\n")[1][:-1]

    def RemoveFile(self, f_name):
        """Deletes a remote file.

        Args:
            f_name: name fo the remote file to be deleted(str)
        """

        if f_name == "/" or f_name == "/root":
            LOGGER.debug("Deleting root dir not allowed")
            return
        command = "rm -rf %s" % f_name
        self.Exec(command, "#")

    def GetServicePid(self, service):
        """Gets the pid of Linux service specified.

        Args:
            service: name of linux service (str)
        """
        command = "service %s status" % service
        result = self.Exec(command)
        # removing color codes from console output
        result = re.sub(r"\033\[[0-9;]+m", "", result)
        pid = re.findall(r"\d+", result)[0]
        return pid

    def Put(self, source_file, destination_file, timeout=360):
        """Puts source file on remote machine

        Args:
            source_file: source file path (str)
            destination_file: destination file path (str)
            timeout: timeout value in seconds (int)
        """

        return self.CopyFile(source_file, destination_file, operation="to_remote", timeout=timeout)

    def Get(self, source_file, destination_file, timeout=360):
        """Gets source file from remote machine

        Args:
            source_file: source file path (str)
            destination_file: destination file path (str)
            timeout: timeout value in seconds (int)
        """

        return self.CopyFile(source_file, destination_file, operation="to_local", timeout=timeout)

    def CopyFile(self, source_file, destination_file, operation="to_local", timeout=360):
        """Copy a file from the local host to the remote host.

        Args:
            source_file: source file path (str)
            destination_file: destination file path (str)
            operation: to_local/to_remote (str)
                - to_local : copy from remote to local
                - to_remote: copy from local to remote
            timeout: timeout value in seconds (int)

        Raises:
            pexpect.ExceptionPexpect: If command execution timeout or EOF
        """

        cmd = None
        if operation == "to_local":
            # verifying source file exists remotely
            if not self.AssertRemoteFileExists(source_file):
                raise OSError(f"'{source_file}' file not found on {self.host}")
            cmd = "scp {}@{}:{} {}".format(
                self.user,
                self.host,
                source_file,
                destination_file,
            )
        elif operation == "to_remote":
            # verifying source file exists
            if not os.path.exists(source_file):
                raise OSError("'%s' file not found on localhost" % source_file)
            cmd = "scp -r {} {}@{}:{}".format(
                source_file,
                self.user,
                self.host,
                destination_file,
            )
        LOGGER.debug("Executing scp command: %s", cmd)
        try:
            _connection = pexpect.spawnu(cmd)
            index = _connection.expect([r"[Pp]assword:", r"yes/no", pexpect.EOF], timeout=timeout)
            if index == 0:
                _connection.sendline(self.password)
                _connection.expect(pexpect.EOF, timeout=timeout)
                LOGGER.debug("scp output: %s", _connection.before)
            elif index == 1:
                _connection.sendline("yes")
                _connection.expect(r"[Pp]assword:", timeout=timeout)
                _connection.sendline(self.password)
                _connection.expect(pexpect.EOF, timeout=timeout)
                LOGGER.debug("scp output: %s", _connection.before)
            elif index == 2:
                LOGGER.debug("scp output: %s", _connection.before)
                return True
        except pexpect.ExceptionPexpect as e:
            raise pexpect.ExceptionPexpect(f'Error execution command "{cmd}" with exception: {e}')

    def AssertRemoteFileExists(self, file_path):
        """Asserts if a file exists on remote host

        Args:
            file_path: file path to check (str)

        Returns:
            True if file exists, False otherwise (bool)
        """

        LOGGER.info(f"Verifying if '{file_path}' file exists on '{self.host}'")
        time.sleep(5)
        return_code = self.Exec("test -f %s ; echo $?" % file_path).split("\n")[1].strip()
        # debug line below causing failure as its not correctly casting the return code
        # so using print instead
        # LOGGER.debug('Return code value: %s and type: %s', return_code, type(return_code))
        print("Return code value for verify file exists: %s", return_code)
        return return_code == "0"
