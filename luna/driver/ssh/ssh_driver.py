import datetime
import logging
import re
import time

import paramiko

# Disable paramiko logger to put debugging logs, we can integrate it with system logging level
# But it wont be much helpful until you are debugging paramiko related issues
from luna.framework.common.exception import SFTPException, SSHConnectionException
from luna.framework.common.logger import LOGGER

logging.getLogger("paramiko").setLevel(logging.WARNING)


class SshDriver:
    """Create an SSH connection to a server and execute commands"""

    @property
    def host(self):
        return self.__host

    @property
    def port(self):
        return self.__port

    @property
    def username(self):
        return self.__username

    @property
    def password(self):
        return self.__password

    @property
    def home_dir(self):
        return self.__home_dir

    def __init__(self, host, username, password, port=22, compress=True, connect=True):
        """Initialize the SSHConnection Object

        Args:
            host: Host name or address to connect (str)
            username: Username to authentication (str)
            password: Password for authentication (str)
            port: ssh nw_port to connect (int)
            compress: Enable or disable compression (bool)
            connect: Connect on init (bool)
        """
        self.__host = host
        self.__username = username
        self.__password = password
        self.__port = port
        self.__home_dir = None
        self.ssh = None
        self.transport = None
        self.compress = compress
        self.bufsize = 65536
        self.sftp_live = False
        self.sftp = None
        if connect:
            self.Connect()

    def __del__(self):
        self.Close()

    def Connect(self):
        """Make an SSH Connection to the host

        Raises:
            SSHConnectionException
        """
        if not self.connected():
            LOGGER.debug("Connecting %s@%s:%d", self.__username, self.__host, self.__port)
            self.ssh = paramiko.SSHClient()
            self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            try:
                self.ssh.connect(
                    hostname=self.__host,
                    port=self.__port,
                    username=self.__username,
                    password=self.__password,
                )
                self.transport = self.ssh.get_transport()
                self.transport.use_compression(self.compress)
                LOGGER.debug("SSH Connection Succeeded to Host: %s", self.__host)
            except Exception as e:
                self.transport = None
                LOGGER.error("SSH Connection failed to Host: %s ERROR: %s", self.__host, str(e))
                raise SSHConnectionException(message="ConnectionError %s" % e)
            if self.transport is None:
                raise SSHConnectionException(message="No Transport to Host %s" % self.__host)
            self.__home_dir = self.Exec(command="pwd")
        else:
            LOGGER.debug("Already connected to host:%s using the same transport", self.__host)

    def Exec(self, command, input_data=None, timeout=300):
        """Execute a command with optional input data

        Args:
            command: The command to run (str)
            input_data:  The input data (dict)
            timeout : The timeout in seconds (int)

        Returns:
            The the output (stdout and stderr combined) (str)
        """

        LOGGER.debug("Executing command: %s with timeout (%d)", command, timeout)
        if not self.connected():
            LOGGER.error("No connection/transport to host:%s", self.__host)
            raise SSHConnectionException(message="No Transport to Host %s" % self.__host)
        # Initialize the session.
        session = self.transport.open_session()
        session.set_combine_stderr(True)
        session.get_pty()
        session.exec_command(command)
        output = self._run_poll(session, timeout, input_data)
        status = session.recv_exit_status()
        LOGGER.debug("Command Execution Status %d and output %s", status, output)
        return output

    def GetServicePid(self, service, use_service=True):
        """Gets the pid of Linux service specified.

        Args:
            service: name of linux service (str)
            use_service: whether or not to check using 'service <servicename> status' (bool)
        """
        if use_service:
            command = "service %s status" % service
        else:
            command = "status %s" % service
        result = self.Exec(command)
        # removing color codes from console output
        result = re.sub(r"\033\[[0-9;]+m", "", result)
        pid = re.findall(r"\d+", result)[0]
        return pid

    def connected(self):
        """Connection Status

        Returns:
            True if connected or false otherwise (bool)
        """

        return self.transport is not None

    def _run_poll(self, session, timeout, input_data):
        """Poll until the command completes

        Args:
            session: The session (obj)
            timeout: The timeout in seconds (int)
            input_data: The input data (dict)

        Returns:
            Returns the output (str)
        """

        max_seconds = timeout
        # Poll until completion or timeout, we are not directly use the stdout file descriptor
        # because it stalls at 64K bytes (65536).
        timeout_flag = False
        start = datetime.datetime.now()
        start_secs = time.mktime(start.timetuple())
        output = ""
        session.setblocking(0)
        while True:
            if session.recv_ready():
                data = session.recv(self.bufsize)
                str_data = data.decode(encoding="UTF-8")
                LOGGER.debug("Output Data Received %s", str_data)
                output += str_data
                if session.send_ready():
                    # We received a potential prompt. this could be made to work more like pexpect
                    # with pattern matching where key is the prompt matcher.
                    # (lazy unique key implementation as of now we can put a better pattern
                    # matcher) This is a minimal handler for blocking command executions
                    if input_data is not None:
                        for input_key, input_value in input_data.items():
                            if input_key in str_data:
                                LOGGER.debug("Sending input data %s", input_value)
                                session.send("%s\n" % input_value)
            if session.exit_status_ready():
                break
            # Timeout check
            now = datetime.datetime.now()
            now_secs = time.mktime(now.timetuple())
            et_secs = now_secs - start_secs
            if et_secs > max_seconds:
                timeout_flag = True
                break
            time.sleep(0.200)
        if session.recv_ready():
            data = session.recv(self.bufsize)
            output += data.decode(encoding="UTF-8")
        if timeout_flag:
            output += "ERROR: Command Execution timeout after %d seconds\n" % timeout
            session.close()
        return output

    def Close(self):
        """Close the ssh connection"""
        if self.connected():
            self.transport.close()
            self.transport = None
            if self.sftp:
                self.sftp.close()

    # SFTP methods to transfer files between remote systems

    def _sftp_connect(self):
        """Establish the SFTP connection."""

        if not self.sftp_live:
            self.sftp = paramiko.SFTPClient.from_transport(self.transport)
            if self.sftp:
                self.sftp_live = True
            else:
                raise SFTPException("Unable to get SFTP Connection")

    def get_file(self, from_file_path, to_file_path):
        """Copy a file from Remote host to local host

        Args:
            from_file_path: Path from the file to be copied (str)
            to_file_path: Path where the file to be copied (str)
        """

        self._sftp_connect()
        LOGGER.debug("Getting File:%s From %s To:%s", from_file_path, self.__host, to_file_path)
        return self.sftp.get(from_file_path, to_file_path)

    def put_file(self, from_file_path, to_file_path):
        """Copy a file from the local host to the remote host

        Args:
            from_file_path: Path from the file to be copied (str)
            to_file_path: Path where the file to be copied (str)
        """

        self._sftp_connect()
        LOGGER.debug("Copying File:%s To %s:%s", from_file_path, self.__host, to_file_path)
        return self.sftp.put(from_file_path, to_file_path)

    def make_temp(self, file_content, filename=None):
        """Makes a temp file

        Args:
            file_content: the content to be saved in a temp file (str)
            filename: Name of the temp file to be created (str)
        """

        if filename:
            temp_file = f"/tmp/{filename}"
        else:
            temp_file = self.Exec("mktemp")
        if "'" in file_content:
            if '"' in file_content:
                file_content = file_content.replace('"', r"\"")
            self.Exec(f'echo "{file_content}" > {temp_file}')
        else:
            self.Exec(f"echo '{file_content}' > {temp_file}")
        return temp_file

    def remove_file(self, f_name):
        """Deletes a remote file/directory.

        Args:
            f_name: name fo the remote file/directory to be deleted(str)
        """

        if f_name == "/" or f_name == "/root":
            LOGGER.debug("Deleting root dir not allowed")
            return
        command = "rm -rf %s" % f_name
        LOGGER.debug("Removing the file: %s" % f_name)
        self.Exec(command)

    def copy_file(self, source_file, destination_file, operation="to_local"):
        """Copy a file from the local host to the remote host.

        Args:
            source_file: source file path (str)
            destination_file: destination file path (str)
            operation: to_local/to_remote (str)
                - to_local : copy from remote to local
                - to_remote: copy from local to remote
        """

        if operation == "to_local":
            LOGGER.debug("Copying file to local location")
            self.get_file(source_file, destination_file)
        elif operation == "to_remote":
            LOGGER.debug("Copying file to remote location")
            self.put_file(source_file, destination_file)

    def is_file_exist(self, file_path):
        """Assert if a file exists on remote host

        Args:
            file_path: File path to check (str)
        """
        LOGGER.debug("Verifying if '%s' file exists on '%s'", file_path, self.__host)
        output = self.Exec("test -f %s ; echo $?" % file_path).strip()
        if output == "1":
            return False
        if output == "0":
            return True
