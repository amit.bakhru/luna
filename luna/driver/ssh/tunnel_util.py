"""Class for SSH Tunnel Utility"""

from sshtunnel import BaseSSHTunnelForwarderError, SSHTunnelForwarder

from luna.framework.common.logger import LOGGER


class SshTunnel:
    """SSH utility to create an SSH Tunnel to the specified ports from a Host"""

    def __init__(self, host, username, password, timeout=120):
        """Initializes the SSHTunnelUtil with required properties

        Args:
            host: Host from where the SSH Tunnel should be established (str)
            username: Username to authenticate to host (str)
            password: Password to authenticate to host (str)
            timeout: Timeout seconds to wait for tunneling (int)
        """

        self.host = host
        self.username = username
        self.password = password
        self.timeout = timeout
        self.tunnel = None

    def Open(self, ports):
        """Creates SSH Tunnel so that service is accessible locally.

        Args:
            ports: A list of ports to establish the tunneling (list)

        Raises:
            Exception if ports are not opened
        """

        remote_bind_address_list = []
        local_bind_address_list = []
        for port in ports:
            remote_bind_address = ("localhost", port)
            local_bind_address = ("", port)
            remote_bind_address_list.append(remote_bind_address)
            local_bind_address_list.append(local_bind_address)
        LOGGER.info(
            "[SshTunnel] Opening ports: %s on localhost from remote %s",
            ports,
            self.host,
        )
        try:
            self.tunnel = SSHTunnelForwarder(
                ssh_address_or_host=(self.host, 22),
                ssh_username=self.username,
                ssh_password=self.password,
                local_bind_addresses=local_bind_address_list,
                remote_bind_addresses=remote_bind_address_list,
            )
            self.tunnel.start()
            LOGGER.info("[SshTunnel] Local Ports Opened: %s", self.tunnel.local_bind_ports)
        except Exception as e:
            LOGGER.error("[SshTunnel] OpenTunnelError %s", e)
            raise Exception(f"[SshTunnel] SSHTunnelError: {e}")

    def Close(self):
        """Close the SSH tunnel"""

        try:
            if self.tunnel:
                LOGGER.debug("[SshTunnel] Closing ssh tunnel")
                self.tunnel.stop()
                LOGGER.debug("[SshTunnel] Tunnel Closed Successfully")
        except BaseSSHTunnelForwarderError as msg:
            raise BaseSSHTunnelForwarderError(f"[SshTunnel] SSHTunnelCloseError: {msg}")
