#!/usr/bin/env python
import time

from ast import literal_eval
import os
from playwright.sync_api import sync_playwright, expect, Playwright
import subprocess
from pathlib import Path

HEADLESS = literal_eval(os.getenv("HEADLESS", "False"))


class AWSSSOAutoLogin:
    last_line = None
    browser = None
    context = None
    page = None

    def launch_browser(self, playwright: Playwright, devtools=False, slow_mo=None):
        """
        "/Applications/Google Chrome.app/Contents/MacOS/Google Chrome --kiosk"
        """
        playwright.chromium.launch()
        self.browser = playwright.chromium.launch(
            headless=HEADLESS, devtools=devtools, slow_mo=slow_mo
        )
        self.context = self.browser.new_context()
        self.page = self.context.new_page()

    def start_aws_cli_login(self):
        subprocess.Popen("aws sso login --no-browser > out", shell=True)
        time.sleep(2)
        t = Path("./out").read_text()
        print(t)
        self.last_line = t.rsplit()[-1]
        Path("./out").unlink()

    def auto_login(self):
        play_wright = sync_playwright().start()
        self.launch_browser(playwright=play_wright)
        self.page.goto(self.last_line)
        expect(self.page).to_have_url(self.last_line)
        self.page.locator("text=Confirm and continue").click()
        self.page.locator("id=identifierId").fill("amit.bakhru@devo.com")
        self.page.locator("text=Next").click()
        devo_gmail_pwd = os.getenv("GMAIL_PASSWORD")
        assert devo_gmail_pwd is not None, "Please export GMAIL_PASSWORD !!"
        # import pdb;pdb.set_trace()
        self.page.get_by_label("Enter your password").fill(devo_gmail_pwd)
        self.page.locator("text=Next").click()
        self.page.locator("text=Allow").click()
        self.close()

    def close(self):
        self.context.close()
        self.browser.close()


if __name__ == "__main__":
    p = AWSSSOAutoLogin()
    p.start_aws_cli_login()
    p.auto_login()
