import os
import sys


PATH_SEPARATOR = os.sep
PLATFORM = sys.platform.lower()
WINDOWS = "win"
MAC = "darwin"
