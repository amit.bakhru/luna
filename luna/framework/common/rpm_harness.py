import logging
import os
import re

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning

from luna.config import config
from luna.driver.ssh.ssh_driver import SshDriver
from luna.framework.common.exception import MissingElementError
from luna.framework.common.harness import Harness
from luna.framework.common.logger import LOGGER


class RPMHarness(Harness):
    """Harness for a service installed through an RPM."""

    @property
    def ssh_shell(self):
        return self.__ssh_shell

    def __init__(self, testcase, service_name, host, user="root", password=None, log_path=None):
        if password is None:
            raise MissingElementError("password config element not specified")
        self.host = host
        self.user = user
        self.password = password
        self.service_name = service_name
        self.log_path = log_path
        self.service_pid = None
        no_proxy_env = os.getenv("no_proxy")
        if no_proxy_env is not None:
            os.environ["no_proxy"] = no_proxy_env + "," + self.host
        else:
            os.environ["no_proxy"] = self.host
        self.__ssh_shell = SshDriver(host=self.host, username=self.user, password=self.password)
        super().__init__(testcase)

    def GetPid(self):
        """Returns pid of RPM Service process."""

        wrapper_pid = self.ssh_shell.Exec(f"service {self.service_name} status")
        wrapper_pid = re.sub(r"\033\[[0-9;]+m", "", wrapper_pid)
        wrapper_pid = re.findall(r"\d+", wrapper_pid)[0]
        _pid = self.ssh_shell.Exec("pgrep -P %s" % wrapper_pid)
        self.service_pid = _pid.split("\n")[-2].strip()
        LOGGER.debug("%s Service pid: %s", self.service_name, self.service_pid)

    def StartService(self):
        """Starts the RPM installed service"""

        LOGGER.debug("Starting the %s service", self.service_name)
        self.ssh_shell.Exec("service %s start" % self.service_name)
        self.GetPid()

    def StopService(self):
        """Stops the RPM installed service"""

        LOGGER.debug("Stopping the %s service", self.service_name)
        self.ssh_shell.Exec("service %s stop" % self.service_name)
        if "esa" in self.service_name:
            LOGGER.debug("Killing the carlos process")
            carlos_port = config["servers"]["esa"]["port"]["carlos"]
            # listen_cmd = (
            #     f"lsof -i tcp:{carlos_port}"
            #     f" | grep -v grep | awk 'FNR == 2 {print $2}'"
            # )
            listen_pid = self.ssh_shell.Exec(listen_cmd)
            LOGGER.debug(f"Got listen pid : {listen_pid}")
            listen_pid = listen_pid.split("\r\n")
            # cmd = f"ps -ef|grep {carlos_port}|grep -v grep|awk '{print $2}'"
            pids = self.ssh_shell.Exec(cmd)
            LOGGER.debug(f"Got following processes with pids: {pids}")
            pids = pids.split("\r\n")
            LOGGER.debug("Check if the process is really running and only then kill it")
            if len(pids) == 3:
                try:
                    if len(listen_pid) == 3:
                        listener_process_pid = listen_pid[1]
                    else:
                        listener_process_pid = " "
                    self.ssh_shell.Exec(f"kill -9 {pids[1]} {listener_process_pid}")
                    pids = self.ssh_shell.Exec(cmd)
                    if len(pids) > 1:
                        LOGGER.critical("Unable to kill the running ESA process!!!Needs attention")
                except OSError:
                    LOGGER.error("Failed to kill stale esa process")

    def RestartService(self):
        """Restarts the RPM installed service"""

        LOGGER.debug("Restarting the %s service", self.service_name)
        return self.ssh_shell.Exec("service %s restart" % self.service_name)

    def StatusService(self):
        """Returns Running Status of RPM Service"""

        LOGGER.debug("Status of the %s service", self.service_name)
        return self.ssh_shell.Exec("service %s status" % self.service_name)

    def IsRunning(self):
        current_status = self.StatusService().lower()
        if "running" in current_status and "not running" not in current_status:
            LOGGER.debug("%s service is running", self.service_name)
            return True
        LOGGER.debug("%s service is NOT running", self.service_name)
        return False

    @staticmethod
    def GetLaunchRequest(url):
        """Returns the launch REST API call value in JSON

        Args:
            url: micro-service access url (str)
        """

        requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
        logging.getLogger("requests").setLevel(logging.WARNING)
        out = requests.get(f"{url}/launch", verify=False)
        return out.json()

    def ValidateNwPackages(self, expected_packages):
        """Validates that all the specified RPMs are present on device.

        Args:
            expected_packages: packages to be validated (list)

        Returns:
            True on success
        """

        LOGGER.info("Getting complete list of rpm installed on device")
        actual_pkg_list = self.ssh_shell.Exec("rpm -qa").replace("\r", "").split("\n")
        for i in range(len(actual_pkg_list)):
            actual_pkg_list[i] = re.sub(r"-\d.*$", "", actual_pkg_list[i])
        all_pkg_present = True
        for pkg in expected_packages:
            if pkg not in actual_pkg_list:
                LOGGER.error("Package %s not found.", pkg)
                all_pkg_present = False
        return all_pkg_present

    def Close(self):
        """Closes the SSH connection to remote machine"""

        self.ssh_shell.Close()
