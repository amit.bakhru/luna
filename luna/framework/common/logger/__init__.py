# framework/common/logger/__init__.py

import io
import itertools
import logging
import os.path
import re
import sys
from distutils.util import strtobool
from logging.handlers import SysLogHandler

from colorlog import ColoredFormatter
from rich.console import Console
from rich.logging import RichHandler

from luna.config import config


class Error(Exception):
    """Base class of exceptions raised by this module."""

    pass


class BadPriorityError(Error):
    """Raised if an invalid logging priority name is specified."""

    def __init__(self, priority_name):
        Error.__init__(self, "Unrecognized logger priority: %r" % priority_name)


class BadFacilityError(Error):
    """Raised if an invalid syslog facility name or code is specified."""

    def __init__(self, facility_name_or_code):
        Error.__init__(self, "Unrecognized syslog facility: %r" % facility_name_or_code)


def GetLogger(name=os.path.basename(sys.argv[0])):
    """Replacement for logging.getLogger which extends the Logger API.

    Args:
        name: Name of the logger (str)

    Returns:
        logging.Logger object with additional pseudo-methods.
    """

    logger = logging.getLogger(name)

    # Add a pseudo-method to print an exception stack trace under debug logging.
    setattr(logger, "debug_exception", _DebugException(logger))

    return logger


def LogInit(logger_conf):
    """Configures logging from a <logger> section of universal conf.

    Args:
        logger_conf: universal.Logger object
    """
    if logger_conf:
        if logger_conf.priority:
            level = LogPriorityCode(logger_conf.priority)
            LOGGER.setLevel(level)
            LOGGER.debug("Log level set to %d (%s)", level, logger_conf.priority)

        if logger_conf.facility:
            LogToSyslog(logger_conf.facility)
            LOGGER.debug("Log facility set to %s", logger_conf.facility)


def LogPriorityCode(priority_name):
    """Encodes a logging priority name.

    Inverse of LogPriorityName.

    Args:
        priority_name: Silver Tail standard name of a logger priority, e.g. 'DEBUG', 'INFO' (str)

    Returns:
        Numeric level compatible with standard Python logging module levels (int)

    Raises:
        BadPriorityError: If priority name is not recognized.
    """
    level = _PRIORITY_CODES_BY_NAME.get(priority_name.upper(), -1)
    if level < 0:
        match = _PRIORITY_NAME_RE.match(priority_name.upper())
        if match:
            level = match.group(1)
            return int(level)
        else:
            raise BadPriorityError(priority_name)
    else:
        return level


def LogPriorityName(priority_code):
    """Decodes a logging priority name.

    Inverse of LogPriorityCode.

    Args:
        priority_code: Numeric level compatible with standard Python logging module levels (int)

    Returns:
        Silver Tail standard name of a logger priority, e.g. 'DEBUG', 'INFO' (str)
    """
    priority_name = _PRIORITY_NAMES_BY_CODE.get(priority_code, None)
    if not priority_name:
        priority_name = "PRIORITY-%d" % priority_code
    return priority_name


# Map of numeric level codes to their string representation.  These are aligned with the syslog
# terms, rather than the Python logging terms.
_PRIORITY_NAMES_BY_CODE = {
    logging.DEBUG: "DEBUG",
    logging.INFO: "INFO",
    logging.WARNING: "WARN",
    logging.ERROR: "ERROR",
    logging.CRITICAL: "CRIT",
}

# Map of the schema level logging codes to the python logger codes
_SCHEMA_PRIORITY_CODE_MAPPING = {
    "NOTICE": logging.INFO,
    "WARNING": logging.WARNING,
    "ERR": logging.ERROR,
    "CRITICAL": logging.CRITICAL,
    "ALERT": logging.CRITICAL,
    "EMERG": logging.CRITICAL,
}

_PRIORITY_CODES_BY_NAME = {name: code for code, name in list(_PRIORITY_NAMES_BY_CODE.items())}
# Append the schema priority codes to the existing map
_PRIORITY_CODES_BY_NAME.update(_SCHEMA_PRIORITY_CODE_MAPPING)

# Regular expression for parsing the non-standard levels.
_PRIORITY_NAME_RE = re.compile("PRIORITY-([0-9]+)")


def LogFacilityCode(facility_name):
    """Encodes a facility names.

    Inverse of LogFacilityName.

    Args:
        facility_name: Standard name of the syslog facility (str)

    Returns:
        Python standard code for the facility (int)

    Raises:
        BadFacilityError: If the name does not match any of the defined facilities.
    """
    facility_code = _FACILITY_CODES_BY_NAME.get(facility_name.lower(), None)
    if not facility_code:
        raise BadFacilityError(facility_name)
    else:
        return facility_code


def LogFacilityName(facility_code):
    """Returns a string description of the syslog facility code.

    Inverse of LogFacilityCode.

    Args:
        facility_code: Python standard code for a syslog facility (int)

    Returns:
        Standard name of the facility (str)

    Raises:
        BadFacilityError: If the code is not one of the standard facility codes.
    """
    facility_name = _FACILITY_NAMES_BY_CODE.get(facility_code, None)
    if not facility_name:
        raise BadFacilityError(facility_code)
    else:
        return facility_name


# Aligned with 1-prim/logger/logger.cpp gFacilities.
# Note: these aren't the codes used by syslog, but those used by SysLogHandler
#   syslog.<code> = 8 x SysLogHandler.<code>
_FACILITY_NAMES_BY_CODE = {
    SysLogHandler.LOG_AUTH: "auth",
    SysLogHandler.LOG_AUTHPRIV: "authpriv",
    SysLogHandler.LOG_CRON: "cron",
    SysLogHandler.LOG_DAEMON: "daemon",
    # SysLogHandler.LOG_FTP: 'ftp',   NOT SUPPORTED IN PYTHON
    SysLogHandler.LOG_KERN: "kern",
    SysLogHandler.LOG_LPR: "lpr",
    SysLogHandler.LOG_MAIL: "mail",
    SysLogHandler.LOG_NEWS: "news",
    SysLogHandler.LOG_SYSLOG: "syslog",
    SysLogHandler.LOG_USER: "user",
    SysLogHandler.LOG_UUCP: "uucp",
    SysLogHandler.LOG_LOCAL0: "local0",
    SysLogHandler.LOG_LOCAL1: "local1",
    SysLogHandler.LOG_LOCAL2: "local2",
    SysLogHandler.LOG_LOCAL3: "local3",
    SysLogHandler.LOG_LOCAL4: "local4",
    SysLogHandler.LOG_LOCAL5: "local5",
    SysLogHandler.LOG_LOCAL6: "local6",
    SysLogHandler.LOG_LOCAL7: "local7",
}

_FACILITY_CODES_BY_NAME = {name: code for code, name in list(_FACILITY_NAMES_BY_CODE.items())}
_FACILITY_CODES_BY_NAME["security"] = SysLogHandler.LOG_AUTH  # DEPRECATED


def LogConf(msg, *args, **kwargs):
    """Logs a configuration error message and exits with a standard error code.

    Args:
        msg: Format string for the message (str)
        args: Any positional arguments required by the format string
        kwargs: Any keyword arguments required by the format string
    """
    LOGGER.critical(msg, *args, **kwargs)
    # Attempt to exit.  This only works from the main thread, but it's all we need normally, and
    # it's more testable.
    sys.exit(LOG_CONF_EXIT_CODE)


# Align with 1-prim/logger/logger.h constant by the same name.
LOG_CONF_EXIT_CODE = 3


class _DebugException:
    """Pseudo-method that logs an exception stack trace, but only if we're at DEBUG level."""

    def __init__(self, logger):
        """Initializes a _DebugException object.

        Args:
            logger: logging.Logger object
        """
        self.__logger = logger

    def __call__(self, exc):
        """Logs the stack trace of an exception when DEBUG logging is enabled.

        Args:
            exc: Exception object
        """
        if self.__logger.getEffectiveLevel() <= logging.DEBUG:
            self.__logger.exception(exc)


LOGGER = GetLogger()
_LOGGER_FORMAT = (
    "%(asctime)s:"
    "%(log_color)s%(name)s[%(process)d]%(reset)s:"
    "%(log_color)s%(threadName)s| "
    "%(log_color)s%(levelname)-3s%(reset)s |"
    "%(log_color)s%(message)s%(reset)s"
)
if strtobool(os.getenv("COLORLOG", "1")):
    _LOG_FORMATTER = "%(asctime)s: %(name)s [%(process)d] | %(message)s "
    error_console = Console(stderr=True)
    _STDERR_HANDLER = RichHandler(console=error_console, rich_tracebacks=True, show_time=True)
    _STDERR_HANDLER.setFormatter(logging.Formatter(datefmt="%Y-%m-%dT%H:%M:%S%z "))
    _STDERR_HANDLER._log_render.show_path = False
else:
    _LOG_FORMATTER = ColoredFormatter(_LOGGER_FORMAT)
    _STDERR_HANDLER = logging.StreamHandler(sys.stderr)
    _STDERR_HANDLER.setFormatter(_LOG_FORMATTER)

LOGGER.setLevel("INFO")
if strtobool(os.getenv("LUNA_DEBUG", "0")) or config["common"]["logging"] == "DEBUG":
    LOGGER.setLevel("DEBUG")


def LogToStderr():
    """Changes the logging to write to stderr instead of syslog."""
    try:
        LOGGER.removeHandler(_SYSLOG_HANDLER)
    except NameError:
        pass  # It hasn't been instantiated, ignore

    LOGGER.addHandler(_STDERR_HANDLER)


def LogToSyslog(facility_name=None):
    """Changes the logging to write to syslog instead of stderr.

    Args:
        facility_name: Standard name of a syslog facility, or None for default, which is 'user'
            (str)
    """
    if sys.platform == "darwin":
        _SYSLOG_HANDLER = SysLogHandler("/var/run/syslog")  # Definition for Apple
        _SYSLOG_HANDLER.setFormatter(_LOG_FORMATTER)
    elif "linux" in sys.platform:
        _SYSLOG_HANDLER = SysLogHandler("/dev/log")  # Definition for Linux
        _SYSLOG_HANDLER.setFormatter(_LOG_FORMATTER)
    else:
        _SYSLOG_HANDLER = None  # Windows No handler required
        if facility_name:
            _SYSLOG_HANDLER.facility = LogFacilityCode(facility_name)

    LOGGER.removeHandler(_STDERR_HANDLER)
    LOGGER.addHandler(_SYSLOG_HANDLER)


def LogDebugStringIf(closure, condition=True, priority=logging.DEBUG):
    """Captures logging to a string at a temporary priority.

    Args:
        closure: Closure to run while logs are being captured.
        condition: If False, then logs will not be captured, but closure is still run (bool).
        priority: Temporary logging priority (int).

    Returns:
        Log output (str) or None if condition is False.
    """
    if condition:
        log_output = io.StringIO()
        handler = logging.StreamHandler(log_output)
        LOGGER.addHandler(handler)

        original_level = LOGGER.getEffectiveLevel()
        if original_level > logging.DEBUG:
            LOGGER.setLevel(priority)

        try:
            closure()
        finally:
            LOGGER.removeHandler(handler)
            LOGGER.setLevel(original_level)

        return log_output.getvalue()
    else:
        closure()
        return None


class LogStream:
    """Registers a stream so that it receives log messages, one per Process call.

    Based on the LogStream Guard pattern class in 1-prim/logger.
    """

    @classmethod
    def Register(cls, stream):
        """Registers a stream so that it receives log messages, one per Process call.

        Returns an ID that can be used in Unregister.

        Args:
            stream: Stream-like object to receive log messages.

        Returns:
            ID that can be used by Unregister (int)
        """
        handler = logging.StreamHandler(stream)
        handler.setFormatter(_LOG_FORMATTER)
        LOGGER.addHandler(handler)
        _id = next(cls.__ID)
        cls.__STREAMS[_id] = handler
        return _id

    @classmethod
    def Unregister(cls, _id):
        """Removes the stream associated with the ID, which originated from Register.

        Calling this function with the same ID and scope is idempotent.

        Args:
            _id: ID previously returned by Register (int)
        """
        handler = cls.__STREAMS.get(_id)
        if handler:
            del cls.__STREAMS[_id]
            LOGGER.removeHandler(handler)

    __STREAMS = {}
    __ID = itertools.count()


# Default to logging to stderr.
LogToStderr()
