# framework/common/logger/logsaver.py

"""Log handler that saves log messages in a A1S-defined way."""

import logging
import time

from luna.framework.common import logger


def FormatMessages(messages, include_timestamps=None, unique=False):
    """Formats messages accumulated by a LogSaver.

    If current logging verbosity is at least DEBUG, then timestamps will be included.

    Args:
        messages: List of logger.Message objects
        include_timestamps: If True, timestamps will be included; if False, timestamps will not be
                included; if None, then timestamps will be included only if the logging verbosity
                is DEBUG or higher.
        unique: If True, then messages will be sorted and duplicates removed; if False, then
                messages will be returned in order, including duplicates.

    Returns:
        List of formatted strings.
    """
    if include_timestamps is None:
        include_timestamps = logger.LOGGER.getEffectiveLevel() <= logging.DEBUG
    if include_timestamps:
        return [
            "%s [%s] %s"
            % (
                time.strftime("%b %d %H:%M:%S", time.gmtime(float(msg.timestampUtcMicros) / 1e6)),
                msg.priority,
                msg.text,
            )
            for msg in messages
        ]
    else:
        formatted_messages = [f"[{msg.priority}] {msg.text}" for msg in messages]
        if unique:
            return sorted(set(formatted_messages))
        else:
            return formatted_messages


class LogSaver(logging.Handler):
    def __init__(self, *args, **kwargs):
        logging.Handler.__init__(self, *args, **kwargs)
        self.__messages = []

    @property
    def messages(self):
        """
        Returns:
            List of A1S_logger.Message objects
        """
        return list(self.__messages)

    def emit(self, record):
        """Implements this pure virtual method by storing the record.

        Args:
            record: Logging record
        """
        now = _Now()
        priority = logger.LogPriorityName(record.levelno).lower()
        text = record.getMessage()
        self.__messages.append(
            gacc_logger.Message(timestampUtcMicros=int(now * 1e6), priority=priority, text=text)
        )


def _Now():
    """Dependency injection for the system clock.

    Returns:
        Number of seconds since the epoch, in UTC (float)
    """
    return time.time()
