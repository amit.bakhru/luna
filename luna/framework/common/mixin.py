"""Various mixins for Devo Apps automation."""

import simplejson as json

from luna.framework.common.logger import LOGGER
from luna.framework.utils.json_util import ParseableJson


class FastTimeoutMixin:
    """Mix-in to provide the 'fast' timeout for a ComponentTestCase."""

    TIMEOUT_SECS = 5


class SlowTimeoutMixin:
    """Mix-in to provide the 'slow' timeout for a ComponentTestCase."""

    TIMEOUT_SECS = 60


class GlacialTimeoutMixin:
    """Mix-in to provide the 'glacial' timeout for a ComponentTestCase."""

    TIMEOUT_SECS = 60 * 5


class NoTimeoutMixin:
    """Mix-in to disable timeouts for a ComponentTestCase."""

    TIMEOUT_SECS = None


class JsonMixin:
    """Mixin for test classes that deal with JSON.

    Contains some useful test utilities.
    """

    @staticmethod
    def PrettyDumpJson(json_object, output_filename):
        """Dump a JSON object to a file, pretty-printed.

        Args:
            json_object: A JSON object
            output_filename: Path to an output file (str)
        """

        with open(output_filename, "w") as outfile:
            outfile.write(json.dumps(json_object, sort_keys=True, indent=4))

    def assertJSONFileAlmostEqualsKnownGood(
        self, knowngood_filename, output_filename, ignorefields=None
    ):
        """Assert that two JSON files are equal, except for the content of specified fields.

        If the CTF_UPDATEKNOWNGOOD env var is set to a non-empty value, then the known good
        json file will be overwritten with the contents of the actual json file.

        Args:
            knowngood_filename: The path/file name of the known good JSON file(filename)
            output_filename: The path/file name of the test output JSON file(filename)
            ignorefields: A list of dictionary keys to ignore when comparing.
        """

        if ignorefields is None:
            ignorefields = []
        LOGGER.debug("Ignorefields list: %s", ignorefields)
        self._MaybeUpdateKnownGood(knowngood_filename, output_filename)

        with open(knowngood_filename) as knowngood_file, open(output_filename) as output_file:
            parsed_knowngood = ParseableJson(json.load(knowngood_file), ignore_list=ignorefields)
            LOGGER.debug("***** PARSED KNOWNGOOD ***** %s", parsed_knowngood.base_dict)
            comparison_dict = parsed_knowngood.CompareJson(json.load(output_file))
            LOGGER.debug("***** COMPARSION DICT ***** %s", comparison_dict)
        assert parsed_knowngood.base_dict == comparison_dict
