# luna/framework/utils/dotdict.py

"""Dict to dot notation Utility for Devo Apps automation."""


class DotDict(dict):
    """Utility to access dictionary using dot notation."""

    def __getattr__(self, attr):
        return self.get(attr)
