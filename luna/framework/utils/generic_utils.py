# luna/framework/utils/generic_utils.py

import os
import random
import re
import time
import timeit

import sortedcontainers

from luna.framework.common.logger import LOGGER


class GenericUtilities:
    """A bunch of general use utilities"""

    @staticmethod
    def get_unique_id():
        """Generates a unique id based off of current time

        Returns:
            a unique id based off of current time (int)
        """
        return int(time.time() * 100 + random.randrange(100))

    @staticmethod
    def search_file(search_pattern, return_dir=False, start_path=None):
        """Searches a file with pattern specified in up to 6 levels of dir.

        Stops looking when you hit the repository root, looking for marker file .jenkins_properties.

        Args:
            search_pattern: file name pattern to be searched (string)
            return_dir: returns the dirname containing the file if True (boolean)
            start_path: path of the dir to start the search from (str)

        Returns:
            The full path of the searched file or the dir containing the file or an empty file
            string if the file was not found.
        """

        search_path_list = [
            ".",
            "..",
            "../..",
            "../../..",
            "../../../..",
            "../../../../..",
        ]
        if start_path is not None:
            search_path_list = [start_path] + search_path_list

        start_time = timeit.default_timer()
        for _path in search_path_list:
            _files = [
                os.path.join(r, f) for r, d, fs in os.walk(_path) for f in fs if search_pattern in f
            ]
            if len(_files):
                # sort the list
                _files = sortedcontainers.SortedList(_files)
                full_path = os.path.realpath(_files[0])
                LOGGER.debug(
                    "File Search for %s pattern took : %s seconds",
                    search_pattern,
                    timeit.default_timer() - start_time,
                )
                LOGGER.debug(f"final full file path: {full_path}")
                if return_dir:
                    return os.path.dirname(full_path)
                else:
                    return full_path
        return None

    @staticmethod
    def kill_process(ssh_obj=None, process_string=None):
        """Kills the process specified.

        Args:
            ssh_obj: SSH handle object on the host. (obj)
            process_string: name of the process to kill (str)
        """

        if ssh_obj is None:
            raise NotImplementedError("SSH obj not defined")
        output = ssh_obj.Exec("ps ax|grep %s|grep -v grep" % process_string).split("\n")[1:-1]
        if len(output):
            ssh_obj.Exec("killall -9 %s" % process_string)

    @staticmethod
    def read_contents(file):
        with open(file) as f:
            file_text = "".join(f.readlines())
        return file_text

    @staticmethod
    def replaceTextInJsonFile(source_file, target_file, search_pattern, replace_text):
        """This method replaces a pattern w/ a string and writes to a new file

        Args:
            source_file: File name to read from.(str)
            target_file: File name to write modified data to.(str)
            search_pattern: Reg-ex pattern to be searched in the file.(str)
            replace_text: the String to be used as replacement for the search pattern (Str)

        """
        with open(source_file) as src:
            src_txt = src.read()

        text = re.sub(r'"' + search_pattern + '"', '"' + replace_text + '"', src_txt)

        with open(target_file, "w") as tgt:
            tgt.write(text)

    @staticmethod
    def ReplaceTextInFile(source_file, target_file, search_text, replace_text):
        """This method replaces a pattern w/ a string and writes to a new file

        Args:
            source_file: File name to read from.(str)
            target_file: File name to write modified data to.(str)
            search_text: String to be searched in the file.(str)
            replace_text: the String to be used as replacement for the search pattern (Str)

        """

        lines = []
        with open(source_file) as src:
            for line in src:
                if search_text in line:
                    line = line.replace(search_text, replace_text)
                lines.append(line)

        with open(target_file, "w") as tgt:
            for line in lines:
                tgt.write(line)

    @staticmethod
    def CleanDirectory(dir_path, file_type="all"):
        """Cleans directory by file type specified.

        Args:
            dir_path: directory path.(str)
            file_type: file type or file extension to remove. Defaults to all which remove all
                       files.(str)
                       example input: file_type='zip'
        """

        if file_type.lower() == "all":
            for root, dirs, files in os.walk(dir_path, topdown=False):
                for name in files:
                    os.remove(os.path.join(root, name))
                for name in dirs:
                    os.remove(os.path.join(root, name))
        else:
            file_type = "." + file_type
            for root, dirs, files in os.walk(dir_path, topdown=False):
                for name in files:
                    if name.endswith(file_type):
                        os.remove(os.path.join(root, name))
