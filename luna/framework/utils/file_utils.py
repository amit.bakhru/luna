import os
import time
import zipfile

import simplejson as json

from luna.driver.ssh.ssh_driver import SshDriver
from luna.framework.common.exception import FileBasedException
from luna.framework.common.logger import LOGGER


class FileUtilities:
    """A bunch of File utilities"""

    def IsFileTypePresent(self, file_type, file_path=None):
        """Asserts file of file_type is present.

        Args:
            file_type: extension of file to look for.(str)
                       example input: file_type='zip'
            file_path: Directory path under which file should be searched.(str)
                       If file_path is not specified then test case output dir path will be
                       considered.
        """

        if file_path is None:
            file_path = self.test_case_o_dir
        file_type = "." + file_type
        for root, direc, files in os.walk(file_path, topdown=True):
            for name in files:
                if name.endswith(file_type):
                    return True
            LOGGER.debug(root)
            LOGGER.debug(direc)
        return False

    @staticmethod
    def Wait_for_file_presence(file_path, timeout=180):
        """Wait for a file to be present after download

        Args:
            file_path: Path of the file (str)
            timeout: Waiting time for the download completion (int)

        Returns:
            status: Status of the presence (bool)
        """

        status = False
        start_time = time.time()
        while not status:
            if os.path.exists(file_path):
                status = True
            elif timeout:
                current_time = time.time()
                if current_time > start_time + timeout:
                    break
                time.sleep(5)
        return status

    def Wait_for_file_download_complete(self, file_path, timeout=180):
        """Wait for a file download Complete

        Args:
            file_path: Path of the File along with the File (str)
            timeout: Waiting time for the download completion (int)

        Returns:
            status: Status of the Download Completion (bool)
        """

        try:
            status = False
            start_time = time.time()
            if self.Wait_for_file_presence(file_path, timeout):
                file_size = os.path.getsize(file_path)
                time.sleep(2)
                file_new_size = os.path.getsize(file_path)
                while not status:
                    if file_size == file_new_size:
                        status = True
                    elif file_size != file_new_size:
                        if timeout:
                            current_time = time.time()
                            if current_time > start_time + timeout:
                                break
                            time.sleep(2)
            else:
                raise ValueError("File does not exist or is inaccessible")
            return status
        except Exception as e:
            LOGGER.error("File does not exist or is inaccessible while Download, %s", e)

    @staticmethod
    def ExtractZipFile(file_path, zip_file_name):
        """Extracts the content present in the file path

        Args:
            file_path: Path of the file
            zip_file_name: Name of the Zip file
        """

        try:
            file_content = os.sep.join([file_path, zip_file_name])
            zip_file = zipfile.ZipFile(file_content)
            zip_file.extractall(file_path)
        except Exception as e:
            LOGGER.error(e)
            raise FileBasedException("File Cannot be Extracted !")

    @staticmethod
    def IsFilePresent(file_name, file_path):
        """Asserts file of file_type is present.

        Args:
            file_name: extension of file to look for.(str)
                       example input: file_type='zip'
            file_path: Directory path under which file should be searched.(str)
                       If file_path is not specified then test case output dir path will be
                       considered.

        Returns:
            True if file found, False otherwise (bool)
        """

        for root, direc, files in os.walk(file_path, topdown=True):
            for name in files:
                if name == file_name:
                    return True
            LOGGER.debug(root)
            LOGGER.debug(direc)
        return False

    @staticmethod
    def ReadFileDataAsJsonObj(file_path=None):
        """Reads file data as json object and returns it

        Args:
            file_path: Path of the file to read (str)

        Returns:
            Contents of the file in json object form(json)
        """
        with open(file_path) as f:
            data = json.load(f)

        LOGGER.debug("Returning the following json data: %s ", data)
        return data

    @staticmethod
    def CopyAMQPCerts(host_name, appl_user_name, appl_pass):
        """Copies ssl certs from remote host to local host for successful ssl connection.

        Args:
            host_name: Name of the host to copy the file from (str)
            appl_user_name: username of the appliance (str)
            appl_pass: password of the appliance (str)

        Returns:
            Path of the ssl cert and key file (tuple)
        """
        ssh = SshDriver(host=host_name, username=appl_user_name, password=appl_pass)
        ssl_cert_file = os.path.join(os.getcwd(), "cert.pem")
        ssl_key_file = os.path.join(os.getcwd(), "key.pem")
        LOGGER.debug("Copying %s file ", ssl_cert_file)
        ssh.copy_file(
            source_file="/etc/rabbitmq/ssl/server/cert.pem",
            destination_file=ssl_cert_file,
        )
        LOGGER.debug("Copying %s file ", ssl_key_file)
        ssh.copy_file(
            source_file="/etc/rabbitmq/ssl/server/key.pem",
            destination_file=ssl_key_file,
        )
        return ssl_cert_file, ssl_key_file
