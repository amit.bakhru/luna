# luna/framework/utils/pcap_player.py

"""PCAP injecting utilities for Devo Apps automation."""

import os

from luna.driver.ssh.ssh_driver import SshDriver
from luna.framework.common.exception import TcpreplayNotFoundError
from luna.framework.common.logger import LOGGER


class PCAPPlayer:
    """Class for publishing on PCAP files using tcpreplay."""

    def __init__(self, appliance=None, host=None, user=None, password=None):
        self.host = host
        self.tcpreplay_path = "/usr/bin/tcpreplay"
        self.tcpdump_path = "/usr/sbin/tcpdump"
        self.is_new_connection = False
        if appliance.ssh_driver is None:
            self.ssh_shell = SshDriver(host, user, password)
            self.is_new_connection = True
        else:
            self.ssh_shell = appliance.ssh_driver
        self.tcpreplay_path = self.verify_install("tcpreplay")
        self.tcpdump_path = self.verify_install("tcpdump")

    def verify_install(self, binary="tcpreplay"):
        """Verify and install tcpreplay RPM"""

        binary_path = None
        if binary == "tcpreplay":
            binary_path = self.tcpreplay_path
        elif binary == "tcpdump":
            binary_path = self.tcpdump_path
        if not self.ssh_shell.is_file_exist(binary_path):
            try:
                binary_path = self.ssh_shell.Exec("which %s" % binary).strip()
                LOGGER.debug("which binary_path: %s" % binary_path)
                if not self.ssh_shell.is_file_exist(binary_path):
                    installed = self.ssh_shell.Exec("rpm -qa | grep %s" % binary)
                    LOGGER.debug("rpm install check : %s", installed)
                    if binary not in installed:
                        raise TcpreplayNotFoundError(
                            '%s is not installed on host : "%s" !'
                            " Please install %s rpm" % (binary, self.host, binary)
                        )
                    raise TcpreplayNotFoundError("%s not found in PATH!" % binary)
            except Exception:
                raise TcpreplayNotFoundError("%s not found in PATH!" % binary)
        return binary_path

    def GetNumPackets(self, pcap_file):
        """Counts number of packets in the pcap file provided

        Args:
            pcap_file: PCAP file to be published (str)

        Returns:
            number of packets in the PCAP file.
        """

        LOGGER.debug("Getting the number of packets in the file")
        cmd = f"{self.tcpdump_path} -r {pcap_file} | wc -l"
        if self.ssh_shell.is_file_exist(pcap_file):
            return self.ssh_shell.Exec(cmd).split("\n")[0:-1][1].replace("\r", "")
        else:
            raise OSError("PCAP file does not exists on %s host" % self.ssh_shell.host)

    def PlayPcap(
        self,
        input_file=None,
        pcap_dir=None,
        rate=None,
        interface=None,
        timeout=300,
        number_of_packets=None,
    ):
        """Publishes events from input file on PacketDecoder.

        Play back a pcap using tcpreplay.

        Args:
            input_file: file path to read PCAP events from and publish (str)
            pcap_dir: dir containing pcap files for publishing (dir name)
            rate: rate of publishing, Events per second (int)
            interface: network interface to publish on (str)
            timeout: publish duration in seconds (int)
            number_of_packets: number of packets to publish from PCAP file (int)

        Returns:
            True if Publishing successful, False Otherwise

        Example: tcpreplay --intf1=eth0 --mbps=10.0 sample.pcap
                    --intf1=eth0    replay the data over the eth0 interface
                    --mbps=10.0     replay the data at 10 mbps
                    sample.pcap     name of file to replay
                # for max speed.
                tcpreplay --topspeed --intf1=eth1 <path_to_pcap>

        Raises:
            TcpreplayNotFoundError: if tcpreplay is not installed on this test system
        """

        pcap_files = list()
        if interface is None:
            interface = "lo"
        if pcap_dir is not None:
            pcap_files = [os.path.join(r, f) for r, d, fs in os.walk(pcap_dir) for f in fs]
        if input_file is not None:
            pcap_files.append(input_file)

        LOGGER.debug("Publishing PCAP files list: %s", pcap_files)
        self.ssh_shell.Exec("sudo service iptables stop")

        for pcap in pcap_files:
            dest_pcap = "/tmp/" + os.path.basename(pcap)
            LOGGER.info("dest file %s", dest_pcap)
            LOGGER.info("Replaying PCAP file: %s", pcap)
            self.ssh_shell.copy_file(pcap, dest_pcap, operation="to_remote")
            LOGGER.debug(
                "Publishing %s packets, total packets: %s",
                number_of_packets,
                self.GetNumPackets(dest_pcap),
            )
            cmd1 = self.tcpreplay_path
            if rate is None:
                cmd1 += " --topspeed --intf1=%s " % interface
            else:
                cmd1 += f" --mbps={rate} --intf1={interface} "
            if number_of_packets is None:
                cmd1 += dest_pcap
            else:
                cmd1 += f"-L {number_of_packets} {dest_pcap}"
            LOGGER.debug("Executing command: %s", cmd1)
            self.ssh_shell.Exec(cmd1, timeout=timeout)
            self.ssh_shell.remove_file(dest_pcap)
        return True

    def Close(self):
        """Closes ssh connection created"""

        if self.is_new_connection:
            self.ssh_shell.Close()
