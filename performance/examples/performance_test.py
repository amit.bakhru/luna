import logging
import os
import time

from colorlog import ColoredFormatter
from locust import HttpUser, task
from pathlib import Path

LOGGER = logging.getLogger(__name__)
FORMAT = "%(asctime)s  %(log_color)s%(levelname)-8s%(reset)s | %(log_color)s%(message)s%(reset)s"
stream = logging.StreamHandler()
stream.setFormatter(ColoredFormatter(FORMAT))
logging.basicConfig(handlers=[stream], level=logging.INFO)
LOGGER.setLevel("DEBUG")

# where you want log files to go
OUTDIR = Path("o")

ENV = [
    "api.stage.devo.com",
    # 'api-v2.us.devo.com'
]

ENDPOINTS = [
    "total_emails",
    "bec_targets",
    "threat_types",
    "detection_stats",
    "system_stats",
    "threat_origins",
    "last_hour_by_customer",
    "org_spoofs",
    "domain_proximity",
]

payload = {"nocache": "1"}
headers = {}
base_url = os.getenv("DEVO_SERVER", "https://api.stage.devo.com")


class MyUser(HttpUser):
    @task
    def days_report(self):
        for _day in ["7", "30", "90"]:
            for customer in CUSTOMERS:
                for _endpoint in ENDPOINTS:
                    final_url = (
                        f"/metrics/recipient/{_endpoint}?customer={customer}&days={_day}&top=-1"
                    )
                    LOGGER.info(f"[GET] {base_url}{final_url}")
                    self.client.get(url=final_url)
                    time.sleep(1)
