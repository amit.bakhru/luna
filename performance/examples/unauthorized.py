from locust import HttpUser, task, between


class Unauthorized(HttpUser):
    wait_time = between(1, 5)

    @task
    def domains_unauthorized(self):
        # 401 is the expected code
        with self.client.get("/list_domains", catch_response=True) as response:
            if response.status_code in [401, 403]:
                response.success()
            else:
                response.failure(f"Unexpected code: {response.status_code}")
