import datetime
import json
import logging
import os

from configurable import HttpUserConfigurable
from locust import task, between, SequentialTaskSet


class CollectorTasksBase(SequentialTaskSet):
    """CollectorTasksBase is an abstract class to enable which names for env var configuration use"""

    abstract = True

    def __init__(self, *args, **kwargs):
        if "env" not in kwargs:
            raise Exception(
                "I need environment variable mapping in 'env' param: dict with param_id: ENV_VAR_NAME values"
            )

        env_map = kwargs["env"].copy()
        del kwargs["env"]

        super().__init__(*args, **kwargs)

        for key in ["token", "coll_recipe", "job_spec", "job_params", "max_jobs"]:
            if key not in env_map:
                raise Exception(f"Missing '{key}' inv 'env' mapping")

        token = os.getenv(env_map["token"])
        if token is None:
            raise Exception(f"{env_map['token']} env var is not defined (.env file can be used)")
        self.headers = {
            "standAloneToken": token,
        }

        self.coll_recipe = os.getenv(env_map["coll_recipe"])
        if self.coll_recipe is None:
            raise Exception(
                f"{env_map['coll_recipe']} env var is not defined (.env file can be used)"
            )
        self.coll_recipe = json.loads(self.coll_recipe)
        if (
            type(self.coll_recipe) is not dict
            or "name" not in self.coll_recipe.keys()
            or "version" not in self.coll_recipe.keys()
            or "channel" not in self.coll_recipe.keys()
        ):
            raise Exception(
                f"Valid json, expected in {env_map['coll_recipe']}, is an object following next format "
                + "{name: NAME, version: VERSION, channel: CHANNEL}"
            )

        self.job_spec = os.getenv(env_map["job_spec"])
        if self.job_spec is None:
            raise Exception(f"{env_map['job_spec']} env var is not defined (.env file can be used)")
        self.job_spec = json.loads(self.job_spec)
        if type(self.job_spec) is not dict:
            raise Exception(
                f"Valid json, expected in {env_map['job_spec']}, is an object with the job creation spec"
            )

        self.job_params = os.getenv(env_map["job_params"])
        if self.job_params is None:
            raise Exception(
                f"{env_map['job_params']} env var is not defined (.env file can be used)"
            )
        self.job_params = json.loads(self.job_params)
        if type(self.job_params) is not dict:
            raise Exception(
                f"Valid json, expected in {env_map['job_params']}, is an object with the job creation spec"
            )

        self.max_jobs = os.getenv(env_map["max_jobs"])
        if self.max_jobs is None:
            raise Exception(f"{env_map['max_jobs']} env var is not defined (.env file can be used)")
        self.max_jobs = int(self.max_jobs)

        self.cols = []
        self.pods = {}
        self.logs = {}


class ContinuousCollectorTasks(CollectorTasksBase):
    """ContinuousCollectorTasks defines the tasks to run when test of a simple flow of creation, check and
    delete continuous collectors
    """

    def __init__(self, *args, **kwargs):
        kwargs["env"] = {
            "token": "DEVO_STANDALONE_TOKEN",
            "coll_recipe": "CONTINUOUS_COLL_RECIPE_JSON",
            "job_spec": "CONTINUOUS_JOB_SPEC",
            "job_params": "CONTINUOUS_JOB_PARAMS",
            "max_jobs": "CONTINUOUS_JOB_MAX",
        }
        super().__init__(*args, **kwargs)

    @task(1)
    def get_collector_catalog(self):
        self.client.get("/get_collector_recipes", headers=self.headers)

    @task(1)
    def get_collector_recipe_versions(self):
        self.client.get(
            f'/get_collector_recipe_versions?collectorName={self.coll_recipe["name"]}'
            + f'&channel={self.coll_recipe["channel"]}',
            name="/get_collector_recipe_versions",
            headers=self.headers,
        )

    @task(1)
    def get_collector_recipe_meta(self):
        self.client.get(
            f'/get_collector_recipe_meta?collectorName={self.coll_recipe["name"]}&'
            + f'version={self.coll_recipe["version"]}&channel={self.coll_recipe["channel"]}',
            name="/get_collector_recipe_meta",
            headers=self.headers,
        )

    @task(2)
    def create_collector(self):
        if len(self.cols) >= self.max_jobs:
            logging.warn(
                "ContinuousCollectorTasks.create_collector: Max number of collectors "
                + f"({self.max_jobs}) reached skipping. Cols {self.cols}"
            )
            return

        # Ensure some values are valid
        now = datetime.datetime.now().strftime("%Y%m%dT%H%M%S")
        data = {
            **self.job_spec,
            **{
                "job-type": "continuous",
                "job-name": f"{self.coll_recipe['name']}-{now}",
                "job-params": json.dumps(self.job_params),
            },
        }

        resp = self.client.post(
            "/create_job", name="/create_continuous_job", headers=self.headers, json=data
        )
        if resp.ok:
            self.cols.append(resp.json()["collectorId"])
        else:
            logging.error(
                f"ContinuousCollectorTasks.create_collector: reason: {resp.reason}, payload: {resp.text}"
            )

    @task(6)
    def get_collector_pods(self):
        # Collector not created yet
        if len(self.cols) == 0:
            logging.info("ContinuousCollectorTasks.get_collector_pods: No collector created yet")
            return

        col = self.cols[0]

        resp = self.client.get(
            f"/get_collector_pods?collectorId={col}",
            headers=self.headers,
            name="/get_collector_continuous_pods",
        )
        if resp.ok:
            pod_list = resp.json()
            if pod_list:
                self.pods[col] = pod_list[0]["id"]
            else:
                logging.warning(
                    f"ContinuousCollectorTasks.get_collector_pods: collector {col} without any pod ready"
                )

        else:
            logging.error(
                f"ContinuousCollectorTasks.get_collector_pods: reason: {resp.reason}, payload: {resp.text}"
            )

    @task(6)
    def get_pod_log(self):
        # Collector not created yet
        if len(self.cols) == 0:
            logging.info("ContinuousCollectorTasks.get_pod_log: No collector created yet")
            return

        col = self.cols[0]
        pod = self.pods.get(col)
        # Pod is not ready yet
        if pod is None:
            logging.info(
                f"ContinuousCollectorTasks.get_pod_log: No pod scanned yet for collector {col}"
            )
            return

        resp = self.client.get(
            f"/get_collector_pod_logs?collectorId={col}&podId={pod}"
            + "&sinceSeconds=0&includeTimestamps=true",
            headers=self.headers,
            name="/get_collector_continuous_logs",
        )
        if resp.ok:
            self.logs[pod] = True
        else:
            logging.error(
                f"ContinuousCollectorTasks.get_pod_log: reason: {resp.reason}, payload: {resp.text}"
            )

    @task(2)
    def delete_coll(self):
        # Collector not created yet
        if len(self.cols) == 0:
            logging.info("ContinuousCollectorTasks.delete_coll: No collector created yet")
            return

        col = self.cols[0]
        pod = self.pods.get(col)
        # Pod is not ready yet
        if pod is None:
            logging.info(
                f"ContinuousCollectorTasks.delete_coll: No pod scanned yet for collector {col}"
            )
            return

        log = self.logs.get(pod, False)
        if not log:
            logging.info(
                f"ContinuousCollectorTasks.delete_coll: Logs of {col} and {pod} no downloaded yet"
            )
            return

        resp = self.client.delete(
            f"/delete_collector/{col}?collectorType=continuous",
            headers=self.headers,
            name="/delete_continuous_job",
        )
        if resp.ok:
            self.cols.pop(0)
            del self.pods[col]
            del self.logs[pod]
        else:
            logging.error(
                f"ContinuousCollectorTasks.delete_coll: reason: {resp.reason}, payload: {resp.text}"
            )

    def on_stop(self):
        # Deleting remaining collectors
        logging.info(f"ContinuousCollectorTasks: Deleting remaining collectors: {self.cols}")
        while len(self.cols) > 0:
            col = self.cols.pop()
            self.client.delete(
                f"/delete_collector/{col}?collectorType=continuous",
                headers=self.headers,
                name="/delete_remaining_continuous_job",
            )


class ContinuousCollector(HttpUserConfigurable):
    wait_time = between(1, 5)
    weight = 3
    tasks = [ContinuousCollectorTasks]


class ScheduledCollectorTasks(CollectorTasksBase):
    """ScheduledCollectorTasks defines the tasks to run when test of a simple flow of creation, check and
    delete scheduled collectors
    """

    def __init__(self, *args, **kwargs):
        kwargs["env"] = {
            "token": "DEVO_STANDALONE_TOKEN",
            "coll_recipe": "SCHEDULED_COLL_RECIPE_JSON",
            "job_spec": "SCHEDULED_JOB_SPEC",
            "job_params": "SCHEDULED_JOB_PARAMS",
            "max_jobs": "SCHEDULED_JOB_MAX",
        }
        super().__init__(*args, **kwargs)

    @task(1)
    def get_collector_catalog(self):
        self.client.get("/get_collector_recipes", headers=self.headers)

    @task(1)
    def get_collector_recipe_versions(self):
        self.client.get(
            f'/get_collector_recipe_versions?collectorName={self.coll_recipe["name"]}'
            + f'&channel={self.coll_recipe["channel"]}',
            name="/get_collector_recipe_versions",
            headers=self.headers,
        )

    @task(1)
    def get_collector_recipe_meta(self):
        self.client.get(
            f'/get_collector_recipe_meta?collectorName={self.coll_recipe["name"]}&'
            + f'version={self.coll_recipe["version"]}&channel={self.coll_recipe["channel"]}',
            name="/get_collector_recipe_meta",
            headers=self.headers,
        )

    @task(2)
    def create_collector(self):
        if len(self.cols) >= self.max_jobs:
            logging.warn(
                "ScheduledCollectorTasks.create_collector: Max number of "
                + f"collectors ({self.max_jobs}) reached skipping. Cols {self.cols}"
            )
            return

        # Ensure some values are valid
        now = datetime.datetime.now().strftime("%Y%m%dT%H%M%S")
        data = {
            **self.job_spec,
            **{
                "job-type": "scheduled",
                "job-name": f"{self.coll_recipe['name']}-{now}",
                "job-params": json.dumps(self.job_params),
            },
        }

        resp = self.client.post(
            "/create_job", name="/create_scheduled_job", headers=self.headers, json=data
        )
        if resp.ok:
            self.cols.append(resp.json()["collectorId"])
        else:
            logging.error(
                f"ScheduledCollectorTasks.create_collector: reason: {resp.reason}, payload: {resp.text}"
            )

    @task(6)
    def get_collector_pods(self):
        # Collector not created yet
        if len(self.cols) == 0:
            logging.info("ScheduledCollectorTasks.get_collector_pods: No collector created yet")
            return

        col = self.cols[0]

        resp = self.client.get(
            f"/get_collector_pods?collectorId={col}",
            headers=self.headers,
            name="/get_collector_scheduled_pods",
        )
        if resp.ok:
            pod_list = resp.json()
            if pod_list:
                self.pods[col] = pod_list[0]["id"]
            else:
                logging.warning(
                    f"ScheduledCollectorTasks.get_collector_pods: collector {col} without any pod ready"
                )

        else:
            logging.error(
                f"ScheduledCollectorTasks.get_collector_pods: reason: {resp.reason}, payload: {resp.text}"
            )

    @task(6)
    def get_pod_log(self):
        # Collector not created yet
        if len(self.cols) == 0:
            logging.info("ScheduledCollectorTasks.get_pod_log: No collector created yet")
            return

        col = self.cols[0]
        pod = self.pods.get(col)
        # Pod is not ready yet
        if pod is None:
            logging.info(
                f"ScheduledCollectorTasks.get_pod_log: No pod scanned yet for collector {col}"
            )
            return

        resp = self.client.get(
            f"/get_collector_pod_logs?collectorId={col}&podId={pod}"
            + "&sinceSeconds=0&includeTimestamps=true",
            headers=self.headers,
            name="/get_collector_scheduled_logs",
        )
        if resp.ok:
            self.logs[pod] = True
        else:
            logging.error(
                f"ScheduledCollectorTasks.get_pod_log: reason: {resp.reason}, payload: {resp.text}"
            )

    @task(2)
    def delete_coll(self):
        # Collector not created yet
        if len(self.cols) == 0:
            logging.info("ScheduledCollectorTasks.delete_coll: No collector created yet")
            return

        col = self.cols[0]
        pod = self.pods.get(col)
        # Pod is not ready yet
        if pod is None:
            logging.info(
                f"ScheduledCollectorTasks.delete_coll: No pod scanned yet for collector {col}"
            )
            return

        log = self.logs.get(pod, False)
        if not log:
            logging.info(
                f"ScheduledCollectorTasks.delete_coll: Logs of {col} and {pod} no downloaded yet"
            )
            return

        resp = self.client.delete(
            f"/delete_collector/{col}?collectorType=scheduled",
            headers=self.headers,
            name="/delete_scheduled_job",
        )
        if resp.ok:
            self.cols.pop(0)
            del self.pods[col]
            del self.logs[pod]
        else:
            logging.error(
                f"ScheduledCollectorTasks.delete_coll: reason: {resp.reason}, payload: {resp.text}"
            )

    def on_stop(self):
        # Deleting remaining collectors
        logging.info(f"ScheduledCollectorTasks: Deleting remaining collectors: {self.cols}")
        while len(self.cols) > 0:
            col = self.cols.pop()
            self.client.delete(
                f"/delete_collector/{col}?collectorType=scheduled",
                headers=self.headers,
                name="/delete_remaining_scheduled_job",
            )


class ScheduledCollector(HttpUserConfigurable):
    wait_time = between(1, 5)
    weight = 3
    tasks = [ScheduledCollectorTasks]
