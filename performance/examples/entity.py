from locust import HttpUser, task, between


class EntityMethods(HttpUser):
    wait_time = between(1, 5)

    @task
    def healthz(self):
        self.client.get("/entity/healthz")

    @task
    def apidocs(self):
        self.client.get("/entity/apidocs/#/")

    @task
    def apispec(self):
        self.client.get("/apispec_1.json")
