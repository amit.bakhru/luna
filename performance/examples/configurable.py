import logging

from dotenv import load_dotenv
from locust import HttpUser


class HttpUserConfigurable(HttpUser):
    abstract = True

    def __init__(self, *args, **kwargs):
        found = load_dotenv()
        if found:
            logging.info("Custom .env file loaded")

        super().__init__(*args, **kwargs)
