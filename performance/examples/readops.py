import json
import os

from locust import task, between

from performance.configurable import HttpUserConfigurable


class UserDomain(HttpUserConfigurable):
    wait_time = between(1, 10)
    weight = 3

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        token = os.getenv("DEVO_STANDALONE_TOKEN")
        if token is None:
            raise Exception("DEVO_STANDALONE_TOKEN env var is not defined (.env file can be used)")

        self.headers = {"standAloneToken": token}

        # self.domain = os.getenv("CS_DOMAIN")
        # if self.token is None:
        #     raise Exception("CS_DOMAIN env var is not defined (.env file can be used)")

    @task
    def list_domains(self):
        self.client.get("/list_domains", headers=self.headers)

    @task
    def domain_initialized(self):
        self.client.get("/domain_initialized", headers=self.headers)

    @task
    def list_scheduled_jobs(self):
        self.client.get("/list_scheduled_jobs", headers=self.headers)

    @task
    def list_continuous_jobs(self):
        self.client.get("/list_continuous_jobs", headers=self.headers)

    @task
    def get_collector_recipes(self):
        self.client.get("/get_collector_recipes", headers=self.headers)

    @task
    def keychains_list(self):
        self.client.get("/keychains/list", headers=self.headers)


class CollectorsRecipe(HttpUserConfigurable):
    wait_time = between(1, 10)
    weight = 1

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        token = os.getenv("DEVO_STANDALONE_TOKEN")
        if token is None:
            raise Exception("DEVO_STANDALONE_TOKEN env var is not defined (.env file can be used)")

        self.headers = {"standAloneToken": token}

        self.recipes = os.getenv("COL_RECIPES_JSON")
        if self.recipes is None:
            raise Exception("COL_RECIPES_JSON env var is not defined (.env file can be used)")

        self.recipes = json.loads(self.recipes)
        if type(self.recipes) is not list:
            raise Exception(
                "Valid json with array of {name: NAME, version: VERSION} expected in COL_RECIPES_JSON"
            )

        for recipe in self.recipes:
            if type(recipe) is not dict and "name" not in recipe and "version" not in recipe:
                raise Exception(
                    f"Value with invalid type or name or version in {recipe} recipe (COL_RECIPES_JSON)"
                )

    @task
    def get_collector_recipe_meta(self):
        for recipe in self.recipes:
            self.client.get(
                f'/get_collector_recipe_meta?collectorName={recipe["name"]}&version={recipe["version"]}&'
                + "channel=official",
                name="/get_collector_recipe_meta",
                headers=self.headers,
            )

    @task
    def get_collector_recipe_versions(self):
        for recipe in self.recipes:
            self.client.get(
                f'/get_collector_recipe_versions?collectorName={recipe["name"]}',
                name="/get_collector_recipe_versions",
                headers=self.headers,
            )
