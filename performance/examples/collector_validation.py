import datetime
import json
import logging
import os

from configurable import HttpUserConfigurable
from locust import task, between
from management.collector import CollectorTasksBase


class ValidationCollectorTasks(CollectorTasksBase):
    """ValidationCollectorTasks defines the tasks to run when test of a simple flow of creation, check and
    delete validation collector
    """

    def __init__(self, *args, **kwargs):
        kwargs["env"] = {
            "token": "DEVO_STANDALONE_TOKEN",
            "coll_recipe": "VALIDATION_COLL_RECIPE_JSON",
            "job_spec": "VALIDATION_JOB_SPEC",
            "job_params": "VALIDATION_JOB_PARAMS",
            "max_jobs": "__dummy_validation_max_jobs",
        }
        # max_jobs is required by CollectorTasksBase, but ignored in our tasks
        os.environ["__dummy_validation_max_jobs"] = "1"
        super().__init__(*args, **kwargs)

        self.domain = os.getenv("VALIDATON_DOMAIN")

        # Remove unused props from supper
        del self.cols
        del self.pods
        del self.logs

    @task(1)
    def get_collector_catalog(self):
        self.client.get("/get_collector_recipes", headers=self.headers)

    @task(1)
    def get_collector_recipe_versions(self):
        self.client.get(
            f'/get_collector_recipe_versions?collectorName={self.coll_recipe["name"]}'
            + f'&channel={self.coll_recipe["channel"]}',
            name="/get_collector_recipe_versions",
            headers=self.headers,
        )

    @task(1)
    def get_collector_recipe_meta(self):
        self.client.get(
            f'/get_collector_recipe_meta?collectorName={self.coll_recipe["name"]}&'
            + f'version={self.coll_recipe["version"]}&channel={self.coll_recipe["channel"]}',
            name="/get_collector_recipe_meta",
            headers=self.headers,
        )

    @task(2)
    def create_collector(self):
        # Ensure some values are valid
        now = datetime.datetime.now().strftime("%Y%m%dT%H%M%S")
        data = {
            **self.job_spec,
            **{
                "delete-job-if-timeout": True,
                "job-name": f"{self.coll_recipe['name']}-{now}",
                "job-params": json.dumps(self.job_params),
            },
        }

        self.client.post(
            "/validate_collector_creds_synchro",
            name="/validate_collector_creds_synchro",
            headers=self.headers,
            json=data,
        )

    def on_stop(self):
        if self.domain:
            logging.info("ValidationCollectorTasks. loading all validation jobs to PURGE it")
            resp = self.client.get(
                f"/{self.domain}/get_validation_jobs",
                headers=self.headers,
                name="/get_validation_jobs",
            )

            if resp.ok:
                jobs = [i["job_id"] for i in resp.json()]

                # Deleting remaining jobs
                logging.info(f"ValidationCollectorTasks: Deleting validation jobs: {jobs}")
                logging.error(
                    "FIXME: There is a bug in management-api while delete validation jobs, because it is not "
                    + "supported to check api_creation for validation jobs type"
                )
                return
                while len(jobs) > 0:
                    col = jobs.pop()
                    self.client.delete(
                        f"/delete_collector/{col}?collectorType=validation",
                        headers=self.headers,
                        name="/delete_remaining_validation_job",
                    )


class ValidationCollector(HttpUserConfigurable):
    wait_time = between(1, 5)
    weight = 3
    tasks = [ValidationCollectorTasks]
