## Luna Performance Test Tool

#### References:
- [Grafana Dashboard](https://grafana.com/grafana/dashboards/10878-locust/)
- [locust](https://locust.io/)
- [locust-plugins](https://github.com/SvenskaSpel/locust-plugins)
- Maybe use [locust_exporter](https://github.com/ContainerSolutions/locust_exporter) for exporting locust metrics to prometheus storage
- minikube + locust + helm https://github.com/KimSoungRyoul/locust-plugins-helm


### Grafana Dashboards
- import the following grafana dashboards
```bash
# k8s cluster - https://grafana.com/grafana/dashboards/15661-1-k8s-for-prometheus-dashboard-20211010/
15661
# AWS KAFKA dashboard topic - https://grafana.com/grafana/dashboards/12010-aws-kafka-topic/
12010
# Apache Spark - https://grafana.com/grafana/dashboards/7890-spark-performance-metrics/
7890
# EKS Cluster Workload namespace: https://grafana.com/grafana/dashboards/17288-kubernetes-compute-resources-namespace-workloads/
17288
# https://grafana.com/grafana/dashboards/12740-kubernetes-monitoring/
12740
# https://grafana.com/grafana/dashboards/15758-kubernetes-views-namespaces/
15758
```
