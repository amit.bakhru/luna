#!/usr/bin/env python

import time
from typing import List, Tuple

from locust import task
from performance.testplans.base_api_test import BaseApiTest

from performance.testplans import (
    CREATE_STREAM,
    EDIT_STREAM,
    GET_STREAM_FILTERS,
    GET_STREAMS,
    GET_STREAM_STATES,
    EDIT_STREAMS_IN_BULK,
    RENAME_STREAM,
    DELETE_STREAM,
    PAUSE_STREAM,
    RESUME_STREAM,
    RERUN_BATCHES,
    GET_BATCHES,
    RERUN_ERROR_BATCHES,
    CANCEL_QUEUED_AND_SCHEDULED_BATCHES,
    CREATE_BATCH_FOR_ON_DEMAND_STREAM,
    GET_SINGLE_BATCH_DETAILS,
    GET_SINGLE_BATCH_CORRELATION,
    GET_NODE_DETAILS,
    REPROCESS_SINGLE_BATCH,
)


class BaseStream:
    API_BASE = "/case-management/api"

    abstract = True

    STREAM_BASE_PAYLOAD = {
        "baselineNode": None,
        "batchIntervalMillis": 900000,
        "correlationsEnabled": False,
        "cron": None,
        "executionDelayInMins": 0,
        "exportDestination": None,
        "exportEnabled": False,
        "exportMaxEvents": None,
        "exportScoreThreshold": None,
        "frequencyInMillis": 900000,
        "maxRetryCount": 0,
        "numHistoricalIntervalsForBaseline": None,
        "onDemandEnabled": False,
        "overlapIntervalMillis": 0,
        "pausedOnBatchFailure": False,
        "timeoutInMillis": None,
        "userPreference": {
            "batchIntervalMillis": "minutes",
            "frequencyInMillis": "minutes",
            "executionDelayInMins": "minutes",
        },
    }

    BATCH_BASE_PAYLOAD = {
        "status": [],
        "excludeBatchesWithZeroEvents": False,
    }

    STREAM_OPERATION_BASE_PAYLOAD = {
        "entityIds": [],
        "folderId": "cf69202f-75e7-462e-b5a1-91a03b3c0ba8",
    }

    GET_STREAM_BASE_PAYLOAD = {
        "filters": [],
        "offset": 0,
        "pageSize": 25,
        "sortColumn": "lastUpdated",
        "sortOrder": "DESC",
    }

    create_playbook_payload = {"method": "addFlow", "parameters": {"name": ""}}

    add_node_sql_operation_payload = {
        "description": "",
        "id": None,
        "isLeaf": True,
        "isNew": True,
        "kind": "augmentation",
        "lql": "SELECT 'John' AS username, 'login' AS event_name, TRUE AS isEvent, 110.0 AS source_value, 81 AS user_id_num FROM _Start_Node\nUNION\nSELECT 'Jane' AS username, 'logout' AS event_name, TRUE AS isEvent, 120.5 AS source_value, 82 AS user_id_num FROM _Start_Node\nUNION\nSELECT 'Alice' AS username, 'login' AS event_name, FALSE AS isEvent, 99.8 AS source_value, 83 AS user_id_num FROM _Start_Node\nUNION\nSELECT 'Bob' AS username, 'purchase' AS event_name, TRUE AS isEvent, 300.0 AS source_value, 84 AS user_id_num FROM _Start_Node\nUNION\nSELECT 'Charlie' AS username, 'logout' AS event_name, FALSE AS isEvent, 105.7 AS source_value, 85 AS user_id_num FROM _Start_Node\nUNION\nSELECT 'Dave' AS username, 'login' AS event_name, TRUE AS isEvent, 115.2 AS source_value, 86 AS user_id_num FROM _Start_Node\nUNION\nSELECT 'Eve' AS username, 'purchase' AS event_name, TRUE AS isEvent, 500.0 AS source_value, 87 AS user_id_num FROM _Start_Node\nUNION\nSELECT 'Frank' AS username, 'login' AS event_name, FALSE AS isEvent, 200.0 AS source_value, 88 AS user_id_num FROM _Start_Node\nUNION\nSELECT 'Grace' AS username, 'logout' AS event_name, TRUE AS isEvent, 50.5 AS source_value, 89 AS user_id_num FROM _Start_Node\nUNION\nSELECT 'Hank' AS username, 'login' AS event_name, TRUE AS isEvent, 250.0 AS source_value, 90 AS user_id_num FROM _Start_Node;\n",
        "name": "SQL_Operation",
        "title": "SQL Operation",
        "x": 602,
        "y": 112,
    }

    add_output_node_payload = {
        "isLeaf": True,
        "kind": "output",
        "name": "Output",
        "nodes": [""],
        "x": 0,
        "y": 80,
    }

    delete_playbook_payload = [{"entityTypeId": "playbook", "id": ""}]

    create_stream_payload = {
        "method": "addStream",
        "parameters": {
            **STREAM_BASE_PAYLOAD,
            "flow": "",
            "lastUpdate": None,
            "newBatchesCount": 0,
            "state": None,
            "name": "",
        },
    }

    edit_stream_payload = {
        **STREAM_BASE_PAYLOAD,
        "frequencyInMillis": 60000,
        "maxRetryCount": 1,
        "name": "",
    }

    get_stream_payload = {**GET_STREAM_BASE_PAYLOAD}

    get_stream_states_payload = {"streams": []}

    edit_streams_in_bulk_template = {**STREAM_BASE_PAYLOAD, "id": ""}

    edit_streams_in_bulk_payload = {"streams": []}

    copy_stream_payload = {
        **STREAM_OPERATION_BASE_PAYLOAD,
        "entityIds": [{"id": "", "entityTypeId": "stream"}],
    }

    copy_streams_in_bulk_payload = {**STREAM_OPERATION_BASE_PAYLOAD}

    rename_stream_payload = {"entityId": {"id": "", "entityTypeId": "stream"}, "name": ""}

    delete_stream_payload = [{"id": "0", "entityTypeId": "stream"}]

    delete_streams_in_bulk_payload = []

    resume_stream_payload = []

    resume_stream_in_bulk_payload = []

    pause_stream_payload = []

    pause_stream_in_bulk_payload = []

    get_batches_payload = {**BATCH_BASE_PAYLOAD}

    rerun_batches_payload = {**BATCH_BASE_PAYLOAD, "batchIds": [], "isAll": False}

    rerun_error_batches_payload = {"method": "rescheduleAll", "parameters": {}}

    reprocess_single_batch_payload = {"method": "reschedule", "parameters": {}}


class PlaybookDetailsPageFromStream(BaseApiTest, BaseStream):
    current_time_millis = 0
    fifteen_minutes_back_millis = 0

    # TODO need common flow id for all the stream requests
    def on_start(self) -> None:
        super().on_start()
        self.current_time_millis = int(time.time() * 1000)
        self.fifteen_minutes_back_millis = self.current_time_millis - (15 * 60 * 1000)
        self.flow_id = self.pre_requisite_create_playbook_with_sql_and_output_node()
        self.node_id = self.pre_requisite_create_sql_node()
        self.pre_requisite_create_output_node()

    def on_stop(self) -> None:
        self.clean_up_delete_playbook(self.flow_id)

    def pre_requisite_create_playbook_with_sql_and_output_node(self) -> str:
        self.create_playbook_payload["parameters"][
            "name"
        ] = f"new_playbook_{self.generate_random_number()}"
        response = self._make_request(
            self.API_BASE, "post", "/demo/hub", json=self.create_playbook_payload
        )
        return response.json().get("result", {}).get("id") if response.status_code == 200 else ""

    def pre_requisite_create_sql_node(self) -> str:
        response = self.make_request(
            self.API_BASE,
            "post",
            f"/flow/{self.flow_id}/node?from={self.fifteen_minutes_back_millis}&to={self.current_time_millis}",
            json=self.add_node_sql_operation_payload,
        )
        return (
            response.json().get("result", {}).get("nodes", [])[-1].get("id")
            if response.status_code == 200
            else ""
        )

    def pre_requisite_create_output_node(self) -> None:
        self.add_output_node_payload["nodes"][0] = self.node_id
        self.make_request(
            self.API_BASE, "post", f"/flow/{self.flow_id}/node", json=self.add_output_node_payload
        )

    def clean_up_delete_playbook(self, flow_id: str) -> None:
        self.delete_playbook_payload[0]["id"] = self.fetch_numeric_part(flow_id, "flow-")
        self._make_request(
            self.API_BASE, "post", "/content-management/delete", json=self.delete_playbook_payload
        )

    def clean_up_delete_stream(self, stream_id: str) -> None:
        self.delete_stream_payload[0]["id"] = stream_id.replace("stream-", "")
        self._make_request(
            self.API_BASE, "post", "/content-management/delete", json=self.delete_stream_payload
        )

    @task(2)
    def create_stream_with_run_frequency(self):
        self.create_stream_payload["parameters"]["flow"] = self.flow_id
        self.create_stream_payload["parameters"][
            "name"
        ] = f"new_stream_{self.generate_random_number()}"
        response = self.client.post(
            CREATE_STREAM,
            headers=self.get_headers(),
            json=self.create_stream_payload,
        )
        if response.status_code == 200:
            stream_id = response.json().get("result", {}).get("id")
            self.clean_up_delete_stream(stream_id)

    @task(2)
    def create_on_demand_stream(self):
        self.create_stream_payload["parameters"]["flow"] = self.flow_id
        self.create_stream_payload["parameters"][
            "name"
        ] = f"new_stream_{self.generate_random_number()}"
        self.create_stream_payload["parameters"]["frequencyInMillis"] = None
        self.create_stream_payload["parameters"]["onDemandEnabled"] = True
        response = self.client.post(
            CREATE_STREAM,
            headers=self.get_headers(),
            json=self.create_stream_payload,
        )
        if response.status_code == 200:
            stream_id = response.json().get("result", {}).get("id")
            self.clean_up_delete_stream(stream_id)


class StreamListingPage(BaseApiTest, BaseStream):
    current_time_millis = 0
    fifteen_minutes_back_millis = 0

    # TODO need common flow id for all the stream requests
    def on_start(self) -> None:
        super().on_start()
        self.current_time_millis = int(time.time() * 1000)
        self.fifteen_minutes_back_millis = self.current_time_millis - (15 * 60 * 1000)
        self.flow_id = self.pre_requisite_create_playbook_with_sql_and_output_node()
        self.node_id = self.pre_requisite_create_sql_node()
        self.pre_requisite_create_output_node()
        self.stream_id = self.pre_requisite_create_stream()
        self.stream_id_numeric_part = self.stream_id.replace("stream-", "")
        self.stream_ids = []
        for i in range(3):
            self.stream_ids.append(self.pre_requisite_create_stream())
        self.batch_ids = self.pre_requisite_get_batches()

    def on_stop(self) -> None:
        for cleanup in [
            (self.clean_up_delete_stream, self.stream_id),
            (self.clean_up_delete_streams_in_bulk, self.stream_ids),
            (self.clean_up_delete_playbook, self.flow_id),
        ]:
            cleanup[0](cleanup[1])

    def pre_requisite_create_playbook_with_sql_and_output_node(self) -> str:
        self.create_playbook_payload["parameters"][
            "name"
        ] = f"new_playbook_{self.generate_random_number()}"
        response = self._make_request(
            self.API_BASE, "post", "/demo/hub", json=self.create_playbook_payload
        )
        return response.json().get("result", {}).get("id") if response.status_code == 200 else ""

    def pre_requisite_create_sql_node(self) -> str:
        response = self.make_request(
            self.API_BASE,
            "post",
            f"/flow/{self.flow_id}/node?from={self.fifteen_minutes_back_millis}&to={self.current_time_millis}",
            json=self.add_node_sql_operation_payload,
        )
        return (
            response.json().get("result", {}).get("nodes", [])[-1].get("id")
            if response.status_code == 200
            else ""
        )

    def pre_requisite_create_output_node(self) -> None:
        self.add_output_node_payload["nodes"][0] = self.node_id
        self.make_request(
            self.API_BASE, "post", f"/flow/{self.flow_id}/node", json=self.add_output_node_payload
        )

    def pre_requisite_create_stream(self) -> str:
        self.create_stream_payload["parameters"]["flow"] = self.flow_id
        self.create_stream_payload["parameters"][
            "name"
        ] = f"new_stream_{self.generate_random_number()}"
        response = self._make_request(
            self.API_BASE, "post", "/demo/hub", json=self.create_stream_payload
        )
        return response.json().get("result", {}).get("id") if response.status_code == 200 else ""

    def pre_requisite_get_batches(self) -> List[str]:
        response = self.make_request(
            self.API_BASE,
            "post",
            f"/stream/{self.stream_id}/batches/filter?pageSize=25&after=0",
            json=self.get_batches_payload,
        )
        response_data_array = response.json().get("result", {}).get("data", [])
        return [item.get("id") for item in response_data_array[:5] if item.get("id") is not None]

    def clean_up_delete_playbook(self, flow_id: str) -> None:
        self.delete_playbook_payload[0]["id"] = self.fetch_numeric_part(flow_id, "flow-")
        self._make_request(
            self.API_BASE, "post", "/content-management/delete", json=self.delete_playbook_payload
        )

    def clean_up_delete_stream(self, stream_id: str) -> None:
        self.delete_stream_payload[0]["id"] = stream_id.replace("stream-", "")
        self._make_request(
            self.API_BASE, "post", "/content-management/delete", json=self.delete_stream_payload
        )

    def clean_up_delete_streams_in_bulk(self, stream_ids: List[str]) -> None:
        self.delete_streams_in_bulk_payload = []
        for stream_id in stream_ids:
            self.delete_streams_in_bulk_payload.append(
                {"id": self.fetch_numeric_part(stream_id, ""), "entityTypeId": "stream"}
            )
        self._make_request(
            self.API_BASE,
            "post",
            "/content-management/delete",
            json=self.delete_streams_in_bulk_payload,
        )

    @task(5)
    def edit_stream(self):
        self.edit_stream_payload["name"] = f"update_stream_{self.generate_random_number()}"
        self.client.patch(
            EDIT_STREAM.format(stream_id=self.stream_id),
            headers=self.get_headers(),
            json=self.edit_stream_payload,
        )

    @task(9)
    def get_stream_filters(self):
        self.client.get(GET_STREAM_FILTERS, headers=self.get_headers())

    @task(9)
    def get_streams(self):
        self.client.post(
            GET_STREAMS,
            headers=self.get_headers(),
            json=self.get_stream_payload,
        )

    @task(4)
    def get_streams_playbook_filter(self):
        self.get_stream_payload["filters"] = [{"playbooks": [self.flow_id]}]
        self.client.post(
            GET_STREAMS,
            headers=self.get_headers(),
            json=self.get_stream_payload,
        )

    @task(4)
    def get_streams_status_filter(self):
        self.get_stream_payload["filters"] = [{"statuses": ["ready"]}]
        self.client.post(
            GET_STREAMS,
            headers=self.get_headers(),
            json=self.get_stream_payload,
        )

    @task(4)
    def get_streams_owner_filter(self):
        self.get_stream_payload["filters"] = [{"owners": ["indrajeetkumar.sah@devo.com"]}]
        self.client.post(
            GET_STREAMS,
            headers=self.get_headers(),
            json=self.get_stream_payload,
        )

    @task(7)
    def get_streams_search_value(self):
        self.get_stream_payload["filters"] = [{"searchText": "stream"}]
        self.client.post(
            GET_STREAMS,
            headers=self.get_headers(),
            json=self.get_stream_payload,
        )

    @task(3)
    def get_streams_name_sorted_ascending(self):
        self.get_stream_payload["sortColumn"] = "name"
        self.get_stream_payload["sortOrder"] = "ASC"
        self.client.post(
            GET_STREAMS,
            headers=self.get_headers(),
            json=self.get_stream_payload,
        )

    @task(3)
    def get_streams_name_sorted_descending(self):
        self.get_stream_payload["sortColumn"] = "name"
        self.get_stream_payload["sortOrder"] = "DESC"
        self.client.post(
            GET_STREAMS,
            headers=self.get_headers(),
            json=self.get_stream_payload,
        )

    @task(3)
    def get_streams_stream_status_sorted_ascending(self):
        self.get_stream_payload["sortColumn"] = "streamStatus"
        self.get_stream_payload["sortOrder"] = "ASC"
        self.client.post(
            GET_STREAMS,
            headers=self.get_headers(),
            json=self.get_stream_payload,
        )

    @task(3)
    def get_streams_stream_status_sorted_descending(self):
        self.get_stream_payload["sortColumn"] = "streamStatus"
        self.get_stream_payload["sortOrder"] = "DESC"
        self.client.post(
            GET_STREAMS,
            headers=self.get_headers(),
            json=self.get_stream_payload,
        )

    @task(3)
    def get_streams_batch_length_sorted_ascending(self):
        self.get_stream_payload["sortColumn"] = "batchLength"
        self.get_stream_payload["sortOrder"] = "ASC"
        self.client.post(
            GET_STREAMS,
            headers=self.get_headers(),
            json=self.get_stream_payload,
        )

    @task(3)
    def get_streams_batch_length_sorted_descending(self):
        self.get_stream_payload["sortColumn"] = "batchLength"
        self.get_stream_payload["sortOrder"] = "DESC"
        self.client.post(
            GET_STREAMS,
            headers=self.get_headers(),
            json=self.get_stream_payload,
        )

    @task(3)
    def get_streams_origin_playbook_sorted_ascending(self):
        self.get_stream_payload["sortColumn"] = "originPlaybook"
        self.get_stream_payload["sortOrder"] = "ASC"
        self.client.post(
            GET_STREAMS,
            headers=self.get_headers(),
            json=self.get_stream_payload,
        )

    @task(3)
    def get_streams_origin_playbook_sorted_descending(self):
        self.get_stream_payload["sortColumn"] = "originPlaybook"
        self.get_stream_payload["sortOrder"] = "DESC"
        self.client.post(
            GET_STREAMS,
            headers=self.get_headers(),
            json=self.get_stream_payload,
        )

    @task(3)
    def get_streams_owner_sorted_ascending(self):
        self.get_stream_payload["sortColumn"] = "owner"
        self.get_stream_payload["sortOrder"] = "ASC"
        self.client.post(
            GET_STREAMS,
            headers=self.get_headers(),
            json=self.get_stream_payload,
        )

    @task(3)
    def get_streams_owner_sorted_descending(self):
        self.get_stream_payload["sortColumn"] = "owner"
        self.get_stream_payload["sortOrder"] = "DESC"
        self.client.post(
            GET_STREAMS,
            headers=self.get_headers(),
            json=self.get_stream_payload,
        )

    @task(3)
    def get_streams_last_updated_sorted_ascending(self):
        self.get_stream_payload["sortColumn"] = "lastUpdated"
        self.get_stream_payload["sortOrder"] = "ASC"
        self.client.post(
            GET_STREAMS,
            headers=self.get_headers(),
            json=self.get_stream_payload,
        )

    @task(3)
    def get_streams_last_updated_sorted_descending(self):
        self.get_stream_payload["sortColumn"] = "lastUpdated"
        self.get_stream_payload["sortOrder"] = "DESC"
        self.client.post(
            GET_STREAMS,
            headers=self.get_headers(),
            json=self.get_stream_payload,
        )

    @task(9)
    def get_stream_states(self):
        self.get_stream_states_payload["streams"] = self.stream_ids
        self.client.post(
            GET_STREAM_STATES,
            headers=self.get_headers(),
            json=self.get_stream_states_payload,
        )

    @task(3)
    def edit_streams_in_bulk(self):
        self.edit_streams_in_bulk_payload["streams"] = [
            {**self.edit_streams_in_bulk_template, "id": stream_id} for stream_id in self.stream_ids
        ]
        self.client.patch(
            EDIT_STREAMS_IN_BULK,
            headers=self.get_headers(),
            json=self.edit_streams_in_bulk_payload,
        )

    # @task(2)
    # def copy_stream(self):
    #     #TODO folder id is static as it will be deprecated later on
    #     self.copy_stream_payload["entityId"][0]["id"] = self.stream_id_numeric_part
    #     self.copy_stream_payload["name"] = f"renamed_stream_{self.generate_random_number()}"
    #     self.client.post(COPY_STREAM, headers=self.get_headers(), json=self.copy_stream_payload)
    #
    # @task(2)
    # def copy_streams_in_bulk(self):
    #     #TODO folder id is static as it will be deprecated later on
    #     self.copy_streams_in_bulk_payload["entityIds"] = []
    #     for stream_id in self.stream_ids:
    #         self.copy_streams_in_bulk_payload["entityIds"].append({
    #             "id": int(self.fetch_numeric_part(stream_id, "")),
    #             "entityTypeId": "stream"
    #         })
    #     self.copy_streams_in_bulk_payload["folderId"] = "cf69202f-75e7-462e-b5a1-91a03b3c0ba8"
    #     self.client.post(COPY_STREAM, headers=self.get_headers(), json=self.copy_streams_in_bulk_payload)

    @task(5)
    def rename_stream(self):
        self.rename_stream_payload["entityId"]["id"] = self.stream_id_numeric_part
        self.rename_stream_payload["name"] = f"renamed_stream_{self.generate_random_number()}"
        self.client.post(
            RENAME_STREAM,
            headers=self.get_headers(),
            json=self.rename_stream_payload,
        )

    @task(1)
    def delete_stream(self):
        stream_id = self.pre_requisite_create_stream()
        self.delete_stream_payload[0]["id"] = stream_id.replace("stream-", "")
        self.client.post(
            DELETE_STREAM,
            headers=self.get_headers(),
            json=self.delete_stream_payload,
        )

    @task(1)
    def delete_streams_in_bulk(self):
        stream_ids = []
        self.delete_streams_in_bulk_payload = []
        for i in range(3):
            stream_ids.append(self.pre_requisite_create_stream())
        for stream_id in stream_ids:
            self.delete_streams_in_bulk_payload.append(
                {"id": self.fetch_numeric_part(stream_id, ""), "entityTypeId": "stream"}
            )
        self.client.post(
            DELETE_STREAM,
            headers=self.get_headers(),
            json=self.delete_streams_in_bulk_payload,
        )

    @task(5)
    def pause_and_resume_stream(self):
        self.pause_stream_payload = [self.stream_id]
        self.client.post(
            PAUSE_STREAM,
            headers=self.get_headers(),
            json=self.pause_stream_payload,
        )

        self.resume_stream_in_bulk_payload = self.stream_ids
        self.client.post(
            RESUME_STREAM,
            headers=self.get_headers(),
            json=self.resume_stream_in_bulk_payload,
        )

    @task(3)
    def pause_and_resume_stream_in_bulk(self):
        self.pause_stream_in_bulk_payload = self.stream_ids
        self.client.post(
            PAUSE_STREAM,
            headers=self.get_headers(),
            json=self.pause_stream_in_bulk_payload,
        )

        self.resume_stream_payload = [self.stream_id]
        self.client.post(
            RESUME_STREAM,
            headers=self.get_headers(),
            json=self.resume_stream_payload,
        )

    @task(7)
    def rerun_batches(self):
        self.rerun_batches_payload["batchIds"] = self.batch_ids
        self.client.patch(
            RERUN_BATCHES.format(stream_id=self.stream_id),
            headers=self.get_headers(),
            json=self.rerun_batches_payload,
        )


class BatchListingPage(BaseApiTest, BaseStream):
    current_time_millis = 0
    fifteen_minutes_back_millis = 0

    # TODO need common flow id for all the stream requests
    def on_start(self) -> None:
        super().on_start()
        self.current_time_millis = int(time.time() * 1000)
        self.fifteen_minutes_back_millis = self.current_time_millis - (15 * 60 * 1000)
        self.flow_id = self.pre_requisite_create_playbook_with_sql_and_output_node()
        self.node_id = self.pre_requisite_create_sql_node()
        self.pre_requisite_create_output_node()
        self.stream_id = self.pre_requisite_create_stream()
        self.on_demand_stream_id, self.token = self.pre_requisite_create_on_demand_stream()
        self.batch_ids = self.pre_requisite_get_batches()

    def on_stop(self) -> None:
        for cleanup in [
            (self.clean_up_delete_stream, self.stream_id),
            (self.clean_up_delete_stream, self.on_demand_stream_id),
            (self.clean_up_delete_playbook, self.flow_id),
        ]:
            cleanup[0](cleanup[1])

    def pre_requisite_create_playbook_with_sql_and_output_node(self) -> str:
        self.create_playbook_payload["parameters"][
            "name"
        ] = f"new_playbook_{self.generate_random_number()}"
        response = self._make_request(
            self.API_BASE, "post", "/demo/hub", json=self.create_playbook_payload
        )
        return response.json().get("result", {}).get("id") if response.status_code == 200 else ""

    def pre_requisite_create_sql_node(self) -> str:
        response = self.make_request(
            self.API_BASE,
            "post",
            f"/flow/{self.flow_id}/node?from={self.fifteen_minutes_back_millis}&to={self.current_time_millis}",
            json=self.add_node_sql_operation_payload,
        )
        return (
            response.json().get("result", {}).get("nodes", [])[-1].get("id")
            if response.status_code == 200
            else ""
        )

    def pre_requisite_create_output_node(self) -> None:
        self.add_output_node_payload["nodes"][0] = self.node_id
        self.make_request(
            self.API_BASE, "post", f"/flow/{self.flow_id}/node", json=self.add_output_node_payload
        )

    def pre_requisite_create_stream(self) -> str:
        self.create_stream_payload["parameters"]["flow"] = self.flow_id
        self.create_stream_payload["parameters"][
            "name"
        ] = f"new_stream_{self.generate_random_number()}"
        response = self._make_request(
            self.API_BASE, "post", "/demo/hub", json=self.create_stream_payload
        )
        return response.json().get("result", {}).get("id") if response.status_code == 200 else ""

    def pre_requisite_create_on_demand_stream(self) -> Tuple[str, str]:
        self.create_stream_payload["parameters"]["flow"] = self.flow_id
        self.create_stream_payload["parameters"][
            "name"
        ] = f"new_stream_{self.generate_random_number()}"
        self.create_stream_payload["parameters"]["onDemandEnabled"] = True
        response = self._make_request(
            self.API_BASE, "post", "/demo/hub", json=self.create_stream_payload
        )

        return (
            response.json().get("result", {}).get("id"),
            response.json().get("result", {}).get("token") if response.status_code == 200 else "",
        )

    def pre_requisite_get_batches(self) -> List[str]:
        response = self.make_request(
            self.API_BASE,
            "post",
            f"/stream/{self.stream_id}/batches/filter?pageSize=25&after=0",
            json=self.get_batches_payload,
        )
        response_data_array = response.json().get("result", {}).get("data", [])
        return [
            item.get("id") for item in response_data_array[:5] if item.get("id") is not None
        ] or []

    def clean_up_delete_playbook(self, flow_id: str) -> None:
        self.delete_playbook_payload[0]["id"] = self.fetch_numeric_part(flow_id, "flow-")
        self._make_request(
            self.API_BASE, "post", "/content-management/delete", json=self.delete_playbook_payload
        )

    def clean_up_delete_stream(self, stream_id: str) -> None:
        self.delete_stream_payload[0]["id"] = stream_id.replace("stream-", "")
        self._make_request(
            self.API_BASE, "post", "/content-management/delete", json=self.delete_stream_payload
        )

    @task(9)
    def get_batches(self):
        self.client.post(
            GET_BATCHES.format(stream_id=self.stream_id),
            headers=self.get_headers(),
            json=self.get_batches_payload,
        )

    @task(4)
    def get_batches_status_filter(self):
        self.get_batches_payload["status"] = ["ready"]
        self.client.post(
            GET_BATCHES.format(stream_id=self.stream_id),
            headers=self.get_headers(),
            json=self.get_batches_payload,
        )

    @task(2)
    def rerun_error_batches(self):
        self.client.post(
            RERUN_ERROR_BATCHES.format(stream_id=self.stream_id),
            headers=self.get_headers(),
            json=self.rerun_error_batches_payload,
        )

    @task(2)
    def cancel_queued_and_scheduled_batches(self):
        self.rerun_batches_payload["batchIds"] = self.batch_ids
        self.client.patch(
            CANCEL_QUEUED_AND_SCHEDULED_BATCHES.format(stream_id=self.stream_id),
            headers=self.get_headers(),
            json=self.rerun_batches_payload,
        )

        # @task(4)
        # def create_batch_for_on_demand_stream(self):
        #     #TODO different host is being for this request
        self.client.post(
            CREATE_BATCH_FOR_ON_DEMAND_STREAM.format(
                on_demand_stream_id=self.on_demand_stream_id, token=self.token
            ),
            headers=self.get_headers(),
            json={},
        )


class BatchDetailsPage(BaseApiTest, BaseStream):
    current_time_millis = 0
    fifteen_minutes_back_millis = 0

    # TODO need common flow id for all the stream requests
    def on_start(self) -> None:
        super().on_start()
        self.current_time_millis = int(time.time() * 1000)
        self.fifteen_minutes_back_millis = self.current_time_millis - (15 * 60 * 1000)
        self.flow_id = self.pre_requisite_create_playbook_with_sql_and_output_node()
        self.node_id = self.pre_requisite_create_sql_node()
        self.pre_requisite_create_output_node()
        self.stream_id = self.pre_requisite_create_stream()
        self.batch_ids = self.pre_requisite_get_batches()

    def on_stop(self) -> None:
        for cleanup in [
            (self.clean_up_delete_stream, self.stream_id),
            (self.clean_up_delete_playbook, self.flow_id),
        ]:
            cleanup[0](cleanup[1])

    def pre_requisite_create_playbook_with_sql_and_output_node(self) -> str:
        self.create_playbook_payload["parameters"][
            "name"
        ] = f"new_playbook_{self.generate_random_number()}"
        response = self._make_request(
            self.API_BASE, "post", "/demo/hub", json=self.create_playbook_payload
        )
        return response.json().get("result", {}).get("id") if response.status_code == 200 else ""

    def pre_requisite_create_sql_node(self) -> str:
        response = self.make_request(
            self.API_BASE,
            "post",
            f"/flow/{self.flow_id}/node?from={self.fifteen_minutes_back_millis}&to={self.current_time_millis}",
            json=self.add_node_sql_operation_payload,
        )
        return (
            response.json().get("result", {}).get("nodes", [])[-1].get("id")
            if response.status_code == 200
            else ""
        )

    def pre_requisite_create_output_node(self) -> None:
        self.add_output_node_payload["nodes"][0] = self.node_id
        self.make_request(
            self.API_BASE, "post", f"/flow/{self.flow_id}/node", json=self.add_output_node_payload
        )

    def pre_requisite_create_stream(self) -> str:
        self.create_stream_payload["parameters"]["flow"] = self.flow_id
        self.create_stream_payload["parameters"][
            "name"
        ] = f"new_stream_{self.generate_random_number()}"
        response = self._make_request(
            self.API_BASE, "post", "/demo/hub", json=self.create_stream_payload
        )
        return response.json().get("result", {}).get("id") if response.status_code == 200 else ""

    def pre_requisite_get_batches(self) -> List[str]:
        response = self.make_request(
            self.API_BASE,
            "post",
            f"/stream/{self.stream_id}/batches/filter?pageSize=25&after=0",
            json=self.get_batches_payload,
        )
        response_data_array = response.json().get("result", {}).get("data", [])
        return [item.get("id") for item in response_data_array[:5]] if response_data_array else []

    def clean_up_delete_playbook(self, flow_id: str) -> None:
        self.delete_playbook_payload[0]["id"] = self.fetch_numeric_part(flow_id, "flow-")
        self._make_request(
            self.API_BASE, "post", "/content-management/delete", json=self.delete_playbook_payload
        )

    def clean_up_delete_stream(self, stream_id: str) -> None:
        self.delete_stream_payload[0]["id"] = stream_id.replace("stream-", "")
        self._make_request(
            self.API_BASE, "post", "/content-management/delete", json=self.delete_stream_payload
        )

    @task(9)
    def get_single_batch_details(self):
        self.client.get(
            GET_SINGLE_BATCH_DETAILS.format(batch_id=self.batch_ids[0]),
            headers=self.get_headers(),
        )

    @task(9)
    def get_single_batch_correlation(self):
        self.client.get(
            GET_SINGLE_BATCH_CORRELATION.format(batch_id=self.batch_ids[0]),
            headers=self.get_headers(),
        )

    @task(9)
    def get_nodes_details(self):
        self.client.get(GET_NODE_DETAILS.format(flow_id=self.flow_id), headers=self.get_headers())

    @task(2)
    def reprocess_single_batch(self):
        self.client.post(
            REPROCESS_SINGLE_BATCH.format(batch_id=self.batch_ids[0]),
            headers=self.get_headers(),
            json=self.reprocess_single_batch_payload,
        )
