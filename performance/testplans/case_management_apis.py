#!/usr/bin/env python

from typing import List

from locust import task
from performance.testplans.base_api_test import BaseApiTest

from performance.testplans import (
    GET_CASES,
    GET_CASE_STATUS_DATA,
    DOWNLOAD_CASE_LIST,
    CREATE_SAVED_SEARCH,
    GET_SAVED_SEARCH,
    EDIT_OR_DELETE_SAVED_SEARCH,
    CREATE_CASE,
    DELETE_CASES_IN_BULK,
    CREATE_COMMENT,
    EDIT_OR_DELETE_COMMENT,
    GET_COMMENTS,
    GET_CASE_HISTORY,
    EDIT_CASE,
    ADD_ATTACHMENT,
    DOWNLOAD_ATTACHMENT,
    CREATE_TASK_FOR_CASE,
    GET_OR_UPDATE_PREFIX,
    UPDATE_WHITELISTED_URL_OR_FILE_HASH_OR_IP_LIST,
    GET_WHITELISTED_URL_FILE_HASH_IP_LIST,
    CREATE_CASE_TYPE,
    UPDATE_CASE_TYPE,
    ADD_OR_REMOVE_FIELD_FROM_CASE_TYPE,
    CREATE_TASK_FOR_CASE_TYPE,
    EDIT_OR_DELETE_TASK_IN_CASE_TYPE,
    GET_CASE_TYPES,
    GET_OR_CREATE_PRIORITY,
    EDIT_PRIORITY,
    REORDER_PRIORITY,
    DELETE_AND_REASSIGN_PRIORITY,
    PUBLISH_STATUS_WORKFLOW,
    GET_OR_CREATE_STATUS_WORKFLOW,
    EDIT_STATUS_WORKFLOW,
    GET_IS_USED_STATUS_WORKFLOW,
    CLONE_STATUS_WORKFLOW,
    DELETE_AND_REASSIGN_STATUS_WORKFLOW,
    GET_FIELDS,
    GET_FIELDS_OWNER_COLUMN_DESCENDING,
    GET_FIELDS_OWNER_COLUMN_ASCENDING,
    GET_FIELDS_FIELD_TYPE_COLUMN_DESCENDING,
    GET_FIELDS_FIELD_TYPE_COLUMN_ASCENDING,
    GET_FIELDS_DESCRIPTION_COLUMN_DESCENDING,
    GET_FIELDS_DESCRIPTION_COLUMN_ASCENDING,
    GET_FIELDS_DISPLAY_NAME_COLUMN_DESCENDING,
    GET_FIELDS_DISPLAY_NAME_COLUMN_ASCENDING,
    GET_FIELDS_FIELD_NAME_COLUMN_DESCENDING,
    GET_FIELDS_FIELD_NAME_COLUMN_ASCENDING,
    EDIT_OR_DELETE_FIELD,
    CREATE_FIELD,
    GET_FIELD_DETAILS,
    GET_FIELDS_WITH_SEARCH,
)


class BaseCaseManagement:
    API_BASE = "/case-management/api/casemanagement"

    abstract = True

    GET_CASES_BASE_PAYLOAD = {
        "fields": [
            {
                "fieldName": "created",
                "fieldType": "Date",
                "value": ["1737024401120", "1737629201120"],
            }
        ],
        "pageNumber": 0,
        "pageSize": 25,
        "text": "",
        "includeTaskEntityType": True,
        "includeWorkflow": True,
    }

    SAVED_SEARCH_BASE_PAYLOAD = {
        "description": "New saved search",
        "isDefault": False,
        "name": "",
    }

    TASK_BASE_PAYLOAD = {
        "descriptionJsonString": "",
        "priority": "Low",
        "issueTypeString": "Manual Task",
        "status": "Waiting",
        "reporterUserName": "mukund.gupta@devo.com",
    }

    DELETE_CASES_BASE_PAYLOAD = {"caseIds": []}

    TASK_IN_CASE_TYPE_BASE_PAYLOAD = {"priority": "Low", "status": "Waiting", "title": ""}

    CLONE_OR_PUBLISH_STATUS_WORKFLOW_BASE_PAYLOAD = {"id": "", "stateMappings": {}}
    CREATE_OR_UPDATE_STATUS_WORKFLOW_BASE_PAYLOAD = {"name": "", "description": "Description"}
    get_cases_with_filter_values_payload = {
        "priority": ["Low"],
        "status": ["Closed"],
        "reporter": ["unassigned"],
        "assignee": ["unassigned"],
        "types": ["case"],
        **GET_CASES_BASE_PAYLOAD,
    }

    get_cases_payload = {**GET_CASES_BASE_PAYLOAD}

    create_saved_search_filter_payload = {
        **SAVED_SEARCH_BASE_PAYLOAD,
        "query": '{"priority":["Low"],"status":["New"],"reporter":["unassigned"],"assignee":["unassigned"],"types":["case"],"text":"case"}',
        "queryType": "basic",
    }

    edit_saved_search_payload = {**SAVED_SEARCH_BASE_PAYLOAD}

    download_case_list_payload = {
        "fields": [],
        "downloadFields": [
            "id",
            "title",
            "issueType",
            "priority",
            "status",
            "reporter",
            "assignee",
            "createdAt",
            "modifiedAt",
        ],
        "text": "",
        "sortCol": "id",
        "sortOrder": "desc",
    }

    create_case_payload = {
        "fields": [
            {
                "className": "lh-case-listing-create-case-field",
                "defaultValue": "",
                "fieldName": "title",
                "id": "title",
                "inputType": "text",
                "label": "Title",
                "optional": False,
                "placeholder": "Enter title",
                "required": True,
                "value": "",
            },
            {
                "className": "lh-case-listing-create-case-field",
                "defaultValue": "",
                "fieldName": "assignee",
                "id": "assignee",
                "inputType": "groupSelect",
                "label": "Assignee",
                "mode": "single",
                "optional": True,
                "placeholder": "Select assignee",
                "required": False,
                "value": "unassigned",
            },
            {
                "className": "lh-case-listing-create-case-field",
                "defaultValue": "",
                "fieldName": "priority",
                "id": "priority",
                "inputType": "select",
                "label": "Priority",
                "placeholder": "Select priority",
                "required": True,
                "value": "Low",
                "optional": False,
            },
        ],
        "issueTypeString": "Default",
        "attachments": [],
    }

    delete_cases_payload = {**DELETE_CASES_BASE_PAYLOAD}

    delete_cases_in_bulk_payload = {**DELETE_CASES_BASE_PAYLOAD}

    create_comment_payload = {
        "cmEntityType": "CaseEntity",
        "comment": "hey",
        "userName": "mukund.gupta@devo.com",
        "type": "plain_text",
    }

    edit_comment_payload = {"comment": "edited comment"}

    update_case_priority_payload = {"priority": "Low"}

    update_case_status_payload = {"status": "In Progress"}

    update_case_assigned_to_payload = {"assigneeUserName": "unassigned"}

    update_case_description_payload = {
        "descriptionRaw": "This is a new case description",
        "descriptionJsonString": ""
    }

    create_manual_task_payload = {
        "title": "manual_task_new_case",
        "descriptionRaw": "Creating a manual task for case",
        **TASK_BASE_PAYLOAD,
    }

    create_integration_task_payload = {
        "title": "New_integration_task",
        "descriptionRaw": "Creating new integration task for case",
        **TASK_BASE_PAYLOAD,
        "taskParameter": {
            "mapping": {},
            "integrationCommandArgument": {
                "actionName": "Lookup IP V2",
                "connectionName": "[LogicHub BuiltIn][2.0.7] ARIN Whois Connection",
                "integrationName": "ARIN Whois",
                "parameters": [{"key": "IP", "value": "192.168.0.1"}],
            },
        },
    }

    create_command_task_payload = {
        "title": "New_command_task_for_case",
        "descriptionRaw": "This command accepts one or more IP addresses as input, which can either be mapped from a case field or provided manually by the user. It then queries the VirusTotal V3 API to search for relevant information, with the enrichment results stored in the task output.",
        **TASK_BASE_PAYLOAD,
        "taskParameter": {"mapping": {"IP_Address": "192.168.0.1"}},
    }

    delete_task_payload = {
        "title": "New_command_task_for_case",
        "descriptionRaw": "This command accepts one or more IP addresses as input, which can either be mapped from a case field or provided manually by the user. It then queries the VirusTotal V3 API to search for relevant information, with the enrichment results stored in the task output.",
        "taskParameter": {"mapping": {"IP_Address": "192.168.0.1"}},
        **TASK_BASE_PAYLOAD,
    }

    get_alerts_payload = {
        "pageNumber": 1,
        "pageSize": 50,
        "language": "en",
        "orderBy": "WHEN",
        "orderByType": "DESC",
        "timezone": "Asia/Calcutta",
        "from": "2025-01-17T10:55:21Z",
        "to": "2025-01-24T10:55:21Z",
    }

    link_alert_with_case_payload = {"alertCases": [{"alertId": None, "caseIds": []}]}

    add_attachment_payload = {"caseId": "", "content": "", "name": ""}

    update_case_prefix_payload = {"prefix": "Case"}

    update_whitelisted_url_list_payload = ["https://www.google.co.in"]

    update_whitelisted_file_hash_list_payload = [
        "2c25489cca1e4a349d394a426e677c25b6795e2b879b841498e5e60abfd2d286"
    ]

    update_whitelisted_ip_list_payload = ["192.168.0.1"]

    create_case_type_payload = {
        "description": "New description",
        "watcherEmails": "",
        "id": "",
        "name": "",
    }

    update_case_type_payload = {
        "defaultAssigneeUsername": "unassigned",
        "description": "Updated description",
        "name": "",
    }

    add_field_to_case_type_payload = {"closeRequired": False, "createRequired": False}

    reassign_status_workflow_to_case_type_payload = {
        "caseTypeId": "",
        "from": "",
        "stateMappings": {
            "New": "ToDo",
            "In Progress": "ToDo",
            "Pending": "ToDo",
            "Resolved": "Done",
            "Closed": "Done",
        },
        "to": "",
    }

    add_manual_task_to_case_type_payload = {
        "assigneeUserName": "mukund.gupta@devo.com",
        "description": "Creating a manual task for case",
        **TASK_IN_CASE_TYPE_BASE_PAYLOAD,
        "taskIssueTypeString": "Manual Task",
    }

    update_manual_task_in_case_type_payload = {
        "assigneeUserName": "mukund.gupta@devo.com",
        "description": "Creating a manual task for case",
        "taskIssueTypeString": "Manual Task",
        **TASK_IN_CASE_TYPE_BASE_PAYLOAD,
    }

    add_integration_task_to_case_type_payload = {
        "description": "Creating new integration task for case",
        "taskIssueTypeString": "Automatic Task",
        **TASK_IN_CASE_TYPE_BASE_PAYLOAD,
        "taskParameter": {
            "mapping": {},
            "integrationCommandArgument": {
                "actionName": "Lookup IP V2",
                "connectionName": "[LogicHub BuiltIn][2.0.7] ARIN Whois Connection",
                "integrationName": "ARIN Whois",
                "parameters": [{"key": "IP", "value": "192.168.0.1"}],
            },
        },
    }

    update_integration_task_in_case_type_payload = {
        "description": "Creating new integration task for case",
        "taskIssueTypeString": "Automatic Task",
        **TASK_IN_CASE_TYPE_BASE_PAYLOAD,
        "taskParameter": {
            "mapping": {},
            "integrationCommandArgument": {
                "actionName": "Lookup IP V2",
                "connectionName": "[LogicHub BuiltIn][2.0.7] ARIN Whois Connection",
                "integrationName": "ARIN Whois",
                "parameters": [{"key": "IP", "value": "192.168.0.1"}],
            },
        },
    }

    add_command_task_to_case_type_payload = {
        "description": "This command accepts one or more IP addresses as input, which can either be mapped from a case field or provided manually by the user. It then queries the VirusTotal V3 API to search for relevant information, with the enrichment results stored in the task output.",
        "flowId": "",
        **TASK_IN_CASE_TYPE_BASE_PAYLOAD,
        "taskIssueTypeString": "Automatic Task",
        "taskParameter": {"mapping": {"IP_Address": "192.168.0.1"}},
    }

    update_command_task_in_case_type_payload = {
        "description": "This command accepts one or more IP addresses as input, which can either be mapped from a case field or provided manually by the user. It then queries the VirusTotal V3 API to search for relevant information, with the enrichment results stored in the task output.",
        "flowId": "flow-1007",
        **TASK_IN_CASE_TYPE_BASE_PAYLOAD,
        "taskIssueTypeString": "Automatic Task",
        "taskParameter": {"mapping": {"IP_Address": "192.168.0.1"}},
    }

    create_field_payload = {
        "associatedCaseTypes": [],
        "description": "Detailed description of the case/task.",
        "displayName": "",
        "fieldName": "",
        "fieldType": "TextArea",
        "isAssociatedWithAllCaseTypes": True,
        "isObservable": False,
        "options": [],
    }

    get_fields_with_search_payload = {
        "page": 0,
        "pageSize": 25,
        "query": "description",
        "systemFields": True,
    }

    update_field_payload = {
        "displayName": "",
        "fieldName": "",
    }

    create_priority_payload = {"icon": "low", "name": ""}

    update_priority_payload = {"icon": "normal", "name": ""}

    reorder_priority_payload = {"order": []}

    delete_and_reassign_priority_payload = {"from": "", "to": ""}

    create_status_workflow_payload = {
        **CREATE_OR_UPDATE_STATUS_WORKFLOW_BASE_PAYLOAD,
        "draftStateTransitions": {"transitions": []},
    }

    publish_status_workflow_payload = {**CLONE_OR_PUBLISH_STATUS_WORKFLOW_BASE_PAYLOAD}

    update_status_workflow_payload = {**CREATE_OR_UPDATE_STATUS_WORKFLOW_BASE_PAYLOAD}

    delete_and_reassign_status_workflow_payload = {"from": "", "stateMappings": {}, "to": ""}

    clone_status_workflow_payload = {**CLONE_OR_PUBLISH_STATUS_WORKFLOW_BASE_PAYLOAD}

    GET_STATUS_DATA_BASE_PAYLOAD = {
        "searchRequestParams":{
            "text":"",
            "fields":[
                {
                    "fieldName":"created",
                    "fieldType":"Date",
                    "value":[
                        "1739358642000",
                        "1739963442000"
                    ]
                }
            ]
        },
        "zoneId":"Asia/Calcutta"
    }

    get_status_data_payload_with_filters = {
        "priority": ["Low"],
        "status": ["Closed"],
        "reporter": ["unassigned"],
        "assignee": ["unassigned"],
        **GET_STATUS_DATA_BASE_PAYLOAD
    }

class CaseListingPage(BaseApiTest, BaseCaseManagement):
    def on_start(self) -> None:
        super().on_start()
        self.case_id = self.pre_requisite_create_case()
        self.saved_search_id = self.pre_requisite_create_saved_search()

    def on_stop(self) -> None:
        for cleanup in [
            (self.clean_up_delete_saved_search, self.saved_search_id),
            (self.clean_up_delete_case, self.case_id),
        ]:
            cleanup[0](cleanup[1])

    def pre_requisite_create_saved_search(self) -> str:
        self.create_saved_search_filter_payload[
            "name"
        ] = f"new_saved_search_{self.generate_random_number()}"
        response = self._make_request(
            self.API_BASE, "post", "/cases/search", json=self.create_saved_search_filter_payload
        )
        return response.json().get("result", {}).get("id") if response.status_code == 200 else ""

    def pre_requisite_create_case(self) -> str:
        self.create_case_payload["fields"][0]["value"] = f"new_case_{self.generate_random_number()}"
        response = self._make_request(
            self.API_BASE, "post", "/case-ui", json=self.create_case_payload
        )
        return response.json().get("result", {}).get("id") if response.status_code == 200 else ""

    def pre_requisite_create_cases(self, number_of_cases: int) -> List[str]:
        case_ids_list = []
        for i in range(number_of_cases):
            self.create_case_payload["fields"][0][
                "value"
            ] = f"new_case_{self.generate_random_number()}"
            response = self._make_request(
                self.API_BASE, "post", "/case-ui", json=self.create_case_payload
            )
            case_ids_list.append(
                response.json().get("result", {}).get("id")
            ) if response.status_code == 200 else ""
        return case_ids_list

    def clean_up_delete_case(self, case_id: str) -> None:
        self.make_request(self.API_BASE, "delete", f"/case/{case_id}", json={})

    def clean_up_delete_saved_search(self, saved_search_id: str) -> None:
        self.make_request(self.API_BASE, "delete", f"/cases/search/{saved_search_id}", json={})

    @task(9)
    def get_cases(self):
        self.client.post(
            GET_CASES,
            headers=self.get_headers(),
            json=self.get_cases_payload,
        )

    @task(3)
    def get_cases_id_sorted_ascending(self):
        self.get_cases_payload["sortCol"] = "id"
        self.get_cases_payload["sortOrder"] = "asc"
        self.client.post(
            GET_CASES,
            headers=self.get_headers(),
            json=self.get_cases_payload,
        )

    @task(3)
    def get_cases_id_sorted_descending(self):
        self.get_cases_payload["sortCol"] = "id"
        self.get_cases_payload["sortOrder"] = "desc"
        self.client.post(
            GET_CASES,
            headers=self.get_headers(),
            json=self.get_cases_payload,
        )

    @task(3)
    def get_cases_title_sorted_ascending(self):
        self.get_cases_payload["sortCol"] = "title"
        self.get_cases_payload["sortOrder"] = "asc"
        self.client.post(
            GET_CASES,
            headers=self.get_headers(),
            json=self.get_cases_payload,
        )

    @task(3)
    def get_cases_title_sorted_descending(self):
        self.get_cases_payload["sortCol"] = "title"
        self.get_cases_payload["sortOrder"] = "desc"
        self.client.post(
            GET_CASES,
            headers=self.get_headers(),
            json=self.get_cases_payload,
        )

    @task(3)
    def get_cases_issue_type_sorted_ascending(self):
        self.get_cases_payload["sortCol"] = "issueType"
        self.get_cases_payload["sortOrder"] = "asc"
        self.client.post(
            GET_CASES,
            headers=self.get_headers(),
            json=self.get_cases_payload,
        )

    @task(3)
    def get_cases_issue_type_sorted_descending(self):
        self.get_cases_payload["sortCol"] = "issueType"
        self.get_cases_payload["sortOrder"] = "desc"
        self.client.post(
            GET_CASES,
            headers=self.get_headers(),
            json=self.get_cases_payload,
        )

    @task(3)
    def get_cases_priority_sorted_ascending(self):
        self.get_cases_payload["sortCol"] = "priority"
        self.get_cases_payload["sortOrder"] = "asc"
        self.client.post(
            GET_CASES,
            headers=self.get_headers(),
            json=self.get_cases_payload,
        )

    @task(3)
    def get_cases_priority_sorted_descending(self):
        self.get_cases_payload["sortCol"] = "priority"
        self.get_cases_payload["sortOrder"] = "desc"
        self.client.post(
            GET_CASES,
            headers=self.get_headers(),
            json=self.get_cases_payload,
        )

    @task(3)
    def get_cases_status_sorted_ascending(self):
        self.get_cases_payload["sortCol"] = "status"
        self.get_cases_payload["sortOrder"] = "asc"
        self.client.post(
            GET_CASES,
            headers=self.get_headers(),
            json=self.get_cases_payload,
        )

    @task(3)
    def get_cases_status_sorted_descending(self):
        self.get_cases_payload["sortCol"] = "status"
        self.get_cases_payload["sortOrder"] = "desc"
        self.client.post(
            GET_CASES,
            headers=self.get_headers(),
            json=self.get_cases_payload,
        )

    @task(3)
    def get_cases_reporter_sorted_ascending(self):
        self.get_cases_payload["sortCol"] = "reporter"
        self.get_cases_payload["sortOrder"] = "asc"
        self.client.post(
            GET_CASES,
            headers=self.get_headers(),
            json=self.get_cases_payload,
        )

    @task(3)
    def get_cases_reporter_sorted_descending(self):
        self.get_cases_payload["sortCol"] = "reporter"
        self.get_cases_payload["sortOrder"] = "desc"
        self.client.post(
            GET_CASES,
            headers=self.get_headers(),
            json=self.get_cases_payload,
        )

    @task(3)
    def get_cases_assignee_sorted_ascending(self):
        self.get_cases_payload["sortCol"] = "assignee"
        self.get_cases_payload["sortOrder"] = "asc"
        self.client.post(
            GET_CASES,
            headers=self.get_headers(),
            json=self.get_cases_payload,
        )

    @task(3)
    def get_cases_assignee_sorted_descending(self):
        self.get_cases_payload["sortCol"] = "assignee"
        self.get_cases_payload["sortOrder"] = "desc"
        self.client.post(
            GET_CASES,
            headers=self.get_headers(),
            json=self.get_cases_payload,
        )

    @task(3)
    def get_cases_created_at_sorted_ascending(self):
        self.get_cases_payload["sortCol"] = "createdAt"
        self.get_cases_payload["sortOrder"] = "asc"
        self.client.post(
            GET_CASES,
            headers=self.get_headers(),
            json=self.get_cases_payload,
        )

    @task(3)
    def get_cases_created_at_sorted_descending(self):
        self.get_cases_payload["sortCol"] = "createdAt"
        self.get_cases_payload["sortOrder"] = "desc"
        self.client.post(
            GET_CASES,
            headers=self.get_headers(),
            json=self.get_cases_payload,
        )

    @task(3)
    def get_cases_modified_at_sorted_ascending(self):
        self.get_cases_payload["sortCol"] = "modifiedAt"
        self.get_cases_payload["sortOrder"] = "asc"
        self.client.post(
            GET_CASES,
            headers=self.get_headers(),
            json=self.get_cases_payload,
        )

    @task(3)
    def get_cases_modified_at_sorted_descending(self):
        self.get_cases_payload["sortCol"] = "modifiedAt"
        self.get_cases_payload["sortOrder"] = "desc"
        self.client.post(
            GET_CASES,
            headers=self.get_headers(),
            json=self.get_cases_payload,
        )

    @task(7)
    def get_cases_with_search_value(self):
        self.get_cases_payload["text"] = "case"
        self.client.post(
            GET_CASES,
            headers=self.get_headers(),
            json=self.get_cases_payload,
        )

    @task(4)
    def get_cases_with_filter_values(self):
        self.client.post(
            GET_CASES,
            headers=self.get_headers(),
            json=self.get_cases_with_filter_values_payload,
        )

    @task(7)
    def get_case_status_with_search_value(self):
        self.get_cases_payload["text"] = "case"
        self.client.post(
            GET_CASE_STATUS_DATA,
            headers=self.get_headers(),
            json=self.GET_STATUS_DATA_BASE_PAYLOAD,
        )

    @task(4)
    def get_case_status_with_filter_values(self):
        self.client.post(
            GET_CASE_STATUS_DATA,
            headers=self.get_headers(),
            json=self.get_status_data_payload_with_filters
        )

    @task(1)
    def download_case_list(self):
        self.client.post(
            DOWNLOAD_CASE_LIST,
            headers=self.get_headers(),
            json=self.download_case_list_payload,
        )

    @task(2)
    def create_saved_search(self):
        self.create_saved_search_filter_payload[
            "name"
        ] = f"new_saved_search_{self.generate_random_number()}"
        response = self.client.post(
            CREATE_SAVED_SEARCH,
            headers=self.get_headers(),
            json=self.create_saved_search_filter_payload,
        )
        if response.status_code == 200:
            saved_search_id = response.json().get("result", {}).get("id")
            self.clean_up_delete_saved_search(saved_search_id)

    @task(9)
    def get_saved_search(self):
        self.client.get(GET_SAVED_SEARCH, headers=self.get_headers())

    @task(5)
    def edit_saved_search(self):
        self.edit_saved_search_payload["name"] = f"new_saved_search_{self.generate_random_number()}"
        self.client.patch(
            EDIT_OR_DELETE_SAVED_SEARCH.format(saved_search_id=self.saved_search_id),
            headers=self.get_headers(),
            json=self.edit_saved_search_payload,
        )

    # @task(1)
    # def delete_saved_search(self):
    #     saved_search_id = self.pre_requisite_create_saved_search()
    #     self.client.delete(EDIT_OR_DELETE_SAVED_SEARCH.format(saved_search_id=self.saved_search_id), headers=self.get_headers(), json={})

    @task(2)
    def create_case(self):
        self.create_case_payload["fields"][0]["value"] = f"new_case_{self.generate_random_number()}"
        response = self.client.post(
            CREATE_CASE,
            headers=self.get_headers(),
            json=self.create_case_payload,
        )
        if response.status_code == 200:
            case_id = response.json().get("result", {}).get("id")
            self.clean_up_delete_case(case_id)

    # @task(1)
    # def delete_case(self):
    #     case_id = self.pre_requisite_create_case()
    #     self.client.delete(DELETE_CASE.format(case_id=case_id), headers=self.get_headers(), json={})

    @task(1)
    def delete_cases_in_bulk(self):
        self.delete_cases_in_bulk_payload["caseIds"] = self.pre_requisite_create_cases(3)
        self.client.delete(
            DELETE_CASES_IN_BULK,
            headers=self.get_headers(),
            json=self.delete_cases_in_bulk_payload,
        )


class CaseDetailsPage(BaseApiTest, BaseCaseManagement):
    def on_start(self) -> None:
        super().on_start()
        self.case_id = self.pre_requisite_create_case()
        self.comment_id = self.pre_requisite_create_comment()
        self.file_id = self.pre_requisite_add_attachment()
        # self.alert_id = self.pre_requisite_get_alert()

    def on_stop(self) -> None:
        for cleanup in [
            (self.clean_up_delete_case_comment, self.comment_id),
            (self.clean_up_delete_case_attachment, self.file_id),
            (self.clean_up_delete_case, self.case_id),
        ]:
            cleanup[0](cleanup[1])

    def pre_requisite_create_case(self) -> str:
        self.create_case_payload["fields"][0]["value"] = f"new_case_{self.generate_random_number()}"
        response = self._make_request(
            self.API_BASE, "post", "/case-ui", json=self.create_case_payload
        )
        return response.json().get("result", {}).get("id") if response.status_code == 200 else ""

    def pre_requisite_create_comment(self) -> str:
        self.create_comment_payload["cmEntityId"] = int(
            self.fetch_numeric_part(self.case_id, "Case-")
        )
        response = self._make_request(
            self.API_BASE, "post", "/comment", json=self.create_comment_payload
        )
        return response.json().get("result", {}).get("id") if response.status_code == 200 else ""

    def pre_requisite_add_attachment(self) -> str:
        file_base64_encode = self.encode_file_to_base64(
            file_path="performance/testplans/case_management_attachment.csv"
        )
        self.add_attachment_payload["caseId"] = self.case_id
        self.add_attachment_payload["content"] = f"data:text/csv;base64,{file_base64_encode}"
        self.add_attachment_payload["name"] = "case_management_attachment.csv"
        response = self._make_request(
            self.API_BASE, "post", "/attachment", json=self.add_attachment_payload
        )
        return response.json().get("result", {}).get("id") if response.status_code == 200 else ""

    def pre_requisite_create_integration_task(self) -> str:
        self.create_integration_task_payload["parent"] = self.case_id
        response = self._make_request(
            self.API_BASE, "post", "/case", json=self.create_integration_task_payload
        )
        return response.json().get("result", {}).get("id") if response.status_code == 200 else ""

    def pre_requisite_get_alert(self) -> str:
        response = self._make_request(
            self.API_BASE, "post", "/ada/v1/alerts/search", json=self.get_alerts_payload
        )
        return response.json().get("result", {}).get("id") if response.status_code == 200 else ""

    def clean_up_delete_case(self, case_ids) -> None:
        self.delete_cases_payload["caseIds"] = [case_ids]
        self._make_request(self.API_BASE, "delete", "/cases/bulk", json=self.delete_cases_payload)

    def clean_up_delete_case_comment(self, comment_id: str) -> None:
        self.make_request(self.API_BASE, "delete", f"/comment/{comment_id}", json={})

    def clean_up_delete_case_attachment(self, file_id: str) -> None:
        self.make_request(self.API_BASE, "delete", f"/attachment/{file_id}", json={})

    def clean_up_delete_task(self, task_id: str) -> None:
        self.make_request(self.API_BASE, "delete", f"/case/{task_id}", json={})

    @task(2)
    def create_comment(self):
        self.create_comment_payload["cmEntityId"] = int(
            self.fetch_numeric_part(self.case_id, "Case-")
        )
        response = self.client.post(
            CREATE_COMMENT,
            headers=self.get_headers(),
            json=self.create_comment_payload,
        )
        if response.status_code == 200:
            comment_id = response.json().get("result", {}).get("id")
            self.clean_up_delete_case_comment(comment_id)

    @task(5)
    def edit_comment(self):
        self.client.patch(
            EDIT_OR_DELETE_COMMENT.format(comment_id=self.comment_id),
            headers=self.get_headers(),
            json=self.edit_comment_payload,
        )

    @task(9)
    def get_comments(self):
        self.client.get(
            GET_COMMENTS.format(case_id=self.case_id),
            headers=self.get_headers(),
        )

    # @task(1)
    # def delete_comment(self):
    #     comment_id = self.pre_requisite_create_comment()
    #     self.client.delete(EDIT_OR_DELETE_COMMENT.format(case_id=case_id), json={}, headers=self.get_headers())

    @task(9)
    def get_case_history(self):
        self.client.get(
            GET_CASE_HISTORY.format(case_id=self.case_id),
            headers=self.get_headers(),
        )

    @task(6)
    def update_case_priority(self):
        self.client.patch(
            EDIT_CASE.format(case_id=self.case_id),
            headers=self.get_headers(),
            json=self.update_case_priority_payload,
        )

    @task(6)
    def update_case_status(self):
        self.client.patch(
            EDIT_CASE.format(case_id=self.case_id),
            headers=self.get_headers(),
            json=self.update_case_status_payload,
        )

    @task(5)
    def update_case_assigned_to(self):
        self.client.patch(
            EDIT_CASE.format(case_id=self.case_id),
            headers=self.get_headers(),
            json=self.update_case_assigned_to_payload,
        )

    @task(5)
    def update_case_description(self):
        self.client.patch(
            EDIT_CASE.format(case_id=self.case_id),
            headers=self.get_headers(),
            json=self.update_case_description_payload,
        )

    @task(3)
    def add_attachment(self):
        file_base64_encode = self.encode_file_to_base64(
            file_path="performance/testplans/case_management_attachment.csv"
        )
        self.add_attachment_payload["caseId"] = self.case_id
        self.add_attachment_payload["content"] = f"data:text/csv;base64,{file_base64_encode}"
        self.add_attachment_payload["name"] = "case_management_attachment.csv"
        response = self.client.post(
            ADD_ATTACHMENT,
            headers=self.get_headers(),
            json=self.add_attachment_payload,
        )
        if response.status_code == 200:
            file_id = response.json().get("result", {}).get("id")
            self.clean_up_delete_case_attachment(file_id)

    @task(1)
    def download_attachment(self):
        self.client.get(
            DOWNLOAD_ATTACHMENT.format(file_id=self.file_id),
            headers=self.get_headers(),
        )

    # @task(1)
    # def delete_attachment(self):
    #     file_id = self.pre_requisite_add_attachment()
    #     self.client.delete(DELETE_ATTACHMENT.format(file_id=file_id), headers=self.get_headers(), json={})

    @task(2)
    def create_manual_task(self):
        self.create_manual_task_payload["parent"] = self.case_id
        response = self.client.post(
            CREATE_TASK_FOR_CASE,
            headers=self.get_headers(),
            json=self.create_manual_task_payload,
        )
        if response.status_code == 200:
            task_id = response.json().get("result", {}).get("id")
            self.clean_up_delete_task(task_id)

    @task(2)
    def create_integration_task(self):
        self.create_integration_task_payload["parent"] = self.case_id
        response = self.client.post(
            CREATE_TASK_FOR_CASE,
            headers=self.get_headers(),
            json=self.create_integration_task_payload,
        )
        if response.status_code == 200:
            task_id = response.json().get("result", {}).get("id")
            self.clean_up_delete_task(task_id)

    # @task(2)
    # def create_command_task(self):
    #     #TODO need flow_id for creating command task
    #     self.create_command_task_payload["flowId"] = self.flow_id
    #     self.create_command_task_payload["parent"] = self.case_id
    #     self.client.post(CREATE_TASK_FOR_CASE, headers=self.get_headers(), json=self.create_command_task_payload)
    #
    # @task(1)
    # def delete_task(self):
    #     task_id = self.pre_requisite_create_integration_task()
    #     self.client.delete(DELETE_TASK.format(task_id=task_id), headers=self.get_headers(), json={})
    #
    # @task(2)
    # def link_alert_with_case(self):
    #     #TODO host is different extra api- is present
    #     self.link_alert_with_case_payload["alertCases"][0]["alertId"] = self.alert_id
    #     self.link_alert_with_case_payload["alertCases"][0]["caseIds"] = [
    #         self.fetch_numeric_part(self.case_id, "Case-")
    #     ]
    #     self.client.post(LINK_ALERT_WITH_CASES, headers=self.get_headers(), json=self.link_alert_with_case_payload)
    #
    # @task(1)
    # #TODO host is different extra api- is present
    # def unlink_alert_from_case(self):
    #     self.client.delete(UNLINK_ALERT_WITH_CASES.format(case_id_numeric_part=self.fetch_numeric_part(self.case_id, "Case-"), alert_id=self.alert_id), headers=self.get_headers(), json={})


class CaseSettingsPageGeneralTab(BaseApiTest, BaseCaseManagement):
    def on_start(self) -> None:
        super().on_start()

    @task(2)
    def update_case_prefix(self):
        self.client.post(
            GET_OR_UPDATE_PREFIX,
            headers=self.get_headers(),
            json=self.update_case_prefix_payload,
        )

    @task(5)
    def get_case_prefix(self):
        self.client.get(GET_OR_UPDATE_PREFIX, headers=self.get_headers())

    @task(3)
    def update_whitelisted_url_list(self):
        self.client.patch(
            UPDATE_WHITELISTED_URL_OR_FILE_HASH_OR_IP_LIST.format(field_id="field-8"),
            headers=self.get_headers(),
            json=self.update_whitelisted_url_list_payload,
        )

    @task(3)
    def update_whitelisted_file_hash_list(self):
        self.client.patch(
            UPDATE_WHITELISTED_URL_OR_FILE_HASH_OR_IP_LIST.format(field_id="field-9"),
            headers=self.get_headers(),
            json=self.update_whitelisted_file_hash_list_payload,
        )

    @task(3)
    def update_whitelisted_ip_list(self):
        self.client.patch(
            UPDATE_WHITELISTED_URL_OR_FILE_HASH_OR_IP_LIST.format(field_id="field-7"),
            headers=self.get_headers(),
            json=self.update_whitelisted_ip_list_payload,
        )

    @task(5)
    def get_whitelisted_list_of_regular_expression(self):
        self.client.get(GET_WHITELISTED_URL_FILE_HASH_IP_LIST, headers=self.get_headers())


class CaseSettingsPageCaseTypesTab(BaseApiTest, BaseCaseManagement):
    def on_start(self) -> None:
        super().on_start()
        self.case_type_id = self.pre_requisite_create_case_type()
        self.field_id = self.pre_requisite_create_field()
        self.manual_task_id = self.pre_requisite_add_manual_task_to_case_type()
        self.integration_task_id = self.pre_requisite_add_integration_task_to_case_type()
        # self.command_task_id = self.pre_requisite_add_command_task_to_case_type()

    def on_stop(self) -> None:
        for cleanup in [
            (self.clean_up_delete_case_type, self.case_type_id),
            (self.clean_up_delete_task_in_case_type, self.manual_task_id),
            (self.clean_up_delete_task_in_case_type, self.integration_task_id),
            (self.clean_up_delete_field, self.field_id)
        ]:
            cleanup[0](cleanup[1])

    def pre_requisite_create_field(self) -> str:
        self.create_field_payload["displayName"] = f"description_{self.generate_random_number()}"
        self.create_field_payload["fieldName"] = f"field_{self.generate_random_number()}"
        response = self._make_request(
            self.API_BASE, "post", "/fields", json=self.create_field_payload
        )
        return response.json().get("result", {}).get("id") if response.status_code == 200 else ""

    def pre_requisite_create_case_type(self) -> str:
        self.create_case_type_payload["name"] = f"new_case_{self.generate_random_number()}"
        response = self._make_request(
            self.API_BASE, "post", "/case-types", json=self.create_case_type_payload
        )
        return (
            f'lhcasetype-{response.json().get("result", {}).get("id")}'
            if response.status_code == 200
            else ""
        )

    def pre_requisite_add_manual_task_to_case_type(self) -> str:
        self.add_manual_task_to_case_type_payload[
            "title"
        ] = f"manual_task_new_case_{self.generate_random_number()}"
        response = self._make_request(
            self.API_BASE,
            "post",
            f"/case-types/{self.case_type_id}/task-templates",
            json=self.add_manual_task_to_case_type_payload,
        )
        return response.json().get("result", {}).get("id") if response.status_code == 200 else ""

    def pre_requisite_add_integration_task_to_case_type(self) -> str:
        self.add_integration_task_to_case_type_payload[
            "title"
        ] = f"new_integration_task{self.generate_random_number()}"
        response = self._make_request(
            self.API_BASE,
            "post",
            f"/case-types/{self.case_type_id}/task-templates",
            json=self.add_integration_task_to_case_type_payload,
        )
        return response.json().get("result", {}).get("id") if response.status_code == 200 else ""

    def pre_requisite_add_command_task_to_case_type(self) -> str:
        # TODO need flow_id for this request
        self.add_command_task_to_case_type_payload[
            "title"
        ] = f"new_command_task_{self.generate_random_number()}"
        self.add_command_task_to_case_type_payload["flowId"] = self.flow_id
        response = self._make_request(
            self.API_BASE,
            "post",
            f"/case-types/{self.case_type_id}/task-templates",
            json=self.add_command_task_to_case_type_payload,
        )
        return response.json().get("result", {}).get("id") if response.status_code == 200 else ""

    def clean_up_delete_case_type(self, case_type_id: str) -> None:
        self.make_request(self.API_BASE, "delete", f"/case-types/{case_type_id}", json={})

    def clean_up_delete_task_in_case_type(self, task_id: str) -> None:
        self.make_request(self.API_BASE, "delete", f"/task-template/{task_id}", json={})

    def clean_up_delete_field(self, field_id: str) -> None:
        self.make_request(self.API_BASE, "delete", f"/fields/{field_id}", json={})

    @task(2)
    def create_case_type(self):
        self.create_case_type_payload["name"] = f"new_case_{self.generate_random_number()}"
        response = self.client.post(
            CREATE_CASE_TYPE,
            headers=self.get_headers(),
            json=self.create_case_type_payload,
        )
        if response.status_code == 200:
            case_type_id = f'lhcasetype-{response.json().get("result", {}).get("id")}'
            self.clean_up_delete_case_type(case_type_id)

    @task(5)
    def update_case_type(self):
        self.update_case_type_payload["name"] = f"new_case_{self.generate_random_number()}"
        self.client.patch(
            UPDATE_CASE_TYPE.format(case_type_id=self.case_type_id),
            headers=self.get_headers(),
            json=self.update_case_type_payload,
        )

    @task(2)
    def add_field_and_remove_field_from_case_type(self):
        self.client.post(
            ADD_OR_REMOVE_FIELD_FROM_CASE_TYPE.format(
                case_type_id=self.case_type_id, field_id=self.field_id
            ),
            headers=self.get_headers(),
            json=self.add_field_to_case_type_payload,
        )
        self.client.delete(
            ADD_OR_REMOVE_FIELD_FROM_CASE_TYPE.format(
                case_type_id=self.case_type_id, field_id=self.field_id
            ),
            headers=self.get_headers(),
            json={},
        )

    # @task(5)
    # def reassign_status_workflow_to_case_type(self):
    #     #TODO need status workflow from id, status workflow reassign id for this request
    #     self.reassign_status_workflow_to_case_type_payload["caseTypeId"] = self.case_type_id
    #     self.reassign_status_workflow_to_case_type_payload["from"] = self.status_workflow_id
    #     self.reassign_status_workflow_to_case_type_payload["to"] = self.reassign_to_status_workflow_id
    #     self.client.post(REASSIGN_STATUS_WORKFLOW_TO_CASE_TYPE, headers=self.get_headers(), json=self.reassign_status_workflow_to_case_type_payload)

    @task(2)
    def add_manual_task_to_case_type(self):
        self.add_manual_task_to_case_type_payload[
            "title"
        ] = f"manual_task_new_case_{self.generate_random_number()}"
        response = self.client.post(
            CREATE_TASK_FOR_CASE_TYPE.format(case_type_id=self.case_type_id),
            headers=self.get_headers(),
            json=self.add_manual_task_to_case_type_payload,
        )
        if response.status_code == 200:
            task_id = response.json().get("result", {}).get("id")
            self.clean_up_delete_task_in_case_type(task_id)

    @task(5)
    def update_manual_task_in_case_type(self):
        self.update_manual_task_in_case_type_payload[
            "title"
        ] = f"manual_task_new_case_{self.generate_random_number()}"
        self.client.patch(
            EDIT_OR_DELETE_TASK_IN_CASE_TYPE.format(task_id=self.manual_task_id),
            headers=self.get_headers(),
            json=self.update_manual_task_in_case_type_payload,
        )

    # @task(1)
    # def delete_task_in_case_type(self):
    #     task_id = self.pre_requisite_add_manual_task_to_case_type()
    #     self.client.delete(EDIT_OR_DELETE_TASK_IN_CASE_TYPE.format(task_id=task_id), headers=self.get_headers(), json={})

    @task(2)
    def add_integration_task_to_case_type(self):
        self.add_integration_task_to_case_type_payload[
            "title"
        ] = f"new_integration_task{self.generate_random_number()}"
        response = self.client.post(
            CREATE_TASK_FOR_CASE_TYPE.format(case_type_id=self.case_type_id),
            headers=self.get_headers(),
            json=self.add_integration_task_to_case_type_payload,
        )
        if response.status_code == 200:
            task_id = response.json().get("result", {}).get("id")
            self.clean_up_delete_task_in_case_type(task_id)

    @task(5)
    def update_integration_task_in_case_type(self):
        self.update_integration_task_in_case_type_payload[
            "title"
        ] = f"new_integration_task{self.generate_random_number()}"
        self.client.patch(
            EDIT_OR_DELETE_TASK_IN_CASE_TYPE.format(task_id=self.integration_task_id),
            headers=self.get_headers(),
            json=self.update_integration_task_in_case_type_payload,
        )

    # @task(2)
    # def add_command_task_to_case_type(self):
    #     #TODO need flow_id for this request
    #     self.add_command_task_to_case_type_payload["title"] = f"new_command_task_{self.generate_random_number()}"
    #     self.add_command_task_to_case_type_payload["flowId"] = self.flow_id
    #     self.client.post(CREATE_TASK_FOR_CASE_TYPE.format(case_type_id=self.case_type_id), headers=self.get_headers(), json=self.add_command_task_to_case_type_payload)

    # @task(5)
    # def update_command_task_in_case_type(self):
    # TODO dependent on add command task
    #     self.update_command_task_in_case_type_payload["title"] = f"new_command_task_{self.generate_random_number()}"
    #     self.client.patch(EDIT_OR_DELETE_TASK_IN_CASE_TYPE.format(task_id=self.command_task_id), headers=self.get_headers(), json=self.update_command_task_in_case_type_payload)

    # @task(1)
    # def deprecate_case_type(self):
    #     case_type_id = self.pre_requisite_create_case_type()
    #     self.client.patch(DEPRECATE_CASE_TYPE.format(case_type_id=case_type_id), headers=self.get_headers(), json={})
    #     self.clean_up_delete_case_type(case_type_id)

    # @task(1)
    # def delete_case_type(self):
    #     case_type_id = self.pre_requisite_create_case_type()
    #     self.client.delete(DELETE_CASE_TYPE.format(case_type_id=case_type_id), headers=self.get_headers(), json={})

    @task(9)
    def get_case_types(self):
        self.client.get(
            GET_CASE_TYPES,
            headers=self.get_headers(),
        )


class CaseSettingsPageFieldsTab(BaseApiTest, BaseCaseManagement):
    def on_start(self) -> None:
        super().on_start()
        self.field_id = self.pre_requisite_create_a_field()

    def on_stop(self) -> None:
        self.clean_up_delete_field(self.field_id)

    def pre_requisite_create_a_field(self) -> str:
        self.create_field_payload["displayName"] = f"description_{self.generate_random_number()}"
        self.create_field_payload["fieldName"] = f"field_{self.generate_random_number()}"
        response = self._make_request(
            self.API_BASE, "post", "/fields", json=self.create_field_payload
        )
        return response.json().get("result", {}).get("id") if response.status_code == 200 else ""

    def clean_up_delete_field(self, field_id: str) -> None:
        self.make_request(self.API_BASE, "delete", f"/fields/{field_id}", json={})

    @task(9)
    def get_fields(self):
        self.client.get(
            GET_FIELDS,
            headers=self.get_headers(),
        )

    @task(3)
    def get_fields_field_name_sorted_ascending(self):
        self.client.get(
            GET_FIELDS_FIELD_NAME_COLUMN_ASCENDING,
            headers=self.get_headers(),
        )

    @task(3)
    def get_fields_field_name_sorted_descending(self):
        self.client.get(
            GET_FIELDS_FIELD_NAME_COLUMN_DESCENDING,
            headers=self.get_headers(),
        )

    @task(3)
    def get_fields_display_name_sorted_ascending(self):
        self.client.get(
            GET_FIELDS_DISPLAY_NAME_COLUMN_ASCENDING,
            headers=self.get_headers(),
        )

    @task(3)
    def get_fields_display_name_sorted_descending(self):
        self.client.get(
            GET_FIELDS_DISPLAY_NAME_COLUMN_DESCENDING,
            headers=self.get_headers(),
        )

    @task(3)
    def get_fields_description_sorted_ascending(self):
        self.client.get(
            GET_FIELDS_DESCRIPTION_COLUMN_ASCENDING,
            headers=self.get_headers(),
        )

    @task(3)
    def get_fields_description_sorted_descending(self):
        self.client.get(
            GET_FIELDS_DESCRIPTION_COLUMN_DESCENDING,
            headers=self.get_headers(),
        )

    @task(3)
    def get_fields_field_type_sorted_ascending(self):
        self.client.get(
            GET_FIELDS_FIELD_TYPE_COLUMN_ASCENDING,
            headers=self.get_headers(),
        )

    @task(3)
    def get_fields_field_type_sorted_descending(self):
        self.client.get(
            GET_FIELDS_FIELD_TYPE_COLUMN_DESCENDING,
            headers=self.get_headers(),
        )

    @task(3)
    def get_fields_owner_sorted_ascending(self):
        self.client.get(
            GET_FIELDS_OWNER_COLUMN_ASCENDING,
            headers=self.get_headers(),
        )

    @task(3)
    def get_fields_owner_sorted_descending(self):
        self.client.get(
            GET_FIELDS_OWNER_COLUMN_DESCENDING,
            headers=self.get_headers(),
        )

    @task(7)
    def get_fields_with_search(self):
        self.client.post(
            GET_FIELDS_WITH_SEARCH,
            headers=self.get_headers(),
            json=self.get_fields_with_search_payload,
        )

    @task(9)
    def get_field_details(self):
        self.client.get(
            GET_FIELD_DETAILS.format(field_id=self.field_id),
            headers=self.get_headers(),
        )

    @task(2)
    def create_field(self):
        self.create_field_payload["displayName"] = f"description_{self.generate_random_number()}"
        self.create_field_payload["fieldName"] = f"field_{self.generate_random_number()}"
        response = self.client.post(
            CREATE_FIELD,
            headers=self.get_headers(),
            json=self.create_field_payload,
        )
        if response.status_code == 200:
            field_id = response.json().get("result", {}).get("id")
            self.clean_up_delete_field(field_id)

    @task(5)
    def update_field(self):
        self.update_field_payload["displayName"] = f"description_{self.generate_random_number()}"
        self.update_field_payload["fieldName"] = f"field_{self.generate_random_number()}"
        self.client.patch(
            EDIT_OR_DELETE_FIELD.format(field_id=self.field_id),
            headers=self.get_headers(),
            json=self.update_field_payload,
        )

    # @task(1)
    # def deprecate_field(self):
    #     field_id = self.pre_requisite_create_a_field()
    #     self.client.patch(DEPRECATE_FIELD.format(field_id=field_id), headers=self.get_headers(), json={})
    #     self.clean_up_delete_field(field_id)

    # @task(1)
    # def delete_field(self):
    #     field_id = self.pre_requisite_create_a_field()
    #     self.client.delete(EDIT_OR_DELETE_FIELD.format(field_id=field_id), headers=self.get_headers(), json={})


class CaseSettingsPagePriorityTab(BaseApiTest, BaseCaseManagement):
    def on_start(self) -> None:
        super().on_start()
        self.priority_id = self.pre_requisite_create_priority()
        self.all_priorities_array = self.pre_requisite_get_priority()
        self.reassign_priority_id = self.all_priorities_array[0]

    def on_stop(self) -> None:
        self.clean_up_delete_and_reassign_priority(self.priority_id)

    def pre_requisite_create_priority(self) -> str:
        self.create_priority_payload["name"] = f"New_Priority_{self.generate_random_number()}"
        response = self._make_request(
            self.API_BASE, "post", "/cases/priority", json=self.create_priority_payload
        )
        return response.json().get("result", {}).get("id") if response.status_code == 200 else ""

    def pre_requisite_get_priority(self) -> List[str]:
        response = self._make_request(self.API_BASE, "get", "/cases/priority")
        return (
            [item.get("id") for item in response.json().get("result", {}).get("data", [])]
            if response.status_code == 200
            else []
        )

    def clean_up_delete_and_reassign_priority(self, priority_id: str) -> None:
        self.delete_and_reassign_priority_payload["from"] = priority_id
        self.delete_and_reassign_priority_payload["to"] = self.reassign_priority_id
        self._make_request(
            self.API_BASE,
            "post",
            "/cases/priority/deleteAndReassign",
            json=self.delete_and_reassign_priority_payload,
        )

    @task(2)
    def create_priority(self):
        self.create_priority_payload["name"] = f"New_Priority_{self.generate_random_number()}"
        response = self.client.post(
            GET_OR_CREATE_PRIORITY,
            headers=self.get_headers(),
            json=self.create_priority_payload,
        )
        priority_id = response.json().get("result", {}).get("id")
        self.clean_up_delete_and_reassign_priority(priority_id)

    @task(9)
    def get_priority(self):
        self.client.get(GET_OR_CREATE_PRIORITY, headers=self.get_headers())

    @task(5)
    def update_priority(self):
        self.update_priority_payload["name"] = f"Update_Priority_{self.generate_random_number()}"
        self.client.patch(
            EDIT_PRIORITY.format(priority_id=self.priority_id),
            headers=self.get_headers(),
            json=self.update_priority_payload,
        )

    @task(3)
    def reorder_priority(self):
        self.reorder_priority_payload["order"] = self.pre_requisite_get_priority()
        self.client.put(
            REORDER_PRIORITY,
            headers=self.get_headers(),
            json=self.reorder_priority_payload,
        )

    @task(1)
    def delete_and_reassign_priority(self):
        self.delete_and_reassign_priority_payload["from"] = self.pre_requisite_create_priority()
        self.delete_and_reassign_priority_payload["to"] = self.reassign_priority_id
        self.client.post(
            DELETE_AND_REASSIGN_PRIORITY,
            headers=self.get_headers(),
            json=self.delete_and_reassign_priority_payload,
        )


class CaseSettingsPageStatusWorkflowTab(BaseApiTest, BaseCaseManagement):
    def on_start(self) -> None:
        super().on_start()
        self.status_workflow_id = self.pre_requisite_create_and_publish_status_workflow()
        self.reassign_status_workflow_id = self.pre_requisite_get_status_workflow()

    def on_stop(self) -> None:
        self.clean_up_delete_and_reassign_status_workflow(self.status_workflow_id)

    def pre_requisite_create_and_publish_status_workflow(self) -> str:
        self.create_status_workflow_payload[
            "name"
        ] = f"New Status Workflow{self.generate_random_number()}"
        response = self._make_request(
            self.API_BASE,
            "post",
            "/cases/status-workflow",
            json=self.create_status_workflow_payload,
        )
        if response.status_code == 200:
            status_workflow_id = response.json().get("result", {}).get("id")
            self.publish_status_workflow_payload["id"] = status_workflow_id
            self._make_request(
                self.API_BASE,
                "post",
                "/cases/status-workflow/publish",
                json=self.publish_status_workflow_payload,
            )
            return status_workflow_id if response.status_code == 200 else ""

    def pre_requisite_publish_cloned_status_workflow(self, cloned_status_workflow_id: str) -> None:
        self.clone_status_workflow_payload["id"] = cloned_status_workflow_id
        self._make_request(
            self.API_BASE,
            "post",
            "/cases/status-workflow/publish",
            json=self.clone_status_workflow_payload,
        )

    def pre_requisite_get_status_workflow(self) -> str:
        response = self._make_request(self.API_BASE, "get", "/cases/status-workflow")
        return (
            response.json().get("result", {}).get("data", [{}])[0].get("id")
            if response.status_code == 200
            else ""
        )

    def clean_up_delete_and_reassign_status_workflow(self, status_workflow_id: str) -> None:
        self.delete_and_reassign_status_workflow_payload["from"] = status_workflow_id
        self.delete_and_reassign_status_workflow_payload["to"] = self.reassign_status_workflow_id
        self._make_request(
            self.API_BASE,
            "post",
            "/cases/status-workflow/deleteAndReassign",
            json=self.delete_and_reassign_status_workflow_payload,
        )

    @task(2)
    def create_and_publish_status_workflow(self):
        self.create_status_workflow_payload[
            "name"
        ] = f"New Status Workflow{self.generate_random_number()}"
        response = self.client.post(
            GET_OR_CREATE_STATUS_WORKFLOW,
            headers=self.get_headers(),
            json=self.create_status_workflow_payload,
        )
        status_workflow_id = response.json().get("result", {}).get("id")
        self.publish_status_workflow_payload["id"] = status_workflow_id
        self.client.post(
            PUBLISH_STATUS_WORKFLOW,
            headers=self.get_headers(),
            json=self.publish_status_workflow_payload,
        )

        self.clean_up_delete_and_reassign_status_workflow(status_workflow_id)

    @task(5)
    def update_status_workflow(self):
        self.update_status_workflow_payload[
            "name"
        ] = f"Update Status Workflow {self.generate_random_number()}"
        self.client.patch(
            EDIT_STATUS_WORKFLOW.format(status_workflow_id=self.status_workflow_id),
            headers=self.get_headers(),
            json=self.update_status_workflow_payload,
        )

    @task(9)
    def get_status_workflows(self):
        self.client.get(GET_OR_CREATE_STATUS_WORKFLOW, headers=self.get_headers())

    @task(9)
    def get_status_workflow_is_used_status(self):
        self.client.get(
            GET_IS_USED_STATUS_WORKFLOW.format(status_workflow_id=self.status_workflow_id),
            headers=self.get_headers(),
        )

    @task(2)
    def clone_status_workflow(self):
        response = self.client.post(
            CLONE_STATUS_WORKFLOW.format(status_workflow_id=self.status_workflow_id),
            headers=self.get_headers(),
            json={},
        )
        cloned_status_workflow_id = response.json().get("result", {}).get("id")
        self.pre_requisite_publish_cloned_status_workflow(cloned_status_workflow_id)
        self.clean_up_delete_and_reassign_status_workflow(cloned_status_workflow_id)

    @task(1)
    def delete_and_reassign_status_workflow(self):
        self.delete_and_reassign_status_workflow_payload[
            "from"
        ] = self.pre_requisite_create_and_publish_status_workflow()
        self.delete_and_reassign_status_workflow_payload["to"] = self.reassign_status_workflow_id
        self.client.post(
            DELETE_AND_REASSIGN_STATUS_WORKFLOW,
            headers=self.get_headers(),
            json=self.delete_and_reassign_status_workflow_payload,
        )
