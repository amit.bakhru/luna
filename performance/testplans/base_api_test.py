#!/usr/bin/env python

import os
import random
import re
import requests
from pathlib import Path
import base64
from locust import HttpUser, between


class BaseApiTest(HttpUser):
    """Base class for API testing with common functionality"""

    abstract = True
    wait_time = between(0.5, 1)
    host = "https://app.stage.devo.com"
    access_token: str | None = None

    def _make_request(
        self, api_base: str, method: str, endpoint: str, **kwargs
    ) -> requests.Response:
        """Helper method to make API requests"""
        url = f"{api_base}{endpoint}"

        headers = self.get_headers()
        if "headers" in kwargs:
            headers.update(kwargs.pop("headers"))

        return getattr(self.client, method)(url, headers=headers, **kwargs)
    
    def make_request(self, api_base: str, method: str, endpoint: str, json = {}) -> requests.Response:
        """Helper method to make API requests using 'requests' lib"""
        url = f"{self.host}{api_base}{endpoint}"
        headers = self.get_headers()
        match method:
            case "get":
                return requests.get(url=url, headers=headers)
            case "post":
                return requests.post(url=url, headers=headers, json=json)
            case "put":
                return requests.put(url=url, headers=headers, json=json)
            case "patch":
                return requests.patch(url=url, headers=headers, json=json)
            case "delete":
                return requests.delete(url=url, headers=headers, json=json)

    def generate_random_number(self) -> int:
        """Generate a random number between 1 and 100000"""
        return random.randint(1, 100000)

    def fetch_numeric_part(self, value: str, prefix="") -> str:
        """Extract numeric part from a string with given prefix"""
        match = re.search(rf"{prefix}(\d+)", value)
        return match.group(1) if match else ""

    def encode_file_to_base64(self, file_path: str) -> str:
        script_dir = Path(__file__).resolve().parent
        file_path = script_dir.parents[1] / file_path
        with open(file_path, "rb") as file:
            return base64.b64encode(file.read()).decode("utf-8")

    def get_headers(self) -> dict:
        """Get common headers for API requests"""
        return {"Content-Type": "application/json", "authorization": f"Bearer {self.access_token}"}

    def _get_access_token(self) -> str:
        """Get access token using requests"""
        url = "https://api-internal.int.devo.com/tapu/token"
        headers = {
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": f"Basic {os.getenv('TAPU_AUTHORIZATION_TOKEN')}",
        }
        data = {
            "grant_type": "client_credentials",
            "user": "indrajeetkumar.sah@devo.com",
            "token_type": "bearer",
            "domain": "security_intelligence",
            "owner": "indrajeetkumar.sah@devo.com",
        }

        response = requests.post(url, headers=headers, data=data)
        return response.json().get("access_token")

    def on_start(self) -> None:
        """Common setup before tests start"""
        self.access_token = self._get_access_token()
