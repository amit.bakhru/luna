#!/usr/bin/env python

from typing import List

from performance.testplans.base_api_test import BaseApiTest

from locust import task

from performance.testplans import (
    GET_SCRIPTS,
    DOWNLOAD_SCRIPT,
    GET_INTEGRATION_CONNECTIONS,
    SCHEDULE_PLAYBOOKS,
    GET_SCRIPT,
    ADD_TEST_DATA_FOR_SCRIPT,
    RUN_SCRIPT,
)


class BaseScript:
    API_BASE = "/case-management/api"

    abstract = True

    create_script_payload = {}

    save_script_payload = {"id": ""}

    delete_script_payload = {}

    get_scripts_payload = {"filters": {}, "query": "name:(**)", "pageSize": 25, "page": 0}

    download_script_payload = [""]

    create_session_payload = {
        "importContent": [{"zipContent": "data:application/zip;base64", "resourceType": "Script"}]
    }

    complete_import_payload = {}

    schedule_playbooks_payload = {"sessionId": ""}

    add_test_data_for_script_payload = {
        "content": '[{\n  "num1": 5,\n  "num2": 10\n  },\n  {\n  "num1": 4,\n  "num2": 3\n  }\n  ]\n'
    }

    run_script_payload = {
        "args": ["$.num1", "$.num2"],
        "explodeFields": True,
        "functionName": "sum_and_product",
        "id": "",
        "rowsToProcess": None,
        "testDataId": 1,
    }


class ScriptListingPage(BaseApiTest, BaseScript):
    API_BASE = "/case-management/api"

    def on_start(self) -> None:
        super().on_start()
        self.script_id = self.pre_requisite_create_script()
        self.pre_requisite_save_script(self.script_id)
        self.script_ids = self.pre_requisite_get_scripts()
        self.session_id = self.pre_requisite_create_and_import_session()

    def on_stop(self) -> None:
        self.clean_up_delete_script(self.script_id)

    def pre_requisite_create_script(self) -> str:
        response = self._make_request(
            self.API_BASE, "post", "/execution-scripts/new", json=self.create_script_payload
        )
        return response.json().get("result", {}).get("id") if response.status_code == 200 else ""

    def pre_requisite_save_script(self, script_id: str) -> None:
        self.save_script_payload["id"] = script_id
        self.make_request(
            self.API_BASE,
            "patch",
            f"/execution-scripts/{script_id}?checkForUse=true",
            json=self.save_script_payload,
        )

    def pre_requisite_get_scripts(self) -> List[str]:
        response = self._make_request(
            self.API_BASE, "post", "/search/entities/scripts", json=self.get_scripts_payload
        )
        return response.json().get("result", {}).get("data", [])

    def pre_requisite_create_and_import_session(self) -> str:
        file_base64_encode = self.encode_file_to_base64(
            file_path="performance/testplans/script.zip"
        )
        self.create_session_payload["importContent"] = [
            {
                "zipContent": f"data:application/zip;base64,{file_base64_encode}",
                "resourceType": "Script",
            }
        ]
        response = self.make_request(
            self.API_BASE, "post", "/import/create-session", json=self.create_session_payload
        )
        session_id = (
            response.json().get("result", {}).get("sessionId")
            if response.status_code == 200
            else ""
        )

        self.make_request(self.API_BASE, "get", f"/import/{session_id}")
        return session_id

    def clean_up_delete_script(self, script_id: str) -> None:
        self.make_request(
            self.API_BASE,
            "delete",
            f"/script/{script_id}",
            json=self.delete_script_payload,
        )

    @task(9)
    def get_scripts(self):
        self.client.post(GET_SCRIPTS, headers=self.get_headers(), json=self.get_scripts_payload)

    # # @task(2)
    # # def create_and_save_script(self):
    # #     #TODO this is generating unique endpoint, making report too long
    # #     response = self.client.post(
    # #         CREATE_SCRIPT, headers=self.get_headers(), json=self.create_script_payload
    # #     )
    # #     if response.status_code == 200:
    # #         script_id = response.json().get("result", {}).get("id")
    # #         self.save_script_payload["id"] = script_id
    # #         self.client.patch(
    # #             SAVE_SCRIPT.format(script_id=script_id), headers=self.get_headers(), json=self.save_script_payload
    # #         )
    # #         self.clean_up_delete_script(script_id)
    # #
    # # @task(1)
    # # def delete_script(self):
    # #     #TODO this is generating unique endpoint, making report too long
    # #     script_id = self.pre_requisite_create_script()
    # #     self.pre_requisite_save_script(script_id)
    # #     self.client.delete(
    # #         DELETE_SCRIPT.format(script_id=script_id), headers=self.get_headers(), json=self.delete_script_payload
    # #     )

    @task(2)
    def download_script(self):
        self.download_script_payload = [self.script_id]
        self.client.post(
            DOWNLOAD_SCRIPT, headers=self.get_headers(), json=self.download_script_payload
        )

    #     # @task(2)
    #     # def import_script(self):
    #     #     #TODO this is generating unique endpoint for import session and delete script request, making report too long
    #     #     file_base64_encode = self.encode_file_to_base64(file_path="performance/testplans/script.zip")
    #     #     self.create_session_payload["importContent"] = [{
    #     #             "zipContent": f"data:application/zip;base64,{file_base64_encode}",
    #     #                           "resourceType": "Script"
    #     #         }]
    #     #     response = self.client.post(CREATE_SCRIPT_SESSION, headers=self.get_headers(), json=self.create_session_payload)
    #     #
    #     #     if response.status_code == 200:
    #     #         session_id = response.json().get("result", {}).get("sessionId")
    #     #
    #     #         self.client.get(IMPORT_SCRIPT_SESSION.format(session_id=session_id), headers=self.get_headers())
    #     #
    #     #         self.client.get(COMPLETE_SCRIPT_IMPORT.format(session_id=session_id), headers=self.get_headers(), json=self.complete_import_payload)
    #     #
    #     #         self.clean_up_delete_script("new_script_1449.py")

    @task(2)
    def get_integration_connections(self):
        self.client.get(
            GET_INTEGRATION_CONNECTIONS.format(session_id=self.session_id),
            headers=self.get_headers(),
        )

    @task(2)
    def schedule_playbooks(self):
        self.schedule_playbooks_payload["sessionId"] = self.session_id
        self.client.post(
            SCHEDULE_PLAYBOOKS, headers=self.get_headers(), json=self.schedule_playbooks_payload
        )


class ScriptDetailsPage(BaseApiTest, BaseScript):
    API_BASE = "/case-management/api"

    def on_start(self) -> None:
        super().on_start()
        self.script_id = self.pre_requisite_create_script()
        self.pre_requisite_save_script(self.script_id)

    def on_stop(self) -> None:
        self.clean_up_delete_script(self.script_id)

    def pre_requisite_create_script(self) -> str:
        response = self._make_request(
            self.API_BASE, "post", "/execution-scripts/new", json=self.create_script_payload
        )
        return response.json().get("result", {}).get("id") if response.status_code == 200 else ""

    def pre_requisite_save_script(self, script_id: str) -> None:
        self.save_script_payload["id"] = script_id
        self.make_request(
            self.API_BASE,
            "patch",
            f"/execution-scripts/{script_id}?checkForUse=true",
            json=self.save_script_payload,
        )

    def clean_up_delete_script(self, script_id: str) -> None:
        self.make_request(
            self.API_BASE,
            "delete",
            f"/script/{script_id}",
            json=self.delete_script_payload,
        )

    @task(9)
    def get_script(self):
        self.client.get(GET_SCRIPT.format(script_id=self.script_id), headers=self.get_headers())

    @task(7)
    def add_test_data_for_script(self):
        self.client.post(
            ADD_TEST_DATA_FOR_SCRIPT,
            headers=self.get_headers(),
            json=self.add_test_data_for_script_payload,
        )

    @task(9)
    def run_script(self):
        self.run_script_payload["id"] = self.script_id
        self.client.post(RUN_SCRIPT, headers=self.get_headers(), json=self.run_script_payload)
