#!/usr/bin/env python
import time
from typing import List

from performance.testplans.base_api_test import BaseApiTest

from locust import task

from performance.testplans import (
    CREATE_COMMAND,
    GET_COMMANDS,
    GET_COMMAND_FILTERS,
    DELETE_COMMAND,
    RENAME_COMMAND,
    UPDATE_COMMAND,
    GET_FORM,
)


class BaseCommand:
    API_BASE = "/case-management/api"

    abstract = True

    create_command_payload = {"name": "", "description": ""}

    get_commands_payload = {
        "filters": [],
        "offset": 0,
        "pageSize": 25,
        "sortColumn": "lastUpdated",
        "sortOrder": "DESC",
    }

    delete_command_payload = [{"id": "", "entityTypeId": "command"}]

    create_session_payload = {"flowIds": [""], "downloadWithAssistantData": False}

    copy_command_payload = {
        "entityIds": [{"id": "", "entityTypeId": "command"}],
        "folderId": "b8d953a8-2b15-4d2e-9919-1ce3f9399047",
    }

    download_command_payload = []

    rename_command_payload = {"entityId": {"id": "", "entityTypeId": "command"}, "name": ""}

    update_command_payload = {"name": ""}

    edit_step_payload = {"interval": {"from": 1738661155000, "to": 1738662055000}}

    create_form_payload = {
        "data": {
            "parameterName0": "Address",
            "parameterDescription0": "New Address",
            "parameterDefault0": "",
            "parameterMandatory0": False,
        }
    }


class CommandListingPage(BaseApiTest, BaseCommand):
    API_BASE = "/case-management/api"

    def on_start(self) -> None:
        super().on_start()
        self.flow_id = self.pre_requisite_create_command()
        self.flow_ids = self.pre_requisite_get_commands()
        # self.session_id = self.pre_requisite_create_session()

    def on_stop(self) -> None:
        self.clean_up_delete_command(self.flow_id)

    def pre_requisite_create_command(self) -> str:
        self.create_command_payload["name"] = f"new_command_{self.generate_random_number()}"
        response = self._make_request(
            self.API_BASE, "post", "/commands", json=self.create_command_payload
        )
        return (
            response.json().get("result", {}).get("flowId") if response.status_code == 200 else ""
        )

    def pre_requisite_get_commands(self) -> List[str]:
        response = self._make_request(
            self.API_BASE,
            "post",
            "/content-management/content/command?libraryView=all",
            json=self.get_commands_payload,
        )
        return response.json().get("result", {}).get("data", {}).get("data", [])

    def pre_requisite_create_session(self) -> str:
        self.create_session_payload["flowIds"] = [self.flow_id]
        response = self._make_request(
            self.API_BASE, "post", "/export/create-session", json=self.create_session_payload
        )
        return (
            response.json().get("result", {}).get("sessionId")
            if response.status_code == 200
            else ""
        )

    def clean_up_delete_command(self, flow_id: str) -> None:
        self.delete_command_payload[0]["id"] = self.fetch_numeric_part(flow_id, "flow-")
        self._make_request(
            self.API_BASE,
            "post",
            "/content-management/delete",
            json=self.delete_command_payload,
        )

    @task(9)
    def get_commands(self):
        self.client.post(GET_COMMANDS, headers=self.get_headers(), json=self.get_commands_payload)

    @task(9)
    def get_command_filters(self):
        self.client.get(GET_COMMAND_FILTERS, headers=self.get_headers())

    @task(2)
    def create_command(self):
        self.create_command_payload["name"] = f"new_command_{self.generate_random_number()}"
        response = self.client.post(
            CREATE_COMMAND, headers=self.get_headers(), json=self.create_command_payload
        )
        if response.status_code == 200:
            flow_id = response.json().get("result", {}).get("flowId")
            self.clean_up_delete_command(flow_id)

    # @task(2)
    # def copy_command(self):
    #     #TODO response does not have flow id which can be used for command deletion, also folderId is static in payload
    #     self.copy_command_payload["entityIds"] = [{
    #             "id": self.fetch_numeric_part(self.flow_id, "flow-"),
    #             "entityTypeId": "command"
    #         }]
    #
    #     self.client.post(
    #         COPY_COMMAND, headers=self.get_headers(), json=self.copy_command_payload
    #     )
    #
    #
    # @task(2)
    # def create_session_and_download_command(self):
    #     #TODO this create multiple unique endpoint for each session
    #     self.create_session_payload["flowIds"] = [self.flow_id]
    #     response = self.client.post(
    #         CREATE_DOWNLOAD_COMMAND_SESSION, headers=self.get_headers(), json=self.create_session_payload
    #     )
    #     if response.status_code == 200:
    #         session_id = response.json().get("result", {}).get("sessionId")
    #         self.client.post(
    #             DOWNLOAD_COMMAND.format(session_id=session_id), headers=self.get_headers(), json=self.download_command_payload
    #         )

    @task(1)
    def delete_command(self):
        flow_id = self.pre_requisite_create_command()
        self.delete_command_payload[0]["id"] = self.fetch_numeric_part(flow_id, "flow-")
        self.client.post(
            DELETE_COMMAND, headers=self.get_headers(), json=self.delete_command_payload
        )

    @task(4)
    def rename_command(self):
        self.rename_command_payload["entityId"]["id"] = self.fetch_numeric_part(
            self.flow_id, "flow-"
        )
        self.rename_command_payload["name"] = f"renamed_command_{self.generate_random_number()}"
        self.client.post(
            RENAME_COMMAND, headers=self.get_headers(), json=self.rename_command_payload
        )

    @task(7)
    def update_command_name(self):
        self.update_command_payload["name"] = f"updated_command_{self.generate_random_number()}"
        self.client.patch(
            UPDATE_COMMAND.format(flow_id=self.flow_id),
            headers=self.get_headers(),
            json=self.update_command_payload,
        )


class CommandDetailsPage(BaseApiTest, BaseCommand):
    API_BASE = "/case-management/api"

    def on_start(self) -> None:
        super().on_start()
        self.current_time_millis = int(time.time() * 1000)
        self.fifteen_minutes_back_millis = self.current_time_millis - (15 * 60 * 1000)
        self.flow_id = self.pre_requisite_create_command()
        self.node_id = self.pre_requisite_get_playbook_details()
        self.form_id = self.pre_requisite_edit_step()
        self.pre_requisite_create_form()

    def on_stop(self) -> None:
        self.clean_up_delete_command(self.flow_id)

    def pre_requisite_create_command(self) -> str:
        self.create_command_payload["name"] = f"new_command_{self.generate_random_number()}"
        response = self._make_request(
            self.API_BASE, "post", "/commands", json=self.create_command_payload
        )
        return (
            response.json().get("result", {}).get("flowId") if response.status_code == 200 else ""
        )

    def pre_requisite_get_playbook_details(self) -> str:
        response = self.make_request(self.API_BASE, "get", f"/flow/{self.flow_id}")
        return (
            response.json().get("result", {}).get("nodes", {}).get("data", {})[0].get("id")
            if response.status_code == 200
            else ""
        )

    def pre_requisite_edit_step(self) -> str:
        self.edit_step_payload["interval"]["from"] = self.fifteen_minutes_back_millis
        self.edit_step_payload["interval"]["to"] = self.current_time_millis
        response = self.make_request(
            self.API_BASE,
            "post",
            f"/flow/{self.flow_id}/node/{self.node_id}/edit-step",
            json=self.edit_step_payload,
        )
        return (
            response.json().get("result", {}).get("formId") if response.status_code == 200 else ""
        )

    def pre_requisite_create_form(self) -> None:
        self.make_request(
            self.API_BASE, "post", f"/form/{self.form_id}/page/0", json=self.create_form_payload
        )

    def clean_up_delete_command(self, flow_id: str) -> None:
        self.delete_command_payload[0]["id"] = self.fetch_numeric_part(flow_id, "flow-")
        self._make_request(
            self.API_BASE,
            "post",
            "/content-management/delete",
            json=self.delete_command_payload,
        )

    @task(5)
    def get_form(self):
        self.client.get(GET_FORM.format(form_id=self.form_id), headers=self.get_headers())
