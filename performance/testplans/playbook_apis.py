#!/usr/bin/env python

import time
from typing import List

from performance.testplans.base_api_test import BaseApiTest

from locust import task

from performance.testplans import (
    CREATE_PLAYBOOK,
    DELETE_PLAYBOOK,
    GET_PLAYBOOKS,
    GET_PLAYBOOK_FILTERS,
    RENAME_PLAYBOOK,
    GET_PLAYBOOK_DETAILS,
    GET_PLAYBOOK_GRAPH_DETAILS,
    GET_SPARK_FUNCTIONS,
    GET_CONTENT_LIBRARY_DETAILS,
    MOVE_GRAPH,
    CREATE_SESSION,
    GET_TAGS,
    GET_NODE_RESULTS,
    RERUN_NODE,
    SEARCH_NODE,
    SEARCH_NODE_WITH_FILTER,
    GET_OPERATOR_CATALOG,
    ADD_SQL_NODE_OPERATION,
    NEW_STEP_ADD_FIELDS,
    VALIDATE_EXPRESSION,
    UPDATE_NODE_DETAILS,
    GET_SEARCH_NODE_FILTERS,
    CREATE_SEARCH_NODE_FILTERS,
    EDIT_STEP,
    GET_PLAYBOOK_VERSIONS,
    SET_LATEST_PLAYBOOK_VERSION,
    GET_DEPENDENTS_FOR_FLOW,
    PUBLISH_PLAYBOOK_VERSION,
)


class BasePlaybook:
    API_BASE = "/case-management/api"

    abstract = True
    BASE_PAGINATION_PAYLOAD = {
        "offset": 0,
        "pageSize": 25,
    }

    BASE_ENTITY_ID_PAYLOAD = {"entityId": {"id": "", "entityTypeId": "playbook"}}

    BASE_COORDINATE_PAYLOAD = {
        "x": 0,
        "y": 0,
    }

    create_playbook_payload = {"method": "addFlow", "parameters": {"name": ""}}

    get_playbook_payload = {**BASE_PAGINATION_PAYLOAD, "filters": []}

    rename_playbook_payload = {**BASE_ENTITY_ID_PAYLOAD, "name": ""}

    create_session_payload = {"flowIds": [], "downloadWithAssistantData": False}

    copy_playbook_payload = {
        "entityIds": [BASE_ENTITY_ID_PAYLOAD["entityId"]],
        "folderId": "cf69202f-75e7-462e-b5a1-91a03b3c0ba8",
    }

    delete_playbook_payload = [{"entityTypeId": "playbook", "id": ""}]

    move_graph_payload = {**BASE_COORDINATE_PAYLOAD}

    create_form_payload = {
        "data": {
            "create-data-input": {
                "data": '[{"event_time_epoch":1610469824546,"source_ip":"1.2.3.4","event_category":"UserLoginSuccess","username":"John"},{"event_time_epoch":1610469824892,"source_ip":"3.3.3.3","event_category":"UserLoginFailed","username":"Sam"}]',
                "mode": "TableMode",
            }
        }
    }

    submit_form_payload = {
        "description": "Starting with custom data to begin the playbook",
        "title": "",
    }

    new_step_create_data_table_payload = {
        "description": "",
        "id": "create-data-table",
        "kind": "OtherContentKind",
        "coord": {**BASE_COORDINATE_PAYLOAD},
        "interval": {"from": 0, "to": 0},
    }

    edit_step_payload = {"interval": {"from": 1738661155000, "to": 1738662055000}}

    search_node_payload = {
        "page": 0,
        "pageSize": 1000,
        "query": "*sql*",
        "filters": {"tags": []},
    }

    add_node_sql_operation_payload = {
        "description": "",
        "id": None,
        "isLeaf": True,
        "isNew": True,
        "kind": "augmentation",
        "lql": "select * from _Start_Node",
        "name": "SQL_Operation",
        "title": "SQL Operation",
        "x": 350,
        "y": 126,
    }

    new_step_add_fields_payload = {
        "formValue": {"addFields": [{"fieldName": "user_name", "fieldValue": "$.username"}]},
        "description": "",
        "id": "add-fields",
        "kind": "TransformContentKind",
        "coord": {**BASE_COORDINATE_PAYLOAD},
        "interval": {"from": "", "to": ""},
    }

    validate_expression_payload = {"expression": "$.username"}

    update_node_details_payload = {
        "description": "Add one or more new fields.",
        "title": "Add New Field",
        "name": "Add_New_Field",
    }

    add_output_node_payload = {
        "isLeaf": True,
        "kind": "output",
        "name": "Output",
        "nodes": [""],
        **BASE_COORDINATE_PAYLOAD,
    }

    create_search_node_filter_payload = {
        "query": "Add New Field",
        "title": "Replace add field name",
    }

    publish_playbook_version_payload = {"version": 0, "description": ""}

    get_dependents_for_flow_payload = [""]


class PlaybookListingPage(BaseApiTest, BasePlaybook):
    def on_start(self) -> None:
        super().on_start()
        self.flow_id = self.pre_requisite_create_playbook()
        self.all_flow_ids_array = self.pre_requisite_get_playbook()

    def on_stop(self) -> None:
        self.clean_up_delete_playbook(self.flow_id)

    def pre_requisite_create_playbook(self) -> str:
        self.create_playbook_payload["parameters"][
            "name"
        ] = f"new_playbook_{self.generate_random_number()}"
        response = self._make_request(
            self.API_BASE, "post", "/demo/hub", json=self.create_playbook_payload
        )
        return response.json().get("result", {}).get("id") if response.status_code == 200 else ""

    def pre_requisite_get_playbook(self) -> List[str]:
        response = self._make_request(
            self.API_BASE,
            "post",
            "/content-management/content/playbook?libraryView=all",
            json=self.get_playbook_payload,
        )
        response_data_array = response.json().get("result", {}).get("data", {}).get("data", [])
        return [
            item.get("flowId") for item in (response_data_array[:5] if response_data_array else [])
        ]

    def clean_up_delete_playbook(self, flow_id: str) -> None:
        self.delete_playbook_payload[0]["id"] = self.fetch_numeric_part(flow_id, "flow-")
        self._make_request(
            self.API_BASE, "post", "/content-management/delete", json=self.delete_playbook_payload
        )

    @task(2)
    def create_playbook(self):
        self.create_playbook_payload["parameters"][
            "name"
        ] = f"new_playbook_{self.generate_random_number()}"
        response = self.client.post(
            CREATE_PLAYBOOK,
            headers=self.get_headers(),
            json=self.create_playbook_payload,
        )
        if response.status_code == 200:
            flow_id = response.json().get("result", {}).get("id")
            self.delete_playbook_payload[0]["id"] = self.fetch_numeric_part(flow_id, "flow-")
            self.client.post(
                DELETE_PLAYBOOK,
                headers=self.get_headers(),
                json=self.delete_playbook_payload,
            )

    @task(9)
    def get_playbooks(self):
        self.client.post(
            GET_PLAYBOOKS,
            headers=self.get_headers(),
            json=self.get_playbook_payload,
        )

    @task(3)
    def get_playbooks_name_sorted_ascending(self):
        self.get_playbook_payload["sortColumn"] = "name"
        self.get_playbook_payload["sortOrder"] = "ASC"
        self.client.post(
            GET_PLAYBOOKS,
            headers=self.get_headers(),
            json=self.get_playbook_payload,
        )

    @task(3)
    def get_playbooks_name_sorted_descending(self):
        self.get_playbook_payload["sortColumn"] = "name"
        self.get_playbook_payload["sortOrder"] = "DESC"
        self.client.post(
            GET_PLAYBOOKS,
            headers=self.get_headers(),
            json=self.get_playbook_payload,
        )

    @task(3)
    def get_playbooks_owner_sorted_ascending(self):
        self.get_playbook_payload["sortColumn"] = "owner"
        self.get_playbook_payload["sortOrder"] = "ASC"
        self.client.post(
            GET_PLAYBOOKS,
            headers=self.get_headers(),
            json=self.get_playbook_payload,
        )

    @task(3)
    def get_playbooks_owner_sorted_descending(self):
        self.get_playbook_payload["sortColumn"] = "owner"
        self.get_playbook_payload["sortOrder"] = "DESC"
        self.client.post(
            GET_PLAYBOOKS,
            headers=self.get_headers(),
            json=self.get_playbook_payload,
        )

    @task(3)
    def get_playbooks_last_updated_sorted_ascending(self):
        self.get_playbook_payload["sortColumn"] = "lastUpdated"
        self.get_playbook_payload["sortOrder"] = "ASC"
        self.client.post(
            GET_PLAYBOOKS,
            headers=self.get_headers(),
            json=self.get_playbook_payload,
        )

    @task(3)
    def get_playbooks_last_updated_sorted_descending(self):
        self.get_playbook_payload["sortColumn"] = "lastUpdated"
        self.get_playbook_payload["sortOrder"] = "DESC"
        self.client.post(
            GET_PLAYBOOKS,
            headers=self.get_headers(),
            json=self.get_playbook_payload,
        )

    @task(7)
    def get_playbooks_search_value(self):
        self.get_playbook_payload["filters"] = [{"searchText": "playbook"}]
        self.client.post(
            GET_PLAYBOOKS,
            headers=self.get_headers(),
            json=self.get_playbook_payload,
        )

    @task(4)
    def get_playbooks_owner_filter_value(self):
        self.get_playbook_payload["filters"] = [{"owners": ["mukund.gupta@devo.com"]}]
        self.client.post(
            GET_PLAYBOOKS,
            headers=self.get_headers(),
            json=self.get_playbook_payload,
        )

    @task(9)
    def get_playbook_filters(self):
        self.client.get(GET_PLAYBOOK_FILTERS, headers=self.get_headers())

    @task(5)
    def rename_playbook(self):
        self.rename_playbook_payload["entityId"]["id"] = self.fetch_numeric_part(
            self.flow_id, "flow-"
        )
        self.rename_playbook_payload["name"] = f"new_playbook_{self.generate_random_number()}"
        self.client.post(
            RENAME_PLAYBOOK,
            headers=self.get_headers(),
            json=self.rename_playbook_payload,
        )

    # @task(2)
    # def create_session_and_download_playbook(self):
    #     #TODO this is creating dynamic session id resulting in alot of new endpoints
    #     self.create_session_payload["flowIds"] = [ self.flow_id ]
    #     response = self.client.post(CREATE_SESSION_FOR_PLAYBOOK_DOWNLOAD, headers=self.get_headers(), json=self.create_session_payload)
    #     session_id = response.json().get("result", {}).get("sessionId")
    #     self.client.post(DOWNLOAD_PLAYBOOK.format(session_id=session_id), headers=self.get_headers(), json=[])
    #
    # @task(2)
    # def create_session_and_download_playbooks_in_bulk(self):
    #     #TODO this is creating dynamic session id resulting in alot of new endpoints
    #     self.create_session_payload["flowIds"] = self.all_flow_ids_array
    #     response = self.client.post(CREATE_SESSION_FOR_PLAYBOOK_DOWNLOAD, headers=self.get_headers(), json=self.create_session_payload)
    #     session_id = response.json().get("result", {}).get("sessionId")
    #     self.client.post(DOWNLOAD_PLAYBOOK.format(session_id=session_id), headers=self.get_headers(), json=[])
    #
    # @task(2)
    # def copy_playbook(self):
    #     # TODO need to fetch folder id for copy request and how to delete this as no flow_id is provided
    #     self.copy_playbook_payload["entityIds"][0]["id"] = self.fetch_numeric_part(
    #         self.flow_id, "flow-"
    #     )
    #     self.client.post(
    #         COPY_PLAYBOOK,
    #         headers=self.get_headers(),
    #         json=self.copy_playbook_payload,
    #     )
    #
    # @task(2)
    # def copy_playbooks_in_bulk(self):
    #     # TODO need to fetch folder id for copy request and how to delete this as no flow_ids are provided
    #     playbook_id_numeric_part_array = [
    #         int(self.fetch_numeric_part(item, "flow-")) for item in self.all_flow_ids_array
    #     ]
    #
    #     self.copy_playbook_payload["entityIds"] = [
    #         {"id": str(id_num), "entityTypeId": "playbook"}
    #         for id_num in playbook_id_numeric_part_array
    #     ]
    #     self.client.post(
    #         COPY_PLAYBOOK,
    #         headers=self.get_headers(),
    #         json=self.copy_playbook_payload,
    #     )

    @task(1)
    def delete_playbook(self):
        flow_id = self.pre_requisite_create_playbook()
        self.delete_playbook_payload[0]["id"] = self.fetch_numeric_part(flow_id, "flow-")
        self.client.post(
            DELETE_PLAYBOOK,
            headers=self.get_headers(),
            json=self.delete_playbook_payload,
        )

    @task(1)
    def delete_playbooks_in_bulk(self):
        flow_ids = []
        for i in range(3):
            flow_ids.append(self.pre_requisite_create_playbook())
        playbook_id_numeric_part_array = [
            int(self.fetch_numeric_part(item, "flow-")) for item in flow_ids
        ]
        self.delete_playbook_payload = [
            {"id": str(id_num), "entityTypeId": "playbook"}
            for id_num in playbook_id_numeric_part_array
        ]
        self.client.post(
            DELETE_PLAYBOOK,
            headers=self.get_headers(),
            json=self.delete_playbook_payload,
        )


class PlaybookDetailsPage(BaseApiTest, BasePlaybook):
    current_time_millis = 0
    fifteen_minutes_back_millis = 0

    def on_start(self) -> None:
        super().on_start()
        self.current_time_millis = int(time.time() * 1000)
        self.fifteen_minutes_back_millis = self.current_time_millis - (15 * 60 * 1000)
        self.flow_id = self.pre_requisite_create_playbook()
        self.start_node_id = self.pre_requisite_get_playbook_details()
        self.step_session_id = self.pre_requisite_create_session()
        self.form_id = self.pre_requisite_new_step_create_data_table()
        self.node_id = self.pre_requisite_create_and_submit_form()
        self.search_node_filter_id = self.pre_requisite_create_search_node_filter()
        self.flow_version = self.pre_requisite_get_playbook_versions()

    def on_stop(self) -> None:
        for cleanup in [
            (self.clean_up_delete_search_node_filter, self.search_node_filter_id),
            (self.clean_up_delete_playbook, self.flow_id),
        ]:
            cleanup[0](cleanup[1])

    def pre_requisite_create_playbook(self) -> str:
        self.create_playbook_payload["parameters"][
            "name"
        ] = f"new_playbook_{self.generate_random_number()}"
        response = self._make_request(
            self.API_BASE, "post", "/demo/hub", json=self.create_playbook_payload
        )
        return response.json().get("result", {}).get("id") if response.status_code == 200 else ""

    def pre_requisite_get_playbook_details(self) -> str:
        response = self._make_request(self.API_BASE, "get", f"/flow/{self.flow_id}")
        return (
            response.json().get("result", {}).get("nodes", {}).get("data", [{}])[0].get("id")
            if response.status_code == 200
            else ""
        )

    def pre_requisite_create_session(self) -> str:
        response = self._make_request(
            self.API_BASE,
            "post",
            f"/flow/{self.flow_id}/node/{self.start_node_id}/create-session",
            json={},
        )
        return response.json().get("result") if response.status_code == 200 else ""

    def pre_requisite_new_step_create_data_table(self) -> str:
        self.new_step_create_data_table_payload["interval"][
            "from"
        ] = self.fifteen_minutes_back_millis
        self.new_step_create_data_table_payload["interval"]["to"] = self.current_time_millis
        response = self._make_request(
            self.API_BASE,
            "post",
            f"/flow/{self.flow_id}/node/{self.start_node_id}/new-step?stepSessionId={self.step_session_id}",
            json=self.new_step_create_data_table_payload,
        )
        return (
            response.json().get("result", {}).get("formId") if response.status_code == 200 else ""
        )

    def pre_requisite_create_and_submit_form(self) -> str:
        self.make_request(
            self.API_BASE, "post", f"/form/{self.form_id}/page/0", json=self.create_form_payload
        )
        self.submit_form_payload["title"] = f"Create_Custom_Data_{self.generate_random_number()}"
        response = self.make_request(
            self.API_BASE, "post", f"/form/{self.form_id}/submit", json=self.submit_form_payload
        )
        return (
            response.json().get("result", {}).get("nodes", {})[1].get("id")
            if response.status_code == 200
            else ""
        )

    def pre_requisite_get_playbook_versions(self) -> str:
        response = self._make_request(
            self.API_BASE, "get", f"/flow/{self.flow_id}/versions?offset=0&limit=500", json={}
        )
        return (
            response.json().get("result", {}).get("data", {})[0].get("flowVersion")
            if response.status_code == 200
            else ""
        )

    def pre_requisite_create_search_node_filter(self) -> str:
        self.create_search_node_filter_payload[
            "title"
        ] = f"Replace add field name{self.generate_random_number()}"
        response = self._make_request(
            self.API_BASE,
            "post",
            f"/annotations/flow/{self.flow_id}",
            json=self.create_search_node_filter_payload,
        )
        return response.json().get("result", {}).get("id") if response.status_code == 200 else ""

    def clean_up_delete_playbook(self, flow_id: str) -> None:
        self.delete_playbook_payload[0]["id"] = self.fetch_numeric_part(flow_id, "flow-")
        self._make_request(
            self.API_BASE, "post", "/content-management/delete", json=self.delete_playbook_payload
        )

    def clean_up_delete_node(self, node_id: str) -> None:
        self._make_request(
            self.API_BASE,
            "delete",
            f"/flow/{self.flow_id}/node/{node_id}?deleteChildren=true",
            json={},
        )

    def clean_up_delete_search_node_filter(self, search_node_filter_id: str) -> None:
        self.make_request(
            self.API_BASE,
            "delete",
            f"/annotations/flow/{self.flow_id}/filter/{search_node_filter_id}",
            json={},
        )

    @task(9)
    def get_playbook_details(self):
        self.client.get(
            GET_PLAYBOOK_DETAILS.format(flow_id=self.flow_id), headers=self.get_headers()
        )

    @task(9)
    def get_playbook_graph_details(self):
        self.client.get(
            GET_PLAYBOOK_GRAPH_DETAILS.format(flow_id=self.flow_id), headers=self.get_headers()
        )

    @task(9)
    def get_spark_functions(self):
        self.client.get(GET_SPARK_FUNCTIONS, headers=self.get_headers())

    @task(9)
    def get_content_library_details(self):
        self.client.get(GET_CONTENT_LIBRARY_DETAILS, headers=self.get_headers())

    @task(9)
    def move_graph(self):
        self.client.post(
            MOVE_GRAPH.format(flow_id=self.flow_id, start_node_id=self.start_node_id),
            json=self.move_graph_payload,
            headers=self.get_headers(),
        )

    @task(2)
    def create_session(self):
        self.client.post(
            CREATE_SESSION.format(flow_id=self.flow_id, start_node_id=self.start_node_id),
            json={},
            headers=self.get_headers(),
        )

    @task(9)
    def get_tags(self):
        self.client.get(GET_TAGS, headers=self.get_headers())

    # @task(2)
    # def new_step_create_data_table_create_form_submit_form(self):
    #     #TODO need to figure out how to deal with create and submit form for same node
    #     self.new_step_create_data_table_payload["interval"]["from"] = self.fifteen_minutes_back_millis
    #     self.new_step_create_data_table_payload["interval"]["to"] = self.current_time_millis
    #     self.client.post(CREATE_DATA_TABLE_NEW_STEP.format(flow_id=self.flow_id, start_node_id=self.start_node_id, step_session_id=self.step_session_id), json=self.new_step_create_data_table_payload, headers=self.get_headers())
    #     self.client.post(CREATE_FORM.format(flow_id=self.flow_id), json=self.create_form_payload, headers=self.get_headers())
    #     self.submit_form_payload["title"] = f"Create_Custom_Data_{self.generate_random_number()}"
    #     response = self.client.post(SUBMIT_FORM.format(flow_id=self.flow_id), json=self.submit_form_payload, headers=self.get_headers())
    #     if response.status_code == 200:
    #         node_id = response.json().get("result", {}).get("nodes", {})[1].get("id")
    #         self.clean_up_delete_node(node_id)

    @task(9)
    def get_node_results(self):
        self.client.post(
            GET_NODE_RESULTS.format(
                flow_id=self.flow_id,
                start_node_id=self.start_node_id,
                fifteen_minutes_back_millis=self.fifteen_minutes_back_millis,
                current_time_millis=self.current_time_millis,
            ),
            json={},
            headers=self.get_headers(),
        )

    @task(9)
    def rerun_node(self):
        self.client.post(
            RERUN_NODE.format(flow_id=self.flow_id, node_id=self.node_id),
            json={},
            headers=self.get_headers(),
        )

    @task(5)
    def edit_step(self):
        self.edit_step_payload["interval"]["from"] = self.fifteen_minutes_back_millis
        self.edit_step_payload["interval"]["to"] = self.current_time_millis
        self.client.post(
            EDIT_STEP.format(flow_id=self.flow_id, node_id=self.node_id),
            json=self.edit_step_payload,
            headers=self.get_headers(),
        )

    @task(5)
    def search_node(self):
        self.client.post(
            SEARCH_NODE.format(flow_id=self.flow_id, node_id=self.node_id),
            json=self.search_node_payload,
            headers=self.get_headers(),
        )

    @task(5)
    def search_node_with_filter(self):
        self.search_node_payload["filters"]["tags"] = ["Devo"]
        self.client.post(
            SEARCH_NODE_WITH_FILTER.format(flow_id=self.flow_id, node_id=self.node_id),
            json=self.search_node_payload,
            headers=self.get_headers(),
        )

    @task(9)
    def get_operator_catalog(self):
        self.client.get(
            GET_OPERATOR_CATALOG,
            headers=self.get_headers(),
        )

    @task(2)
    def add_node_sql_operation(self):
        self.client.post(
            ADD_SQL_NODE_OPERATION.format(
                flow_id=self.flow_id,
                fifteen_minutes_back_millis=self.fifteen_minutes_back_millis,
                current_time_millis=self.current_time_millis,
            ),
            headers=self.get_headers(),
            json=self.add_node_sql_operation_payload,
        )

    @task(2)
    def new_step_add_fields(self):
        self.new_step_add_fields_payload["interval"]["from"] = self.fifteen_minutes_back_millis
        self.new_step_add_fields_payload["interval"]["to"] = self.current_time_millis
        self.client.post(
            NEW_STEP_ADD_FIELDS.format(
                flow_id=self.flow_id,
                start_node_id=self.start_node_id,
                step_session_id=self.step_session_id,
            ),
            json=self.new_step_add_fields_payload,
            headers=self.get_headers(),
        )

    @task(9)
    def validate_expression(self):
        self.client.post(
            VALIDATE_EXPRESSION,
            json=self.validate_expression_payload,
            headers=self.get_headers(),
        )

    @task(5)
    def update_node_details(self):
        self.client.patch(
            UPDATE_NODE_DETAILS.format(flow_id=self.flow_id, node_id=self.node_id),
            json=self.update_node_details_payload,
            headers=self.get_headers(),
        )

    # @task(2)
    # def add_output_node(self):
    #     #TODO need to add delete node call else cannot call this again, but delete is generating unique endpoints
    #     self.add_output_node_payload["nodes"][0] = self.node_id
    #     response = self.client.post(ADD_OUTPUT_NODE.format(flow_id=self.flow_id), json=self.add_output_node_payload, headers=self.get_headers())
    #     if response.status_code == 200:
    #         node_id = response.json().get("result", {}).get("nodes", [])[-1].get("id")
    #         self.client.delete(f"/case-management/api/flow/{self.flow_id}/node/{node_id}?deleteChildren=true", json={}, headers=self.get_headers())

    @task(9)
    def get_search_node_filters(self):
        self.client.get(
            GET_SEARCH_NODE_FILTERS.format(flow_id=self.flow_id),
            headers=self.get_headers(),
        )

    @task(2)
    def create_and_delete_search_node_filter(self):
        self.create_search_node_filter_payload[
            "title"
        ] = f"Replace add field name{self.generate_random_number()}"
        response = self.client.post(
            CREATE_SEARCH_NODE_FILTERS.format(flow_id=self.flow_id),
            json=self.create_search_node_filter_payload,
            headers=self.get_headers(),
        )
        # if response.status_code == 200:
        #     search_node_filter_id = response.json().get("result", {}).get("id")
        #     self.client.delete(DELETE_SEARCH_NODE_FILTERS.format(flow_id=self.flow_id, search_node_filter_id=search_node_filter_id), json={}, headers=self.get_headers())

    # @task(1)
    # def delete_search_node_filter(self):
    #     #TODO creating unique endpoints while deletion
    #     search_node_filter_id = self.pre_requisite_create_search_node_filter()
    #     self.client.delete(DELETE_SEARCH_NODE_FILTERS.format(flow_id=self.flow_id, search_node_filter_id=search_node_filter_id), json={}, headers=self.get_headers())
    #
    # @task(1)
    # def delete_node(self):
    #     #TODO need to add this with create node request and creating unique endpoints while deletion
    #     self.client.delete(DELETE_NODE.format(flow_id=self.flow_id, node_id=self.node_id), json={}, headers=self.get_headers())

    @task(9)
    def get_playbook_versions(self):
        self.client.get(
            GET_PLAYBOOK_VERSIONS.format(flow_id=self.flow_id),
            json={},
            headers=self.get_headers(),
        )

    @task(2)
    def set_latest_playbook_version(self):
        self.client.post(
            SET_LATEST_PLAYBOOK_VERSION.format(
                flow_id=self.flow_id, flow_version=self.flow_version
            ),
            json={},
            headers=self.get_headers(),
        )

        # @task(2)
        # def publish_playbook_version(self):
        #     #TODO getting error on publishing same version again
        #     self.publish_playbook_version_payload["version"] = self.flow_version
        self.client.post(
            PUBLISH_PLAYBOOK_VERSION.format(flow_id=self.flow_id),
            json=self.publish_playbook_version_payload,
            headers=self.get_headers(),
        )

    @task(9)
    def get_dependents_for_flow(self):
        self.get_dependents_for_flow_payload[0] = self.flow_id
        self.client.post(
            GET_DEPENDENTS_FOR_FLOW,
            headers=self.get_headers(),
            json=self.get_dependents_for_flow_payload,
        )
