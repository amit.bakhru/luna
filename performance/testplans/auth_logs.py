#!/usr/bin/env python
import time

from locust import task, FastHttpUser, between

LOCUST_KAFKA_SERVERS = "b-1.uebakafkadacluster.khj6h5.c24.kafka.us-east-1.amazonaws.com:9092"


class AuthLogs(FastHttpUser):
    host = "http://devo-data-generator.indrajeet-perf.svc.cluster.local"
    wait_time = between(200, 240)
    topic = "indrajeet_auth_all"
    # https://stackoverflow.com/questions/75233726/locust-attributeerror-object-has-no-attribute
    # abstract = True
    common_payload = {"topic": topic, "profile": "da_soak", "bootstrap": LOCUST_KAFKA_SERVERS}
    headers = {"Content-Type": "application/json"}

    def on_start(self):
        self.client.post(
            "/kafka", json={"topic": self.topic, "bootstrap": LOCUST_KAFKA_SERVERS}, timeout=120
        )
        time.sleep(30)

    @task(3)
    def duo_logs(self):
        payload = {
            "tag": "auth.duo.authentication.events",
            "template": "duo",
            "type": "auth",
            "devo_config": "",
            "num_records": 30000,
            "rate": 1,
        }
        payload.update(self.common_payload)
        self.client.post("/generate", headers=self.headers, json=payload, timeout=120)

    @task(6)
    def okta_logs(self):
        payload = {
            "tag": "auth.okta.events",
            "template": "okta",
            "type": "auth",
            "num_records": 30000,
            "rate": 1,
        }
        payload.update(self.common_payload)
        self.client.post("/generate", headers=self.headers, json=payload, timeout=120)

    @task(9)
    def auth_all_logs(self):
        payload = {
            "tag": "auth.all",
            "template": "authall",
            "type": "auth",
            "devo_config": "",
            "num_records": 30000,
            "rate": 1,
        }
        payload.update(self.common_payload)
        self.client.post("/generate", headers=self.headers, json=payload, timeout=120)

    # @task(2)
    # def usecase_status(self):
    #     payload = {
    #         "tag": "auth.all",
    #         "template": "authall",
    #         "type": "auth",
    #         "devo_config": "",
    #         "num_records": 30000,
    #         "rate": 1
    #     }
    #     payload.update(self.common_payload)
    #     self.client.post("/generate", headers=self.headers, json=payload)


# class KafkaAuthUser(KafkaUser):
#     bootstrap_servers = LOCUST_KAFKA_SERVERS
#
#     def generate_logs(self):
#         # TODO: generate text 1000 lines file and return the text
#         print(self.__dict__)
#         return []
#
#     @task
#     def auth_all(self):
#         for i in self.generate_logs():
#             self.client.send("lafp_test", b"payload")
#             self.client.producer.poll(1)


# Debug Mode
# if __name__ == "__main__":
#     from locust import run_single_user
#
#     run_single_user(SampleAPIServerUser)
