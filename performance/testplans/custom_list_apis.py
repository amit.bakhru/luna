#!/usr/bin/env python

from typing import List

from performance.testplans.base_api_test import BaseApiTest

from locust import task

from performance.testplans import (
    GET_CUSTOM_LISTS,
    CREATE_CUSTOM_LIST,
    DELETE_CUSTOM_LIST,
    DOWNLOAD_CUSTOM_LIST,
    IMPORT_CUSTOM_LIST,
    RENAME_CUSTOM_LIST,
    USAGES_CUSTOM_LIST,
    GET_CUSTOM_LIST_FILTERS,
    GET_CUSTOM_LIST_DATA,
    SEARCH_CUSTOM_LIST_DATA,
    PREVIEW_CUSTOM_LIST_DATA_BEFORE_ADDING,
    ADD_DATA_IN_CUSTOM_LIST,
    EXPORT_CUSTOM_LIST_DATA,
    CLEAR_CUSTOM_LIST_DATA,
    REMOVE_SELECTED_DATA_FROM_CUSTOM_LIST,
    UPDATE_DATA_IN_CUSTOM_LIST,
)


class BaseCustomList:
    API_BASE = "/case-management/api"

    abstract = True

    get_custom_list_payload = {
        "filters": [],
        "offset": 0,
        "pageSize": 25,
    }

    create_custom_list_payload = {
        "columns": [
            {"name": "employee_id", "dataType": "integer", "error": {}},
            {"name": "employee_name", "dataType": "string", "error": {}},
            {"name": "employee_salary", "dataType": "double", "error": {}},
            {"name": "employee_aadhar", "dataType": "bigint", "error": {}},
        ],
        "deduplicate": False,
        "name": "",
        "overflowBehavior": "DeleteOldest",
    }

    delete_custom_list_payload = [{"id": "696", "entityTypeId": "customList"}]

    import_custom_list_payload = {"zipContent": ""}

    rename_custom_list_payload = {"entityId": {"id": "", "entityTypeId": "customList"}, "name": ""}

    copy_custom_list_payload = {
        "entityIds": [{"id": "", "entityTypeId": "customList"}],
        "folderId": "cf69202f-75e7-462e-b5a1-91a03b3c0ba8",
    }

    custom_lists_usage_payload = []

    search_custom_list_data_payload = ' LOWER(employee_id) LIKE LOWER("%a%") OR LOWER(employee_name) LIKE LOWER("%a%") OR LOWER(employee_salary) LIKE LOWER("%a%") OR LOWER(employee_aadhar) LIKE LOWER("%a%")'

    preview_custom_list_data_payload = ""

    add_data_in_custom_list_payload = "ImVtcGxveWVlX2lkIiwiZW1wbG95ZWVfbmFtZSIsImVtcGxveWVlX3NhbGFyeSIsImVtcGxveWVlX2FhZGhhciIKMSxEaWFuYSBCYWtlciwzNzc3My4yMiwxMjU5MTA1CjIsQ2FzZXkgT2JyaWVuLDEwMDAwMCwxNTk2Mzg1CjMsVGltb3RoeSBHcmFudCwxMDAwMDAsNjgxOTI3Nwo0LEplc3NpY2EgV2FyZSw1NzUxNS45OCw0OTM5NzU5CjUsTWF0dGhldyBIb3JuZSwxMDAwMDAsNjQ5Mzk3NQo2LEplZmZyZXkgVHJhbiwxMDAwMDAsMjI2NTA2MAo3LEp1c3RpbiBBbGxlbiwxMDAwMDAsNDE2MjY1MAo4LE1hcnkgTXllcnMsNjY1MTAuMzMsNDc5NTE4MAo5LFRpbmEgUGVyZXosMTAwMDAwLDE4MTMyNTMKMTAsQmFyYmFyYSBHYXJjaWEsMTAwMDAwLDY3NjUwNjA="

    remove_rows_from_custom_list_payload = []

    update_data_in_custom_list_payload = {
        "before": {
            "employee_id": "493",
            "employee_name": "Christopher Reed",
            "__lhub_inserted_time__": "1739268577394",
            "employee_salary": "46115.06",
            "__lhub_id__": "4985",
            "employee_aadhar": "2246987",
        },
        "after": {
            "employee_id": "493",
            "employee_name": "Christopher Reedi",
            "__lhub_inserted_time__": "1739268577394",
            "employee_salary": "46115.06",
            "__lhub_id__": "4985",
            "employee_aadhar": "2246987",
        },
    }


class CustomListListingPage(BaseApiTest, BaseCustomList):
    API_BASE = "/case-management/api"

    def on_start(self) -> None:
        super().on_start()
        self.custom_list_id = self.pre_requisite_create_custom_list()
        self.custom_list_ids = self.pre_requisite_get_custom_lists()

    def on_stop(self) -> None:
        self.clean_up_delete_custom_list(self.custom_list_id)

    def pre_requisite_create_custom_list(self) -> str:
        self.create_custom_list_payload["name"] = f"new_custom_list_{self.generate_random_number()}"
        response = self._make_request(
            self.API_BASE, "put", "/lists", json=self.create_custom_list_payload
        )
        return response.json().get("result", {}).get("id") if response.status_code == 200 else ""

    def pre_requisite_get_custom_lists(self) -> List[str]:
        response = self._make_request(
            self.API_BASE,
            "post",
            "/content-management/content/customList?libraryView=all",
            json=self.get_custom_list_payload,
        )
        response_data_array = response.json().get("result", {}).get("data", {}).get("data", [])
        return [
            item.get("listId") for item in (response_data_array[:5] if response_data_array else [])
        ]

    def pre_requisite_get_custom_list_with_search_value(self, search_value: str) -> str:
        self.get_custom_list_payload["filters"] = [{"searchText": search_value}]
        response = self._make_request(
            self.API_BASE,
            "post",
            "/content-management/content/customList?libraryView=all",
            json=self.get_custom_list_payload,
        )
        return (
            response.json().get("result", {}).get("data", {}).get("data", [])[0].get("listId")
            if response.status_code == 200
            else ""
        )

    def clean_up_delete_custom_list(self, custom_list_id: str) -> None:
        self.delete_custom_list_payload[0]["id"] = custom_list_id
        self._make_request(
            self.API_BASE,
            "post",
            "/content-management/delete",
            json=self.delete_custom_list_payload,
        )

    def clean_up_delete_custom_list_in_bulk(self, custom_list_ids: List[str]) -> None:
        self.delete_custom_list_payload = [
            {"id": str(id_num), "entityTypeId": "customList"} for id_num in custom_list_ids
        ]
        self._make_request(
            self.API_BASE,
            "post",
            "/content-management/delete",
            json=self.delete_custom_list_payload,
        )

    @task(9)
    def get_custom_lists(self):
        self.client.post(
            GET_CUSTOM_LISTS, headers=self.get_headers(), json=self.get_custom_list_payload
        )

    @task(3)
    def get_custom_lists_name_sorted_ascending(self):
        self.get_custom_list_payload["sortColumn"] = "name"
        self.get_custom_list_payload["sortOrder"] = "ASC"
        self.client.post(
            GET_CUSTOM_LISTS, headers=self.get_headers(), json=self.get_custom_list_payload
        )

    @task(3)
    def get_custom_lists_name_sorted_descending(self):
        self.get_custom_list_payload["sortColumn"] = "name"
        self.get_custom_list_payload["sortOrder"] = "DESC"
        self.client.post(
            GET_CUSTOM_LISTS, headers=self.get_headers(), json=self.get_custom_list_payload
        )

    @task(3)
    def get_custom_lists_owner_sorted_ascending(self):
        self.get_custom_list_payload["sortColumn"] = "owner"
        self.get_custom_list_payload["sortOrder"] = "ASC"
        self.client.post(
            GET_CUSTOM_LISTS, headers=self.get_headers(), json=self.get_custom_list_payload
        )

    @task(3)
    def get_custom_lists_owner_sorted_descending(self):
        self.get_custom_list_payload["sortColumn"] = "owner"
        self.get_custom_list_payload["sortOrder"] = "DESC"
        self.client.post(
            GET_CUSTOM_LISTS, headers=self.get_headers(), json=self.get_custom_list_payload
        )

    @task(3)
    def get_custom_lists_last_updated_sorted_ascending(self):
        self.get_custom_list_payload["sortColumn"] = "lastUpdated"
        self.get_custom_list_payload["sortOrder"] = "ASC"
        self.client.post(
            GET_CUSTOM_LISTS, headers=self.get_headers(), json=self.get_custom_list_payload
        )

    @task(3)
    def get_custom_lists_last_updated_sorted_descending(self):
        self.get_custom_list_payload["sortColumn"] = "lastUpdated"
        self.get_custom_list_payload["sortOrder"] = "DESC"
        self.client.post(
            GET_CUSTOM_LISTS, headers=self.get_headers(), json=self.get_custom_list_payload
        )

    @task(4)
    def get_custom_lists_with_owner_filter_value(self):
        self.get_custom_list_payload["filters"] = [{"owners": ["indrajeetkumar.sah@devo.com"]}]
        self.client.post(
            GET_CUSTOM_LISTS, headers=self.get_headers(), json=self.get_custom_list_payload
        )

    @task(4)
    def get_custom_lists_with_deduplicate_filter_value(self):
        self.get_custom_list_payload["filters"] = [{"deduplicate": ["true"]}]
        self.client.post(
            GET_CUSTOM_LISTS, headers=self.get_headers(), json=self.get_custom_list_payload
        )

    @task(7)
    def get_custom_lists_with_search_value(self):
        self.get_custom_list_payload["filters"] = [{"searchText": "list"}]
        self.client.post(
            GET_CUSTOM_LISTS, headers=self.get_headers(), json=self.get_custom_list_payload
        )

    @task(2)
    def create_custom_list(self):
        self.create_custom_list_payload["name"] = f"new_custom_list_{self.generate_random_number()}"
        response = self.client.put(
            CREATE_CUSTOM_LIST, headers=self.get_headers(), json=self.create_custom_list_payload
        )
        if response.status_code == 200:
            self.clean_up_delete_custom_list(response.json().get("result", {}).get("id"))

    @task(1)
    def delete_custom_list(self):
        self.delete_custom_list_payload[0]["id"] = self.pre_requisite_create_custom_list()
        self.client.post(
            DELETE_CUSTOM_LIST, headers=self.get_headers(), json=self.delete_custom_list_payload
        )

    @task(1)
    def delete_custom_list_in_bulk(self):
        custom_list_ids = []
        for i in range(3):
            custom_list_ids.append(self.pre_requisite_create_custom_list())
        self.delete_custom_list_payload = [
            {"id": str(id_num), "entityTypeId": "customList"} for id_num in custom_list_ids
        ]
        self.client.post(
            DELETE_CUSTOM_LIST, headers=self.get_headers(), json=self.delete_custom_list_payload
        )

    @task(2)
    def download_custom_list(self):
        self.client.get(
            DOWNLOAD_CUSTOM_LIST.format(custom_list_id=self.custom_list_id),
            headers=self.get_headers(),
        )

    @task(2)
    def import_custom_list(self):
        # TODO there is an issue in importing same custom list again and again, how to handle that?
        file_base64_encode = self.encode_file_to_base64(
            file_path="performance/testplans/custom_list_import.zip"
        )
        self.import_custom_list_payload[
            "zipContent"
        ] = f"data:application/zip;base64,{file_base64_encode}"
        response = self.client.post(
            IMPORT_CUSTOM_LIST, headers=self.get_headers(), json=self.import_custom_list_payload
        )
        custom_list_id = self.pre_requisite_get_custom_list_with_search_value(
            "new_custom_list_100001"
        )
        if response.status_code == 200:
            self.clean_up_delete_custom_list(custom_list_id)

    @task(2)
    def rename_custom_list(self):
        self.rename_custom_list_payload["entityId"]["id"] = self.custom_list_id
        self.rename_custom_list_payload[
            "name"
        ] = f"renamed_custom_list_{self.generate_random_number()}"
        self.client.post(
            RENAME_CUSTOM_LIST, headers=self.get_headers(), json=self.rename_custom_list_payload
        )

    # @task(2)
    # def copy_custom_list(self):
    #     # TODO folder id is currently static, need to make it dynamic or remove it when it deprecates
    #     # TODO need to figure out how to delete copied custom list as response does not contain any id of newly created custom list by copying
    #     self.copy_custom_list_payload["entityIds"] = [
    #         {"id": self.custom_list_id, "entityTypeId": "customList"}
    #     ]
    #     self.copy_custom_list_payload["folderId"] = "cf69202f-75e7-462e-b5a1-91a03b3c0ba8"
    #     self.client.post(
    #         COPY_CUSTOM_LIST, headers=self.get_headers(), json=self.copy_custom_list_payload
    #     )
    #
    # @task(1)
    # def copy_custom_list_in_bulk(self):
    #     # TODO folder id is currently static, need to make it dynamic or remove it when it deprecates
    #     # TODO need to figure out how to delete copied custom list as response does not contain any id of newly created custom list by copying
    #     custom_list_ids = []
    #     for i in range(3):
    #         custom_list_ids.append(self.pre_requisite_create_custom_list())
    #     self.copy_custom_list_payload["entityIds"] = [
    #         {"id": str(id_num), "entityTypeId": "customList"} for id_num in custom_list_ids
    #     ]
    #     self.copy_custom_list_payload["folderId"] = "cf69202f-75e7-462e-b5a1-91a03b3c0ba8"
    #     self.client.post(
    #         COPY_CUSTOM_LIST, headers=self.get_headers(), json=self.copy_custom_list_payload
    #     )
    #     self.clean_up_delete_custom_list_in_bulk(custom_list_ids)

    @task(9)
    def usage_of_custom_lists(self):
        self.custom_lists_usage_payload = self.custom_list_ids
        self.client.post(
            USAGES_CUSTOM_LIST, headers=self.get_headers(), json=self.custom_lists_usage_payload
        )

    @task(9)
    def get_custom_list_filters(self):
        self.client.get(GET_CUSTOM_LIST_FILTERS, headers=self.get_headers())


class CustomListDetailsPage(BaseApiTest, BaseCustomList):
    API_BASE = "/case-management/api"

    def change_content_type(self, content_type: str):
        headers = self.get_headers()
        headers["Content-Type"] = content_type
        return headers

    def on_start(self) -> None:
        super().on_start()
        self.custom_list_id = self.pre_requisite_create_custom_list()

    def on_stop(self) -> None:
        self.clean_up_delete_custom_list(self.custom_list_id)

    def pre_requisite_create_custom_list(self) -> str:
        self.create_custom_list_payload["name"] = f"new_custom_list_{self.generate_random_number()}"
        response = self.make_request(
            self.API_BASE, "put", "/lists", json=self.create_custom_list_payload
        )
        return response.json().get("result", {}).get("id") if response.status_code == 200 else ""

    def pre_requisite_get_custom_list_data(self) -> List[str]:
        response = self._make_request(
            self.API_BASE, "get", f"/lists/{self.custom_list_id}/data?offset=0&limit=25"
        )
        return response.json().get("result", {}).get("rows", [])

    def pre_requisite_add_data_in_custom_list(self) -> None:
        self.add_data_in_custom_list_payload = self.encode_file_to_base64(
            file_path="performance/testplans/custom_list_data.csv"
        )
        self._make_request(
            self.API_BASE,
            "post",
            f"/lists/{self.custom_list_id}/addCSVData",
            json=self.add_data_in_custom_list_payload,
            headers=self.change_content_type("text/json"),
        )

    def clean_up_delete_custom_list(self, custom_list_id: str) -> None:
        self.delete_custom_list_payload[0]["id"] = custom_list_id
        self.make_request(
            self.API_BASE,
            "post",
            "/content-management/delete",
            json=self.delete_custom_list_payload,
        )

    @task(9)
    def get_custom_list_data(self):
        self.client.get(
            GET_CUSTOM_LIST_DATA.format(custom_list_id=self.custom_list_id),
            headers=self.get_headers(),
        )

    @task(4)
    def search_custom_list_data(self):
        self.client.post(
            SEARCH_CUSTOM_LIST_DATA.format(custom_list_id=self.custom_list_id),
            headers=self.change_content_type("text/json"),
            json=self.search_custom_list_data_payload,
        )

    @task(7)
    def preview_custom_list_data(self):
        self.preview_custom_list_data_payload = self.encode_file_to_base64(
            file_path="performance/testplans/custom_list_data.csv"
        )
        self.client.post(
            PREVIEW_CUSTOM_LIST_DATA_BEFORE_ADDING.format(custom_list_id=self.custom_list_id),
            headers=self.change_content_type("text/json"),
            json=self.preview_custom_list_data_payload,
        )

    @task(3)
    def add_data_in_custom_list(self):
        self.add_data_in_custom_list_payload = self.encode_file_to_base64(
            file_path="performance/testplans/custom_list_data.csv"
        )
        self.client.post(
            ADD_DATA_IN_CUSTOM_LIST.format(custom_list_id=self.custom_list_id),
            headers=self.change_content_type("text/json"),
            json=self.add_data_in_custom_list_payload,
        )

    @task(2)
    def export_custom_list_data(self):
        self.pre_requisite_add_data_in_custom_list()
        self.client.get(
            EXPORT_CUSTOM_LIST_DATA.format(custom_list_id=self.custom_list_id),
            headers=self.get_headers(),
        )

    @task(1)
    def clear_custom_list_data(self):
        self.pre_requisite_add_data_in_custom_list()
        self.client.post(
            CLEAR_CUSTOM_LIST_DATA.format(custom_list_id=self.custom_list_id),
            headers=self.change_content_type("text/json"),
            json="true",
        )

    @task(1)
    def remove_rows_from_custom_list(self):
        self.remove_rows_from_custom_list_payload = []
        self.pre_requisite_add_data_in_custom_list()
        custom_list_row_data = self.pre_requisite_get_custom_list_data()
        for i in range(min(5, len(custom_list_row_data))):
            self.remove_rows_from_custom_list_payload.append(custom_list_row_data[i])
        self.client.post(
            REMOVE_SELECTED_DATA_FROM_CUSTOM_LIST.format(custom_list_id=self.custom_list_id),
            headers=self.get_headers(),
            json=self.remove_rows_from_custom_list_payload,
        )

    @task(1)
    def update_data_in_custom_list(self):
        self.pre_requisite_add_data_in_custom_list()
        custom_list_row_data = self.pre_requisite_get_custom_list_data()
        self.update_data_in_custom_list_payload = {
            "before": custom_list_row_data[0],
            "after": {
                **custom_list_row_data[0],
                "employee_salary": str(float(custom_list_row_data[0]["employee_salary"]) + 0.01),
            },
        }
        self.client.post(
            UPDATE_DATA_IN_CUSTOM_LIST.format(custom_list_id=self.custom_list_id),
            headers=self.get_headers(),
            json=self.update_data_in_custom_list_payload,
        )
