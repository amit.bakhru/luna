CM_API_BASE = "/case-management/api/casemanagement"
API_BASE = "/case-management/api"

GET_CASES = f"{CM_API_BASE}/cases/v2/search/basic"
GET_CASE_STATUS_DATA = f"{CM_API_BASE}/cases/v2/search/getStatusData"
DOWNLOAD_CASE_LIST = f"{CM_API_BASE}/cases/download/basic"
CREATE_SAVED_SEARCH = f"{CM_API_BASE}/cases/search"
GET_SAVED_SEARCH = f"{CM_API_BASE}/cases/searches"
EDIT_OR_DELETE_SAVED_SEARCH = f"{CM_API_BASE}/cases/search/{{saved_search_id}}"
CREATE_CASE = f"{CM_API_BASE}/case-ui"
DELETE_CASE = f"{CM_API_BASE}/case/{{case_id}}"
DELETE_CASES_IN_BULK = f"{CM_API_BASE}/cases/bulk"

CREATE_COMMENT = f"{CM_API_BASE}/comment"
EDIT_OR_DELETE_COMMENT = f"{CM_API_BASE}/comment/{{comment_id}}"
GET_COMMENTS = f"{CM_API_BASE}/comment/{{case_id}}/comments"
GET_CASE_HISTORY = f"{CM_API_BASE}/case/{{case_id}}/history?pageSize=5000&after=0"
EDIT_CASE = f"{CM_API_BASE}/case/{{case_id}}"
ADD_ATTACHMENT = f"{CM_API_BASE}/attachment"
DOWNLOAD_ATTACHMENT = f"{CM_API_BASE}/attachment/{{file_id}}/download"
DELETE_ATTACHMENT = f"{CM_API_BASE}/attachment/{{file_id}}"
CREATE_TASK_FOR_CASE = f"{CM_API_BASE}/case"
DELETE_TASK = f"{CM_API_BASE}/case/{{task_id}}"
LINK_ALERT_WITH_CASES = "/ada/v1/alerts-cases"
UNLINK_ALERT_WITH_CASES = "/ada/v1/cases/{case_id_numeric_part}/alerts?alertsId={alert_id}"

GET_OR_UPDATE_PREFIX = f"{CM_API_BASE}/cases/prefix"
UPDATE_WHITELISTED_URL_OR_FILE_HASH_OR_IP_LIST = f"{CM_API_BASE}/fields/{{field_id}}/computed"
GET_WHITELISTED_URL_FILE_HASH_IP_LIST = f"{CM_API_BASE}/fields/computed"

CREATE_CASE_TYPE = f"{CM_API_BASE}/case-types"
UPDATE_CASE_TYPE = f"{CM_API_BASE}/case-types/{{case_type_id}}"
ADD_OR_REMOVE_FIELD_FROM_CASE_TYPE = (
    f"{CM_API_BASE}/case-types/{{case_type_id}}/fields/{{field_id}}"
)
REASSIGN_STATUS_WORKFLOW_TO_CASE_TYPE = f"{CM_API_BASE}/cases/status-workflow/reassign"
CREATE_TASK_FOR_CASE_TYPE = f"{CM_API_BASE}/case-types/{{case_type_id}}/task-templates"
EDIT_OR_DELETE_TASK_IN_CASE_TYPE = f"{CM_API_BASE}/task-template/{{task_id}}"
DEPRECATE_CASE_TYPE = f"{CM_API_BASE}/case-types/{{case_type_id}}/deprecate"
DELETE_CASE_TYPE = f"{CM_API_BASE}/case-types/{{case_type_id}}"
GET_CASE_TYPES = f"{CM_API_BASE}/case-types?pageSize=1000&after=0&excludeDeprecated=false"

GET_FIELDS = f"{CM_API_BASE}/fields?systemFields=true&pageSize=25&after=0"
GET_FIELDS_FIELD_NAME_COLUMN_ASCENDING = (
    f"{CM_API_BASE}/fields?systemFields=true&pageSize=25&after=0&sortCol=fieldName&sortOrder=asc"
)
GET_FIELDS_FIELD_NAME_COLUMN_DESCENDING = (
    f"{CM_API_BASE}/fields?systemFields=true&pageSize=25&after=0&sortCol=fieldName&sortOrder=desc"
)
GET_FIELDS_DISPLAY_NAME_COLUMN_ASCENDING = (
    f"{CM_API_BASE}/fields?systemFields=true&pageSize=25&after=0&sortCol=displayName&sortOrder=asc"
)
GET_FIELDS_DISPLAY_NAME_COLUMN_DESCENDING = (
    f"{CM_API_BASE}/fields?systemFields=true&pageSize=25&after=0&sortCol=displayName&sortOrder=desc"
)
GET_FIELDS_DESCRIPTION_COLUMN_ASCENDING = (
    f"{CM_API_BASE}/fields?systemFields=true&pageSize=25&after=0&sortCol=description&sortOrder=asc"
)
GET_FIELDS_DESCRIPTION_COLUMN_DESCENDING = (
    f"{CM_API_BASE}/fields?systemFields=true&pageSize=25&after=0&sortCol=description&sortOrder=desc"
)
GET_FIELDS_FIELD_TYPE_COLUMN_ASCENDING = (
    f"{CM_API_BASE}/fields?systemFields=true&pageSize=25&after=0&sortCol=fieldType&sortOrder=asc"
)
GET_FIELDS_FIELD_TYPE_COLUMN_DESCENDING = (
    f"{CM_API_BASE}/fields?systemFields=true&pageSize=25&after=0&sortCol=fieldType&sortOrder=desc"
)
GET_FIELDS_OWNER_COLUMN_ASCENDING = (
    f"{CM_API_BASE}/fields?systemFields=true&pageSize=25&after=0&sortCol=owner&sortOrder=asc"
)
GET_FIELDS_OWNER_COLUMN_DESCENDING = (
    f"{CM_API_BASE}/fields?systemFields=true&pageSize=25&after=0&sortCol=owner&sortOrder=desc"
)
GET_FIELDS_WITH_SEARCH = f"{CM_API_BASE}/search/entities/fields"
GET_FIELD_DETAILS = f"{CM_API_BASE}/fields/{{field_id}}/details"
CREATE_FIELD = f"{CM_API_BASE}/fields"
EDIT_OR_DELETE_FIELD = f"{CM_API_BASE}/fields/{{field_id}}"
DEPRECATE_FIELD = f"{CM_API_BASE}/fields/{{field_id}}/deprecate"

GET_OR_CREATE_PRIORITY = f"{CM_API_BASE}/cases/priority"
EDIT_PRIORITY = f"{CM_API_BASE}/cases/priority/{{priority_id}}"
REORDER_PRIORITY = f"{CM_API_BASE}/cases/priority/reorder"
DELETE_AND_REASSIGN_PRIORITY = f"{CM_API_BASE}/cases/priority/deleteAndReassign"

GET_OR_CREATE_STATUS_WORKFLOW = f"{CM_API_BASE}/cases/status-workflow"
PUBLISH_STATUS_WORKFLOW = f"{CM_API_BASE}/cases/status-workflow/publish"
EDIT_STATUS_WORKFLOW = f"{CM_API_BASE}/cases/status-workflow/{{status_workflow_id}}"
GET_IS_USED_STATUS_WORKFLOW = f"{CM_API_BASE}/cases/status-workflow/{{status_workflow_id}}/is-used"
CLONE_STATUS_WORKFLOW = f"{CM_API_BASE}/cases/status-workflow/{{status_workflow_id}}/clone"
DELETE_AND_REASSIGN_STATUS_WORKFLOW = f"{CM_API_BASE}/cases/status-workflow/deleteAndReassign"

CREATE_PLAYBOOK = f"{API_BASE}/demo/hub"
DELETE_PLAYBOOK = f"{API_BASE}/content-management/delete"
GET_PLAYBOOKS = f"{API_BASE}/content-management/content/playbook?libraryView=all"
GET_PLAYBOOK_FILTERS = f"{API_BASE}/content-management/filters/playbook"
RENAME_PLAYBOOK = f"{API_BASE}/content-management/rename"
CREATE_SESSION_FOR_PLAYBOOK_DOWNLOAD = f"{API_BASE}/export/create-session"
DOWNLOAD_PLAYBOOK = f"{API_BASE}/export/{{session_id}}/complete"
COPY_PLAYBOOK = f"{API_BASE}/content-management/copy"

GET_PLAYBOOK_DETAILS = f"{API_BASE}/flow/{{flow_id}}"
GET_PLAYBOOK_GRAPH_DETAILS = f"{API_BASE}/flow/{{flow_id}}/graph"
GET_SPARK_FUNCTIONS = f"{API_BASE}/spark-functions"
GET_CONTENT_LIBRARY_DETAILS = f"{API_BASE}/content-library/all?root=true"
MOVE_GRAPH = f"{API_BASE}/flow/{{flow_id}}/node/{{start_node_id}}/move"
CREATE_SESSION = f"{API_BASE}/flow/{{flow_id}}/node/{{start_node_id}}/create-session"
GET_TAGS = f"{API_BASE}/content-library/tags"
CREATE_DATA_TABLE_NEW_STEP = (
    f"{API_BASE}/flow/{{flow_id}}/node/{{start_node_id}}/new-step?stepSessionId={{step_session_id}}"
)
CREATE_FORM = f"{API_BASE}/form/{{form_id}}/page/0"
SUBMIT_FORM = f"{API_BASE}/form/{{form_id}}/submit"
GET_NODE_RESULTS = f"{API_BASE}/flow/{{flow_id}}/node/{{start_node_id}}/results?from={{fifteen_minutes_back_millis}}&to={{current_time_millis}}&offset=0&limit=25&cachedOnly=false"
RERUN_NODE = f"{API_BASE}/flow/{{flow_id}}/node/{{node_id}}/rerun"
EDIT_STEP = f"{API_BASE}/flow/{{flow_id}}/node/{{node_id}}/edit-step"
SEARCH_NODE = f"{API_BASE}/search/entities/content-library/flow/{{flow_id}}/node/{{node_id}}?stepSessionId=&isCommand=false"
SEARCH_NODE_WITH_FILTER = f"{API_BASE}/search/entities/content-library/flow/{{flow_id}}/node/{{node_id}}?stepSessionId=&isCommand=false"
GET_OPERATOR_CATALOG = f"{API_BASE}/demo/hub/operatorCatalog?fields=builtinOperators[signature,signatureStr,description]&pageSize=500&after=0"
ADD_SQL_NODE_OPERATION = f"{API_BASE}/flow/{{flow_id}}/node?from={{fifteen_minutes_back_millis}}&to={{current_time_millis}}"
NEW_STEP_ADD_FIELDS = (
    f"{API_BASE}/flow/{{flow_id}}/node/{{start_node_id}}/new-step?stepSessionId={{step_session_id}}"
)
VALIDATE_EXPRESSION = f"{API_BASE}/logichub-expression/validate"
UPDATE_NODE_DETAILS = f"{API_BASE}/flow/{{flow_id}}/node/{{node_id}}"
ADD_OUTPUT_NODE = f"{API_BASE}/flow/{{flow_id}}/node"
GET_SEARCH_NODE_FILTERS = f"{API_BASE}/annotations/flow/{{flow_id}}/filters"
CREATE_SEARCH_NODE_FILTERS = f"{API_BASE}/annotations/flow/{{flow_id}}"
DELETE_SEARCH_NODE_FILTERS = (
    f"{API_BASE}/annotations/flow/{{flow_id}}/filter/{{search_node_filter_id}}"
)
DELETE_NODE = f"{API_BASE}/flow/{{flow_id}}/node/{{node_id}}?deleteChildren=true"
GET_PLAYBOOK_VERSIONS = f"{API_BASE}/flow/{{flow_id}}/versions?offset=0&limit=500"
SET_LATEST_PLAYBOOK_VERSION = f"{API_BASE}/flow/{{flow_id}}/set-latest?version={{flow_version}}"
PUBLISH_PLAYBOOK_VERSION = f"{API_BASE}/flow/{{flow_id}}/publish"
GET_DEPENDENTS_FOR_FLOW = f"{API_BASE}/flow/dependentsForFlows"

CREATE_STREAM = f"{API_BASE}/demo/hub"
EDIT_STREAM = f"{API_BASE}/stream/{{stream_id}}"
GET_STREAM_FILTERS = f"{API_BASE}/content-management/filters/stream"
GET_STREAMS = f"{API_BASE}/content-management/content/stream?libraryView=all"
GET_STREAM_STATES = f"{API_BASE}/stream/states"
EDIT_STREAMS_IN_BULK = f"{API_BASE}/streams/multiple"
COPY_STREAM = f"{API_BASE}/content-management/copy"
RENAME_STREAM = f"{API_BASE}/content-management/rename"
DELETE_STREAM = f"{API_BASE}/content-management/delete"
PAUSE_STREAM = f"{API_BASE}/stream/pause"
RESUME_STREAM = f"{API_BASE}/stream/resume"
RERUN_BATCHES = f"{API_BASE}/stream/{{stream_id}}/batches/rerun"
GET_BATCHES = f"{API_BASE}/stream/{{stream_id}}/batches/filter?pageSize=25&after=0"
RERUN_ERROR_BATCHES = f"{API_BASE}/demo/{{stream_id}}"
CANCEL_QUEUED_AND_SCHEDULED_BATCHES = f"{API_BASE}/stream/{{stream_id}}/batches/cancelBatches"
CREATE_BATCH_FOR_ON_DEMAND_STREAM = f"{API_BASE}/trigger/{{on_demand_stream_id}}/{{token}}"
GET_SINGLE_BATCH_DETAILS = (
    f"{API_BASE}/demo/{{batch_id}}?fields=from,to,state,isStale,stats,total,stream[name,flow]"
)
GET_SINGLE_BATCH_CORRELATION = (
    f"{API_BASE}/demo/{{batch_id}}/correlations?fields=*&pageSize=25&after=0&_=1738242946528"
)
GET_NODE_DETAILS = f"{API_BASE}/demo/{{flow_id}}/nodes"
REPROCESS_SINGLE_BATCH = f"{API_BASE}/demo/{{batch_id}}"

GET_CUSTOM_LISTS = f"{API_BASE}/content-management/content/customList?libraryView=all"
CREATE_CUSTOM_LIST = f"{API_BASE}/lists"
DELETE_CUSTOM_LIST = f"{API_BASE}/content-management/delete"
DOWNLOAD_CUSTOM_LIST = f"{API_BASE}/lists/{{custom_list_id}}/export"
IMPORT_CUSTOM_LIST = f"{API_BASE}/lists/import"
RENAME_CUSTOM_LIST = f"{API_BASE}/content-management/rename"
COPY_CUSTOM_LIST = f"{API_BASE}/content-management/copy"
USAGES_CUSTOM_LIST = f"{API_BASE}/lists/usages"
GET_CUSTOM_LIST_FILTERS = f"{API_BASE}/content-management/filters/customList"

GET_CUSTOM_LIST_DATA = f"{API_BASE}/lists/{{custom_list_id}}/data?offset=0&limit=25"
SEARCH_CUSTOM_LIST_DATA = f"{API_BASE}/lists/{{custom_list_id}}/search?offset=0&limit=25"
PREVIEW_CUSTOM_LIST_DATA_BEFORE_ADDING = f"{API_BASE}/lists/{{custom_list_id}}/previewCSVData"
ADD_DATA_IN_CUSTOM_LIST = f"{API_BASE}/lists/{{custom_list_id}}/addCSVData"
EXPORT_CUSTOM_LIST_DATA = f"{API_BASE}/lists/{{custom_list_id}}/download"
CLEAR_CUSTOM_LIST_DATA = f"{API_BASE}/lists/{{custom_list_id}}/deleteBySearch"
REMOVE_SELECTED_DATA_FROM_CUSTOM_LIST = f"{API_BASE}/lists/{{custom_list_id}}/removeData"
UPDATE_DATA_IN_CUSTOM_LIST = f"{API_BASE}/lists/{{custom_list_id}}/updateRow"

CREATE_SCRIPT = f"{API_BASE}/execution-scripts/new"
SAVE_SCRIPT = f"{API_BASE}/execution-scripts/{{script_id}}?checkForUse=true"
GET_SCRIPTS = f"{API_BASE}/search/entities/scripts"
DOWNLOAD_SCRIPT = f"{API_BASE}/script/bulk-export"
DELETE_SCRIPT = f"{API_BASE}/script/{{script_id}}"
CREATE_SCRIPT_SESSION = f"{API_BASE}/import/create-session"
IMPORT_SCRIPT_SESSION = f"{API_BASE}/import/{{session_id}}"
COMPLETE_SCRIPT_IMPORT = f"{API_BASE}/import/{{session_id}}/complete"
GET_INTEGRATION_CONNECTIONS = f"{API_BASE}/import/{{session_id}}/integration-connections"
SCHEDULE_PLAYBOOKS = f"{API_BASE}/flow/schedulePlaybooks"
GET_SCRIPT = f"{API_BASE}/execution-scripts/{{script_id}}"
ADD_TEST_DATA_FOR_SCRIPT = f"{API_BASE}/execution-scripts/addTestData"
RUN_SCRIPT = f"{API_BASE}/execution-scripts/runScript"

CREATE_COMMAND = f"{API_BASE}/commands"
GET_COMMANDS = f"{API_BASE}/content-management/content/command?libraryView=all"
GET_COMMAND_FILTERS = f"{API_BASE}/content-management/filters/command"
COPY_COMMAND = f"{API_BASE}/content-management/copy"
CREATE_DOWNLOAD_COMMAND_SESSION = f"{API_BASE}/export/create-session"
DOWNLOAD_COMMAND = f"{API_BASE}/export/{{session_id}}/complete"
DELETE_COMMAND = f"{API_BASE}/content-management/delete"
RENAME_COMMAND = f"{API_BASE}/content-management/rename"
UPDATE_COMMAND = f"{API_BASE}/commands/{{flow_id}}"

GET_FORM = f"{API_BASE}/form/{{form_id}}/page/0"
